//
// Created by madleina on 29.05.20.
//

#ifndef BANGOLIN_TESTCASE_H
#define BANGOLIN_TESTCASE_H

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "TRandomGenerator.h"
#include "stdexcept"

class MockRandomGenerator : public TRandomGenerator {
public:
    MOCK_METHOD(double, getNormalRandom, (double dMean, double dStdDev), (override));
    MOCK_METHOD(double, getRand, (), (override));
    MOCK_METHOD(bool, pickOneOfTwo, (const double prob), (override));
};

#endif //BANGOLIN_TESTCASE_H

/*
 * TRangeUnitTests.cpp
 *
 *  Created on: May 14, 2021
 *      Author: Daniel Wegmann
 */


#include "TestCase.h"
#include "strongTypes.h"

using namespace testing;

//------------------------------------------------
// Test
//------------------------------------------------
TEST(StrongTypesTest, uint8){
	using myType = StrongTypes::StrongType<uint8_t, int, StrongTypes::Orderable,
														 StrongTypes::PreIncrementable,
														 StrongTypes::PreDecrementable>;
	myType a(10);
	myType b(11);
	EXPECT_TRUE(a.get() == 10);
	EXPECT_TRUE(a == 10);
	EXPECT_TRUE( (uint8_t) a == 10);

	EXPECT_FALSE(a.get() == 11);
	EXPECT_FALSE(a == 11);
	EXPECT_FALSE( (uint8_t) a == 11);

	EXPECT_FALSE(a == b);
	EXPECT_TRUE(a != b);
	EXPECT_TRUE(a < b);
	EXPECT_FALSE(a > b);

	std::swap(a,b);
	EXPECT_TRUE(a > b);
	EXPECT_FALSE(a < b);

	++b;
	EXPECT_TRUE(a == b);
	++a;
	EXPECT_FALSE(a == b);
	--a;
	EXPECT_TRUE(a == b);
	--b;
	EXPECT_FALSE(a == b);

	myType c(a);
	EXPECT_TRUE(a == c);
	EXPECT_TRUE(a != b);
	b = c;
	EXPECT_TRUE(a == b);
};

TEST(StrongTypesTest, double){
	using myType = StrongTypes::StrongType<double, int, StrongTypes::Orderable>;
	myType a(10);
	myType b(11);
	EXPECT_TRUE(a.get() == 10);
	EXPECT_TRUE(a == 10);
	EXPECT_TRUE( (double) a == 10);

	EXPECT_FALSE(a.get() == 11);
	EXPECT_FALSE(a == 11);
	EXPECT_FALSE( (double) a == 11);

	EXPECT_FALSE(a == b);
	EXPECT_TRUE(a != b);
	EXPECT_TRUE(a < b);
	EXPECT_FALSE(a > b);

	std::swap(a,b);
	EXPECT_TRUE(a > b);
	EXPECT_FALSE(a < b);

	myType c(a);
	EXPECT_TRUE(a == c);
	EXPECT_TRUE(a != b);
	b = c;
	EXPECT_TRUE(a == b);
};

TEST(StrongTypesTest, arithmetics){
	using myType = StrongTypes::StrongType<double, char, StrongTypes::Orderable, StrongTypes::Addable, StrongTypes::Subtractable, StrongTypes::Multiplicable, StrongTypes::Divisible>;

	myType a(2.0);
	myType b(5.0);
	myType c(10.0);

	//multiplication
	EXPECT_DOUBLE_EQ((a*b).get(), c.get());
	myType x(a);
	x *= b;
	EXPECT_DOUBLE_EQ(x.get(), c.get());

	//division
	EXPECT_DOUBLE_EQ((x/b).get(), a.get());
	x /= b;
	EXPECT_DOUBLE_EQ(x.get(), a.get());

	//addition and subtraction
	EXPECT_DOUBLE_EQ((-a).get(), -2.0);
	EXPECT_DOUBLE_EQ((b+b).get(), c.get());
	EXPECT_DOUBLE_EQ((a-b).get(), -3.0);
	x = a + b;
	x += c;
	EXPECT_DOUBLE_EQ(x.get(), 17.0);
};

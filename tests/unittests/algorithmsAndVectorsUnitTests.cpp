//
// Created by madleina on 19.04.21.
//

#include "TestCase.h"
#include "algorithmsAndVectors.h"

using namespace testing;

//------------------------------------------------
// Algorithms
//------------------------------------------------

TEST(algorithmsAndVectorsTests, rankSort){
    std::vector<double> input = {5, 2, 1, 4, 3};
    std::vector<uint64_t > ranks = rankSort(input);

    std::vector<uint64_t > expected = {2, 1, 4, 3, 0};

    EXPECT_EQ(ranks.size(), 5);
    for (uint64_t i = 0; i < ranks.size(); i++){
        EXPECT_EQ(ranks[i], expected[i]);
    }
}

TEST(algorithmsAndVectorsTests, rankSort_increasing){
    std::vector<double> input = {5, 2, 1, 4, 3};
    std::vector<uint64_t > ranks = rankSort(input, true);

    std::vector<uint64_t > expected = {0, 3, 4, 1, 2};

    EXPECT_EQ(ranks.size(), 5);
    for (uint64_t i = 0; i < ranks.size(); i++){
        EXPECT_EQ(ranks[i], expected[i]);
    }
}

TEST(algorithmsAndVectorsTests, intersection){
    std::vector<std::string> Vec1 = {"a", "b", "c", "d", "e"};
    std::vector<std::string> Vec2 = {"e", "d", "a", "b", "f"};

    std::vector<std::pair<bool, size_t>> Res1;
    std::vector<std::pair<bool, size_t>> Res2;

    findIntersection(Vec1, Vec2, Res1, Res2);

    // Res1
    EXPECT_EQ(Res1[0].first, true);
    EXPECT_EQ(Res1[0].second, 0);

    EXPECT_EQ(Res1[1].first, true);
    EXPECT_EQ(Res1[1].second, 1);

    EXPECT_EQ(Res1[2].first, false);
    EXPECT_EQ(Res1[2].second, 0);

    EXPECT_EQ(Res1[3].first, true);
    EXPECT_EQ(Res1[3].second, 2);

    EXPECT_EQ(Res1[4].first, true);
    EXPECT_EQ(Res1[4].second, 3);

    // Res2
    EXPECT_EQ(Res2[0].first, true);
    EXPECT_EQ(Res2[0].second, 3);

    EXPECT_EQ(Res2[1].first, true);
    EXPECT_EQ(Res2[1].second, 2);

    EXPECT_EQ(Res2[2].first, true);
    EXPECT_EQ(Res2[2].second, 0);

    EXPECT_EQ(Res2[3].first, true);
    EXPECT_EQ(Res2[3].second, 1);

    EXPECT_EQ(Res2[4].first, false);
    EXPECT_EQ(Res2[4].second, 0);
}

TEST(algorithmsAndVectorsTests, findDuplicates) {
    // no duplicates
    std::vector<std::string> Vec1 = {"a", "b", "c", "d", "e"};

    EXPECT_EQ(findDuplicate(Vec1).first, false);
    EXPECT_EQ(findDuplicate(Vec1).second, 0);

    // with duplicates
    std::vector<std::string> Vec2 = {"a", "b", "c", "a", "e"};

    EXPECT_EQ(findDuplicate(Vec2).first, true);
    EXPECT_EQ(findDuplicate(Vec2).second, 3);
}

TEST(algorithmsAndVectorsTests, binarySearch_getIndex){
    std::vector<std::string> vec = {"a", "a", "b", "c", "e"};

    // duplicates: return first
    EXPECT_EQ(binarySearch_getIndex(vec.begin(), vec.end(), "a"), 0);

    // easy cases
    EXPECT_EQ(binarySearch_getIndex(vec.begin(), vec.end(), "b"), 2);
    EXPECT_EQ(binarySearch_getIndex(vec.begin(), vec.end(), "c"), 3);
    EXPECT_EQ(binarySearch_getIndex(vec.begin(), vec.end(), "e"), 4);

    // throw if not found
    EXPECT_THROW(binarySearch_getIndex(vec.begin(), vec.end(), "f"), std::runtime_error);

    // other iterators
    EXPECT_EQ(binarySearch_getIndex(vec.begin()+2, vec.end(), "b"), 0);
    EXPECT_EQ(binarySearch_getIndex(vec.begin()+2, vec.end(), "c"), 1);
    EXPECT_EQ(binarySearch_getIndex(vec.begin()+2, vec.end(), "e"), 2);
    EXPECT_THROW(binarySearch_getIndex(vec.begin()+2, vec.end(), "a"), std::runtime_error);
}
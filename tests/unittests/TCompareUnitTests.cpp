//
// Created by madleina on 25.06.20.
//

#include "TestCase.h"
#include "TCompare.h"

struct write2OutputFiles_Fixed_Identical {
    write2OutputFiles_Fixed_Identical() {
        TOutputFile file1("file1.txt", 6);
        TOutputFile file2("file2.txt", 6);
        // string - char - int - float - double - bool
        file1 << "one" << 't' << 3 << 4.123456 << 5.987654321 << true << std::endl;
        file1 << "seven" << 'e' << 9 << -10.123456 << 11.987654321 << false << std::endl;

        file2 << "one" << 't' << 3 << 4.123456 << 5.987654321 << true << std::endl;
        file2 << "seven" << 'e' << 9 << -10.123456 << 11.987654321 << false << std::endl;

        file1.close();
        file2.close();
    }
};

struct write2OutputFiles_Header_Identical {
    write2OutputFiles_Header_Identical() {
        std::vector<std::string> header = {"col1", "col2", "col3", "col4", "col5", "col6"};
        TOutputFile file1("file1.txt", header);
        TOutputFile file2("file2.txt", header);

        // string - char - int - float - double - bool
        file1 << "one" << 't' << 3 << 4.123456 << 5.987654321 << true << std::endl;
        file1 << "seven" << 'e' << 9 << -10.123456 << 11.987654321 << false << std::endl;

        file2 << "one" << 't' << 3 << 4.123456 << 5.987654321 << true << std::endl;
        file2 << "seven" << 'e' << 9 << -10.123456 << 11.987654321 << false << std::endl;

        file1.close();
        file2.close();
    }
};

TEST(TCompareUnitTests, init){
    write2OutputFiles_Fixed_Identical writer; // write 2 input files

    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", fixed); // _init is called by constructor

    // check that variables are correctly initialized
    std::vector<TColumn> cols = comparer.getCols();
    EXPECT_EQ(cols.size(), 6);
    for (auto &it : cols){
        EXPECT_EQ(it.dataType(), _string);
        EXPECT_EQ(it.range(), 0.);
    }
    EXPECT_EQ(comparer.numDiff(), 0);
    EXPECT_EQ(comparer.outName(), "diff_file1_file2.txt");
}


struct write2OutputFiles_File1HasMoreCols {
    write2OutputFiles_File1HasMoreCols() {
        TOutputFile file1("file1.txt", 7);
        TOutputFile file2("file2.txt", 6);
        // string - char - int - float - double - bool
        file1 << "one" << 't' << 3 << 4.123456 << 5.987654321 << true << "extra" << std::endl;
        file2 << "one" << 't' << 3 << 4.123456 << 5.987654321 << true << std::endl;

        file1.close();
        file2.close();
    }
};

struct write2OutputFiles_File2HasMoreCols {
    write2OutputFiles_File2HasMoreCols() {
        TOutputFile file1("file1.txt", 6);
        TOutputFile file2("file2.txt", 7);
        // string - char - int - float - double - bool
        file1 << "one" << 't' << 3 << 4.123456 << 5.987654321 << true << std::endl;
        file2 << "one" << 't' << 3 << 4.123456 << 5.987654321 << true << "extra" << std::endl;

        file1.close();
        file2.close();
    }
};

TEST(TCompareUnitTests, file1HasMoreCols){
    write2OutputFiles_File1HasMoreCols writer; // write 2 input files, file1 has 7 cols, file2 has 6

    // use constructor where the number of columns is guessed
    EXPECT_THROW({try {TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", fixed);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(TCompareUnitTests, file1HasMoreCols_openWithNumCols){
    write2OutputFiles_File1HasMoreCols writer; // write 2 input files, file1 has 7 cols, file2 has 6

    // use constructor where we directly specify numCols
    TFileComparer comparer1("file1.txt", "file2.txt", "diff_file1_file2.txt", 6);
    EXPECT_THROW({try {comparer1.filesAreEqual();} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);

    TFileComparer comparer2("file1.txt", "file2.txt", "diff_file1_file2.txt", 7);
    EXPECT_THROW({try {comparer2.filesAreEqual();} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(TCompareUnitTests, file2HasMoreCols){
    write2OutputFiles_File2HasMoreCols writer; // write 2 input files, file1 has 6 cols, file2 has 7

    // use constructor where the number of columns is guessed
    EXPECT_THROW({try {TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", fixed);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(TCompareUnitTests, file2HasMoreCols_openWithNumCols){
    write2OutputFiles_File2HasMoreCols writer; // write 2 input files, file1 has 7 cols, file2 has 6

    // use constructor where we directly specify numCols
    TFileComparer comparer1("file1.txt", "file2.txt", "diff_file1_file2.txt", 6);
    EXPECT_THROW({try {comparer1.filesAreEqual();} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);

    TFileComparer comparer2("file1.txt", "file2.txt", "diff_file1_file2.txt", 7);
    EXPECT_THROW({try {comparer2.filesAreEqual();} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(TCompareUnitTests, throwIfVariable){
    write2OutputFiles_Fixed_Identical writer; // write 2 input files

    // use constructor where we directly specify numCols
    EXPECT_THROW(TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", variable), std::runtime_error);
}

TEST(TCompareUnitTests, setDataType_checkIfCorrectlyStored){
    write2OutputFiles_Header_Identical writer; // write 2 input files

    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", header);
    comparer.setDataType(0, "float", 0); // that's ok
    comparer.setDataType(1, "string"); // that's ok
    comparer.setDataType(2, "double", 0.01); // that's ok
    comparer.setDataType(3, "float", 0.0265); // that's ok
    comparer.setDataType(4, "string", 0); // that's ok
    comparer.setDataType(5, "int", 6); // that's ok

    // correctly stored?
    std::vector<TColumn> cols = comparer.getCols();
    EXPECT_EQ(cols.size(), 6);
    std::vector<double> ranges; // store in vec for easier comparison
    std::vector<DataTypes> types;
    for (auto &it : cols){
        ranges.push_back(it.range());
        types.push_back(it.dataType());
    }
    EXPECT_THAT(types, ::testing::ElementsAre(_float, _string, _double, _float, _string, _int));
    EXPECT_THAT(ranges, ::testing::ElementsAre(0, 0, 0.01, 0.0265, 0, 6));
}

TEST(TCompareUnitTests, setDataType_throwIfColIsTooBig){
    write2OutputFiles_Header_Identical writer; // write 2 input files

    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", fixed);
    comparer.setDataType(0, "string"); // that's ok
    comparer.setDataType(5, "string"); // that's ok
    EXPECT_THROW(comparer.setDataType(6, "string"), std::runtime_error); // that's too much
}


TEST(TCompareUnitTests, setDataType_throwIfInvalidDataType){
    write2OutputFiles_Header_Identical writer; // write 2 input files

    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", header);
    comparer.setDataType(0, "string"); // that's ok
    comparer.setDataType(0, "double"); // that's ok
    comparer.setDataType(0, "float"); // that's ok
    comparer.setDataType(0, "int"); // that's ok

    EXPECT_THROW(comparer.setDataType(0, "std::string"), std::runtime_error); // not supported
    EXPECT_THROW(comparer.setDataType(0, "bool"), std::runtime_error); // not supported
    EXPECT_THROW(comparer.setDataType(0, "char"), std::runtime_error); // not supported
    EXPECT_THROW(comparer.setDataType(0, ""), std::runtime_error); // not supported
}

TEST(TCompareUnitTests, setDataType_throwIfInvalidMaxDiff){
    write2OutputFiles_Header_Identical writer; // write 2 input files

    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", header);
    comparer.setDataType(0, "double", 0.01); // that's ok
    comparer.setDataType(0, "double", 1); // that's ok

    EXPECT_THROW(comparer.setDataType(0, "double", -0.1), std::runtime_error); // not supported
    EXPECT_THROW(comparer.setDataType(0, "double", -1), std::runtime_error); // not supported
}

TEST(TCompareUnitTests, setDataType_throwIfInvalidMaxDiff_dataTypes){
    write2OutputFiles_Header_Identical writer; // write 2 input files

    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", header);
    comparer.setDataType(0, "double", 0.01); // that's ok
    comparer.setDataType(0, "float", 0.01); // that's ok
    comparer.setDataType(0, "int", 1); // that's ok

    EXPECT_THROW(comparer.setDataType(0, "int", 0.1), std::runtime_error); // not supported
    EXPECT_THROW(comparer.setDataType(0, "string", 0.1), std::runtime_error); // not supported
    EXPECT_THROW(comparer.setDataType(0, "string", 1), std::runtime_error); // not supported
}

TEST(TCompareUnitTests, setDataTypes){
    write2OutputFiles_Header_Identical writer; // write 2 input files

    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", header);
    std::vector<std::string> dataTypes = {"float", "string", "double", "float", "string", "int"};
    std::vector<double> maxDiffs = {0, 0, 0.01, 0.36, 0, 4};
    comparer.setDataTypes(dataTypes, maxDiffs);

    // correctly stored?
    std::vector<TColumn> cols = comparer.getCols();
    EXPECT_EQ(cols.size(), 6);
    std::vector<double> ranges; // store in vec for easier comparison
    std::vector<DataTypes> types;
    for (auto &it : cols){
        ranges.push_back(it.range());
        types.push_back(it.dataType());
    }
    EXPECT_THAT(types, ::testing::ElementsAre(_float, _string, _double, _float, _string, _int));
    EXPECT_THAT(ranges, ::testing::ElementsAre(0, 0, 0.01,  0.36, 0, 4));
}

TEST(TCompareUnitTests, setDataTypes_throwIfTooLong) {
    write2OutputFiles_Header_Identical writer; // write 2 input files

    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", header);
    std::vector<std::string> dataTypes = {"float", "string", "double", "float", "string", "int", "int"};
    std::vector<double> maxDiffs = {0, 0, 0.01, 0.36, 0, 4};

    EXPECT_THROW(comparer.setDataTypes(dataTypes, maxDiffs), std::runtime_error); // dataTypes too long

    dataTypes.pop_back();
    maxDiffs.push_back(0.1);
    EXPECT_THROW(comparer.setDataTypes(dataTypes, maxDiffs), std::runtime_error); // maxDiffs too long
}

TEST(TCompareUnitTests, setDataTypes_throwIfTooShort){
    write2OutputFiles_Header_Identical writer; // write 2 input files

    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", header);
    std::vector<std::string> dataTypes = {"float", "string", "double", "float", "string"};
    std::vector<double> maxDiffs = {0, 0, 0.01, 0.36, 0, 4};

    EXPECT_THROW(comparer.setDataTypes(dataTypes, maxDiffs), std::runtime_error); // dataTypes too short

    dataTypes.emplace_back("int");
    maxDiffs.pop_back();
    EXPECT_THROW(comparer.setDataTypes(dataTypes, maxDiffs), std::runtime_error); // maxDiffs too short
}

TEST(TCompareUnitTests, setDataTypes_defaultMaxDiff){
    write2OutputFiles_Header_Identical writer; // write 2 input files

    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", header);
    std::vector<std::string> dataTypes = {"float", "string", "double", "float", "string", "int"};
    comparer.setDataTypes(dataTypes);

    // correctly stored?
    std::vector<TColumn> cols = comparer.getCols();
    EXPECT_EQ(cols.size(), 6);
    std::vector<double> ranges; // store in vec for easier comparison
    std::vector<DataTypes> types;
    for (auto &it : cols){
        ranges.push_back(it.range());
        types.push_back(it.dataType());
    }
    EXPECT_THAT(types, ::testing::ElementsAre(_float, _string, _double, _float, _string, _int));
    EXPECT_THAT(ranges, ::testing::ElementsAre(0, 0, 0, 0, 0, 0));
}

struct write2OutputFiles_File1HasMoreLines {
    write2OutputFiles_File1HasMoreLines() {
        TOutputFile file1("file1.txt", 6);
        TOutputFile file2("file2.txt", 6);
        // string - char - int - float - double - bool
        file1 << "one" << 't' << 3 << 4.123456 << 5.987654321 << true << std::endl;
        file1 << "one" << 't' << 3 << 4.123456 << 5.987654321 << true << std::endl;

        file2 << "one" << 't' << 3 << 4.123456 << 5.987654321 << true << std::endl;

        file1.close();
        file2.close();
    }
};

struct write2OutputFiles_File2HasMoreLines {
    write2OutputFiles_File2HasMoreLines() {
        TOutputFile file1("file1.txt", 6);
        TOutputFile file2("file2.txt", 6);
        // string - char - int - float - double - bool
        file1 << "one" << 't' << 3 << 4.123456 << 5.987654321 << true << std::endl;

        file2 << "one" << 't' << 3 << 4.123456 << 5.987654321 << true << std::endl;
        file2 << "one" << 't' << 3 << 4.123456 << 5.987654321 << true << std::endl;

        file1.close();
        file2.close();
    }
};

TEST(TCompareUnitTests, file1HasMoreLines){
    write2OutputFiles_File1HasMoreLines writer; // write 2 input files

    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", header);
    EXPECT_THROW({try {comparer.filesAreEqual();} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(TCompareUnitTests, file2HasMoreLines){
    write2OutputFiles_File1HasMoreLines writer; // write 2 input files

    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", header);
    EXPECT_THROW({try {comparer.filesAreEqual();} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}


struct write2OutputFiles_Fixed_Different {
    write2OutputFiles_Fixed_Different() {
        TOutputFile file1("file1.txt", 6);
        TOutputFile file2("file2.txt", 6);
        file1.setPrecision(15);
        file2.setPrecision(15); // TODO: ATTENTION - precision is pretty low by default, such that differences between doubles/floats are not visible
        // string - char - int - float - double - bool
        file1 << "one" << 't' << 7646352 << 4.123456 << 5.987654321 << true << std::endl;
        file1 << "whoppa" << 'e' << 9 << -10.123456 << 11.987654322 << false << std::endl;

        file2 << "one" << 't' << 3 << 4.123456 << 5.987654321 << true << std::endl;
        file2 << "seven" << 'e' << 9 << -10.123456 << 11.987654321 << false << std::endl;

        file1.close();
        file2.close();
    }
};

struct write2OutputFiles_Header_Different {
    write2OutputFiles_Header_Different() {
        std::vector<std::string> header1 = {"col1", "col2", "col3", "col4", "col50", "col6"};
        std::vector<std::string> header2 = {"col1", "col100", "col3", "col4", "col5", "col6"};
        TOutputFile file1("file1.txt", header1);
        TOutputFile file2("file2.txt", header2);
        file1.setPrecision(15);
        file2.setPrecision(15); // TODO: ATTENTION - precision is pretty low by default, such that differences between doubles/floats are not visible
        // string - char - int - float - double - bool
        file1 << "one" << 't' << 7646352 << 4.123456 << 5.987654321 << true << std::endl;
        file1 << "whoppa" << 'e' << 9 << -10.123456 << 11.987654322 << false << std::endl;

        file2 << "one" << 't' << 3 << 4.123456 << 5.987654321 << true << std::endl;
        file2 << "seven" << 'e' << 9 << -10.123456 << 11.987654321 << false << std::endl;

        file1.close();
        file2.close();
    }
};

TEST(IntegrationTestCompare, compareFixedIdentical){
    write2OutputFiles_Fixed_Identical write;
    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", fixed);
    EXPECT_TRUE(comparer.filesAreEqual());
}

TEST(IntegrationTestCompare, compareHeaderIdentical){
    write2OutputFiles_Header_Identical write;
    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", header);
    EXPECT_TRUE(comparer.filesAreEqual());
}

TEST(IntegrationTestCompare, compareFixedDifferent){
    write2OutputFiles_Fixed_Different write;
    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", fixed);
    EXPECT_FALSE(comparer.filesAreEqual());

    // read diffFile
    TInputFile diffFile("diff_file1_file2.txt", header);
    std::vector<std::string> differences;
    diffFile.read(differences);
    EXPECT_THAT(differences, ::testing::ElementsAre("1", "3", "7646352", "3"));
    diffFile.read(differences);
    EXPECT_THAT(differences, ::testing::ElementsAre("2", "1", "whoppa", "seven"));
    diffFile.read(differences);
    EXPECT_THAT(differences, ::testing::ElementsAre("2", "5", "11.987654322", "11.987654321"));
    EXPECT_FALSE(diffFile.read(differences)); // file ends
}


TEST(IntegrationTestCompare, compareHeaderDifferent){
    write2OutputFiles_Header_Different write;
    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", header);
    EXPECT_FALSE(comparer.filesAreEqual());

    // read diffFile
    TInputFile diffFile("diff_file1_file2.txt", header);
    std::vector<std::string> differences;
    diffFile.read(differences);
    EXPECT_THAT(differences, ::testing::ElementsAre("1", "2", "col2", "col100"));
    diffFile.read(differences);
    EXPECT_THAT(differences, ::testing::ElementsAre("1", "5", "col50", "col5"));
    diffFile.read(differences);
    EXPECT_THAT(differences, ::testing::ElementsAre("2", "3", "7646352", "3"));
    diffFile.read(differences);
    EXPECT_THAT(differences, ::testing::ElementsAre("3", "1", "whoppa", "seven"));
    diffFile.read(differences);
    EXPECT_THAT(differences, ::testing::ElementsAre("3", "5", "11.987654322", "11.987654321"));
    EXPECT_FALSE(diffFile.read(differences)); // file ends
}

// maximal number of differences

TEST(IntegrationTestCompare, compareFixedDifferent_maxDiffBetweenFilesIsActualNumDifferences){
    write2OutputFiles_Fixed_Different write;
    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", fixed);
    comparer.limitNumDiffTo(3);
    EXPECT_FALSE(comparer.filesAreEqual());

    // read diffFile
    TInputFile diffFile("diff_file1_file2.txt", header);
    std::vector<std::string> differences;
    EXPECT_TRUE(diffFile.read(differences));
    EXPECT_THAT(differences, ::testing::ElementsAre("1", "3", "7646352", "3"));
    EXPECT_TRUE(diffFile.read(differences));
    EXPECT_THAT(differences, ::testing::ElementsAre("2", "1", "whoppa", "seven"));
    EXPECT_TRUE(diffFile.read(differences));
    EXPECT_THAT(differences, ::testing::ElementsAre("2", "5", "11.987654322", "11.987654321"));
    EXPECT_FALSE(diffFile.read(differences)); // file ends after three differences
}

TEST(IntegrationTestCompare, compareFixedDifferent_maxDiffBetweenFilesIs1){
    write2OutputFiles_Fixed_Different write;
    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", fixed);
    comparer.limitNumDiffTo(1);
    EXPECT_FALSE(comparer.filesAreEqual());

    // read diffFile
    TInputFile diffFile("diff_file1_file2.txt", header);
    std::vector<std::string> differences;
    EXPECT_TRUE(diffFile.read(differences));
    EXPECT_THAT(differences, ::testing::ElementsAre("1", "3", "7646352", "3"));
    EXPECT_FALSE(diffFile.read(differences)); // file ends after one difference
}

TEST(IntegrationTestCompare, compareFixedDifferent_maxDiffBetweenFilesIs0){
    write2OutputFiles_Fixed_Different write;
    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", fixed);
    comparer.limitNumDiffTo(0); // ok to write 0, but it will break immediately after first difference appears -> output file will be empty
    EXPECT_FALSE(comparer.filesAreEqual());

    // read diffFile
    TInputFile diffFile("diff_file1_file2.txt", header);
    std::vector<std::string> differences;
    EXPECT_FALSE(diffFile.read(differences)); // file is empty (except header)
}

TEST(IntegrationTestCompare, compareHeaderDifferent_maxDiffBetweenFilesIsActualNumDifferences){
    write2OutputFiles_Header_Different write;
    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", header);
    comparer.limitNumDiffTo(5);
    EXPECT_FALSE(comparer.filesAreEqual());

    // read diffFile
    TInputFile diffFile("diff_file1_file2.txt", header);
    std::vector<std::string> differences;
    diffFile.read(differences);
    EXPECT_THAT(differences, ::testing::ElementsAre("1", "2", "col2", "col100"));
    diffFile.read(differences);
    EXPECT_THAT(differences, ::testing::ElementsAre("1", "5", "col50", "col5"));
    EXPECT_TRUE(diffFile.read(differences));
    EXPECT_THAT(differences, ::testing::ElementsAre("2", "3", "7646352", "3"));
    EXPECT_TRUE(diffFile.read(differences));
    EXPECT_THAT(differences, ::testing::ElementsAre("3", "1", "whoppa", "seven"));
    EXPECT_TRUE(diffFile.read(differences));
    EXPECT_THAT(differences, ::testing::ElementsAre("3", "5", "11.987654322", "11.987654321"));
    EXPECT_FALSE(diffFile.read(differences)); // file ends after 5 differences
}

TEST(IntegrationTestCompare, compareHeaderDifferent_maxDiffBetweenFilesIs1){
    write2OutputFiles_Header_Different write;
    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", header);
    comparer.limitNumDiffTo(1);
    EXPECT_FALSE(comparer.filesAreEqual());

    // read diffFile
    TInputFile diffFile("diff_file1_file2.txt", header);
    std::vector<std::string> differences;
    EXPECT_TRUE(diffFile.read(differences));
    EXPECT_THAT(differences, ::testing::ElementsAre("1", "2", "col2", "col100"));
    EXPECT_FALSE(diffFile.read(differences)); // file ends after one difference
}

TEST(IntegrationTestCompare, compareHeaderDifferent_maxDiffBetweenFilesIs0){
    write2OutputFiles_Header_Different write;
    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", header);
    comparer.limitNumDiffTo(0); // ok to write 0, but it will break immediately after first difference appears -> output file will be empty
    EXPECT_FALSE(comparer.filesAreEqual());

    // read diffFile
    TInputFile diffFile("diff_file1_file2.txt", header);
    std::vector<std::string> differences;
    EXPECT_FALSE(diffFile.read(differences)); // file is empty (except header)
}

struct write2OutputFiles_Fixed_Different_Complex {
    write2OutputFiles_Fixed_Different_Complex() {
        TOutputFile file1("file1.txt", 6);
        TOutputFile file2("file2.txt", 6);
        file1.setPrecision(15);
        file2.setPrecision(15);
        // string - char - int - float - double - bool
        file1 << "one" << 't' << 7646352 << 4.1234 << 5.9876543 << true << std::endl;
        file1 << "seven" << 'e' << 10 << -10.12345 << 11.98765432 << false << std::endl;

        file2 << "one" << 't' << 7646352 << 4.1236 << 5.9876541 << true << std::endl;
        file2 << "seven" << 'e' << 9 << -10.12346 << 11.98765431 << false << std::endl;

        file1.close();
        file2.close();
    }
};

TEST(IntegrationTestCompare, compareFixedDifferent_differentTypesAndRanges_areEqual){
    write2OutputFiles_Fixed_Different_Complex write;
    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", fixed);
    comparer.setDataType(2, "int", 1);
    comparer.setDataType(3, "float", 0.0002);
    comparer.setDataType(4, "double", 0.0000002);

    EXPECT_TRUE(comparer.filesAreEqual());
}

TEST(IntegrationTestCompare, compareFixedDifferent_differentTypesAndRanges_areDifferent){
    write2OutputFiles_Fixed_Different_Complex write;
    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", fixed);
    comparer.setDataType(2, "int", 0);
    comparer.setDataType(3, "float", 0.00015);
    comparer.setDataType(4, "double", 0.00000015);

    EXPECT_FALSE(comparer.filesAreEqual());

    // read diffFile
    TInputFile diffFile("diff_file1_file2.txt", header);
    std::vector<std::string> differences;
    EXPECT_TRUE(diffFile.read(differences));
    EXPECT_THAT(differences, ::testing::ElementsAre("1", "4", "4.1234", "4.1236"));
    EXPECT_TRUE(diffFile.read(differences));
    EXPECT_THAT(differences, ::testing::ElementsAre("1", "5", "5.9876543", "5.9876541"));
    EXPECT_TRUE(diffFile.read(differences));
    EXPECT_THAT(differences, ::testing::ElementsAre("2", "3", "10", "9"));
    EXPECT_FALSE(diffFile.read(differences)); // file ends
}

struct write2OutputFiles_Header_Different_Complex1 {
    write2OutputFiles_Header_Different_Complex1() {
        std::vector<std::string> header1 = {"col1", "col2", "col3", "col4", "col5", "col6"};
        std::vector<std::string> header2 = {"col1", "col2", "col3", "col4", "col5", "col6"};
        TOutputFile file1("file1.txt", header1);
        TOutputFile file2("file2.txt", header2);
        file1.setPrecision(15);
        file2.setPrecision(15);
        // string - char - int - float - double - bool
        file1 << "one" << 't' << 7646352 << 4.1234 << 5.9876543 << true << std::endl;
        file1 << "seven" << 'e' << 10 << -10.12345 << 11.98765432 << false << std::endl;

        file2 << "one" << 't' << 7646352 << 4.1236 << 5.9876541 << true << std::endl;
        file2 << "seven" << 'e' << 9 << -10.12346 << 11.98765431 << false << std::endl;

        file1.close();
        file2.close();
    }
};

TEST(IntegrationTestCompare, compareHeaderDifferent_differentTypesAndRanges_areEqual){
    write2OutputFiles_Header_Different_Complex1 write;
    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", header);
    comparer.setDataType(2, "int", 1);
    comparer.setDataType(3, "float", 0.0002);
    comparer.setDataType(4, "double", 0.0000002);

    EXPECT_TRUE(comparer.filesAreEqual());
}

struct write2OutputFiles_Header_Different_Complex2 {
    write2OutputFiles_Header_Different_Complex2() {
        std::vector<std::string> header1 = {"col1", "col2", "col3", "col4", "col50", "col6"};
        std::vector<std::string> header2 = {"col1", "col100", "col3", "col4", "col5", "col6"};
        TOutputFile file1("file1.txt", header1);
        TOutputFile file2("file2.txt", header2);
        file1.setPrecision(15);
        file2.setPrecision(15);
        // string - char - int - float - double - bool
        file1 << "one" << 't' << 7646352 << 4.1234 << 5.9876543 << true << std::endl;
        file1 << "seven" << 'e' << 10 << -10.12345 << 11.98765432 << false << std::endl;

        file2 << "one" << 't' << 7646352 << 4.1236 << 5.9876541 << true << std::endl;
        file2 << "seven" << 'e' << 9 << -10.12346 << 11.98765431 << false << std::endl;

        file1.close();
        file2.close();
    }
};

TEST(IntegrationTestCompare, compareHeaderDifferent_differentTypesAndRanges_areDifferent){
    write2OutputFiles_Header_Different_Complex2 write;
    TFileComparer comparer("file1.txt", "file2.txt", "diff_file1_file2.txt", header);
    comparer.setDataType(2, "int", 0);
    comparer.setDataType(3, "float", 0.00015);
    comparer.setDataType(4, "double", 0.00000015);

    EXPECT_FALSE(comparer.filesAreEqual());

    // read diffFile
    TInputFile diffFile("diff_file1_file2.txt", header);
    std::vector<std::string> differences;
    EXPECT_TRUE(diffFile.read(differences));
    EXPECT_THAT(differences, ::testing::ElementsAre("1", "2", "col2", "col100"));
    EXPECT_TRUE(diffFile.read(differences));
    EXPECT_THAT(differences, ::testing::ElementsAre("1", "5", "col50", "col5"));
    EXPECT_TRUE(diffFile.read(differences));
    EXPECT_THAT(differences, ::testing::ElementsAre("2", "4", "4.1234", "4.1236"));
    EXPECT_TRUE(diffFile.read(differences));
    EXPECT_THAT(differences, ::testing::ElementsAre("2", "5", "5.9876543", "5.9876541"));
    EXPECT_TRUE(diffFile.read(differences));
    EXPECT_THAT(differences, ::testing::ElementsAre("3", "3", "10", "9"));
    EXPECT_FALSE(diffFile.read(differences)); // file ends
}

// tidy up

TEST(IntegrationTestCompare, clean){
    // hack to remove all test files that were produced
    remove("file1.txt");
    remove("file2.txt");
    remove("diff_file1_file2.txt");
}

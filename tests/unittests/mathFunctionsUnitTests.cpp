//
// Created by madleina on 29.05.20.
//

#include "TestCase.h"
#include "mathFunctions.h"
using namespace testing;

// gammaln

TEST(mathFunctionsTests, gammaLog){
    // gammaln is known to be unprecise around values of 1-2
    // quoting: gives an accuracy of about
    // 10-12 significant decimal digits except for small regions around X = 1 and
    // X = 2, where the function goes to zero.
    EXPECT_NEAR(gammaLog(1.), 0., 10e-15);
    EXPECT_NEAR(gammaLog(2.), 0., 10e-15);
    EXPECT_FLOAT_EQ(gammaLog(3.), 0.6931472);
    EXPECT_FLOAT_EQ(gammaLog(100.), 359.1342);
}

// digamma

TEST(mathFunctionsTests, digamma){
    // tested against R digamma() function

    // negative x (only small values give reasonable results)
    EXPECT_FLOAT_EQ(TDigamma::digamma(-0.5), 0.03648998);

    // x between 0 and 6
    EXPECT_FLOAT_EQ(TDigamma::digamma(0.0000001), -1e+07);
    EXPECT_FLOAT_EQ(TDigamma::digamma(1.), -0.5772157);
    EXPECT_FLOAT_EQ(TDigamma::digamma(2.), 0.4227843);
    EXPECT_FLOAT_EQ(TDigamma::digamma(3.), 0.9227843);
    EXPECT_FLOAT_EQ(TDigamma::digamma(4.), 1.256118);
    EXPECT_FLOAT_EQ(TDigamma::digamma(5.), 1.506118);
    EXPECT_FLOAT_EQ(TDigamma::digamma(6.), 1.706118);

    // very large x
    EXPECT_FLOAT_EQ(TDigamma::digamma(100000000), 18.42068);
}

// beta distribution

TEST(mathFunctionsTests, dbetaThrow){
    // invalid x
    EXPECT_THROW(TBetaDistr::density(-1., 0.1, 0.1), std::runtime_error);
    EXPECT_THROW(TBetaDistr::density(2., 0.1, 0.1), std::runtime_error);
    // invalid alpha
    EXPECT_THROW(TBetaDistr::density(0.5, -1., 0.1), std::runtime_error);
    EXPECT_THROW(TBetaDistr::density(0.5, 0., 0.1), std::runtime_error);
    // invalid beta
    EXPECT_THROW(TBetaDistr::density(0.5, 0.1, -1.), std::runtime_error);
    EXPECT_THROW(TBetaDistr::density(0.5, 0.1, 0.), std::runtime_error);
}

TEST(mathFunctionsTests, dbetaNoThrow){
    // valid x
    EXPECT_NO_THROW(TBetaDistr::density(0., 0.1, 0.1));
    EXPECT_NO_THROW(TBetaDistr::density(0.5, 0.1, 0.1));
    EXPECT_NO_THROW(TBetaDistr::density(1., 0.1, 0.1));
    // valid alpha and beta
    EXPECT_NO_THROW(TBetaDistr::density(0.5, 2., 0.1));
    EXPECT_NO_THROW(TBetaDistr::density(0.5, 2., 100.));
}

TEST(mathFunctionsTests, dbeta){
    EXPECT_FLOAT_EQ(TBetaDistr::density(0.1, 0.7, 0.7), 1.08441);
    EXPECT_FLOAT_EQ(TBetaDistr::density(0.00000001, 0.1, 2.6), 1800021);
    EXPECT_FLOAT_EQ(TBetaDistr::density(0.1, 28, 99), 0.007578113);
}

TEST(mathFunctionsTests, dbetaLog){
    EXPECT_FLOAT_EQ(TBetaDistr::density(0.1, 0.7, 0.7, true), 0.08103628);
    EXPECT_FLOAT_EQ(TBetaDistr::density(0.00000001, 0.1, 2.6, true), 14.40331);
    EXPECT_FLOAT_EQ(TBetaDistr::density(0.1, 28, 99, true), -4.882491);
}

// exponential distribution

TEST(mathFunctionsTests, dexpThrow){
    // invalid x
    EXPECT_THROW(TExponentialDistr::density(-1., 1.), std::runtime_error);
    // invalid lambda
    EXPECT_THROW(TExponentialDistr::density(1., -1.), std::runtime_error);
    EXPECT_THROW(TExponentialDistr::density(1., 0.), std::runtime_error);
}

TEST(mathFunctionsTests, dexpNoThrow){
    // valid x
    EXPECT_NO_THROW(TExponentialDistr::density(0., 1.));
    EXPECT_NO_THROW(TExponentialDistr::density(1., 1.));
    // valid lambda
    EXPECT_NO_THROW(TExponentialDistr::density(1., 10.5));
}

TEST(mathFunctionsTests, pexpThrow){
    // invalid x
    EXPECT_THROW(TExponentialDistr::cumulativeDistrFunction(-1., 1.), std::runtime_error);
    // invalid lambda
    EXPECT_THROW(TExponentialDistr::cumulativeDistrFunction(1., -1.), std::runtime_error);
    EXPECT_THROW(TExponentialDistr::cumulativeDistrFunction(1., 0.), std::runtime_error);
}

TEST(mathFunctionsTests, pexpNoThrow){
    // valid x
    EXPECT_NO_THROW(TExponentialDistr::cumulativeDistrFunction(0., 1.));
    EXPECT_NO_THROW(TExponentialDistr::cumulativeDistrFunction(1., 1.));
    // valid lambda
    EXPECT_NO_THROW(TExponentialDistr::cumulativeDistrFunction(1., 10.5));
}

TEST(mathFunctionsTests, dexp){
    EXPECT_FLOAT_EQ(TExponentialDistr::density(0., 1.), 1.);
    EXPECT_FLOAT_EQ(TExponentialDistr::density(0.92, 10.), 0.001010394);
    EXPECT_FLOAT_EQ(TExponentialDistr::density(0.0001, 73.2), 72.66613);
}

TEST(mathFunctionsTests, dexpLog){
    EXPECT_FLOAT_EQ(TExponentialDistr::density(0., 1., true), 0.);
    EXPECT_FLOAT_EQ(TExponentialDistr::density(0.92, 10., true), -6.897415);
    EXPECT_FLOAT_EQ(TExponentialDistr::density(0.0001, 73.2, true), 4.285875);
}

TEST(mathFunctionsTests, pexp){
    EXPECT_FLOAT_EQ(TExponentialDistr::cumulativeDistrFunction(0., 1.), 0.);
    EXPECT_FLOAT_EQ(TExponentialDistr::cumulativeDistrFunction(0.92, 10.), 0.999899);
    EXPECT_FLOAT_EQ(TExponentialDistr::cumulativeDistrFunction(0.0001, 73.2), 0.007293274);
}

// chisq distribution

TEST(mathFunctionsTests, dchisqThrow){
    // invalid x
    EXPECT_THROW(TChisqDistr::density(-1., 1), std::runtime_error);
    EXPECT_THROW(TChisqDistr::density(0., 1), std::runtime_error);
    // invalid k
    EXPECT_THROW(TChisqDistr::density(1., -1), std::runtime_error);
    EXPECT_THROW(TChisqDistr::density(1., 0), std::runtime_error);
}

TEST(mathFunctionsTests, dchisqNoThrow){
    // valid x
    EXPECT_NO_THROW(TChisqDistr::density(0., 2));
    EXPECT_NO_THROW(TChisqDistr::density(1., 1.));
    // valid k
    EXPECT_NO_THROW(TChisqDistr::density(1., 10));
}

TEST(mathFunctionsTests, dchisq){
    EXPECT_FLOAT_EQ(TChisqDistr::density(0., 5), 0.);
    EXPECT_FLOAT_EQ(TChisqDistr::density(1.5, 1), 0.1538663);
    EXPECT_FLOAT_EQ(TChisqDistr::density(0.92, 10), 0.0005888635);
}

TEST(mathFunctionsTests, dchisqLog){
    EXPECT_FLOAT_EQ(TChisqDistr::density(1., 5, true), -2.517551);
    EXPECT_FLOAT_EQ(TChisqDistr::density(1.5, 1, true), -1.871671);
    EXPECT_FLOAT_EQ(TChisqDistr::density(0.92, 10, true), -7.437316);
}

// chi distribution

TEST(mathFunctionsTests, dchiThrow){
    // invalid x
    EXPECT_THROW(TChiDistr::density(-1., 1), std::runtime_error);
    // invalid k
    EXPECT_THROW(TChiDistr::density(1., -1), std::runtime_error);
    EXPECT_THROW(TChiDistr::density(1., 0), std::runtime_error);
}

TEST(mathFunctionsTests, dchiNoThrow){
    // valid x
    EXPECT_NO_THROW(TChiDistr::density(0., 1.));
    EXPECT_NO_THROW(TChiDistr::density(1., 1.));
    // valid k
    EXPECT_NO_THROW(TChiDistr::density(1., 10));
}

TEST(mathFunctionsTests, dchi){
    EXPECT_FLOAT_EQ(TChiDistr::density(0., 5), 0.);
    EXPECT_FLOAT_EQ(TChiDistr::density(1.5, 1), 0.2590352);
    EXPECT_FLOAT_EQ(TChiDistr::density(0.92, 10), 0.000805315);
}

TEST(mathFunctionsTests, dchiLog){
    EXPECT_FLOAT_EQ(TChiDistr::density(1., 5, true), -1.824404);
    EXPECT_FLOAT_EQ(TChiDistr::density(1.5, 1, true), -1.350791);
    EXPECT_FLOAT_EQ(TChiDistr::density(0.92, 10, true), -7.124277);
}

// normal distribution

TEST(mathFunctionsTests, dnormThrow){
    // invalid var
    EXPECT_THROW(TNormalDistr::density(0., 1., -1.), std::runtime_error);
    EXPECT_THROW(TNormalDistr::density(0., 1., 0.), std::runtime_error);
}

TEST(mathFunctionsTests, dnormNoThrow){
    // valid var
    EXPECT_NO_THROW(TNormalDistr::density(0., 1., sqrt(0.5)));
    EXPECT_NO_THROW(TNormalDistr::density(0., 1., sqrt(100.)));
}

TEST(mathFunctionsTests, pnormThrow){
    // invalid var
    EXPECT_THROW(TNormalDistr::cumulativeDistrFunction(0., 1., -1.), std::runtime_error);
    EXPECT_THROW(TNormalDistr::cumulativeDistrFunction(0., 1., 0.), std::runtime_error);
}

TEST(mathFunctionsTests, pnormNoThrow){
    // valid var
    EXPECT_NO_THROW(TNormalDistr::density(0., 1., sqrt(0.5)));
    EXPECT_NO_THROW(TNormalDistr::density(0., 1., sqrt(100.)));
}

TEST(mathFunctionsTests, qnormThrow){
    // invalid var
    EXPECT_THROW(TNormalDistr::invCumulativeDistrFunction(0.5, 1., -1.), std::runtime_error);
    EXPECT_THROW(TNormalDistr::invCumulativeDistrFunction(0.5, 1., 0.), std::runtime_error);
    // invalid p
    EXPECT_THROW(TNormalDistr::invCumulativeDistrFunction(-1., 1., sqrt(0.1)), std::runtime_error);
    EXPECT_THROW(TNormalDistr::invCumulativeDistrFunction(2., 1., sqrt(0.1)), std::runtime_error);
    EXPECT_THROW(TNormalDistr::invCumulativeDistrFunction(0., 1., sqrt(0.1)), std::runtime_error);
    EXPECT_THROW(TNormalDistr::invCumulativeDistrFunction(1., 1., sqrt(0.1)), std::runtime_error);
}

TEST(mathFunctionsTests, qnormNoThrow){
    // valid var and p
    EXPECT_NO_THROW(TNormalDistr::invCumulativeDistrFunction(0.5, 1., sqrt(0.5)));
    EXPECT_NO_THROW(TNormalDistr::invCumulativeDistrFunction(0.5, 1., sqrt(100.)));
}

TEST(mathFunctionsTests, dnorm){
    EXPECT_FLOAT_EQ(TNormalDistr::density(0., 0., 1.), 0.3989423);
    EXPECT_FLOAT_EQ(TNormalDistr::density(-10, 9.3, sqrt(50.41)), 0.001396708);
    EXPECT_FLOAT_EQ(TNormalDistr::density(92.91, 88.2, sqrt(9.61)), 0.04057674);
}

TEST(mathFunctionsTests, dnormLog){
    EXPECT_FLOAT_EQ(TNormalDistr::density(0., 0., 1., true), -0.9189385);
    EXPECT_FLOAT_EQ(TNormalDistr::density(-10, 9.3, sqrt(50.41), true), -6.573638);
    EXPECT_FLOAT_EQ(TNormalDistr::density(92.91, 88.2, sqrt(9.61), true), -3.20456);
}

TEST(mathFunctionsTests, pnorm){
    EXPECT_FLOAT_EQ(TNormalDistr::cumulativeDistrFunction(0., 0., 1.), 0.5);
    EXPECT_FLOAT_EQ(TNormalDistr::cumulativeDistrFunction(-10, 9.3, sqrt(50.41)), 0.003280818);
    EXPECT_FLOAT_EQ(TNormalDistr::cumulativeDistrFunction(92.91, 88.2, sqrt(9.61)), 0.9356634);
}

TEST(mathFunctionsTests, qnorm){
    EXPECT_FLOAT_EQ(TNormalDistr::invCumulativeDistrFunction(0.1, 0., 1.), -1.281552);
    EXPECT_FLOAT_EQ(TNormalDistr::invCumulativeDistrFunction(0.3, 9.3, sqrt(50.41)), 5.576756);
    EXPECT_FLOAT_EQ(TNormalDistr::invCumulativeDistrFunction(0.63, 88.2, sqrt(9.61)), 89.22875);
}

// binomial distribution

TEST(mathFunctionsTests, dbinomThrow){
    // invalid n
    EXPECT_THROW(TBinomialDistr::density(-1, -1, 0.6), std::runtime_error);
    // invalid k
    EXPECT_THROW(TBinomialDistr::density(5, -1, 0.6), std::runtime_error);
    EXPECT_THROW(TBinomialDistr::density(5, 6, 0.6), std::runtime_error);
    // invalid p
    EXPECT_THROW(TBinomialDistr::density(5, 4, -1.), std::runtime_error);
    EXPECT_THROW(TBinomialDistr::density(5, 4, 2.), std::runtime_error);
}

TEST(mathFunctionsTests, dbinomNoThrow){
    // valid n
    EXPECT_NO_THROW(TBinomialDistr::density(0, 0, 0.1));
    EXPECT_NO_THROW(TBinomialDistr::density(101, 0, 0.1));
    // valid k
    EXPECT_NO_THROW(TBinomialDistr::density(10, 0, 0.1));
    EXPECT_NO_THROW(TBinomialDistr::density(10, 10, 0.1));
    // valid p
    EXPECT_NO_THROW(TBinomialDistr::density(10, 5, 0.));
    EXPECT_NO_THROW(TBinomialDistr::density(10, 5, 0.5));
    EXPECT_NO_THROW(TBinomialDistr::density(10, 5, 1.));
}

TEST(mathFunctionsTests, dbinom){
    EXPECT_FLOAT_EQ(TBinomialDistr::density(10, 8, 0.9), 0.1937102);
    EXPECT_FLOAT_EQ(TBinomialDistr::density(1, 1, 0.9), 0.9);
    EXPECT_FLOAT_EQ(TBinomialDistr::density(19, 0, 0.11), 0.10924716);
}

TEST(mathFunctionsTests, dbinomLog){
    EXPECT_FLOAT_EQ(TBinomialDistr::density(10, 8, 0.9, true), -1.641392);
    EXPECT_FLOAT_EQ(TBinomialDistr::density(1, 1, 0.9, true), -0.1053605);
    EXPECT_FLOAT_EQ(TBinomialDistr::density(19, 0, 0.11, true), -2.214143);
}

// bernouilli distribution

TEST(mathFunctionsTests, dbernouilliThrow){
    // invalid pi
    EXPECT_THROW(TBernouilliDistr::density(true, -0.1), std::runtime_error);
    EXPECT_THROW(TBernouilliDistr::density(true, 1.1), std::runtime_error);
}

TEST(mathFunctionsTests, dbernouilliNoThrow){
    // valid pi
    EXPECT_NO_THROW(TBernouilliDistr::density(true, 0.));
    EXPECT_NO_THROW(TBernouilliDistr::density(true, 0.5));
    EXPECT_NO_THROW(TBernouilliDistr::density(true, 1.));
}

TEST(mathFunctionsTests, dbernouilli){
    EXPECT_FLOAT_EQ(TBernouilliDistr::density(false, 0.), 1.);
    EXPECT_FLOAT_EQ(TBernouilliDistr::density(false, 1.), 0.);
    EXPECT_FLOAT_EQ(TBernouilliDistr::density(true, 0.), 0.);
    EXPECT_FLOAT_EQ(TBernouilliDistr::density(true, 1.), 1.);
    EXPECT_FLOAT_EQ(TBernouilliDistr::density(true, 0.44), 0.44);
    EXPECT_FLOAT_EQ(TBernouilliDistr::density(false, 0.44), 0.56);
}

TEST(mathFunctionsTests, dbernouilliLog){
    EXPECT_FLOAT_EQ(TBernouilliDistr::density(false, 0., true), 0.);
    EXPECT_FLOAT_EQ(TBernouilliDistr::density(true, 1., true), 0.);
    EXPECT_FLOAT_EQ(TBernouilliDistr::density(true, 0.44, true), -0.8209806);
    EXPECT_FLOAT_EQ(TBernouilliDistr::density(false, 0.44, true), -0.5798185);
}

// dirichlet distribution

TEST(mathFunctionsTests, ddirichletThrow){
    // invalid: alpha <= 0
    EXPECT_THROW(TDirichletDistr::density({0.5, 0.25, 0.25}, {0.1, 0.2, 0.}), std::runtime_error);
    EXPECT_THROW(TDirichletDistr::density({0.5, 0.25, 0.25}, {0.1, 0.2, -0.3}), std::runtime_error);
    // invalid: x < 0
    EXPECT_THROW(TDirichletDistr::density({-0.5, 0.5, 0.5}, {0.1, 0.2, 0.3}), std::runtime_error);
    // invalid: x > 1
    EXPECT_THROW(TDirichletDistr::density({1.5, 0.5, 0.5}, {0.1, 0.2, 0.3}), std::runtime_error);
    // invalid: x does not sum to 1
    EXPECT_THROW(TDirichletDistr::density({0.5, 0.5, 0.5}, {0.1, 0.2, 0.3}), std::runtime_error);
    // invalid: size of x and alpha doesn't match
    EXPECT_THROW(TDirichletDistr::density({0.5, 0.5}, {0.1, 0.2, 0.3}), std::runtime_error);
}

TEST(mathFunctionsTests, ddirichletNoThrow){
    // valid: x == 0
    EXPECT_NO_THROW(TDirichletDistr::density({0., 0.5, 0.5}, {0.1, 0.2, 0.3}));
    // valid: x == 1
    EXPECT_NO_THROW(TDirichletDistr::density({1.}, {0.1}));
}

TEST(mathFunctionsTests, ddirichlet){
    std::vector<double> x = {0.181818181818182,0.163636363636364,0.145454545454545,0.127272727272727,0.109090909090909,0.0909090909090909,0.0727272727272727,0.0545454545454545,0.0363636363636364,0.0181818181818182};
    std::vector<double> alpha = {0.1,0.188888888888889,0.277777777777778,0.366666666666667,0.455555555555556,0.544444444444445,0.633333333333333,0.722222222222222,0.811111111111111,0.9};
    EXPECT_FLOAT_EQ(TDirichletDistr::density(x, alpha), 458.0038);
    EXPECT_FLOAT_EQ(TDirichletDistr::density(x, alpha, true), 6.126878);

    x = {0.139495918701045,0.143278688150917,0.0490180575439956,0.0479300393857425,0.127627647333716,0.0912743516047731,0.0711770008483544,0.01044347693951,0.169024252259974,0.150730567231972};
    alpha = {0.715417139410271,1.18555226917834,0.70766649614947,2.02720824248138,0.0529360190437087,0.0316477520391345,0.200610389001667,0.694285509256988,0.00192753784358501,0.339209412224591};
    EXPECT_FLOAT_EQ(TDirichletDistr::density(x, alpha), 0.09994159);
    EXPECT_FLOAT_EQ(TDirichletDistr::density(x, alpha, true), -2.303169);
}

// dirichlet distribution - reusable

TEST(mathFunctionsTests, ddirichletReUsableThrow){
    // invalid: alpha <= 0
    EXPECT_THROW(TDirichletDistrReUsable dirichlet({0.1, 0.2, 0.}), std::runtime_error);
    EXPECT_THROW(TDirichletDistrReUsable dirichlet({0.1, 0.2, -0.3}), std::runtime_error);

    // this is all what we can test over the constructor - now test if x is checked inside density function
    TDirichletDistrReUsable dirichlet({0.1, 0.2, 0.3});
    // invalid: x < 0
    EXPECT_THROW(dirichlet.density({-0.5, 0.5, 0.5}), std::runtime_error);
    // invalid: x > 1
    EXPECT_THROW(dirichlet.density({1.5, 0.5, 0.5}), std::runtime_error);
    // invalid: x does not sum to 1
    EXPECT_THROW(dirichlet.density({0.5, 0.5, 0.5}), std::runtime_error);
    // invalid: size of x and alpha doesn't match
    EXPECT_THROW(dirichlet.density({0.5, 0.5}), std::runtime_error);
}

TEST(mathFunctionsTests, ddirichletReUsableNoThrow){
    TDirichletDistrReUsable dirichlet({0.1, 0.2, 0.3});
    // valid: x == 0
    EXPECT_NO_THROW(dirichlet.density({0., 0.5, 0.5}));
    // valid: x == 1
    TDirichletDistrReUsable dirichlet2({0.1});
    EXPECT_NO_THROW(dirichlet2.density({1.}));
}

TEST(mathFunctionsTests, ddirichletReUsable){
    std::vector<double> alpha = {0.1,0.188888888888889,0.277777777777778,0.366666666666667,0.455555555555556,0.544444444444445,0.633333333333333,0.722222222222222,0.811111111111111,0.9};
    TDirichletDistrReUsable dirichlet(alpha);

    std::vector<double> x = {0.181818181818182,0.163636363636364,0.145454545454545,0.127272727272727,0.109090909090909,0.0909090909090909,0.0727272727272727,0.0545454545454545,0.0363636363636364,0.0181818181818182};
    EXPECT_FLOAT_EQ(dirichlet.density(x), 458.0038);
    EXPECT_FLOAT_EQ(dirichlet.density(x, true), 6.126878);

    // other values -> reset
    alpha = {0.715417139410271,1.18555226917834,0.70766649614947,2.02720824248138,0.0529360190437087,0.0316477520391345,0.200610389001667,0.694285509256988,0.00192753784358501,0.339209412224591};
    dirichlet.resetParameters(alpha);
    x = {0.139495918701045,0.143278688150917,0.0490180575439956,0.0479300393857425,0.127627647333716,0.0912743516047731,0.0711770008483544,0.01044347693951,0.169024252259974,0.150730567231972};
    EXPECT_FLOAT_EQ(dirichlet.density(x), 0.09994159);
    EXPECT_FLOAT_EQ(dirichlet.density(x, true), -2.303169);
}

// gamma distribution

TEST(mathFunctionsTests, dgammaThrow){
    // invalid x
    EXPECT_THROW(TGammaDistr::density(-1., 0.1, 0.1), std::runtime_error);
    EXPECT_THROW(TGammaDistr::density(0., 0.1, 0.1), std::runtime_error);
    // invalid alpha
    EXPECT_THROW(TGammaDistr::density(0.5, -1., 0.1), std::runtime_error);
    EXPECT_THROW(TGammaDistr::density(0.5, 0., 0.1), std::runtime_error);
    // invalid beta
    EXPECT_THROW(TGammaDistr::density(0.5, 0.1, -1.), std::runtime_error);
    EXPECT_THROW(TGammaDistr::density(0.5, 0.1, 0.), std::runtime_error);
}

TEST(mathFunctionsTests, dgammaNoThrow){
    // valid x
    EXPECT_NO_THROW(TGammaDistr::density(0.1, 0.1, 0.1));
    EXPECT_NO_THROW(TGammaDistr::density(2., 0.1, 0.1));
    // valid alpha and beta
    EXPECT_NO_THROW(TGammaDistr::density(0.5, 2., 0.1));
    EXPECT_NO_THROW(TGammaDistr::density(0.5, 2., 100.));
}

TEST(mathFunctionsTests, pgammaThrow){
    // invalid x
    EXPECT_THROW(TGammaDistr::cumulativeDistrFunction(-1., 0.1, 0.1), std::runtime_error);
    EXPECT_THROW(TGammaDistr::cumulativeDistrFunction(0., 0.1, 0.1), std::runtime_error);
    // invalid alpha
    EXPECT_THROW(TGammaDistr::cumulativeDistrFunction(0.5, -1., 0.1), std::runtime_error);
    EXPECT_THROW(TGammaDistr::cumulativeDistrFunction(0.5, 0., 0.1), std::runtime_error);
    // invalid beta
    EXPECT_THROW(TGammaDistr::cumulativeDistrFunction(0.5, 0.1, -1.), std::runtime_error);
    EXPECT_THROW(TGammaDistr::cumulativeDistrFunction(0.5, 0.1, 0.), std::runtime_error);
}

TEST(mathFunctionsTests, pgammaNoThrow){
    // valid x
    EXPECT_NO_THROW(TGammaDistr::cumulativeDistrFunction(0.1, 0.1, 0.1));
    EXPECT_NO_THROW(TGammaDistr::cumulativeDistrFunction(2., 0.1, 0.1));
    // valid alpha and beta
    EXPECT_NO_THROW(TGammaDistr::cumulativeDistrFunction(0.5, 2., 0.1));
    EXPECT_NO_THROW(TGammaDistr::cumulativeDistrFunction(0.5, 2., 100.));
}

TEST(mathFunctionsTests, dgamma){
    EXPECT_FLOAT_EQ(TGammaDistr::density(0.1, 0.7, 0.7), 1.116541);
    EXPECT_FLOAT_EQ(TGammaDistr::density(0.00000001, 0.1, 2.6), 1832975);
    EXPECT_FLOAT_EQ(TGammaDistr::density(2.354, 0.1, 1), 0.004620663);
}

TEST(mathFunctionsTests, dgammaLog){
    EXPECT_FLOAT_EQ(TGammaDistr::density(0.1, 0.7, 0.7, true), 0.1102358);
    EXPECT_FLOAT_EQ(TGammaDistr::density(0.00000001, 0.1, 2.6, true), 14.42145);
    EXPECT_FLOAT_EQ(TGammaDistr::density(2.354, 0.1, 1, true), -5.377217);
}

TEST(mathFunctionsTests, pgamma){
    EXPECT_FLOAT_EQ(TGammaDistr::cumulativeDistrFunction(0.1, 0.7, 0.7), 0.1662473);
    EXPECT_FLOAT_EQ(TGammaDistr::cumulativeDistrFunction(0.00000001, 0.1, 2.6), 0.1832975);
    EXPECT_FLOAT_EQ(TGammaDistr::cumulativeDistrFunction(2.354, 0.1, 1), 0.996445);
}

// generalized pareto distribution

TEST(mathFunctionsTests, dparetoThrow){
    // invalid x
    EXPECT_THROW(TParetoDistr::density(-1., 0., 0.1, 0.1, true), std::runtime_error);
    EXPECT_THROW(TParetoDistr::density(3., 0., 1., -1., true), std::runtime_error);
    // invalid sd
    EXPECT_THROW(TParetoDistr::density(1., 0., -1., 0.1, true), std::runtime_error);

    // not in log
    EXPECT_THROW(TParetoDistr::density(1., 0., -1., 0.1), std::runtime_error);
}

TEST(mathFunctionsTests, dparetoNoThrow){
    // valid x
    EXPECT_NO_THROW(TParetoDistr::density(0., 0., 0.1, 0.1, true));
    EXPECT_NO_THROW(TParetoDistr::density(3., 0., 1., 1., true));
}

TEST(mathFunctionsTests, pparetoThrow){
    // invalid x
    EXPECT_THROW(TParetoDistr::cumulativeDistrFunction(-1., 0., 0.1, 0.1), std::runtime_error);
    EXPECT_THROW(TParetoDistr::cumulativeDistrFunction(3., 0., 1., -1.), std::runtime_error);
    // invalid sd
    EXPECT_THROW(TParetoDistr::cumulativeDistrFunction(1., 0., -1., 0.1), std::runtime_error);
}

TEST(mathFunctionsTests, pparetoNoThrow){
    // valid x
    EXPECT_NO_THROW(TParetoDistr::cumulativeDistrFunction(0., 0., 0.1, 0.1));
    EXPECT_NO_THROW(TParetoDistr::cumulativeDistrFunction(3., 0., 1., 1.));
}

TEST(mathFunctionsTests, dpareto){
    EXPECT_FLOAT_EQ(TParetoDistr::density(0.1, 0., 1., 0., true), -0.1);
    EXPECT_FLOAT_EQ(TParetoDistr::density(0.9, -1, 0.4, 1.2, true), -2.570906);
    EXPECT_FLOAT_EQ(TParetoDistr::density(-9, -10, 11, 0.2, true), -2.506006);
}

TEST(mathFunctionsTests, ppareto){
    EXPECT_FLOAT_EQ(TParetoDistr::cumulativeDistrFunction(0.1, 0., 1., 0.), 0.09516258);
    EXPECT_FLOAT_EQ(TParetoDistr::cumulativeDistrFunction(0.9, -1, 0.4, 1.2), 0.7950706);
    EXPECT_FLOAT_EQ(TParetoDistr::cumulativeDistrFunction(-9, -10, 11, 0.2), 0.08615337);
}

// incomplete gamma functions

TEST(mathFunctionsTests, lowerIncompleteGamma){
    // compared to: Rgamma(x, a, b, lower = T) from R package zipfR (our implementation is the regularized incomplete gamma function!)
    EXPECT_FLOAT_EQ(TIncompleteGamma::lower(1.8, 0.1), 0.008867713);
    EXPECT_FLOAT_EQ(TIncompleteGamma::lower(0.7, 1.92), 0.9166503);
}

TEST(mathFunctionsTests, upperIncompleteGamma){
    // compared to: Rbeta(x, a, b, lower = F) from R package zipfR (our implementation is the regularized incomplete beta function!)
    EXPECT_FLOAT_EQ(TIncompleteGamma::upper(1.8, 0.1), 0.9911323);
    EXPECT_FLOAT_EQ(TIncompleteGamma::upper(0.7, 1.92), 0.08334971);
}

// factorials

TEST(mathFunctionsTests, factorialThrow){
    TFactorial fac;
    EXPECT_THROW(TFactorial::factorial(-1), std::runtime_error);
    EXPECT_THROW(TFactorial::factorial(171), std::runtime_error);
}

TEST(mathFunctionsTests, factorial){
    EXPECT_EQ(TFactorial::factorial(0), 1);
    EXPECT_EQ(TFactorial::factorial(1), 1);
    EXPECT_EQ(TFactorial::factorial(2), 2);
    EXPECT_EQ(TFactorial::factorial(3), 6);
    EXPECT_EQ(TFactorial::factorial(20), 2432902008176640000);
}

TEST(mathFunctionsTests, factorialLogThrow){
    EXPECT_THROW(TFactorial::factorialLog(-1), std::runtime_error);
}

TEST(mathFunctionsTests, factorialLog){
    EXPECT_NEAR(TFactorial::factorialLog(0), 0., 10e-15);
    EXPECT_NEAR(TFactorial::factorialLog(1), 0., 10e-15);
    EXPECT_FLOAT_EQ(TFactorial::factorialLog(2), 0.6931472);
    EXPECT_FLOAT_EQ(TFactorial::factorialLog(3), 1.791759);
    EXPECT_FLOAT_EQ(TFactorial::factorialLog(170), 706.5731);
}

// choose
TEST(mathFunctionsTests, chooseLog){
    EXPECT_NEAR(chooseLog(0, 0), 0., 10e-15);
    EXPECT_NEAR(chooseLog(1, 0), 0., 10e-15);
    EXPECT_FLOAT_EQ(chooseLog(170, 27), 71.9278);
    EXPECT_FLOAT_EQ(chooseLog(89, 2), 8.272826);
}

TEST(mathFunctionsTests, choose){
    EXPECT_NEAR(choose(0, 0), 1, 10e-15);
    EXPECT_EQ(choose(1, 0), 1);
    EXPECT_EQ(choose(17, 10), 19448);
    EXPECT_EQ(choose(60, 2), 1770);
}

// binomial p value

TEST(mathFunctionsTests, binomPValueClass){
    TBinomPValue binomPValue;
    EXPECT_NEAR(binomPValue.binomPValue(2,10), 0.05469, 10e-2);
    EXPECT_NEAR(binomPValue.binomPValue(2,20), 0.0002012, 10e-2);
    EXPECT_NEAR(binomPValue.binomPValue(53,100),  0.7579, 10e-2);
}

// incomplete beta function

TEST(mathFunctionsTests, incompleteBetaThrow){
    // invalid x
    EXPECT_THROW(TIncompleteBeta::incompleteBeta(0.1, 0.1, -1.), std::runtime_error);
    EXPECT_THROW(TIncompleteBeta::incompleteBeta(0.1, 0.1, 2.), std::runtime_error);
    // invalid alpha
    EXPECT_THROW(TIncompleteBeta::incompleteBeta(-1., 0.1, 0.5), std::runtime_error);
    EXPECT_THROW(TIncompleteBeta::incompleteBeta(0., 0.1, 0.5), std::runtime_error);
    // invalid beta
    EXPECT_THROW(TIncompleteBeta::incompleteBeta(0.1, 0., 0.5), std::runtime_error);
    EXPECT_THROW(TIncompleteBeta::incompleteBeta(0.1, -1., 0.5), std::runtime_error);
}

TEST(mathFunctionsTests, incompleteBetaNoThrow){
    // valid x
    EXPECT_NO_THROW(TIncompleteBeta::incompleteBeta(0.1, 0.1, 0.));
    EXPECT_NO_THROW(TIncompleteBeta::incompleteBeta(0.1, 0.1, 0.5));
    EXPECT_NO_THROW(TIncompleteBeta::incompleteBeta(0.1, 0.1, 1.));
    // valid alpha and beta
    EXPECT_NO_THROW(TIncompleteBeta::incompleteBeta(0.1, 2., 0.5));
    EXPECT_NO_THROW(TIncompleteBeta::incompleteBeta(2., 0.1, 0.5));
}

TEST(mathFunctionsTests, incompleteBeta){
    // compared to: Rbeta(x, a, b) from R package zipfR (our implementation is the regularized incomplete beta function!)
    EXPECT_FLOAT_EQ(TIncompleteBeta::incompleteBeta(0.5, 0.5, 0.1),  0.2048328);
    EXPECT_FLOAT_EQ(TIncompleteBeta::incompleteBeta(5., 0.5, 0.8),  0.1449276);
}

TEST(mathFunctionsTests, inverseIncompleteBeta){
    // compared to: Rbeta.inv(x, a, b) from R package zipfR
    EXPECT_FLOAT_EQ(TIncompleteBeta::inverseIncompleteBeta(0.1, 0.5, 0.5),  0.02447174);
    EXPECT_FLOAT_EQ(TIncompleteBeta::inverseIncompleteBeta(0.8, 5., 0.5),  0.9932759);
}

//------------------------------------------------
// types and numeric limits
//------------------------------------------------

// addition
TEST(mathFunctionsTests, checkForNumericOverflow_addition_double){
    // overflow
    double limit = std::numeric_limits<double>::max();
    double nextSmallerValue = std::nextafter(limit, 0.); // double precision for very large values is different than for small values -> this gives us the next-smaller value below the limit
    double dist = limit - nextSmallerValue; // distance between second largest and largest number

    // jump below max
    EXPECT_TRUE(checkForNumericOverflow_addition(nextSmallerValue, std::nextafter(dist, 0.)));
    // jump exactly on max
    EXPECT_TRUE(checkForNumericOverflow_addition(nextSmallerValue, dist));
    // jump across max
    EXPECT_FALSE(checkForNumericOverflow_addition(nextSmallerValue, 2.*dist));

    // underflow
    limit = std::numeric_limits<double>::lowest();
    nextSmallerValue = std::nextafter(limit, 0.);
    dist = nextSmallerValue - limit; // distance between second smallest and smallest number

    // jump above min
    EXPECT_TRUE(checkForNumericOverflow_addition(nextSmallerValue, -std::nextafter(dist, 0.)));
    // jump exactly on min
    EXPECT_TRUE(checkForNumericOverflow_addition(nextSmallerValue, -dist));
    // jump across min
    EXPECT_FALSE(checkForNumericOverflow_addition(nextSmallerValue, -2.*dist));
}

TEST(mathFunctionsTests, checkForNumericOverflow_addition_int){
    // overflow
    int limit = std::numeric_limits<int>::max();
    int nextSmallerValue = limit - 1;

    // jump below max
    EXPECT_TRUE(checkForNumericOverflow_addition(nextSmallerValue-1, 1));
    // jump exactly on max
    EXPECT_TRUE(checkForNumericOverflow_addition(nextSmallerValue, 1));
    // jump across max
    EXPECT_FALSE(checkForNumericOverflow_addition(nextSmallerValue, 2));

    // underflow
    limit = std::numeric_limits<int>::lowest();
    nextSmallerValue = limit+1;

    // jump above min
    EXPECT_TRUE(checkForNumericOverflow_addition(nextSmallerValue+1, -1));
    // jump exactly on min
    EXPECT_TRUE(checkForNumericOverflow_addition(nextSmallerValue, -1));
    // jump across min
    EXPECT_FALSE(checkForNumericOverflow_addition(nextSmallerValue, -2));
}

TEST(mathFunctionsTests, checkForNumericOverflow_addition_uint8){
    // overflow
    uint8_t limit = std::numeric_limits<uint8_t>::max();
    uint8_t nextSmallerValue = limit - 1;

    // jump below max
    EXPECT_TRUE(checkForNumericOverflow_addition<uint8_t>(nextSmallerValue-1, 1));
    // jump exactly on max
    EXPECT_TRUE(checkForNumericOverflow_addition<uint8_t>(nextSmallerValue, 1));
    // jump across max
    EXPECT_FALSE(checkForNumericOverflow_addition<uint8_t>(nextSmallerValue, 2));

    // underflow
    limit = std::numeric_limits<uint8_t>::lowest();
    nextSmallerValue = limit+1;

    // jump above min
    EXPECT_TRUE(checkForNumericOverflow_addition<uint8_t>(nextSmallerValue+1, -1));
    // jump exactly on min
    EXPECT_TRUE(checkForNumericOverflow_addition<uint8_t>(nextSmallerValue, -1));
    // jump across min
    EXPECT_FALSE(checkForNumericOverflow_addition<uint8_t>(nextSmallerValue, -2));
}

// subtraction
TEST(mathFunctionsTests, checkForNumericOverflow_subtraction_double){
    // overflow
    double limit = std::numeric_limits<double>::max();
    double nextSmallerValue = std::nextafter(limit, 0.); // double precision for very large values is different than for small values -> this gives us the next-smaller value below the limit
    double dist = limit - nextSmallerValue; // distance between second largest and largest number

    // jump below max
    EXPECT_TRUE(checkForNumericOverflow_subtraction(nextSmallerValue, -std::nextafter(dist, 0.)));
    // jump exactly on max
    EXPECT_TRUE(checkForNumericOverflow_subtraction(nextSmallerValue, -dist));
    // jump across max
    EXPECT_FALSE(checkForNumericOverflow_subtraction(nextSmallerValue, -2.*dist));

    // underflow
    limit = std::numeric_limits<double>::lowest();
    nextSmallerValue = std::nextafter(limit, 0.);
    dist = nextSmallerValue - limit; // distance between second smallest and smallest number

    // jump above min
    EXPECT_TRUE(checkForNumericOverflow_subtraction(nextSmallerValue, std::nextafter(dist, 0.)));
    // jump exactly on min
    EXPECT_TRUE(checkForNumericOverflow_subtraction(nextSmallerValue, dist));
    // jump across min
    EXPECT_FALSE(checkForNumericOverflow_subtraction(nextSmallerValue, 2.*dist));
}

TEST(mathFunctionsTests, checkForNumericOverflow_subtraction_int){
    // overflow
    int limit = std::numeric_limits<int>::max();
    int nextSmallerValue = limit - 1;

    // jump below max
    EXPECT_TRUE(checkForNumericOverflow_subtraction(nextSmallerValue-1, -1));
    // jump exactly on max
    EXPECT_TRUE(checkForNumericOverflow_subtraction(nextSmallerValue, -1));
    // jump across max
    EXPECT_FALSE(checkForNumericOverflow_subtraction(nextSmallerValue, -2));

    // underflow
    limit = std::numeric_limits<int>::lowest();
    nextSmallerValue = limit+1;

    // jump above min
    EXPECT_TRUE(checkForNumericOverflow_subtraction(nextSmallerValue+1, 1));
    // jump exactly on min
    EXPECT_TRUE(checkForNumericOverflow_subtraction(nextSmallerValue, 1));
    // jump across min
    EXPECT_FALSE(checkForNumericOverflow_subtraction(nextSmallerValue, 2));
}

TEST(mathFunctionsTests, checkForNumericOverflow_subtraction_uint8){
    // overflow
    uint8_t limit = std::numeric_limits<uint8_t>::max();
    uint8_t nextSmallerValue = limit - 1;

    // jump below max
    EXPECT_TRUE(checkForNumericOverflow_subtraction<uint8_t>(nextSmallerValue-1, -1));
    // jump exactly on max
    EXPECT_TRUE(checkForNumericOverflow_subtraction<uint8_t>(nextSmallerValue, -1));
    // jump across max
    EXPECT_FALSE(checkForNumericOverflow_subtraction<uint8_t>(nextSmallerValue, -2));

    // underflow
    limit = std::numeric_limits<uint8_t>::lowest();
    nextSmallerValue = limit+1;

    // jump above min
    EXPECT_TRUE(checkForNumericOverflow_subtraction<uint8_t>(nextSmallerValue+1, 1));
    // jump exactly on min
    EXPECT_TRUE(checkForNumericOverflow_subtraction<uint8_t>(nextSmallerValue, 1));
    // jump across min
    EXPECT_FALSE(checkForNumericOverflow_subtraction<uint8_t>(nextSmallerValue, 2));
}

// multiplication
TEST(mathFunctionsTests, checkForNumericOverflow_multiplication_double){
    // overflow
    double limit = std::numeric_limits<double>::max();
    double nextSmallerValue = std::nextafter(limit, 0.); // double precision for very large values is different than for small values -> this gives us the next-smaller value below the limit

    // jump below max
    EXPECT_TRUE(checkForNumericOverflow_multiplication(1., nextSmallerValue));
    EXPECT_TRUE(checkForNumericOverflow_multiplication(-1., -nextSmallerValue));
    // jump exactly on max
    EXPECT_TRUE(checkForNumericOverflow_multiplication(1., limit));
    EXPECT_TRUE(checkForNumericOverflow_multiplication(-1., -limit));
    // jump across max
    EXPECT_FALSE(checkForNumericOverflow_multiplication(1., 2.*(limit+nextSmallerValue)));
    EXPECT_FALSE(checkForNumericOverflow_multiplication(-1., -2.*(limit+nextSmallerValue)));

    // underflow
    limit = std::numeric_limits<double>::lowest();
    nextSmallerValue = std::nextafter(limit, 0.);

    // jump above min
    EXPECT_TRUE(checkForNumericOverflow_multiplication(1., -nextSmallerValue));
    EXPECT_TRUE(checkForNumericOverflow_multiplication(-1., nextSmallerValue));
    // jump exactly on min
    EXPECT_TRUE(checkForNumericOverflow_multiplication(1., limit));
    EXPECT_TRUE(checkForNumericOverflow_multiplication(-1., limit));
    // jump across min
    EXPECT_FALSE(checkForNumericOverflow_multiplication(-1., 2.*(limit+nextSmallerValue)));
    EXPECT_FALSE(checkForNumericOverflow_multiplication(1., -2.*(limit+nextSmallerValue)));
}

TEST(mathFunctionsTests, checkForNumericOverflow_multiplication_int){
    // overflow
    int limit = std::numeric_limits<int>::max();
    int nextSmallerValue = limit - 1;

    // jump below max
    EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(1, nextSmallerValue));
    EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(-1, -nextSmallerValue));
    EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(-nextSmallerValue, -1));
    // jump exactly on max
    EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(1, limit));
    EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(-1, -limit));
    EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(-limit, -1));
    // jump across max
    EXPECT_FALSE(checkForNumericOverflow_multiplication<int>(3, limit));

    // underflow
    limit = std::numeric_limits<int>::lowest();
    nextSmallerValue = limit+1;

    // jump above min
    EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(1, -nextSmallerValue));
    EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(-1, nextSmallerValue)); // = max
    EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(nextSmallerValue, -1)); // = max
    // jump exactly on min
    EXPECT_TRUE(checkForNumericOverflow_multiplication<int>(1, limit));
    // jump across min
    EXPECT_FALSE(checkForNumericOverflow_multiplication<int>(-1, limit)); // = abs(min) -> bigger than max
    EXPECT_FALSE(checkForNumericOverflow_multiplication<int>(limit, -1)); // = abs(min) -> bigger than max
    EXPECT_FALSE(checkForNumericOverflow_multiplication<int>(3, limit));
}

TEST(mathFunctionsTests, checkForNumericOverflow_multiplication_uint8){
    // overflow
    uint8_t limit = std::numeric_limits<uint8_t>::max();
    uint8_t nextSmallerValue = limit - 1;

    // jump below max
    EXPECT_TRUE(checkForNumericOverflow_multiplication<uint8_t>(1, nextSmallerValue));
    // jump exactly on max
    EXPECT_TRUE(checkForNumericOverflow_multiplication<uint8_t>(1, limit));
    // jump across max
    EXPECT_FALSE(checkForNumericOverflow_multiplication<uint8_t>(2, limit));

    // underflow -> won't happen, because second argument can't be negative -> will never be < 0
    limit = std::numeric_limits<uint8_t>::lowest();
    nextSmallerValue = limit+1;

    // jump above min
    EXPECT_TRUE(checkForNumericOverflow_multiplication<uint8_t>(1, nextSmallerValue));
    // jump exactly on min
    EXPECT_TRUE(checkForNumericOverflow_multiplication<uint8_t>(1, 0));
    // jump across min -> won't happen, because second argument can't be negative -> will never be < 0
}



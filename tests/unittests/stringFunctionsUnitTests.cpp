//
// Created by madleina on 08.06.20.
//

#include "TestCase.h"
#include "stringFunctions.h"
using namespace testing;

//-------------------------------------------------
// Conversion to string
//-------------------------------------------------

TEST(stringFunctionsTests, toString){
	EXPECT_EQ( toString( (int) 5), "5");
	EXPECT_EQ( readBefore( toString( (float) 75.3476), '0'), "75.3476");
	EXPECT_EQ( toString( "75.3476"), "75.3476");
	EXPECT_EQ( toString( "This is a string!"), "This is a string!");
	EXPECT_EQ( toString(), "");
	EXPECT_EQ( readBefore( toString( (int) -5, "abc", (float) 1.23456), '0' ), "-5abc1.23456");
}

//-------------------------------------------------
// Conversion from string
//-------------------------------------------------

TEST(stringFunctionsTests, stringToInt){
    // valid
    EXPECT_EQ(convertString<int>("0"), 0);
    EXPECT_EQ(convertString<int>("1"), 1);
    EXPECT_EQ(convertString<int>("-1"), -1);
    EXPECT_EQ(convertString<int>("10 823095"), 10);
    EXPECT_EQ(convertString<int>("930 goo"), 930);

    // fails
    EXPECT_NO_THROW(convertString<int>("1e+08"));
    EXPECT_NO_THROW(convertString<int>("foo 18"));
    EXPECT_NO_THROW(convertString<int>("1000000000000000000000000000000000"));
}

TEST(stringFunctionsTests, stringToUnsignedInt){
    // valid
    EXPECT_EQ(convertString<unsigned int>("0"), 0);
    EXPECT_EQ(convertString<unsigned int>("1"), 1);
    EXPECT_EQ(convertString<unsigned int>("-1"), -1);
    EXPECT_EQ(convertString<unsigned int>("10 823095"), 10);
    EXPECT_EQ(convertString<unsigned int>("930 goo"), 930);

    // fails (doesn't throw, but there is undefined behaviour)
    EXPECT_NO_THROW(convertString<unsigned int>("1e+08"));
    EXPECT_NO_THROW(convertString<unsigned int>("foo 18"));
    EXPECT_NO_THROW(convertString<unsigned int>("1000000000000000000000000000000000"));
}

TEST(stringFunctionsTests, stringToLong){
    // valid
    EXPECT_EQ(convertString<long>("0"), 0);
    EXPECT_EQ(convertString<long>("1"), 1);
    EXPECT_EQ(convertString<long>("-1"), -1);
    EXPECT_EQ(convertString<long>("10 823095"), 10);
    EXPECT_EQ(convertString<long>("930 goo"), 930);

    // fails
    EXPECT_NO_THROW(convertString<long>("1e+08"));
    EXPECT_NO_THROW(convertString<long>("foo 18"));
    EXPECT_NO_THROW(convertString<long>("1000000000000000000000000000000000"));
}

TEST(stringFunctionsTests, stringToDouble){
    // valid
    EXPECT_EQ(convertString<double>("1"), 1);
    EXPECT_EQ(convertString<double>("0.73"), 0.73);
    EXPECT_EQ(convertString<double>("1.2619"), 1.2619);
    EXPECT_EQ(convertString<double>("-1.32"), -1.32);
    EXPECT_EQ(convertString<double>("10.5782 823095."), 10.5782);
    EXPECT_EQ(convertString<double>("930.23 goo"), 930.23);

    // fails
    EXPECT_NO_THROW(convertString<double>("1e+08"));
    EXPECT_NO_THROW(convertString<double>("foo 18.1"));
    EXPECT_NO_THROW(convertString<double>("1000000000000000000000000000000000."));
}

TEST(stringFunctionsTests, stringToFloat){
    // valid
    EXPECT_FLOAT_EQ(convertString<float>("1"), 1);
    EXPECT_FLOAT_EQ(convertString<float>("0.73"), 0.73);
    EXPECT_FLOAT_EQ(convertString<float>("1.2619"), 1.2619);
    EXPECT_FLOAT_EQ(convertString<float>("-1.32"), -1.32);
    EXPECT_FLOAT_EQ(convertString<float>("10.5782 823095."), 10.5782);
    EXPECT_FLOAT_EQ(convertString<float>("930.23 goo"), 930.23);

    // fails
    EXPECT_NO_THROW(convertString<float>("1e+08"));
    EXPECT_NO_THROW(convertString<float>("foo 18.1"));
    EXPECT_NO_THROW(convertString<float>("1000000000000000000000000000000000."));
}

TEST(stringFunctionsTests, stringToBool){
    // valid
    EXPECT_TRUE(convertString<bool>("1"));
    EXPECT_TRUE(convertString<bool>("true"));
    EXPECT_FALSE(convertString<bool>("0"));
    EXPECT_FALSE(convertString<bool>("false"));

    // fails, but doesn't throw
    EXPECT_NO_THROW(convertString<bool>("0129"));
    EXPECT_NO_THROW(convertString<bool>("foo"));
    EXPECT_NO_THROW(convertString<bool>("1000000000000000000000000000000000."));
}


TEST(stringFunctionsTests, stringToIntCheck){
    // valid
    EXPECT_EQ(convertStringCheck<int>("0"), 0);
    EXPECT_EQ(convertStringCheck<int>("1"), 1);
    EXPECT_EQ(convertStringCheck<int>("-1"), -1);
    EXPECT_EQ(convertStringCheck<int>("-2147483648"), -2147483648); // smallest possible value for int
    EXPECT_EQ(convertStringCheck<int>("2147483647"), 2147483647); // largest possible value for int

    // fails
    //EXPECT_THROW({try {convertStringCheck<int>("1e+08");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // returns 1!
    EXPECT_THROW({try {convertStringCheck<int>("10 823095");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<int>("930 goo");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<int>("foo 18");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<int>("-2147483649");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // numeric overflow
    EXPECT_THROW({try {convertStringCheck<int>("2147483648");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // numeric overflow
}

TEST(stringFunctionsTests, stringToLongCheck){
    // valid
    EXPECT_EQ(convertStringCheck<long>("0"), 0);
    EXPECT_EQ(convertStringCheck<long>("1"), 1);
    EXPECT_EQ(convertStringCheck<long>("-1"), -1);
    long min = -9223372036854775808U;
    long max = 9223372036854775807U;
    EXPECT_EQ(convertStringCheck<long>("-9223372036854775808"), min); // smallest possible value for long
    EXPECT_EQ(convertStringCheck<long>("9223372036854775807"), max); // largest possible value for long

    // fails
    // EXPECT_THROW({try {convertStringCheck<long>("1e+08");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // returns 1!
    EXPECT_THROW({try {convertStringCheck<long>("10 823095");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<long>("930 goo");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<long>("foo 18");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<long>("-9223372036854775809");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // numeric overflow
    EXPECT_THROW({try {convertStringCheck<long>("9223372036854775808");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // numeric overflow
}

TEST(stringFunctionsTests, stringToUint8tCheck){
    // valid
    EXPECT_EQ(convertStringCheck<uint8_t>("0"), 0);
    EXPECT_EQ(convertStringCheck<uint8_t>("1"), 1);
    EXPECT_EQ(convertStringCheck<uint8_t>("255"), 255); // max

    // fails
    EXPECT_THROW({try {convertStringCheck<uint8_t>("10 20");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<uint8_t>("45 goo");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<uint8_t>("foo 18");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<uint8_t>("-1");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);  // numeric underflow
    EXPECT_THROW({try {convertStringCheck<uint8_t>("256");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // numeric overflow
}

TEST(stringFunctionsTests, stringToUint16tCheck){
    // valid
    EXPECT_EQ(convertStringCheck<uint16_t>("0"), 0);
    EXPECT_EQ(convertStringCheck<uint16_t>("1"), 1);
    EXPECT_EQ(convertStringCheck<uint16_t>("65535"), 65535); // max

    // fails
    EXPECT_THROW({try {convertStringCheck<uint16_t>("10 4312");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<uint16_t>("930 goo");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<uint16_t>("foo 18");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<uint16_t>("-1");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);  // numeric underflow
    EXPECT_THROW({try {convertStringCheck<uint16_t>("65536");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // numeric overflow
}

TEST(stringFunctionsTests, stringToUint32tCheck){
    // valid
    EXPECT_EQ(convertStringCheck<uint32_t>("0"), 0);
    EXPECT_EQ(convertStringCheck<uint32_t>("1"), 1);
    EXPECT_EQ(convertStringCheck<uint32_t>("4294967295"), 4294967295); // max
    // fails
    EXPECT_THROW({try {convertStringCheck<uint32_t>("10 823095");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<uint32_t>("930 goo");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<uint32_t>("foo 18");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<uint32_t>("-1");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);  // numeric underflow
    EXPECT_THROW({try {convertStringCheck<uint32_t>("4294967296");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // numeric overflow
}

TEST(stringFunctionsTests, stringToUint64tCheck){
    // valid
    EXPECT_EQ(convertStringCheck<uint64_t>("0"), 0);
    EXPECT_EQ(convertStringCheck<uint64_t>("1"), 1);
    uint64_t max = 18446744073709551615U;
    EXPECT_EQ(convertStringCheck<uint64_t>("18446744073709551615"), max); // max

    // fails
    EXPECT_THROW({try {convertStringCheck<uint64_t>("10 823095");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<uint64_t>("930 goo");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<uint64_t>("foo 18");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<uint64_t>("-1");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);  // numeric underflow
    EXPECT_THROW({try {convertStringCheck<uint64_t>("18446744073709551616");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // numeric overflow
}

TEST(stringFunctionsTests, stringToDoubleCheck){
    // valid
    EXPECT_EQ(convertStringCheck<double>("1"), 1);
    EXPECT_EQ(convertStringCheck<double>("0.73"), 0.73);
    EXPECT_EQ(convertStringCheck<double>("1.2619"), 1.2619);
    EXPECT_EQ(convertStringCheck<double>("-1.32"), -1.32);
    EXPECT_EQ(convertStringCheck<double>("1e+08"), 1e+08);

    // fails
    EXPECT_THROW({try {convertStringCheck<double>("10 823095");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<double>("930 goo");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<double>("foo 18.1");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<double>("1.89769e+308");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(stringFunctionsTests, stringToFloatCheck){
    // valid
    EXPECT_FLOAT_EQ(convertStringCheck<float>("1"), 1);
    EXPECT_FLOAT_EQ(convertStringCheck<float>("0.73"), 0.73);
    EXPECT_FLOAT_EQ(convertStringCheck<float>("1.2619"), 1.2619);
    EXPECT_FLOAT_EQ(convertStringCheck<float>("-1.32"), -1.32);
    EXPECT_FLOAT_EQ(convertStringCheck<float>("1e+08"), 1e+08);

    // fails
    EXPECT_THROW({try {convertStringCheck<float>("10 823095");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<float>("930 goo");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<float>("foo 18.1");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<float>("3.50282e+38");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(stringFunctionsTests, stringToBoolCheck){
    // valid
    EXPECT_TRUE(convertStringCheck<bool>("1"));
    EXPECT_TRUE(convertStringCheck<bool>("true"));
    EXPECT_FALSE(convertStringCheck<bool>("0"));
    EXPECT_FALSE(convertStringCheck<bool>("false"));

    // fails
    EXPECT_THROW({try {convertStringCheck<bool>("True");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<bool>("1.234");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try {convertStringCheck<bool>("930 goo");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

//-------------------------------------------------
// String contains
//-------------------------------------------------

TEST(stringFunctionsTests, stringContainsString){
    // evaluates true
    std::string s = "foo12-3foo\tblah\n892 ";
    std::string needle = "f";
    EXPECT_TRUE(stringContains(s, needle));
    needle = "foo";
    EXPECT_TRUE(stringContains(s, needle));
    needle = "12-3";
    EXPECT_TRUE(stringContains(s, needle));
    needle = "\t";
    EXPECT_TRUE(stringContains(s, needle));
    needle = "\n";
    EXPECT_TRUE(stringContains(s, needle));
    needle = " ";
    EXPECT_TRUE(stringContains(s, needle));

    // evaluates false
    needle = "j";
    EXPECT_FALSE(stringContains(s, needle));
    needle = "123";
    EXPECT_FALSE(stringContains(s, needle));
    s = "";
    EXPECT_FALSE(stringContains(s, needle));
}

TEST(stringFunctionsTests, stringContainsAny){
    // evaluates true
    std::string s = "foo12-3foo\tblah\n892 ";
    std::string needle = "f";
    EXPECT_TRUE(stringContainsAny(s, needle));
    needle = "foo";
    EXPECT_TRUE(stringContainsAny(s, needle));
    needle = "12-3";
    EXPECT_TRUE(stringContainsAny(s, needle));
    needle = "\t";
    EXPECT_TRUE(stringContainsAny(s, needle));
    needle = "\n";
    EXPECT_TRUE(stringContainsAny(s, needle));
    needle = " ";
    EXPECT_TRUE(stringContainsAny(s, needle));
    needle = "12345678910";
    EXPECT_TRUE(stringContainsAny(s, needle));

    // evaluates false
    needle = "j";
    EXPECT_FALSE(stringContainsAny(s, needle));
    needle = "567";
    EXPECT_FALSE(stringContainsAny(s, needle));
    s = "";
    EXPECT_FALSE(stringContainsAny(s, needle));
}

TEST(stringFunctionsTests, stringContainsChar){
    // evaluates true
    std::string s = "foo12-3foo\tblah\n892 ";
    char needle = 'f';
    EXPECT_TRUE(stringContains(s, needle));
    needle = '1';
    EXPECT_TRUE(stringContains(s, needle));
    needle = '-';
    EXPECT_TRUE(stringContains(s, needle));
    needle = '\t';
    EXPECT_TRUE(stringContains(s, needle));
    needle = '\n';
    EXPECT_TRUE(stringContains(s, needle));
    needle = ' ';
    EXPECT_TRUE(stringContains(s, needle));

    // evaluates false
    needle = 'j';
    EXPECT_FALSE(stringContains(s, needle));
    needle = '5';
    EXPECT_FALSE(stringContains(s, needle));
    s = "";
    EXPECT_FALSE(stringContains(s, needle));
}

TEST(stringFunctionsTests, stringContainsOnly){
    // evaluates true
    std::string s = "aaaaaa";
    std::string needle = "a";
    EXPECT_TRUE(stringContainsOnly(s, needle));
    s = "            ";
    needle = "  ";
    EXPECT_TRUE(stringContainsOnly(s, needle));
    s = "123123123123";
    needle = "321";
    EXPECT_TRUE(stringContainsOnly(s, needle));
    s = "1234827105749204828481946593272372917174901";
    needle = "1234567890";
    EXPECT_TRUE(stringContainsOnly(s, needle));


    // evaluates false
    s = "";
    EXPECT_FALSE(stringContainsOnly(s, needle));
    s = "aaaaaaaaaabaaaaaaaaa";
    needle = "a";
    EXPECT_FALSE(stringContainsOnly(s, needle));
    s = "123123123123";
    needle = "31";
    EXPECT_FALSE(stringContainsOnly(s, needle));
}

TEST(stringFunctionsTests, stringStartsWithString){
    // evaluates true
    std::string s = "foo12-3foo\tblah\n892 ";
    std::string needle = "f";
    EXPECT_TRUE(stringStartsWith(s, needle));
    needle = "foo12";
    EXPECT_TRUE(stringStartsWith(s, needle));

    // evaluates false
    needle = "a";
    EXPECT_FALSE(stringStartsWith(s, needle));
    needle = "foo123";
    EXPECT_FALSE(stringStartsWith(s, needle));
}

TEST(stringFunctionsTests, stringStartsWithChar){
    // evaluates true
    std::string s = "foo12-3foo\tblah\n892 ";
    char needle = 'f';
    EXPECT_TRUE(stringStartsWith(s, needle));

    // evaluates false
    needle = '1';
    EXPECT_FALSE(stringStartsWith(s, needle));
}

TEST(stringFunctionsTests, allEntriesAreUnique){
    // evaluates true
    std::vector<double> vec = {1.1, -0.24, 10.372, 1.111};
    EXPECT_TRUE(allEntriesAreUnique(vec));
    std::vector<std::string> vec2 = {"one", "two", "three", "four"};
    EXPECT_TRUE(allEntriesAreUnique(vec2));

    // evaluates false
    vec.push_back(-0.24);
    EXPECT_FALSE(allEntriesAreUnique(vec));
    vec2.emplace_back("three");
    EXPECT_FALSE(allEntriesAreUnique(vec2));
}

TEST(stringFunctionsTests, getIteratorToFirstNotUniqueEntry) {
    // not found
    std::vector<double> vec = {1.1, -0.24, 10.372, 1.111};
    auto it = vec.end();
    EXPECT_EQ(getIteratorToFirstNonUniqueEntry(vec), it);

    std::vector<std::string> vec2 = {"one", "two", "three"};
    auto it2 = vec2.end();
    EXPECT_EQ(getIteratorToFirstNonUniqueEntry(vec2), it2);

    // found
    vec.push_back(10.372);
    vec.push_back(7.1);
    vec.push_back(1.111);
    it = vec.begin();
    it += 4;
    EXPECT_EQ(*getIteratorToFirstNonUniqueEntry(vec), *it);

    vec2.emplace_back("two");
    vec2.emplace_back("one");
    it2 = vec2.begin();
    it += 3;
    EXPECT_EQ(*getIteratorToFirstNonUniqueEntry(vec2), *it2);
}

TEST(stringFunctionsTests, findNthInstanceInString_Char){
    std::string haystack = "one,two,three\tfour,five\t,seven,";
    char needle = ',';

    EXPECT_EQ(findNthInstanceInString(0, haystack, needle), std::string::npos);
    EXPECT_EQ(findNthInstanceInString(1, haystack, needle), 3);
    EXPECT_EQ(findNthInstanceInString(2, haystack, needle), 7);
    EXPECT_EQ(findNthInstanceInString(3, haystack, needle), 18);
    EXPECT_EQ(findNthInstanceInString(4, haystack, needle), 24);
    EXPECT_EQ(findNthInstanceInString(5, haystack, needle), 30);
    EXPECT_EQ(findNthInstanceInString(6, haystack, needle), std::string::npos);
}

TEST(stringFunctionsTests, findNthInstanceInString_String){
    std::string haystack = "one,two,three\tfour,five\t,seven,";
    std::string needle = ",";

    EXPECT_EQ(findNthInstanceInString(0, haystack, needle), std::string::npos);
    EXPECT_EQ(findNthInstanceInString(1, haystack, needle), 3);
    EXPECT_EQ(findNthInstanceInString(2, haystack, needle), 7);
    EXPECT_EQ(findNthInstanceInString(3, haystack, needle), 18);
    EXPECT_EQ(findNthInstanceInString(4, haystack, needle), 24);
    EXPECT_EQ(findNthInstanceInString(5, haystack, needle), 30);
    EXPECT_EQ(findNthInstanceInString(6, haystack, needle), std::string::npos);
}

//-------------------------------------------------
// Compare and replace
//-------------------------------------------------

TEST(stringFunctionsTests, stringReplaceChar){
    // don't replace
    std::string s = "007hay_stack123-blah";
    EXPECT_EQ(stringReplace('j', "monster", s), s);
    EXPECT_EQ(stringReplace(' ', "monster", s), s);

    // do replace
    EXPECT_EQ(stringReplace('-', "monster", s), "007hay_stack123monsterblah");
    EXPECT_EQ(stringReplace('h', "monster", s), "007monsteray_stack123-blamonster");
    EXPECT_EQ(stringReplace('0', "-1", s), "-1-17hay_stack123-blah");
}

TEST(stringFunctionsTests, stringReplaceString){
    // don't replace
    std::string s = "007hay_stack123-blah";
    EXPECT_EQ(stringReplace("j", "monster", s), s);
    EXPECT_EQ(stringReplace(" ", "monster", s), s);
    EXPECT_EQ(stringReplace("haya", "monster", s), s);

    // do replace
    EXPECT_EQ(stringReplace("-", "monster", s), "007hay_stack123monsterblah");
    EXPECT_EQ(stringReplace("h", "monster", s), "007monsteray_stack123-blamonster");
    EXPECT_EQ(stringReplace("0", "-1", s), "-1-17hay_stack123-blah");
    EXPECT_EQ(stringReplace("stack", "monster", s), "007hay_monster123-blah");
    EXPECT_EQ(stringReplace("007hay_stack123-blah", "monster", s), "monster");
}

template <typename T> class EqualAs : public ::testing::Test {
public:
    bool areEqual_maxDiff0 = equalAs<T>("1", "1", 0);
    bool areEqual_maxDiffSmall = equalAs<T>("1", "1", 0.000001);
    bool areEqual_maxDiffBig = equalAs<T>("1", "1", 100);
    bool areNotEqual_maxDiff0 = equalAs<T>("1", "2", 0);
    bool areNotEqual_maxDiffSmall = equalAs<T>("1", "2", 0.000001);
    bool areNotEqual_maxDiffExactly = equalAs<T>("1", "2", 1);
    bool areNotEqual_maxDiffBig = equalAs<T>("1", "2", 1.00001);

};

using MyTypes = ::testing::Types<double, float, int>;
TYPED_TEST_SUITE(EqualAs, MyTypes);

TYPED_TEST(EqualAs, equalAs_float_int_double) {
    EXPECT_TRUE(this->areEqual_maxDiff0);
    EXPECT_TRUE(this->areEqual_maxDiffSmall);
    EXPECT_TRUE(this->areEqual_maxDiffBig);
    EXPECT_FALSE(this->areNotEqual_maxDiff0);
    EXPECT_FALSE(this->areNotEqual_maxDiffSmall);
    EXPECT_TRUE(this->areNotEqual_maxDiffExactly);
    EXPECT_TRUE(this->areNotEqual_maxDiffBig);
}

TEST(EqualAs, equalAs_float_double) {
    // now test some things that only apply to float & double
    EXPECT_TRUE(equalAs<double>("1.7394", "1.7393", 0.001));
    EXPECT_TRUE(equalAs<float>("1.7394", "1.7393", 0.001));
    EXPECT_FALSE(equalAs<double>("1.7394", "1.7393", 0.000000001));
    EXPECT_FALSE(equalAs<float>("1.7394", "1.7393", 0.0000000001));
}


//-------------------------------------------------
// Erase, extract and trim
//-------------------------------------------------

TEST(stringFunctionsTests, eraseAllOccurencesExactly){
    // don't erase
    std::string s = "007hay_stack123-blah";
    std::string newS = s; eraseAllOccurences(newS, "j", false); EXPECT_EQ(newS, s);
    newS = s; eraseAllOccurences(newS, " ", false); EXPECT_EQ(newS, s);
    newS = s; eraseAllOccurences(newS, "haya", false); EXPECT_EQ(newS, s);

    // do replace
    newS = s; eraseAllOccurences(newS, "-", false); EXPECT_EQ(newS, "007hay_stack123blah");
    newS = s; eraseAllOccurences(newS, "h", false); EXPECT_EQ(newS, "007ay_stack123-bla");
    newS = s; eraseAllOccurences(newS, "0", false); EXPECT_EQ(newS, "7hay_stack123-blah");
    newS = s; eraseAllOccurences(newS, "stack", false); EXPECT_EQ(newS, "007hay_123-blah");
    newS = s; eraseAllOccurences(newS, "007hay_stack123-blah", false); EXPECT_EQ(newS, "");
}

TEST(stringFunctionsTests, eraseAllOccurencesAny){
    // don't erase
    std::string s = "007hay_stack123-blah";
    std::string newS = s; eraseAllOccurences(newS, "j", true); EXPECT_EQ(newS, s);
    newS = s; eraseAllOccurences(newS, " ", true); EXPECT_EQ(newS, s);

    // do replace
    newS = s; eraseAllOccurences(newS, "-", true); EXPECT_EQ(newS, "007hay_stack123blah");
    newS = s; eraseAllOccurences(newS, "h", true); EXPECT_EQ(newS, "007ay_stack123-bla");
    newS = s; eraseAllOccurences(newS, "0", true); EXPECT_EQ(newS, "7hay_stack123-blah");
    newS = s; eraseAllOccurences(newS, "stack", true); EXPECT_EQ(newS, "007hy_123-blh");
    newS = s; eraseAllOccurences(newS, "007hay_stack123-blah", true); EXPECT_EQ(newS, "");
}

TEST(stringFunctionsTests, eraseAllOccurencesChar){
    // don't erase
    std::string s = "007hay_stack123-blah";
    std::string newS = s; eraseAllOccurences(newS, 'j'); EXPECT_EQ(newS, s);
    newS = s; eraseAllOccurences(newS, ' '); EXPECT_EQ(newS, s);

    // do replace
    newS = s; eraseAllOccurences(newS, '-'); EXPECT_EQ(newS, "007hay_stack123blah");
    newS = s; eraseAllOccurences(newS, 'h'); EXPECT_EQ(newS, "007ay_stack123-bla");
    newS = s; eraseAllOccurences(newS, '0'); EXPECT_EQ(newS, "7hay_stack123-blah");
}

TEST(stringFunctionsTests, eraseAllWhiteSpaces){
    // don't erase
    std::string s = "007hay_stack123-blah";
    eraseAllWhiteSpaces(s); EXPECT_EQ(s, "007hay_stack123-blah");

    // do erase
    s = "007 hay    _st\tack\f123\n-blah\r\v";
    eraseAllWhiteSpaces(s); EXPECT_EQ(s, "007hay_stack123-blah");
}


TEST(stringFunctionsTests, eraseAllNonAlphabeticCharacters){
    // don't erase
    std::string s = "abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUVWXYZäöüÄÖÜàéèÀÉÈ";
    eraseAllNonAlphabeticCharacters(s); EXPECT_EQ(s, "abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");

    // do erase
    s = "007123_ 47/1..,1872";
    eraseAllNonAlphabeticCharacters(s); EXPECT_EQ(s, "");
    s = "007hay_stack 123-blah";
    eraseAllNonAlphabeticCharacters(s); EXPECT_EQ(s, "haystackblah");
}

//read
/////////////////// read before ///////////////////

TEST(stringFunctionsTests, readBeforeExactly){
    // don't read
    std::string s = "007hay_stack123-blah";
    std::string p = readBefore(s, "j"); EXPECT_EQ(p, s);
    p = readBefore(s, " "); EXPECT_EQ(p, s);
    p = readBefore(s, "haya"); EXPECT_EQ(p, s);

    // do read
    p = readBefore(s, "-"); EXPECT_EQ(p, "007hay_stack123");
    p = readBefore(s, "h"); EXPECT_EQ(p, "007");
    p = readBefore(s, "0"); EXPECT_EQ(p, "");
    p = readBefore(s, "stack"); EXPECT_EQ(p, "007hay_");
    p = readBefore(s, s); EXPECT_EQ(p, "");
}

TEST(stringFunctionsTests, readBeforeAny){
    // don't read
    std::string s = "007hay_stack123-blah";
    std::string p = readBefore(s, "j", true); EXPECT_EQ(p, s);
    p = readBefore(s, " ", true); EXPECT_EQ(p, s);

    // do read
    p = readBefore(s, "haya", true); EXPECT_EQ(p, "007");
    p = readBefore(s, "-", true); EXPECT_EQ(p, "007hay_stack123");
    p = readBefore(s, "h", true); EXPECT_EQ(p, "007");
    p = readBefore(s, "0", true); EXPECT_EQ(p, "");
    p = readBefore(s, "stack", true); EXPECT_EQ(p, "007h");
    p = readBefore(s, s, true); EXPECT_EQ(p, "");
}

TEST(stringFunctionsTests, readBefore){
    // don't read
    std::string s = "007hay_stack123-blah";
    std::string p = readBefore(s, 'j'); EXPECT_EQ(p, s);
    p = readBefore(s, ' '); EXPECT_EQ(p, s);

    // do read
    p = readBefore(s, '-'); EXPECT_EQ(p, "007hay_stack123");
    p = readBefore(s, 'h'); EXPECT_EQ(p, "007");
    p = readBefore(s, '0'); EXPECT_EQ(p, "");
}

/////////////////// read until ///////////////////

TEST(stringFunctionsTests, readUntilExactly){
    // don't read
    std::string s = "007hay_stack123-blah";
    std::string p = readUntil(s, "j"); EXPECT_EQ(p, s);
    p = readUntil(s, " "); EXPECT_EQ(p, s);
    p = readUntil(s, "haya"); EXPECT_EQ(p, s);

    // do read
    p = readUntil(s, "-"); EXPECT_EQ(p, "007hay_stack123-");
    p = readUntil(s, "h"); EXPECT_EQ(p, "007h");
    p = readUntil(s, "0"); EXPECT_EQ(p, "0");
    p = readUntil(s, "stack"); EXPECT_EQ(p, "007hay_stack");
    p = readUntil(s, s); EXPECT_EQ(p, s);
}

TEST(stringFunctionsTests, readUntilAny){
    // don't read
    std::string s = "007hay_stack123-blah";
    std::string p = readUntil(s, "j", true); EXPECT_EQ(p, s);
    p = readUntil(s, " ", true); EXPECT_EQ(p, s);

    // do read
    p = readUntil(s, "haya", true); EXPECT_EQ(p, "007h");
    p = readUntil(s, "-", true); EXPECT_EQ(p, "007hay_stack123-");
    p = readUntil(s, "h", true); EXPECT_EQ(p, "007h");
    p = readUntil(s, "0", true); EXPECT_EQ(p, "0");
    p = readUntil(s, "stack", true); EXPECT_EQ(p, "007ha");
    p = readUntil(s, s, true); EXPECT_EQ(p, "0");
}

TEST(stringFunctionsTests, readUntil){
    // don't read
    std::string s = "007hay_stack123-blah";
    std::string p = readUntil(s, 'j'); EXPECT_EQ(p, s);
    p = readUntil(s, ' '); EXPECT_EQ(p, s);

    // do read
    p = readUntil(s, '-'); EXPECT_EQ(p, "007hay_stack123-");
    p = readUntil(s, 'h'); EXPECT_EQ(p, "007h");
    p = readUntil(s, '0'); EXPECT_EQ(p, "0");
}

/////////////////// read before last ///////////////////

TEST(stringFunctionsTests, readBeforeLastExactly){
    // don't read
    std::string s = "007hay_stack123-blah";
    std::string p = readBeforeLast(s, "j"); EXPECT_EQ(p, s);
    p = readBeforeLast(s, " "); EXPECT_EQ(p, s);
    p = readBeforeLast(s, "haya"); EXPECT_EQ(p, s);

    // do read
    p = readBeforeLast(s, "-"); EXPECT_EQ(p, "007hay_stack123");
    p = readBeforeLast(s, "h"); EXPECT_EQ(p, "007hay_stack123-bla");
    p = readBeforeLast(s, "0"); EXPECT_EQ(p, "0");
    p = readBeforeLast(s, "stack"); EXPECT_EQ(p, "007hay_");
    p = readBeforeLast(s, s); EXPECT_EQ(p, "");
}

TEST(stringFunctionsTests, readBeforeLastAny){
    // don't read
    std::string s = "007hay_stack123-blah";
    std::string p = readBeforeLast(s, "j", true); EXPECT_EQ(p, s);
    p = readBeforeLast(s, " ", true); EXPECT_EQ(p, s);

    // do read
    p = readBeforeLast(s, "haya", true); EXPECT_EQ(p, "007hay_stack123-bla");
    p = readBeforeLast(s, "-", true); EXPECT_EQ(p, "007hay_stack123");
    p = readBeforeLast(s, "h", true); EXPECT_EQ(p, "007hay_stack123-bla");
    p = readBeforeLast(s, "0", true); EXPECT_EQ(p, "0");
    p = readBeforeLast(s, "stack", true); EXPECT_EQ(p, "007hay_stack123-bl");
    p = readBeforeLast(s, s, true); EXPECT_EQ(p, "007hay_stack123-bla");
}

TEST(stringFunctionsTests, readBeforeLastChar){
    // don't read
    std::string s = "007hay_stack123-blah";
    std::string p = readBeforeLast(s, 'j'); EXPECT_EQ(p, s);
    p = readBeforeLast(s, ' '); EXPECT_EQ(p, s);

    // do read
    p = readBeforeLast(s, '-'); EXPECT_EQ(p, "007hay_stack123");
    p = readBeforeLast(s, 'h'); EXPECT_EQ(p, "007hay_stack123-bla");
    p = readBeforeLast(s, '0'); EXPECT_EQ(p, "0");
}

/////////////////// read until last ///////////////////

TEST(stringFunctionsTests, readUntilLastExactly){
    // don't read
    std::string s = "007hay_stack123-blah";
    std::string p = readUntilLast(s, "j"); EXPECT_EQ(p, s);
    p = readUntilLast(s, " "); EXPECT_EQ(p, s);
    p = readUntilLast(s, "haya"); EXPECT_EQ(p, s);

    // do read
    p = readUntilLast(s, "-"); EXPECT_EQ(p, "007hay_stack123-");
    p = readUntilLast(s, "h"); EXPECT_EQ(p, "007hay_stack123-blah");
    p = readUntilLast(s, "0"); EXPECT_EQ(p, "00");
    p = readUntilLast(s, "stack"); EXPECT_EQ(p, "007hay_stack");
    p = readUntilLast(s, s); EXPECT_EQ(p, s);
}

TEST(stringFunctionsTests, readUntilLastAny){
    // don't read
    std::string s = "007hay_stack123-blah";
    std::string p = readUntilLast(s, "j", true); EXPECT_EQ(p, s);
    p = readUntilLast(s, " ", true); EXPECT_EQ(p, s);

    // do read
    p = readUntilLast(s, "haya", true); EXPECT_EQ(p, "007hay_stack123-blah");
    p = readUntilLast(s, "-", true); EXPECT_EQ(p, "007hay_stack123-");
    p = readUntilLast(s, "h", true); EXPECT_EQ(p, "007hay_stack123-blah");
    p = readUntilLast(s, "0", true); EXPECT_EQ(p, "00");
    p = readUntilLast(s, "stack", true); EXPECT_EQ(p, "007hay_stack123-bla");
    p = readUntilLast(s, s, true); EXPECT_EQ(p, s);
}

TEST(stringFunctionsTests, readUntilLastChar){
    // don't read
    std::string s = "007hay_stack123-blah";
    std::string p = readUntilLast(s, 'j'); EXPECT_EQ(p, s);
    p = readUntilLast(s, ' '); EXPECT_EQ(p, s);

    // do read
    p = readUntilLast(s, '-'); EXPECT_EQ(p, "007hay_stack123-");
    p = readUntilLast(s, 'h'); EXPECT_EQ(p, "007hay_stack123-blah");
    p = readUntilLast(s, '0'); EXPECT_EQ(p, "00");
}

/////////////////// read after ///////////////////

TEST(stringFunctionsTests, readAfterExactly){
    // don't read
    std::string s = "007hay_stack123-blah";
    std::string p = readAfter(s, "j"); EXPECT_EQ(p, s);
    p = readAfter(s, " "); EXPECT_EQ(p, s);
    p = readAfter(s, "haya"); EXPECT_EQ(p, s);

    // do read
    p = readAfter(s, "-"); EXPECT_EQ(p, "blah");
    p = readAfter(s, "h"); EXPECT_EQ(p, "ay_stack123-blah");
    p = readAfter(s, "0"); EXPECT_EQ(p, "07hay_stack123-blah");
    p = readAfter(s, "stack"); EXPECT_EQ(p, "123-blah");
    p = readAfter(s, s); EXPECT_EQ(p, "");
}

TEST(stringFunctionsTests, readAfterAny){
    // don't read
    std::string s = "007hay_stack123-blah";
    std::string p = readAfter(s, "j", true); EXPECT_EQ(p, s);
    p = readAfter(s, " ", true); EXPECT_EQ(p, s);

    // do read
    p = readAfter(s, "haya", true); EXPECT_EQ(p, "ay_stack123-blah");
    p = readAfter(s, "-", true); EXPECT_EQ(p, "blah");
    p = readAfter(s, "h", true); EXPECT_EQ(p, "ay_stack123-blah");
    p = readAfter(s, "0", true); EXPECT_EQ(p, "07hay_stack123-blah");
    p = readAfter(s, "stack", true); EXPECT_EQ(p, "y_stack123-blah");
    p = readAfter(s, s, true); EXPECT_EQ(p, "07hay_stack123-blah");
}

TEST(stringFunctionsTests, readAfter){
    // don't read
    std::string s = "007hay_stack123-blah";
    std::string p = readAfter(s, 'j'); EXPECT_EQ(p, s);
    p = readAfter(s, ' '); EXPECT_EQ(p, s);

    // do read
    p = readAfter(s, '-'); EXPECT_EQ(p, "blah");
    p = readAfter(s, 'h'); EXPECT_EQ(p, "ay_stack123-blah");
    p = readAfter(s, '0'); EXPECT_EQ(p, "07hay_stack123-blah");
}

/////////////////// read after last ///////////////////

TEST(stringFunctionsTests, readAfterLastExactly){
    // don't read
    std::string s = "007hay_stack123-blah";
    std::string p = readAfterLast(s, "j"); EXPECT_EQ(p, s);
    p = readAfterLast(s, " "); EXPECT_EQ(p, s);
    p = readAfterLast(s, "haya"); EXPECT_EQ(p, s);

    // do read
    p = readAfterLast(s, "-"); EXPECT_EQ(p, "blah");
    p = readAfterLast(s, "h"); EXPECT_EQ(p, "");
    p = readAfterLast(s, "0"); EXPECT_EQ(p, "7hay_stack123-blah");
    p = readAfterLast(s, "stack"); EXPECT_EQ(p, "123-blah");
    p = readAfterLast(s, s); EXPECT_EQ(p, "");
}

TEST(stringFunctionsTests, readAfterLastAny){
    // don't read
    std::string s = "007hay_stack123-blah";
    std::string p = readAfterLast(s, "j", true); EXPECT_EQ(p, s);
    p = readAfterLast(s, " ", true); EXPECT_EQ(p, s);

    // do read
    p = readAfterLast(s, "haya", true); EXPECT_EQ(p, "");
    p = readAfterLast(s, "-", true); EXPECT_EQ(p, "blah");
    p = readAfterLast(s, "h", true); EXPECT_EQ(p, "");
    p = readAfterLast(s, "0", true); EXPECT_EQ(p, "7hay_stack123-blah");
    p = readAfterLast(s, "stack", true); EXPECT_EQ(p, "h");
    p = readAfterLast(s, s, true); EXPECT_EQ(p, "");
}

TEST(stringFunctionsTests, readAfterLast){
    // don't read
    std::string s = "007hay_stack123-blah";
    std::string p = readAfterLast(s, 'j'); EXPECT_EQ(p, s);
    p = readAfterLast(s, ' '); EXPECT_EQ(p, s);

    // do read
    p = readAfterLast(s, '-'); EXPECT_EQ(p, "blah");
    p = readAfterLast(s, 'h'); EXPECT_EQ(p, "");
    p = readAfterLast(s, '0'); EXPECT_EQ(p, "7hay_stack123-blah");
}

//manipulations
/////////////////// extract before ///////////////////

TEST(stringFunctionsTests, extractBeforeExactly){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractBefore(newS, "j", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractBefore(newS, " ", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractBefore(newS, "haya", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    newS = s; p = extractBefore(newS, "-", false); EXPECT_EQ(p, "007hay_stack123"); EXPECT_EQ(newS, "-blah");
    newS = s; p = extractBefore(newS, "h", false); EXPECT_EQ(p, "007"); EXPECT_EQ(newS, "hay_stack123-blah");
    newS = s; p = extractBefore(newS, "0", false); EXPECT_EQ(p, ""); EXPECT_EQ(newS, "007hay_stack123-blah");
    newS = s; p = extractBefore(newS, "stack", false); EXPECT_EQ(p, "007hay_"); EXPECT_EQ(newS, "stack123-blah");
    newS = s; p = extractBefore(newS, "007hay_stack123-blah", false); EXPECT_EQ(p, ""); EXPECT_EQ(newS, s);
}

TEST(stringFunctionsTests, extractBeforeAny){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractBefore(newS, "j", true); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractBefore(newS, " ", true); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    newS = s; p = extractBefore(newS, "haya", true); EXPECT_EQ(p, "007"); EXPECT_EQ(newS, "hay_stack123-blah");
    newS = s; p = extractBefore(newS, "-", true); EXPECT_EQ(p, "007hay_stack123"); EXPECT_EQ(newS, "-blah");
    newS = s; p = extractBefore(newS, "h", true); EXPECT_EQ(p, "007"); EXPECT_EQ(newS, "hay_stack123-blah");
    newS = s; p = extractBefore(newS, "0", true); EXPECT_EQ(p, ""); EXPECT_EQ(newS, "007hay_stack123-blah");
    newS = s; p = extractBefore(newS, "stack", true); EXPECT_EQ(p, "007h"); EXPECT_EQ(newS, "ay_stack123-blah");
    newS = s; p = extractBefore(newS, "007hay_stack123-blah", true); EXPECT_EQ(p, ""); EXPECT_EQ(newS, s);
}

TEST(stringFunctionsTests, extractBeforeChar){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractBefore(newS, 'j'); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractBefore(newS, ' '); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    newS = s; p = extractBefore(newS, '-'); EXPECT_EQ(p, "007hay_stack123"); EXPECT_EQ(newS, "-blah");
    newS = s; p = extractBefore(newS, 'h'); EXPECT_EQ(p, "007"); EXPECT_EQ(newS, "hay_stack123-blah");
    newS = s; p = extractBefore(newS, '0'); EXPECT_EQ(p, ""); EXPECT_EQ(newS, "007hay_stack123-blah");
}

TEST(stringFunctionsTests, extractBeforeDoubleSlash){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractBeforeDoubleSlash(newS); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    s = "007hay_stac//k123-blah";
    newS = s; p = extractBeforeDoubleSlash(newS); EXPECT_EQ(p, "007hay_stac"); EXPECT_EQ(newS, "//k123-blah");
    s = "//007hay_stack123-blah";
    newS = s; p = extractBeforeDoubleSlash(newS); EXPECT_EQ(p, ""); EXPECT_EQ(newS, "//007hay_stack123-blah");
    s = "007hay_stack123-blah//";
    newS = s; p = extractBeforeDoubleSlash(newS); EXPECT_EQ(p, "007hay_stack123-blah"); EXPECT_EQ(newS, "//");
}

TEST(stringFunctionsTests, extractBeforeWhiteSpace){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractBeforeWhiteSpace(newS); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    s = "007hay_stac k123-blah";
    newS = s; p = extractBeforeWhiteSpace(newS); EXPECT_EQ(p, "007hay_stac"); EXPECT_EQ(newS, " k123-blah");
    s = " 007hay_stack123- blah";
    newS = s; p = extractBeforeWhiteSpace(newS); EXPECT_EQ(p, ""); EXPECT_EQ(newS, " 007hay_stack123- blah");
    s = "007hay_stack123-blah   ";
    newS = s; p = extractBeforeWhiteSpace(newS); EXPECT_EQ(p, "007hay_stack123-blah"); EXPECT_EQ(newS, "   ");
}

/////////////////// extract until ///////////////////


TEST(stringFunctionsTests, extractUntilExactly){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractUntil(newS, "j", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractUntil(newS, " ", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractUntil(newS, "haya", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    newS = s; p = extractUntil(newS, "-", false); EXPECT_EQ(p, "007hay_stack123-"); EXPECT_EQ(newS, "blah");
    newS = s; p = extractUntil(newS, "h", false); EXPECT_EQ(p, "007h"); EXPECT_EQ(newS, "ay_stack123-blah");
    newS = s; p = extractUntil(newS, "0", false); EXPECT_EQ(p, "0"); EXPECT_EQ(newS, "07hay_stack123-blah");
    newS = s; p = extractUntil(newS, "stack", false); EXPECT_EQ(p, "007hay_stack"); EXPECT_EQ(newS, "123-blah");
    newS = s; p = extractUntil(newS, "007hay_stack123-blah", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
}

TEST(stringFunctionsTests, extractUntilAny){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractUntil(newS, "j", true); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractUntil(newS, " ", true); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    newS = s; p = extractUntil(newS, "haya", true); EXPECT_EQ(p, "007h"); EXPECT_EQ(newS, "ay_stack123-blah");
    newS = s; p = extractUntil(newS, "-", true); EXPECT_EQ(p, "007hay_stack123-"); EXPECT_EQ(newS, "blah");
    newS = s; p = extractUntil(newS, "h", true); EXPECT_EQ(p, "007h"); EXPECT_EQ(newS, "ay_stack123-blah");
    newS = s; p = extractUntil(newS, "0", true); EXPECT_EQ(p, "0"); EXPECT_EQ(newS, "07hay_stack123-blah");
    newS = s; p = extractUntil(newS, "stack", true); EXPECT_EQ(p, "007ha"); EXPECT_EQ(newS, "y_stack123-blah");
    newS = s; p = extractUntil(newS, "007hay_stack123-blah", true); EXPECT_EQ(p, "0"); EXPECT_EQ(newS, "07hay_stack123-blah");
}

TEST(stringFunctionsTests, extractUntilChar){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractUntil(newS, 'j'); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractUntil(newS, ' '); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    newS = s; p = extractUntil(newS, '-'); EXPECT_EQ(p, "007hay_stack123-"); EXPECT_EQ(newS, "blah");
    newS = s; p = extractUntil(newS, 'h'); EXPECT_EQ(p, "007h"); EXPECT_EQ(newS, "ay_stack123-blah");
    newS = s; p = extractUntil(newS, '0'); EXPECT_EQ(p, "0"); EXPECT_EQ(newS, "07hay_stack123-blah");
}
/////////////////// extract before last ///////////////////

TEST(stringFunctionsTests, extractBeforeLastExactly){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractBeforeLast(newS, "j", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractBeforeLast(newS, " ", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractBeforeLast(newS, "haya", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    newS = s; p = extractBeforeLast(newS, "-", false); EXPECT_EQ(p, "007hay_stack123"); EXPECT_EQ(newS, "-blah");
    newS = s; p = extractBeforeLast(newS, "h", false); EXPECT_EQ(p, "007hay_stack123-bla"); EXPECT_EQ(newS, "h");
    newS = s; p = extractBeforeLast(newS, "0", false); EXPECT_EQ(p, "0"); EXPECT_EQ(newS, "07hay_stack123-blah");
    newS = s; p = extractBeforeLast(newS, "stack", false); EXPECT_EQ(p, "007hay_"); EXPECT_EQ(newS, "stack123-blah");
    newS = s; p = extractBeforeLast(newS, "007hay_stack123-blah", false); EXPECT_EQ(p, ""); EXPECT_EQ(newS, s);
}

TEST(stringFunctionsTests, extractBeforeLastAny){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractBeforeLast(newS, "j", true); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractBeforeLast(newS, " ", true); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    newS = s; p = extractBeforeLast(newS, "haya", true); EXPECT_EQ(p, "007hay_stack123-bla"); EXPECT_EQ(newS, "h");
    newS = s; p = extractBeforeLast(newS, "-", true); EXPECT_EQ(p, "007hay_stack123"); EXPECT_EQ(newS, "-blah");
    newS = s; p = extractBeforeLast(newS, "h", true); EXPECT_EQ(p, "007hay_stack123-bla"); EXPECT_EQ(newS, "h");
    newS = s; p = extractBeforeLast(newS, "0", true); EXPECT_EQ(p, "0"); EXPECT_EQ(newS, "07hay_stack123-blah");
    newS = s; p = extractBeforeLast(newS, "stack", true); EXPECT_EQ(p, "007hay_stack123-bl"); EXPECT_EQ(newS, "ah");
    newS = s; p = extractBeforeLast(newS, "007hay_stack123-blah", true); EXPECT_EQ(p, "007hay_stack123-bla"); EXPECT_EQ(newS, "h");
}

TEST(stringFunctionsTests, extractBeforeLastChar){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractBeforeLast(newS, 'j'); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractBeforeLast(newS, ' '); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    newS = s; p = extractBeforeLast(newS, '-'); EXPECT_EQ(p, "007hay_stack123"); EXPECT_EQ(newS, "-blah");
    newS = s; p = extractBeforeLast(newS, 'h'); EXPECT_EQ(p, "007hay_stack123-bla"); EXPECT_EQ(newS, "h");
    newS = s; p = extractBeforeLast(newS, '0'); EXPECT_EQ(p, "0"); EXPECT_EQ(newS, "07hay_stack123-blah");
}

/////////////////// extract until last ///////////////////

TEST(stringFunctionsTests, extractUntilLastExactly){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractUntilLast(newS, "j", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractUntilLast(newS, " ", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractUntilLast(newS, "haya", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    newS = s; p = extractUntilLast(newS, "-", false); EXPECT_EQ(p, "007hay_stack123-"); EXPECT_EQ(newS, "blah");
    newS = s; p = extractUntilLast(newS, "h", false); EXPECT_EQ(p, "007hay_stack123-blah"); EXPECT_EQ(newS, "");
    newS = s; p = extractUntilLast(newS, "0", false); EXPECT_EQ(p, "00"); EXPECT_EQ(newS, "7hay_stack123-blah");
    newS = s; p = extractUntilLast(newS, "stack", false); EXPECT_EQ(p, "007hay_stack"); EXPECT_EQ(newS, "123-blah");
    newS = s; p = extractUntilLast(newS, "007hay_stack123-blah", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
}

TEST(stringFunctionsTests, extractUntilLastAny){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractUntilLast(newS, "j", true); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractUntilLast(newS, " ", true); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    newS = s; p = extractUntilLast(newS, "haya", true); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractUntilLast(newS, "-", true); EXPECT_EQ(p, "007hay_stack123-"); EXPECT_EQ(newS, "blah");
    newS = s; p = extractUntilLast(newS, "h", true); EXPECT_EQ(p, "007hay_stack123-blah"); EXPECT_EQ(newS, "");
    newS = s; p = extractUntilLast(newS, "0", true); EXPECT_EQ(p, "00"); EXPECT_EQ(newS, "7hay_stack123-blah");
    newS = s; p = extractUntilLast(newS, "stack", true); EXPECT_EQ(p, "007hay_stack123-bla"); EXPECT_EQ(newS, "h");
    newS = s; p = extractUntilLast(newS, "007hay_stack123-blah", true); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
}

TEST(stringFunctionsTests, extractUntilLastChar){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractUntilLast(newS, 'j'); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractUntilLast(newS, ' '); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    newS = s; p = extractUntilLast(newS, '-'); EXPECT_EQ(p, "007hay_stack123-"); EXPECT_EQ(newS, "blah");
    newS = s; p = extractUntilLast(newS, 'h'); EXPECT_EQ(p, "007hay_stack123-blah"); EXPECT_EQ(newS, "");
    newS = s; p = extractUntilLast(newS, '0'); EXPECT_EQ(p, "00"); EXPECT_EQ(newS, "7hay_stack123-blah");
}

/////////////////// extract after ///////////////////

TEST(stringFunctionsTests, extractAfterExactly){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractAfter(newS, "j", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractAfter(newS, " ", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractAfter(newS, "haya", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    newS = s; p = extractAfter(newS, "-", false); EXPECT_EQ(p, "blah"); EXPECT_EQ(newS, "007hay_stack123-");
    newS = s; p = extractAfter(newS, "h", false); EXPECT_EQ(p, "ay_stack123-blah" ); EXPECT_EQ(newS, "007h");
    newS = s; p = extractAfter(newS, "0", false); EXPECT_EQ(p, "07hay_stack123-blah"); EXPECT_EQ(newS, "0");
    newS = s; p = extractAfter(newS, "stack", false); EXPECT_EQ(p, "123-blah"); EXPECT_EQ(newS, "007hay_stack");
    newS = s; p = extractAfter(newS, "007hay_stack123-blah", false); EXPECT_EQ(p, ""); EXPECT_EQ(newS, s);
}

TEST(stringFunctionsTests, extractAfterAny){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractAfter(newS, "j", true); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractAfter(newS, " ", true); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    newS = s; p = extractAfter(newS, "haya", true); EXPECT_EQ(p, "ay_stack123-blah"); EXPECT_EQ(newS, "007h");
    newS = s; p = extractAfter(newS, "-", true); EXPECT_EQ(p, "blah"); EXPECT_EQ(newS, "007hay_stack123-");
    newS = s; p = extractAfter(newS, "h", true); EXPECT_EQ(p, "ay_stack123-blah" ); EXPECT_EQ(newS, "007h");
    newS = s; p = extractAfter(newS, "0", true); EXPECT_EQ(p, "07hay_stack123-blah"); EXPECT_EQ(newS, "0");
    newS = s; p = extractAfter(newS, "stack", true); EXPECT_EQ(p, "y_stack123-blah"); EXPECT_EQ(newS, "007ha");
    newS = s; p = extractAfter(newS, "007hay_stack123-blah", true); EXPECT_EQ(p, "07hay_stack123-blah"); EXPECT_EQ(newS, "0");
}

TEST(stringFunctionsTests, extractAfterChar){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractAfter(newS, 'j'); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractAfter(newS, ' '); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    newS = s; p = extractAfter(newS, '-'); EXPECT_EQ(p, "blah"); EXPECT_EQ(newS, "007hay_stack123-");
    newS = s; p = extractAfter(newS, 'h'); EXPECT_EQ(p, "ay_stack123-blah" ); EXPECT_EQ(newS, "007h");
    newS = s; p = extractAfter(newS, '0'); EXPECT_EQ(p, "07hay_stack123-blah"); EXPECT_EQ(newS, "0");
}

/////////////////// extract after last ///////////////////

TEST(stringFunctionsTests, extractAfterLastExactly){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractAfterLast(newS, "j", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractAfterLast(newS, " ", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractAfterLast(newS, "haya", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    newS = s; p = extractAfterLast(newS, "-", false); EXPECT_EQ(p, "blah"); EXPECT_EQ(newS, "007hay_stack123-");
    newS = s; p = extractAfterLast(newS, "h", false); EXPECT_EQ(p, "" ); EXPECT_EQ(newS, "007hay_stack123-blah");
    newS = s; p = extractAfterLast(newS, "0", false); EXPECT_EQ(p, "7hay_stack123-blah"); EXPECT_EQ(newS, "00");
    newS = s; p = extractAfterLast(newS, "stack", false); EXPECT_EQ(p, "123-blah"); EXPECT_EQ(newS, "007hay_stack");
    newS = s; p = extractAfterLast(newS, "007hay_stack123-blah", false); EXPECT_EQ(p, ""); EXPECT_EQ(newS, s);
}

TEST(stringFunctionsTests, extractAfterLastAny){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractAfterLast(newS, "j", true); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractAfterLast(newS, " ", true); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    newS = s; p = extractAfterLast(newS, "haya", true); EXPECT_EQ(p, ""); EXPECT_EQ(newS, s);
    newS = s; p = extractAfterLast(newS, "-", true); EXPECT_EQ(p, "blah"); EXPECT_EQ(newS, "007hay_stack123-");
    newS = s; p = extractAfterLast(newS, "h", true); EXPECT_EQ(p, "" ); EXPECT_EQ(newS, "007hay_stack123-blah");
    newS = s; p = extractAfterLast(newS, "0", true); EXPECT_EQ(p, "7hay_stack123-blah"); EXPECT_EQ(newS, "00");
    newS = s; p = extractAfterLast(newS, "stack", true); EXPECT_EQ(p, "h"); EXPECT_EQ(newS, "007hay_stack123-bla");
    newS = s; p = extractAfterLast(newS, "007hay_stack123-blah", true); EXPECT_EQ(p, ""); EXPECT_EQ(newS, s);
}

TEST(stringFunctionsTests, extractAfterLastChar){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractAfterLast(newS, 'j'); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = extractAfterLast(newS, ' '); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    newS = s; p = extractAfterLast(newS, '-'); EXPECT_EQ(p, "blah"); EXPECT_EQ(newS, "007hay_stack123-");
    newS = s; p = extractAfterLast(newS, 'h'); EXPECT_EQ(p, "" ); EXPECT_EQ(newS, "007hay_stack123-blah");
    newS = s; p = extractAfterLast(newS, '0'); EXPECT_EQ(p, "7hay_stack123-blah"); EXPECT_EQ(newS, "00");
}

TEST(stringFunctionsTests, extractPath){
    // don't extract
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = extractPath(newS); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do extract
    s = "/007hay_stack123-blah";
    newS = s; p = extractPath(newS); EXPECT_EQ(p, "/"); EXPECT_EQ(newS, "007hay_stack123-blah");
    s = "007hay_sta/ck123-blah";
    newS = s; p = extractPath(newS); EXPECT_EQ(p, "007hay_sta/"); EXPECT_EQ(newS, "ck123-blah");
    s = "007hay_stack123-blah/";
    newS = s; p = extractPath(newS); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    s = "/007/hay_stack/123-bla/h";
    newS = s; p = extractPath(newS); EXPECT_EQ(p, "/007/hay_stack/123-bla/"); EXPECT_EQ(newS, "h");
}

/////////////////// trim ///////////////////

TEST(stringFunctionsTests, trimString){
    // don't trim
    std::string s = "007hay_stack123-blah";
    std::string newS = s; trimString(newS, "j"); EXPECT_EQ(newS, s);
    newS = s; trimString(newS, " "); EXPECT_EQ(newS, s);
    newS = s; trimString(newS, "-"); EXPECT_EQ(newS, s); // in the middle
    newS = s; trimString(newS, "stack"); EXPECT_EQ(newS, s); // in the middle

    // do trim
    newS = s; trimString(newS, "h"); EXPECT_EQ(newS, "007hay_stack123-bla"); // trim off end
    newS = s; trimString(newS, "0"); EXPECT_EQ(newS, "7hay_stack123-blah"); // trim off beginning
    newS = s; trimString(newS, "007hay_stack123-blah"); EXPECT_EQ(newS, ""); // trim everything
    s = "007hay_stack123-blah00";
    newS = s; trimString(newS, "0"); EXPECT_EQ(newS, "7hay_stack123-blah"); // trim off end and beginning
    s = "007hay_stack123-blah0k0";
    newS = s; trimString(newS, "0"); EXPECT_EQ(newS, "7hay_stack123-blah0k"); // trim off end and beginning
    newS = s; trimString(newS, "fd0dhw"); EXPECT_EQ(newS, "7hay_stack123-blah0k"); // trim off end and beginning
}


TEST(stringFunctionsTests, trimStringWhiteSpace){
    // don't trim
    std::string s = "007hay_stack123-blah";
    std::string newS = s; trimString(newS); EXPECT_EQ(newS, s);

    // do trim
    s = "\n007 hay    _st\tack\f123\n-blah\r\v";
    newS = s; trimString(newS); EXPECT_EQ(newS, "007 hay    _st\tack\f123\n-blah");
}

TEST(stringFunctionsTests, trimStringEndlines){
    // don't trim
    std::string s = "007hay_stack123-blah";
    std::string newS = s; trimEndlineString(newS); EXPECT_EQ(newS, s);
    s = " 007 hay    _st\tack\f123\n-blah\r \t";
    newS = s; trimEndlineString(newS); EXPECT_EQ(newS, s);

    // do trim
    s = "\n007 hay    _st\tack\f123\n-blah\r\v";
    newS = s; trimEndlineString(newS); EXPECT_EQ(newS, "007 hay    _st\tack\f123\n-blah");
}

/////////////////// split ///////////////////

TEST(stringFunctionsTests, splitAny){
    // don't split
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = split(newS, "j", true); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = split(newS, " "); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do split
    newS = s; p = split(newS, "haya", true); EXPECT_EQ(p, "007"); EXPECT_EQ(newS, "ay_stack123-blah");
    newS = s; p = split(newS, "-", true); EXPECT_EQ(p, "007hay_stack123"); EXPECT_EQ(newS, "blah");
    newS = s; p = split(newS, "h", true); EXPECT_EQ(p, "007"); EXPECT_EQ(newS, "ay_stack123-blah");
    newS = s; p = split(newS, "0", true); EXPECT_EQ(p, ""); EXPECT_EQ(newS, "07hay_stack123-blah");
    newS = s; p = split(newS, "stack", true); EXPECT_EQ(p, "007h"); EXPECT_EQ(newS, "y_stack123-blah");
    newS = s; p = split(newS, "007hay_stack123-blah", true); EXPECT_EQ(p, ""); EXPECT_EQ(newS, "07hay_stack123-blah");
}

TEST(stringFunctionsTests, splitExactly){
    // don't split
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = split(newS, "j", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = split(newS, " ", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = split(newS, "haya", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do split
    newS = s; p = split(newS, "-", false); EXPECT_EQ(p, "007hay_stack123"); EXPECT_EQ(newS, "blah");
    newS = s; p = split(newS, "h", false); EXPECT_EQ(p, "007"); EXPECT_EQ(newS, "ay_stack123-blah");
    newS = s; p = split(newS, "0", false); EXPECT_EQ(p, ""); EXPECT_EQ(newS, "07hay_stack123-blah");
    newS = s; p = split(newS, "stack", false); EXPECT_EQ(p, "007hay_"); EXPECT_EQ(newS, "123-blah");
    newS = s; p = split(newS, "007hay_stack123-blah", false); EXPECT_EQ(p, ""); EXPECT_EQ(newS, "");
}

TEST(stringFunctionsTests, splitChar){
    // don't split
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = split(newS, 'j'); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = split(newS, ' '); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do split
    newS = s; p = split(newS, '-'); EXPECT_EQ(p, "007hay_stack123"); EXPECT_EQ(newS, "blah");
    newS = s; p = split(newS, 'h'); EXPECT_EQ(p, "007"); EXPECT_EQ(newS, "ay_stack123-blah");
    newS = s; p = split(newS, '0'); EXPECT_EQ(p, ""); EXPECT_EQ(newS, "07hay_stack123-blah");
}

/////////////////// split last ///////////////////

TEST(stringFunctionsTests, splitLastAny){
    // don't split
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = splitAtLast(newS, "j", true); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = splitAtLast(newS, " "); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do split
    newS = s; p = splitAtLast(newS, "haya", true); EXPECT_EQ(p, "007hay_stack123-bla"); EXPECT_EQ(newS, "");
    newS = s; p = splitAtLast(newS, "-", true); EXPECT_EQ(p, "007hay_stack123"); EXPECT_EQ(newS, "blah");
    newS = s; p = splitAtLast(newS, "h", true); EXPECT_EQ(p, "007hay_stack123-bla"); EXPECT_EQ(newS, "");
    newS = s; p = splitAtLast(newS, "0", true); EXPECT_EQ(p, "0"); EXPECT_EQ(newS, "7hay_stack123-blah");
    newS = s; p = splitAtLast(newS, "stack", true); EXPECT_EQ(p, "007hay_stack123-bl"); EXPECT_EQ(newS, "h");
    newS = s; p = splitAtLast(newS, "007hay_stack123-blah", true); EXPECT_EQ(p, "007hay_stack123-bla"); EXPECT_EQ(newS, "");
}

TEST(stringFunctionsTests, splitLastExactly){
    // don't split
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = splitAtLast(newS, "j", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = splitAtLast(newS, " ", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = splitAtLast(newS, "haya", false); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do split
    newS = s; p = splitAtLast(newS, "-", false); EXPECT_EQ(p, "007hay_stack123"); EXPECT_EQ(newS, "blah");
    newS = s; p = splitAtLast(newS, "h", false); EXPECT_EQ(p, "007hay_stack123-bla"); EXPECT_EQ(newS, "");
    newS = s; p = splitAtLast(newS, "0", false); EXPECT_EQ(p, "0"); EXPECT_EQ(newS, "7hay_stack123-blah");
    newS = s; p = splitAtLast(newS, "stack", false); EXPECT_EQ(p, "007hay_"); EXPECT_EQ(newS, "123-blah");
    newS = s; p = splitAtLast(newS, "007hay_stack123-blah", false); EXPECT_EQ(p, ""); EXPECT_EQ(newS, "");
}

TEST(stringFunctionsTests, splitLastChar){
    // don't split
    std::string s = "007hay_stack123-blah";
    std::string newS = s; std::string p = splitAtLast(newS, 'j'); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    newS = s; p = splitAtLast(newS, ' '); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do split
    newS = s; p = splitAtLast(newS, '-'); EXPECT_EQ(p, "007hay_stack123"); EXPECT_EQ(newS, "blah");
    newS = s; p = splitAtLast(newS, 'h'); EXPECT_EQ(p, "007hay_stack123-bla"); EXPECT_EQ(newS, "");
    newS = s; p = splitAtLast(newS, '0'); EXPECT_EQ(p, "0"); EXPECT_EQ(newS, "7hay_stack123-blah");
}

TEST(stringFunctionsTests, splitAtPos){
    // don't split
    std::string s = "007hay_stack123-blah";
    std::string::size_type l = s.find_first_of('j');

    std::string newS = s; std::string p = splitAtPos(newS, l); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");
    l = s.find_first_of(' ');
    newS = s; p = splitAtPos(newS, l); EXPECT_EQ(p, s); EXPECT_EQ(newS, "");

    // do split
    l = s.find_first_of('h');
    newS = s; p = splitAtPos(newS, l); EXPECT_EQ(p, "007"); EXPECT_EQ(newS, "ay_stack123-blah");
    l = s.find_first_of('-');
    newS = s; p = splitAtPos(newS, l); EXPECT_EQ(p, "007hay_stack123"); EXPECT_EQ(newS, "blah");
    l = s.find_first_of('0');
    newS = s; p = splitAtPos(newS, l); EXPECT_EQ(p, ""); EXPECT_EQ(newS, "07hay_stack123-blah");
}


//-------------------------------------------------
// Concatenation
//-------------------------------------------------

TEST(stringFunctionsTests, concatenateString){
    std::vector<int> vec;
    EXPECT_EQ(concatenateString(vec, ", "), "");


    vec = {0, 72, 364, -18, 894};
    EXPECT_EQ(concatenateString(vec, ", "), "0, 72, 364, -18, 894");
    EXPECT_EQ(concatenateString(vec, "."), "0.72.364.-18.894");
    EXPECT_EQ(concatenateString(vec), "072364-18894");

    std::string s;
    fillConcatenatedString(vec, s);
    EXPECT_EQ(s, "072364-18894");

    s.clear();
    fillConcatenatedString(vec, s, ", ");
    EXPECT_EQ(s, "0, 72, 364, -18, 894");
}

TEST(stringFunctionsTests, concatenateStringFrom){
    std::vector<int> vec;
    EXPECT_EQ(concatenateString(vec, ", ", 100), "");

    vec = {0, 72, 364, -18, 894};
    EXPECT_EQ(concatenateString(vec, ", ", 0), "0, 72, 364, -18, 894");
    EXPECT_EQ(concatenateString(vec, ", ", 2), "364, -18, 894");
    EXPECT_EQ(concatenateString(vec, ", ", 4), "894");
    EXPECT_EQ(concatenateString(vec, ", ", 5), "");
    EXPECT_EQ(concatenateString(vec, ", ", 100), "");
    EXPECT_EQ(concatenateString(vec, 2), "364-18894");

    std::string s;
    fillConcatenatedString(vec, s, 2);
    EXPECT_EQ(s, "364-18894");

    s.clear();
    fillConcatenatedString(vec, s, ", ", 2);
    EXPECT_EQ(s, "364, -18, 894");
}

TEST(stringFunctionsTests, concatenateStringArray){
    const int len = 5;
    double array[len] = {0., 72.1, 364., -18., 894.};

    EXPECT_EQ(concatenateString(array, 5, ", "), "0.000000, 72.100000, 364.000000, -18.000000, 894.000000");
}

TEST(stringFunctionsTests, paste_equalLength){
    std::vector<int> vec1 = {1,2,3,4,5};
    std::vector<std::string> vec2 = {"A", "B", "C", "D", "E"};

    std::vector<std::string> result = paste(vec1, vec2, "_");

    EXPECT_THAT(result, ElementsAre("1_A", "2_B", "3_C", "4_D", "5_E"));
}

TEST(stringFunctionsTests, paste_firstIsLonger){
    std::vector<int> vec1 = {1,2,3,4,5,6,7,8,9,10};
    std::vector<std::string> vec2 = {"A", "B", "C", "D", "E"};

    std::vector<std::string> result = paste(vec1, vec2, "_");

    EXPECT_THAT(result, ElementsAre("1_A",  "2_B",  "3_C",  "4_D",  "5_E",  "6_A",  "7_B",  "8_C",  "9_D",  "10_E"));
}

TEST(stringFunctionsTests, paste_secondIsLonger){
    std::vector<int> vec1 = {1,2,3,4,5};
    std::vector<std::string> vec2 = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};

    std::vector<std::string> result = paste(vec1, vec2, "_");

    EXPECT_THAT(result, ElementsAre("1_A", "2_B", "3_C", "4_D", "5_E", "1_F", "2_G", "3_H", "4_I", "5_J"));
}

TEST(stringFunctionsTests, paste_throw_notMultiple){
    std::vector<int> vec1 = {1,2,3,4,5};
    std::vector<std::string> vec2 = {"A", "B", "C", "D", "E", "F"}; // 1 element longer

    EXPECT_THROW(paste(vec1, vec2, "_"), std::runtime_error);
}

//-------------------------------------------------
// Split into vector/set/array etc.
//-------------------------------------------------

// will test only with vectors, but behaviour is the same for std::set etc.

TEST(stringFunctionsTests, fillContainerFromString_Char){
    // empty string
    std::string s;
    std::vector<int> vec;
    fillContainerFromString(s, vec, ' ');
    EXPECT_TRUE(vec.empty());

    // vec is cleared
    vec = {1,2,3};
    fillContainerFromString(s, vec, ' ');
    EXPECT_TRUE(vec.empty());

    // correct string
    s = "0 72 364 -18 894";
    fillContainerFromString(s, vec, ' ');
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

    // string with extra whitespaces
    s = "   0 72 364 \n -18 894    ";
    fillContainerFromString(s, vec, ' ');
    EXPECT_THAT(vec, ElementsAre(0, 0, 0, 0, 72, 364, 0, -18, 894, 0, 0, 0, 0)); // if you want correct result, then skipEmpty!

    // weird string
    s = "0 72 foo! -18 894";
    EXPECT_NO_THROW(fillContainerFromString(s, vec, ' ')); // if you want throw, then check!

    // weird string with extra whitespaces
    s = "  0 72 \t  foo! -18  \n 894\n";
    EXPECT_NO_THROW(fillContainerFromString(s, vec, ' ')); // if you want throw, then check!
}

TEST(stringFunctionsTests, fillContainerFromStringSkipEmpty_Char){
    // empty string
    std::string s;
    std::vector<int> vec;
    fillContainerFromString(s, vec, ' ', true);
    EXPECT_TRUE(vec.empty());

    // vec is cleared
    vec = {1,2,3};
    fillContainerFromString(s, vec, ' ', true);
    EXPECT_TRUE(vec.empty());

    // correct string
    s = "0 72 364 -18 894";
    fillContainerFromString(s, vec, ' ', true);
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

    // string with extra whitespaces
    s = "   0 72 364 \n -18 894    ";
    fillContainerFromString(s, vec, ' ', true);
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

    // weird string
    s = "0 72 foo! -18 894";
    EXPECT_NO_THROW(fillContainerFromString(s, vec, ' ', true)); // if you want throw, then check!

    // weird string with extra whitespaces
    s = "  0 72 \t  foo! -18  \n 894\n";
    EXPECT_NO_THROW(fillContainerFromString(s, vec, ' ', true)); // if you want throw, then check!
}

TEST(stringFunctionsTests, fillContainerFromStringSkipEmptyCheck_Char){
    // empty string
    std::string s;
    std::vector<int> vec;
    fillContainerFromString(s, vec, ' ', true, true);
    EXPECT_TRUE(vec.empty());

    // vec is cleared
    vec = {1,2,3};
    fillContainerFromString(s, vec, ' ', true, true);
    EXPECT_TRUE(vec.empty());

    // correct string
    s = "0 72 364 -18 894";
    fillContainerFromString(s, vec, ' ', true, true);
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

    // string with extra whitespaces
    s = "   0 72 364 \n -18 894    ";
    fillContainerFromString(s, vec, ' ', true, true);
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

    // weird string
    s = "0 72 foo! -18 894";
    EXPECT_THROW({try {fillContainerFromString(s, vec, ' ', true, true);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);

    // weird string with extra whitespaces
    s = "  0 72 \t  foo! -18  \n 894\n";
    EXPECT_THROW({try {fillContainerFromString(s, vec, ' ', true, true);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(stringFunctionsTests, fillContainerFromString_String){
    // empty string
    std::string s;
    std::vector<std::string> vec;
    fillContainerFromString(s, vec, " ");
    EXPECT_TRUE(vec.empty());

    // vec is cleared
    vec = {"1","2","3"};
    fillContainerFromString(s, vec, " ");
    EXPECT_TRUE(vec.empty());

    // correct string, ending with delim
    s = "0a72ab364ad-18dcab894bad";
    fillContainerFromString(s, vec, "ad");
    EXPECT_THAT(vec, ElementsAre("0a72ab364", "-18dcab894b", ""));

    // correct string, ending with non-delim
    s = "0a72ab364ad-18dcab894bad5";
    fillContainerFromString(s, vec, "ad");
    EXPECT_THAT(vec, ElementsAre("0a72ab364", "-18dcab894b", "5"));

    // string with extra whitespaces, ending with delim
    s = "   0a72ab364\nadad-18dca\tb894    bad";
    fillContainerFromString(s, vec, "ad");
    EXPECT_THAT(vec, ElementsAre("0a72ab364", "", "-18dca\tb894    b", "")); // if you want correct result, then skip empty!

    // string with extra whitespaces, ending with non-delim
    s = "   0a72ab364\nadad-18dca\tb894    bad5";
    fillContainerFromString(s, vec, "ad");
    EXPECT_THAT(vec, ElementsAre("0a72ab364", "", "-18dca\tb894    b", "5")); // if you want correct result, then skip empty!
}

TEST(stringFunctionsTests, fillContainerFromStringSkipEmpty_String){
    // empty string
    std::string s;
    std::vector<std::string> vec;
    fillContainerFromString(s, vec, " ", true);
    EXPECT_TRUE(vec.empty());

    // vec is cleared
    vec = {"1","2","3"};
    fillContainerFromString(s, vec, " ", true);
    EXPECT_TRUE(vec.empty());

    // correct string, ending with delim
    s = "0a72ab364ad-18dcab894bad";
    fillContainerFromString(s, vec, "ad", true);
    EXPECT_THAT(vec, ElementsAre("0a72ab364", "-18dcab894b"));

    // correct string, ending with non-delim
    s = "0a72ab364ad-18dcab894bad5";
    fillContainerFromString(s, vec, "ad", true);
    EXPECT_THAT(vec, ElementsAre("0a72ab364", "-18dcab894b", "5"));

    // string with extra whitespaces, ending with delim
    s = "   0a72ab364\nadad-18dca\tb894    bad";
    fillContainerFromString(s, vec, "ad", true);
    EXPECT_THAT(vec, ElementsAre("0a72ab364", "-18dca\tb894    b"));

    // string with extra whitespaces, ending with non-delim
    s = "   0a72ab364\nadad-18dca\tb894    bad5";
    fillContainerFromString(s, vec, "ad", true);
    EXPECT_THAT(vec, ElementsAre("0a72ab364", "-18dca\tb894    b", "5"));
}

TEST(stringFunctionsTests, fillContainerFromString_Any){
    // empty string
    std::string s;
    std::vector<int> vec;
    fillContainerFromStringAny(s, vec, " ");
    EXPECT_TRUE(vec.empty());

    // vec is cleared
    vec = {1,2,3};
    fillContainerFromStringAny(s, vec, " ");
    EXPECT_TRUE(vec.empty());

    // correct string, ending with delim
    s = "0a72ab364ad-18dcab894bad";
    fillContainerFromStringAny(s, vec, "abcd");
    EXPECT_THAT(vec, ElementsAre(0, 72, 0, 364, 0, -18, 0, 0, 0, 894, 0, 0, 0));

    // correct string, ending with non-delim
    s = "0a72ab364ad-18dcab894bad5";
    fillContainerFromStringAny(s, vec, "abcd");
    EXPECT_THAT(vec, ElementsAre(0, 72, 0, 364, 0, -18, 0, 0, 0, 894, 0, 0, 5));

    // string with extra whitespaces, ending with delim
    s = "   0a72ab364\nad-18dca\tb894    bad";
    fillContainerFromStringAny(s, vec, "abcd");
    EXPECT_THAT(vec, ElementsAre(0, 72, 0, 364, 0, -18, 0, 0, 0, 894, 0, 0, 0)); // if you want correct result, then skip empty!

    // string with extra whitespaces, ending with non-delim
    s = "   0a72ab364\nad-18dca\tb894    bad5";
    fillContainerFromStringAny(s, vec, "abcd");
    EXPECT_THAT(vec, ElementsAre(0, 72, 0, 364, 0, -18, 0, 0, 0, 894, 0, 0, 5)); // if you want correct result, then skip empty!

    // weird string
    s = "0a72ab364ad-18dcab894foo!bad";
    EXPECT_NO_THROW(fillContainerFromStringAny(s, vec, "abcd")); // if you want throw, then check!

    // weird string with extra whitespaces
    s = "   0a72ab364\nad-18dca\tbfoo!894    bad";
    EXPECT_NO_THROW(fillContainerFromStringAny(s, vec, "abcd")); // if you want throw, then check!
}

TEST(stringFunctionsTests, fillContainerFromStringSkipEmpty_Any){
    // empty string
    std::string s;
    std::vector<int> vec;
    fillContainerFromStringAny(s, vec, " ", true);
    EXPECT_TRUE(vec.empty());

    // vec is cleared
    vec = {1,2,3};
    fillContainerFromStringAny(s, vec, " ", true);
    EXPECT_TRUE(vec.empty());

    // correct string, ending with delim
    s = "0a72ab364ad-18dcab894bad";
    fillContainerFromStringAny(s, vec, "abcd", true);
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

    // correct string, ending with non-delim
    s = "0a72ab364ad-18dcab894bad5";
    fillContainerFromStringAny(s, vec, "abcd", true);
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894, 5));

    // string with extra whitespaces, ending with delim
    s = "   0a72ab364\nad-18dca\tb894    bad";
    fillContainerFromStringAny(s, vec, "abcd", true);
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

    // string with extra whitespaces, ending with non-delim
    s = "   0a72ab364\nad-18dca\tb894    bad5";
    fillContainerFromStringAny(s, vec, "abcd", true);
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894, 5));

    // weird string
    s = "0a72ab364ad-18dcab894foo!bad";
    EXPECT_NO_THROW(fillContainerFromStringAny(s, vec, "abcd", true)); // if you want throw, then check!

    // weird string with extra whitespaces
    s = "   0a72ab364\nad-18dca\tbfoo!894    bad";
    EXPECT_NO_THROW(fillContainerFromStringAny(s, vec, "abcd", true)); // if you want throw, then check!
}

TEST(stringFunctionsTests, fillContainerFromStringSkipEmptyCheck_Any){
    // empty string
    std::string s;
    std::vector<int> vec;
    fillContainerFromStringAny(s, vec, " ", true, true);
    EXPECT_TRUE(vec.empty());

    // vec is cleared
    vec = {1,2,3};
    fillContainerFromStringAny(s, vec, " ", true, true);
    EXPECT_TRUE(vec.empty());

    // correct string, ending with delim
    s = "0a72ab364ad-18dcab894bad";
    fillContainerFromStringAny(s, vec, "abcd", true, true);
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

    // correct string, ending with non-delim
    s = "0a72ab364ad-18dcab894bad5";
    fillContainerFromStringAny(s, vec, "abcd", true, true);
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894, 5));

    // string with extra whitespaces, ending with delim
    s = "   0a72ab364\nad-18dca\tb894    bad";
    fillContainerFromStringAny(s, vec, "abcd", true, true);
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

    // string with extra whitespaces, ending with non-delim
    s = "   0a72ab364\nad-18dca\tb894    bad5";
    fillContainerFromStringAny(s, vec, "abcd", true, true);
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894, 5));

    // weird string
    s = "0a72ab364ad-18dcab894foo!bad";
    EXPECT_THROW({try {fillContainerFromStringAny(s, vec, "abcd", true, true);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);

    // weird string with extra whitespaces
    s = "   0a72ab364\nad-18dca\tbfoo!894    bad";
    EXPECT_THROW({try {fillContainerFromStringAny(s, vec, "abcd", true, true);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

//////////////// whitespaces ////////////////
TEST(stringFunctionsTests, fillContainerFromStringWhiteSpace){
    // empty string
    std::string s;
    std::vector<int> vec;
    fillContainerFromStringWhiteSpace(s, vec);
    EXPECT_TRUE(vec.empty());

    // vec is cleared
    vec = {1,2,3};
    fillContainerFromStringWhiteSpace(s, vec);
    EXPECT_TRUE(vec.empty());

    // correct string, ending with delim
    s = "\r0\t72\t364 -18 894 ";
    fillContainerFromStringWhiteSpace(s, vec);
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894, 0));

    // correct string, ending with non-delim
    s = "\r0\t72\t364 -18 894 5";
    fillContainerFromStringWhiteSpace(s, vec);
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894, 5));

    // string with extra whitespaces, ending with delim
    s = "\r 0 \t\t  \n 72\r364\n\v-18 \t 894\n";
    fillContainerFromStringWhiteSpace(s, vec);
    EXPECT_THAT(vec, ElementsAre(0, 0, 0, 0, 0, 0, 0, 72, 0, 0, 894));

    // string with extra whitespaces, ending with non-delim
    s = "\r 0 \t\t  \n 72\r364\n\v-18 \t 894";
    fillContainerFromStringWhiteSpace(s, vec);
    EXPECT_THAT(vec, ElementsAre(0, 0, 0, 0, 0, 0, 0, 72, 0, 0, 894));

    // weird string
    s = "\r0\t72\rfoooo!364\v-18 894\n";
    EXPECT_NO_THROW(fillContainerFromStringWhiteSpace(s, vec)); // if you want throw, then check!

    // weird string with extra whitespaces
    s = "\r 0 \t\t foooo!  \n 72\r364\n\v-18 \t 894";
    EXPECT_NO_THROW(fillContainerFromStringWhiteSpace(s, vec)); // if you want throw, then check!
}

TEST(stringFunctionsTests, fillContainerFromStringWhiteSpaceSkipEmpty){
    // empty string
    std::string s;
    std::vector<int> vec;
    fillContainerFromStringWhiteSpace(s, vec, true);
    EXPECT_TRUE(vec.empty());

    // vec is cleared
    vec = {1,2,3};
    fillContainerFromStringWhiteSpace(s, vec, true);
    EXPECT_TRUE(vec.empty());

    // correct string, ending with delim
    s = "\r0\t72\t364 -18 894\n";
    fillContainerFromStringWhiteSpace(s, vec, true);
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

    // correct string, ending with non-delim
    s = "\r0\t72\t364 -18 894\n";
    fillContainerFromStringWhiteSpace(s, vec, true);
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

    // string with extra whitespaces, ending with delim
    s = "\r 0 \t\t  \n 72\r364\n\v-18 \t 894\n";
    fillContainerFromStringWhiteSpace(s, vec, true);
    EXPECT_THAT(vec, ElementsAre(0, 72, 894));

    // string with extra whitespaces, ending with non-delim
    s = "\r 0 \t\t  \n 72\r364\n\v-18 \t 894";
    fillContainerFromStringWhiteSpace(s, vec, true);
    EXPECT_THAT(vec, ElementsAre(0, 72, 894));

    // weird string
    s = "\r0\t72\rfoooo!364\v-18 894\n";
    EXPECT_NO_THROW(fillContainerFromStringWhiteSpace(s, vec, true)); // if you want throw, then check!

    // weird string with extra whitespaces
    s = "\r 0 \t\t foooo!  \n 72\r364\n\v-18 \t 894";

    EXPECT_NO_THROW(fillContainerFromStringWhiteSpace(s, vec, true)); // if you want throw, then check!
}

TEST(stringFunctionsTests, fillContainerFromStringWhiteSpaceSkipEmptyCheck){
    // empty string
    std::string s;
    std::vector<int> vec;
    fillContainerFromStringWhiteSpace(s, vec, true, true);
    EXPECT_TRUE(vec.empty());

    // vec is cleared
    vec = {1,2,3};
    fillContainerFromStringWhiteSpace(s, vec, true, true);
    EXPECT_TRUE(vec.empty());

    // correct string, ending with delim
    s = "\r0\t72\t364 -18 894\n";
    fillContainerFromStringWhiteSpace(s, vec, true, true);
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

    // correct string, ending with non-delim
    s = "\r0\t72\t364 -18 894\n";
    fillContainerFromStringWhiteSpace(s, vec, true, true);
    EXPECT_THAT(vec, ElementsAre(0, 72, 364, -18, 894));

    // string with extra whitespaces, ending with delim
    s = "\r 0 \t\t  \n 72\r364\n\v-18 \t 894\n";
    EXPECT_THROW({try {fillContainerFromStringWhiteSpace(s, vec, true, true);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);

    // string with extra whitespaces, ending with non-delim
    s = "\r 0 \t\t  \n 72\r364\n\v-18 \t 894";
    EXPECT_THROW({try {fillContainerFromStringWhiteSpace(s, vec, true, true);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);

    // weird string
    s = "\r0\t72\rfoooo!364\v-18 894\n";
    EXPECT_THROW({try {fillContainerFromStringWhiteSpace(s, vec, true, true);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);

    // weird string with extra whitespaces
    s = "\r 0 \t\t foooo!  \n 72\r364\n\v-18 \t 894";
    EXPECT_THROW({try {fillContainerFromStringWhiteSpace(s, vec, true, true);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(stringFunctionsTests, fillProbabilityContainerFromString_Char) {
    std::string s = "0.9,0.5,0.3,0,1";
    std::vector<double> vec;
    fillProbabilityContainerFromString(s, vec, ',', true);
    EXPECT_THAT(vec, ElementsAre(0.9, 0.5, 0.3, 0., 1.));
}

TEST(stringFunctionsTests, fillProbabilityContainerFromString_String) {
    std::string s = "0.9bla0.5bla0.3bla0bla1";
    std::vector<double> vec;
    fillProbabilityContainerFromString(s, vec, "bla", true);
    EXPECT_THAT(vec, ElementsAre(0.9, 0.5, 0.3, 0., 1.));
}

TEST(stringFunctionsTests, fillProbabilityContainerFromString_String_Any) {
    std::string s = "0.9,0.5_0.3/0,1";
    std::vector<double> vec;
    fillProbabilityContainerFromStringAny(s, vec, ",_/", true);
    EXPECT_THAT(vec, ElementsAre(0.9, 0.5, 0.3, 0., 1.));
}

TEST(stringFunctionsTests, fillContainerFromLine){
    // write
    std::string filename = "stream.txt";
    std::ofstream output(filename.c_str());
    output << 1 << '\t' << 2 << '\t' << 3 << '\t' << 4 << '\t' << 5 << std::endl;
    output << 6 << '\t' << 7 << '\t' << 8 << '\t' << 9 << '\t' << 10 << std::endl;
    output.close();

    // read
    std::vector<int> vec;
    std::ifstream input(filename.c_str());
    fillContainerFromLine(input, vec, '\t');
    EXPECT_THAT(vec, ElementsAre(1, 2, 3, 4, 5));

    input.close();
    remove(filename.c_str());
}

TEST(stringFunctionsTests, fillContainerFromLine_Any){
    // write
    std::string filename = "stream.txt";
    std::ofstream output(filename.c_str());
    output << 1 << "," << 2 << ',' << 3 << '/' << 4 << '_' << 5 << std::endl;
    output << 6 << '\t' << 7 << '\t' << 8 << '\t' << 9 << '\t' << 10 << std::endl;
    output.close();

    // read
    std::vector<int> vec;
    std::ifstream input(filename.c_str());
    fillContainerFromLineAny(input, vec, ",/_");
    EXPECT_THAT(vec, ElementsAre(1, 2, 3, 4, 5));

    input.close();
    remove(filename.c_str());
}

TEST(stringFunctionsTests, fillContainerFromLine_WhiteSpace){
    // write
    std::string filename = "stream.txt";
    std::ofstream output(filename.c_str());
    output << 1 << '\t' << 2 << ' ' << 3 << '\t' << 4 << ' ' << 5 << std::endl;
    output << 6 << '\t' << 7 << '\t' << 8 << '\t' << 9 << '\t' << 10 << std::endl;
    output.close();

    // read
    std::vector<int> vec;
    std::ifstream input(filename.c_str());
    fillContainerFromLineWhiteSpace(input, vec);
    EXPECT_THAT(vec, ElementsAre(1, 2, 3, 4, 5));

    input.close();
    remove(filename.c_str());
}

//-------------------------------------------------
// Sequences and indexes
//-------------------------------------------------

TEST(stringFunctionsTests, numericToUpperCaseAlphabetIndex){
    EXPECT_EQ(numericToUpperCaseAlphabetIndex(0), "A");
    EXPECT_EQ(numericToUpperCaseAlphabetIndex(1), "B");
    EXPECT_EQ(numericToUpperCaseAlphabetIndex(2), "C");
    EXPECT_EQ(numericToUpperCaseAlphabetIndex(25), "Z");
    EXPECT_EQ(numericToUpperCaseAlphabetIndex(26), "AA");
    EXPECT_EQ(numericToUpperCaseAlphabetIndex(27), "AB");
    EXPECT_EQ(numericToUpperCaseAlphabetIndex(28), "AC");
    EXPECT_EQ(numericToUpperCaseAlphabetIndex(52), "BA");
    EXPECT_EQ(numericToUpperCaseAlphabetIndex(53), "BB");
    EXPECT_EQ(numericToUpperCaseAlphabetIndex(54), "BC");
    EXPECT_EQ(numericToUpperCaseAlphabetIndex(78), "CA");
    EXPECT_EQ(numericToUpperCaseAlphabetIndex(701), "ZZ");
    EXPECT_EQ(numericToUpperCaseAlphabetIndex(702), "AAA");
    EXPECT_EQ(numericToUpperCaseAlphabetIndex(703), "AAB");
    EXPECT_EQ(numericToUpperCaseAlphabetIndex(704), "AAC");
    EXPECT_EQ(numericToUpperCaseAlphabetIndex(18277), "ZZZ");
    EXPECT_EQ(numericToUpperCaseAlphabetIndex(18278), "AAAA");
    EXPECT_EQ(numericToUpperCaseAlphabetIndex(18279), "AAAB");
    // ...
}

TEST(stringFunctionsTests, numericToLowerCaseAlphabetIndex){
    EXPECT_EQ(numericToLowerCaseAlphabetIndex(0), "a");
    EXPECT_EQ(numericToLowerCaseAlphabetIndex(1), "b");
    EXPECT_EQ(numericToLowerCaseAlphabetIndex(2), "c");
    EXPECT_EQ(numericToLowerCaseAlphabetIndex(25), "z");
    EXPECT_EQ(numericToLowerCaseAlphabetIndex(26), "aa");
    EXPECT_EQ(numericToLowerCaseAlphabetIndex(27), "ab");
    EXPECT_EQ(numericToLowerCaseAlphabetIndex(28), "ac");
    EXPECT_EQ(numericToLowerCaseAlphabetIndex(52), "ba");
    EXPECT_EQ(numericToLowerCaseAlphabetIndex(53), "bb");
    EXPECT_EQ(numericToLowerCaseAlphabetIndex(54), "bc");
    EXPECT_EQ(numericToLowerCaseAlphabetIndex(78), "ca");
    EXPECT_EQ(numericToLowerCaseAlphabetIndex(701), "zz");
    EXPECT_EQ(numericToLowerCaseAlphabetIndex(702), "aaa");
    EXPECT_EQ(numericToLowerCaseAlphabetIndex(703), "aab");
    EXPECT_EQ(numericToLowerCaseAlphabetIndex(704), "aac");
    EXPECT_EQ(numericToLowerCaseAlphabetIndex(18277), "zzz");
    EXPECT_EQ(numericToLowerCaseAlphabetIndex(18278), "aaaa");
    EXPECT_EQ(numericToLowerCaseAlphabetIndex(18279), "aaab");
    // ...
}

TEST(stringFunctionsTests, lowerCaseAlphabetIndexToNumeric){
    EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("a"), 0);
    EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("b"), 1);
    EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("c"), 2);
    EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("z"), 25);
    EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("aa"), 26);
    EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("ab"), 27);
    EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("ac"), 28);
    EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("ba"), 52);
    EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("bb"), 53);
    EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("bc"), 54);
    EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("ca"), 78);
    EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("zz"), 701);
    EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("aaa"), 702);
    EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("aab"), 703);
    EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("aac"), 704);
    EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("zzz"), 18277);
    EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("aaaa"), 18278);
    EXPECT_EQ(lowerCaseAlphabetIndexToNumeric("aaab"), 18279);
    // ...
}

TEST(stringFunctionsTests, upperCaseAlphabetIndexToNumeric){
    EXPECT_EQ(upperCaseAlphabetIndexToNumeric("A"), 0);
    EXPECT_EQ(upperCaseAlphabetIndexToNumeric("B"), 1);
    EXPECT_EQ(upperCaseAlphabetIndexToNumeric("C"), 2);
    EXPECT_EQ(upperCaseAlphabetIndexToNumeric("Z"), 25);
    EXPECT_EQ(upperCaseAlphabetIndexToNumeric("AA"), 26);
    EXPECT_EQ(upperCaseAlphabetIndexToNumeric("AB"), 27);
    EXPECT_EQ(upperCaseAlphabetIndexToNumeric("AC"), 28);
    EXPECT_EQ(upperCaseAlphabetIndexToNumeric("BA"), 52);
    EXPECT_EQ(upperCaseAlphabetIndexToNumeric("BB"), 53);
    EXPECT_EQ(upperCaseAlphabetIndexToNumeric("BC"), 54);
    EXPECT_EQ(upperCaseAlphabetIndexToNumeric("CA"), 78);
    EXPECT_EQ(upperCaseAlphabetIndexToNumeric("ZZ"), 701);
    EXPECT_EQ(upperCaseAlphabetIndexToNumeric("AAA"), 702);
    EXPECT_EQ(upperCaseAlphabetIndexToNumeric("AAB"), 703);
    EXPECT_EQ(upperCaseAlphabetIndexToNumeric("AAC"), 704);
    EXPECT_EQ(upperCaseAlphabetIndexToNumeric("ZZZ"), 18277);
    EXPECT_EQ(upperCaseAlphabetIndexToNumeric("AAAA"), 18278);
    EXPECT_EQ(upperCaseAlphabetIndexToNumeric("AAAB"), 18279);
    // ...
}

TEST(stringFunctionsTests, addRepeatedIndexIfRepeated){
    // empty string
    std::string s;
    std::vector<std::string> vec;
    EXPECT_FALSE(addRepeatedIndexIfRepeated(s, vec));

    // invalid strings
    s = "blub";
    EXPECT_FALSE(addRepeatedIndexIfRepeated(s, vec));
    s = "{4}blub";
    EXPECT_FALSE(addRepeatedIndexIfRepeated(s, vec));
    s = "b{lub{4}";
    EXPECT_THROW({try {addRepeatedIndexIfRepeated(s, vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    s = "blub}4{";
    EXPECT_THROW({try {addRepeatedIndexIfRepeated(s, vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    s = "blub{4}blub";
    EXPECT_THROW({try {addRepeatedIndexIfRepeated(s, vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    s = "blub{[4]}";
    EXPECT_THROW({try {addRepeatedIndexIfRepeated(s, vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    s = "blub{4]";
    EXPECT_THROW({try {addRepeatedIndexIfRepeated(s, vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    s = "blub{0}";
    EXPECT_THROW({try {addRepeatedIndexIfRepeated(s, vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    s = "blub{bar4}";
    EXPECT_THROW({try {addRepeatedIndexIfRepeated(s, vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    s = "blub{4bar}";
    EXPECT_THROW({try {addRepeatedIndexIfRepeated(s, vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);

    // valid strings
    s = "blub{4}";
    addRepeatedIndexIfRepeated(s, vec);
    EXPECT_THAT(vec, ElementsAre("blub", "blub", "blub", "blub"));
}

TEST(stringFunctionsTests, addExpandedIndexIfToExpand){
    // empty string
    std::string s;
    std::vector<std::string> vec;
    EXPECT_FALSE(addExpandedIndexIfToExpand(s, vec));

    // invalid strings
    s = "blub"; vec.clear();
    EXPECT_FALSE(addExpandedIndexIfToExpand(s, vec));
    s = "b[lub[4]"; vec.clear();
    EXPECT_THROW({try {addExpandedIndexIfToExpand(s, vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    s = "blub[4"; vec.clear();
    EXPECT_THROW({try {addExpandedIndexIfToExpand(s, vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    s = "blub[4]]"; vec.clear();
    EXPECT_THROW({try {addExpandedIndexIfToExpand(s, vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    s = "blub[[4]]"; vec.clear();
    EXPECT_THROW({try {addExpandedIndexIfToExpand(s, vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    s = "blub]4["; vec.clear();
    EXPECT_THROW({try {addExpandedIndexIfToExpand(s, vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    s = "blub{4]"; vec.clear();
    EXPECT_FALSE(addExpandedIndexIfToExpand(s, vec));
    s = "blub[0]"; vec.clear();
    EXPECT_THROW({try {addExpandedIndexIfToExpand(s, vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    s = "blub[bar4]"; vec.clear();

    // valid strings
    s = "[4]blub"; vec.clear();
    addExpandedIndexIfToExpand(s, vec);
    EXPECT_THAT(vec, ElementsAre("1blub", "2blub", "3blub", "4blub"));

    s = "blub[4]"; vec.clear();
    addExpandedIndexIfToExpand(s, vec);
    EXPECT_THAT(vec, ElementsAre("blub1", "blub2", "blub3", "blub4"));

    s = "blub[4]blub"; vec.clear();
    addExpandedIndexIfToExpand(s, vec);
    EXPECT_THAT(vec, ElementsAre("blub1blub", "blub2blub", "blub3blub", "blub4blub"));

    s = "blub{[4]}"; vec.clear();
    addExpandedIndexIfToExpand(s, vec);
    EXPECT_THAT(vec, ElementsAre("blub{1}", "blub{2}", "blub{3}", "blub{4}"));
}

TEST(stringFunctionsTests, addRepeatedIndex){
    // empty string
    std::string s;
    std::vector<std::string> vec;
    addRepeatedIndex(s, vec);
    EXPECT_THAT(vec, ElementsAre(""));

    // invalid strings
    s = "blub"; vec.clear();
    addRepeatedIndex(s, vec);
    EXPECT_THAT(vec, ElementsAre("blub"));
    s = "{4}blub"; vec.clear();
    addRepeatedIndex(s, vec);
    EXPECT_THAT(vec, ElementsAre("{4}blub"));

    // valid strings
    s = "blub{4}"; vec.clear();
    addRepeatedIndex(s, vec);
    EXPECT_THAT(vec, ElementsAre("blub", "blub", "blub", "blub"));
}

TEST(stringFunctionsTests, addExpandIndex){
    // empty string
    std::string s;
    std::vector<std::string> vec;
    addExpandIndex(s, vec);
    EXPECT_THAT(vec, ElementsAre(""));

    // invalid strings
    s = "blub"; vec.clear();
    addExpandIndex(s, vec);
    EXPECT_THAT(vec, ElementsAre("blub"));
    s = "blub{4]"; vec.clear();
    addExpandIndex(s, vec);
    EXPECT_THAT(vec, ElementsAre("blub{4]"));

    // valid strings
    s = "blub[4]"; vec.clear();
    addExpandIndex(s, vec);
    EXPECT_THAT(vec, ElementsAre("blub1", "blub2", "blub3", "blub4"));
}

TEST(stringFunctionsTests, addRepeatedAndExpandIndexes){
    // empty string
    std::string s;
    std::vector<std::string> vec;
    addRepeatedAndExpandIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre(""));

    // invalid strings
    s = "blub"; vec.clear();
    addRepeatedAndExpandIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre("blub"));
    s = "{4}blub"; vec.clear();
    addRepeatedIndex(s, vec);
    EXPECT_THAT(vec, ElementsAre("{4}blub"));

    // valid strings
    s = "blub{4}"; vec.clear();
    addRepeatedAndExpandIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre("blub", "blub", "blub", "blub"));

    s = "blub[4]"; vec.clear();
    addRepeatedAndExpandIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre("blub1", "blub2", "blub3", "blub4"));
}

TEST(stringFunctionsTests, repeatAndExpandIndexes){
    // empty string
    std::vector<std::string> s;
    std::vector<std::string> vec;
    repeatAndExpandIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre());

    // invalid strings
    s.clear(); s.emplace_back("blub"); vec.clear();
    repeatAndExpandIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre("blub"));
    s.clear(); s.emplace_back("{4}blub"); vec.clear();
    repeatAndExpandIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre("{4}blub"));

    // valid strings
    s.clear(); s.emplace_back("blub{4}"); vec.clear();
    repeatAndExpandIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre("blub", "blub", "blub", "blub"));

    s.clear(); s.emplace_back("blub[4]"); vec.clear();
    repeatAndExpandIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre("blub1", "blub2", "blub3", "blub4"));

    s.clear(); s.emplace_back("blub[4]"); s.emplace_back("bluh{3}"); s.emplace_back("blu[1]"); vec.clear();
    repeatAndExpandIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre("blub1", "blub2", "blub3", "blub4", "bluh", "bluh", "bluh", "blu1"));
}

TEST(stringFunctionsTests, repeatIndexesStrings){
    // empty string
    std::vector<std::string> s;
    std::vector<std::string> vec;
    repeatIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre());

    // invalid strings
    s.clear(); s.emplace_back("blub"); vec.clear();
    repeatIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre("blub"));
    s.clear(); s.emplace_back("{4}blub"); vec.clear();
    repeatIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre("{4}blub"));

    // valid strings
    s.clear(); s.emplace_back("blub{4}"); vec.clear();
    repeatIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre("blub", "blub", "blub", "blub"));

    s.clear(); s.emplace_back("blab{2}"); s.emplace_back("bluh{3}"); s.emplace_back("blu{1}"); vec.clear();
    repeatIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre("blab", "blab", "bluh", "bluh", "bluh", "blu"));
}

TEST(stringFunctionsTests, repeatIndexesInts){
    // empty string
    std::vector<std::string> s;
    std::vector<int> vec;
    repeatIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre());

    // invalid strings
    s.clear(); s.emplace_back("3"); vec.clear();
    repeatIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre(3));
    s.clear(); s.emplace_back("{4}2"); vec.clear();
    EXPECT_THROW({try { repeatIndexes(s, vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);

    // valid strings
    s.clear(); s.emplace_back("4{4}"); vec.clear();
    repeatIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre(4, 4, 4, 4));

    s.clear(); s.emplace_back("6{2}"); s.emplace_back("-102{3}"); s.emplace_back("1047{1}"); vec.clear();
    repeatIndexes(s, vec);
    EXPECT_THAT(vec, ElementsAre(6, 6, -102, -102, -102, 1047));
}

TEST(stringFunctionsTests, expandOverMultiDimensions){
    std::vector<int> vec = {3};

    // 1D
    std::vector<std::string> res = expandOverMultiDimensions(vec);
    EXPECT_THAT(res, ElementsAre("0", "1", "2"));

    // 2D
    vec = {3,2};
    res = expandOverMultiDimensions(vec);
    EXPECT_THAT(res, ElementsAre("0_0", "0_1", "1_0", "1_1", "2_0", "2_1"));

    // 3D
    vec = {3,2,2};
    res = expandOverMultiDimensions(vec);
    EXPECT_THAT(res, ElementsAre("0_0_0", "0_0_1", "0_1_0", "0_1_1", "1_0_0", "1_0_1", "1_1_0", "1_1_1", "2_0_0", "2_0_1", "2_1_0", "2_1_1"));
}

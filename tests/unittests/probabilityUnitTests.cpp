/*
 * TRangeUnitTests.cpp
 *
 *  Created on: May 14, 2021
 *      Author: Daniel Wegmann
 */


#include "TestCase.h"
#include "probability.h"

using namespace testing;

//------------------------------------------------
// Test
//------------------------------------------------
TEST(ProbabilityTest, Probability){
	EXPECT_ANY_THROW(Probability(1.2));
	EXPECT_ANY_THROW(Probability(-0.1));
	EXPECT_NO_THROW(Probability(0.1));

	Probability a(0.1);
	EXPECT_DOUBLE_EQ(a, 0.1);
	Probability b(0.5);
	EXPECT_DOUBLE_EQ(a, 0.5);

	EXPECT_FALSE(a == b);
	EXPECT_TRUE(a != b);
	EXPECT_TRUE(a < b);
	EXPECT_FALSE(a > b);

	Probability c(a);
	EXPECT_TRUE(c == a);
	EXPECT_TRUE(c != b);

	c *= a;
	EXPECT_TRUE(c == a*a);
};

TEST(ProbabilityTest, LogProbability){
	EXPECT_ANY_THROW(LogProbability(1.2));
	EXPECT_NO_THROW(LogProbability(-0.1));
	EXPECT_NO_THROW(LogProbability(0.0));

	LogProbability a(-5.0);
	EXPECT_DOUBLE_EQ((double) a, -5.0);
	LogProbability b(-4.0);
	EXPECT_DOUBLE_EQ((double) a, -4.0);

	EXPECT_FALSE(a == b);
	EXPECT_TRUE(a != b);
	EXPECT_TRUE(a < b);
	EXPECT_FALSE(a > b);

	LogProbability c(a);
	EXPECT_TRUE(c == a);
	EXPECT_TRUE(c != b);

	c += a;
	EXPECT_TRUE(c == a+a);
};

TEST(ProbabilityTest, Log10Probability){
	EXPECT_ANY_THROW(Log10Probability(1.2));
	EXPECT_NO_THROW(Log10Probability(-0.1));
	EXPECT_NO_THROW(Log10Probability(0.0));

	Log10Probability a(-5.0);
	EXPECT_DOUBLE_EQ((double) a, -5.0);
	Log10Probability b(-4.0);
	EXPECT_DOUBLE_EQ((double) a, -4.0);

	EXPECT_FALSE(a == b);
	EXPECT_TRUE(a != b);
	EXPECT_TRUE(a < b);
	EXPECT_FALSE(a > b);

	Log10Probability c(a);
	EXPECT_TRUE(c == a);
	EXPECT_TRUE(c != b);

	c += a;
	EXPECT_TRUE(c == a+a);
};

TEST(ProbabilityTest, ProbabilityConversions){
	Probability a(0.45);

	LogProbability b(a);
	EXPECT_DOUBLE_EQ((double) b, log(0.45));
	EXPECT_DOUBLE_EQ((double) a, (double) (Probability) b);

	Log10Probability c(a);
	EXPECT_DOUBLE_EQ((double) c, log10(0.45));
	EXPECT_DOUBLE_EQ((double) a, (double) (Probability) c);
	EXPECT_DOUBLE_EQ((double) (Probability) b, (double) (Probability) c);

	Log10Probability d = a;
	LogProbability e = d;
	Probability f = e;
	EXPECT_DOUBLE_EQ((double) f, (double) a);
};

//
// Created by caduffm on 8/20/20.
//

#include "TestCase.h"
#include "counters.h"

//----------------------------------------------
// TCountDistribution
//----------------------------------------------

TEST(TCountDistribution, constructor){
    TCountDistribution vec(10);

    for (int i = 0; i < 10; i++)
        EXPECT_EQ(vec[i], 0);
}

TEST(TCountDistribution, add){
    TCountDistribution vec;
    vec.add(0);
    vec.add(0);
    vec.add(1);
    vec.add(10);

    EXPECT_EQ(vec[0], 2);
    EXPECT_EQ(vec[1], 1);
    EXPECT_EQ(vec[10], 1);
    for (int i = 2; i < 10; i++)
        EXPECT_EQ(vec[i], 0);
}

TEST(TCountDistribution, add_frequency){
    TCountDistribution vec;
    vec.add(0, 10);
    vec.add(0, 1);
    vec.add(1, 100);
    vec.add(10, 200);

    EXPECT_EQ(vec[0], 11);
    EXPECT_EQ(vec[1], 100);
    EXPECT_EQ(vec[10], 200);
    for (int i = 2; i < 10; i++)
        EXPECT_EQ(vec[i], 0);
}

TEST(TCountDistribution, add_other){
    TCountDistribution vec;
    vec.add(0, 10);
    vec.add(0, 1);
    vec.add(1, 100);
    vec.add(10, 200);

    TCountDistribution vec2;
    vec2.add(vec);

    EXPECT_EQ(vec2[0], 11);
    EXPECT_EQ(vec2[1], 100);
    EXPECT_EQ(vec2[10], 200);
    for (int i = 2; i < 10; i++)
        EXPECT_EQ(vec2[i], 0);
}

TEST(TCountDistribution, operator_square_brackets){
    TCountDistribution vec;
    vec.add(0, 10);
    vec.add(1, 100);
    vec.add(10, 200);

    vec[0] = vec[0] + 10;
    EXPECT_EQ(vec[0], 20);

    // outside range: just resizes
    vec[20] = 150;
    EXPECT_EQ(vec[20], 150);
}

TEST(TCountDistribution, operator_square_brackets_const){
    const TCountDistribution vec;
    // outside range
    EXPECT_THROW(const uint64_t val = vec[100], std::runtime_error);
}

TEST(TCountDistribution, at){
    TCountDistribution vec;
    vec.add(0, 10);
    vec.add(1, 100);
    vec.add(10, 200);

    EXPECT_EQ(vec.at(0), 10);
    EXPECT_EQ(vec.at(1), 100);
    EXPECT_EQ(vec.at(10), 200);
    for (int i = 2; i < 10; i++)
        EXPECT_EQ(vec.at(i), 0);

    // outside range: just return 0
    EXPECT_EQ(vec.at(100), 0);
}

TEST(TCountDistribution, empty_clear){
    TCountDistribution vec;
    EXPECT_TRUE(vec.empty());
    vec.add(10);
    EXPECT_FALSE(vec.empty());
    vec.clear();
    EXPECT_TRUE(vec.empty());
}

TEST(TCountDistribution, counts){
    TCountDistribution vec;

    vec.add(0, 100);
    vec.add(1);
    vec.add(10, 200);
    EXPECT_EQ(vec.counts(), 301);
}

TEST(TCountDistribution, countsLarger){
    TCountDistribution vec;

    vec.add(0, 100);
    vec.add(1);
    vec.add(2, 10);
    vec.add(10, 200);
    EXPECT_EQ(vec.countsLarger(1), 210);
}

TEST(TCountDistribution, countsLargerZero){
    TCountDistribution vec;

    vec.add(0, 100);
    vec.add(1);
    vec.add(2, 10);
    vec.add(10, 200);
    EXPECT_EQ(vec.countsLargerZero(), 211);
}

TEST(TCountDistribution, sum){
    TCountDistribution vec;

    vec.add(0, 100);
    vec.add(1);
    vec.add(2, 10);
    vec.add(10, 200);
    EXPECT_EQ(vec.sum(), 2021);
}

TEST(TCountDistribution, mean){
    TCountDistribution vec;

    vec.add(0, 100);
    vec.add(1);
    vec.add(2, 10);
    vec.add(10, 200);
    EXPECT_FLOAT_EQ(vec.mean(), 6.498392);
}

TEST(TCountDistribution, min){
    TCountDistribution vec;

    vec.add(2, 10);
    vec.add(10, 200);
    EXPECT_EQ(vec.min(), 2);
}

TEST(TCountDistribution, max){
    TCountDistribution vec;

    vec.add(2, 10);
    vec.add(10, 200);
    vec.add(20, 0);
    EXPECT_EQ(vec.max(), 10);
}

TEST(TCountDistribution, fracLarger){
    TCountDistribution vec;

    vec.add(0, 100);
    vec.add(1);
    vec.add(2, 10);
    vec.add(10, 200);
    EXPECT_FLOAT_EQ(vec.fracLarger(1), 0.6752412);
}

TEST(TCountDistribution, fracLargerZero){
    TCountDistribution vec;

    vec.add(0, 100);
    vec.add(1);
    vec.add(2, 10);
    vec.add(10, 200);
    EXPECT_FLOAT_EQ(vec.fracLargerZero(), 0.6784566);
}

TEST(TCountDistribution, write){
    TCountDistribution vec;
    TOutputFile out("file.txt");

    vec.add(0, 0);
    vec.add(1, 1);
    vec.add(2, 2);
    vec.add(3, 3);

    vec.write(out);
    out.close();

    TInputFile in("file.txt", 2);
    std::vector<uint64_t> inVec;
    int c = 0;
    while (in.read(inVec)){
        EXPECT_EQ(inVec[0], c); EXPECT_EQ(inVec[1], c);
        c++;
    }
    in.close();
    remove("file.txt");
}

TEST(TCountDistribution, write_name){
    TCountDistribution vec;
    TOutputFile out("file.txt");

    vec.add(0, 0);
    vec.add(1, 1);
    vec.add(2, 2);
    vec.add(3, 3);

    vec.write(out, "ID");
    out.close();

    TInputFile in("file.txt", 3);
    std::vector<std::string> inVec;
    int c = 0;
    while (in.read(inVec)){
        EXPECT_EQ(inVec[0], "ID"); EXPECT_EQ(inVec[1], toString(c)); EXPECT_EQ(inVec[2], toString(c));
        c++;
    }
    in.close();
    remove("file.txt");
}

TEST(TCountDistribution, write_label){
    TCountDistribution vec;

    vec.add(0, 0);
    vec.add(1, 1);
    vec.add(2, 2);
    vec.add(3, 3);

    vec.write("file.txt", "ID");

    TInputFile in("file.txt", header);

    std::vector<std::string> header = in.header();
    EXPECT_EQ(header[0], "ID");
    EXPECT_EQ(header[1], "counts");

    std::vector<uint64_t> inVec;
    int c = 0;
    while (in.read(inVec)){
        EXPECT_EQ(inVec[0], c); EXPECT_EQ(inVec[1], c);
        c++;
    }
    in.close();
    remove("file.txt");
}

//----------------------------------------------
// TCountDistributionVector
//----------------------------------------------

void fillComplicatedDistr(TCountDistributionVector& vec){
    vec.add(0, 0, 10);
    vec.add(0, 0, 1);
    vec.add(0, 1, 100);
    vec.add(10, 0, 400);
    vec.add(10, 1, 800);
    vec.add(10, 5, 200);
}

TEST(TCountDistributionVector, constructor){
    TCountDistributionVector vec(10);

    for (int i = 0; i < 10; i++)
        EXPECT_EQ(vec[i].empty(), true);
}

TEST(TCountDistributionVector, add){
    TCountDistributionVector vec(5);

    vec.add(0, 0);
    vec.add(0, 0);
    vec.add(0, 1);
    vec.add(10, 5);

    // ID = 0
    EXPECT_EQ(vec[0][0], 2);
    EXPECT_EQ(vec[0][1], 1);
    // ID = 1
    EXPECT_EQ(vec[1].empty(), true);
    // ID = 2
    EXPECT_EQ(vec[2].empty(), true);
    // ...
    // ID = 10
    EXPECT_EQ(vec[10][0], 0);
    EXPECT_EQ(vec[10][1], 0);
    EXPECT_EQ(vec[10][2], 0);
    EXPECT_EQ(vec[10][3], 0);
    EXPECT_EQ(vec[10][4], 0);
    EXPECT_EQ(vec[10][5], 1);
}

TEST(TCountDistributionVector, add_frequency){
    TCountDistributionVector vec(5);

    vec.add(0, 0, 10);
    vec.add(0, 0, 1);
    vec.add(0, 1, 100);
    vec.add(10, 5, 200);

    // ID = 0
    EXPECT_EQ(vec[0][0], 11);
    EXPECT_EQ(vec[0][1], 100);
    // ID = 1
    EXPECT_EQ(vec[1].empty(), true);
    // ID = 2
    EXPECT_EQ(vec[2].empty(), true);
    // ...
    // ID = 10
    EXPECT_EQ(vec[10][0], 0);
    EXPECT_EQ(vec[10][1], 0);
    EXPECT_EQ(vec[10][2], 0);
    EXPECT_EQ(vec[10][3], 0);
    EXPECT_EQ(vec[10][4], 0);
    EXPECT_EQ(vec[10][5], 200);
}

TEST(TCountDistributionVector, add_id_other){
    TCountDistribution vec(5);

    vec.add( 0, 10);
    vec.add(0, 1);
    vec.add(1, 100);

    TCountDistributionVector vec2;
    vec2.add(0, vec);
    vec2.add(10, 5, 200);

    // ID = 0
    EXPECT_EQ(vec2[0][0], 11);
    EXPECT_EQ(vec2[0][1], 100);
    // ID = 1
    EXPECT_EQ(vec2[1].empty(), true);
    // ID = 2
    EXPECT_EQ(vec2[2].empty(), true);
    // ...
    // ID = 10
    EXPECT_EQ(vec2[10][0], 0);
    EXPECT_EQ(vec2[10][1], 0);
    EXPECT_EQ(vec2[10][2], 0);
    EXPECT_EQ(vec2[10][3], 0);
    EXPECT_EQ(vec2[10][4], 0);
    EXPECT_EQ(vec2[10][5], 200);
}

TEST(TCountDistributionVector, add_other){
    TCountDistributionVector vec(5);

    vec.add(0, 0, 10);
    vec.add(0, 0, 1);
    vec.add(0, 1, 100);
    vec.add(10, 5, 200);

    TCountDistributionVector vec2;
    vec2.add(vec);

    // ID = 0
    EXPECT_EQ(vec2[0][0], 11);
    EXPECT_EQ(vec2[0][1], 100);
    // ID = 1
    EXPECT_EQ(vec2[1].empty(), true);
    // ID = 2
    EXPECT_EQ(vec2[2].empty(), true);
    // ...
    // ID = 10
    EXPECT_EQ(vec2[10][0], 0);
    EXPECT_EQ(vec2[10][1], 0);
    EXPECT_EQ(vec2[10][2], 0);
    EXPECT_EQ(vec2[10][3], 0);
    EXPECT_EQ(vec2[10][4], 0);
    EXPECT_EQ(vec2[10][5], 200);
}

TEST(TCountDistributionVector, size_clear) {
    TCountDistributionVector vec(10);
    EXPECT_EQ(vec.size(), 10);
    vec.clear();
    EXPECT_EQ(vec.size(), 0);
}

TEST(TCountDistributionVector, counts){
    TCountDistributionVector vec(10);
    fillComplicatedDistr(vec);

    EXPECT_EQ(vec.counts(), 1511);
}

TEST(TCountDistributionVector, countsLarger){
    TCountDistributionVector vec(10);
    fillComplicatedDistr(vec);

    EXPECT_EQ(vec.countsLarger(1), 200);
}

TEST(TCountDistributionVector, countsLargerZero){
    TCountDistributionVector vec(10);
    fillComplicatedDistr(vec);

    EXPECT_EQ(vec.countsLargerZero(), 1100);
}

TEST(TCountDistributionVector, sum){
    TCountDistributionVector vec(10);
    fillComplicatedDistr(vec);

    EXPECT_EQ(vec.sum(), 1900);
}

TEST(TCountDistributionVector, mean){
    TCountDistributionVector vec(10);
    fillComplicatedDistr(vec);


    EXPECT_FLOAT_EQ(vec.mean(), 1.257445);
}

TEST(TCountDistributionVector, min){
    TCountDistributionVector vec(10);
    fillComplicatedDistr(vec);

    EXPECT_EQ(vec.min(), 0);
}


TEST(TCountDistributionVector, max){
    TCountDistributionVector vec(10);
    fillComplicatedDistr(vec);

    EXPECT_EQ(vec.max(), 5); // 5, because 5 is the largest non-zero value
}

TEST(TCountDistributionVector, fracLarger){
    TCountDistributionVector vec(10);
    fillComplicatedDistr(vec);

    EXPECT_FLOAT_EQ(vec.fracLarger(1), 0.1323627);
}

TEST(TCountDistributionVector, fracLargerZero){
    TCountDistributionVector vec(10);
    fillComplicatedDistr(vec);

    EXPECT_FLOAT_EQ(vec.fracLargerZero(), 0.7279947);
}

TEST(TCountDistributionVector, exists){
    TCountDistributionVector vec(10);
    fillComplicatedDistr(vec);

    EXPECT_EQ(vec.exists(10), true);
    EXPECT_EQ(vec.exists(11), false);
}

TEST(TCountDistributionVector, operator_square_brackets_const){
    const TCountDistributionVector vec(10);
    // outside range
    EXPECT_THROW(vec[100], std::runtime_error);
}

TEST(TCountDistributionVector, fillCombinedDistribution){
    TCountDistributionVector vec(5);
    fillComplicatedDistr(vec);

    // fill with stuff -> should be cleared afterwards
    TCountDistribution combined;
    combined.add(100, 28);

    vec.fillCombinedDistribution(combined);

    EXPECT_EQ(combined[0], 411);
    EXPECT_EQ(combined[1], 900);
    EXPECT_EQ(combined[2], 0);
    EXPECT_EQ(combined[3], 0);
    EXPECT_EQ(combined[4], 0);
    EXPECT_EQ(combined[5], 200);
}

TEST(TCountDistributionVector, RSquared){
    // TODO: can not reproduce R2 in R... (?)
    TCountDistributionVector vec;
    /*vec.add(0, 0, 10);
    vec.add(0, 0, 1);
    vec.add(0, 1, 100);
    vec.add(10, 0, 400);
    vec.add(10, 1, 800);
    vec.add(10, 5, 200);*/

    vec.add(0, 0, 10);
    vec.add(0, 1, 20);
    vec.add(0, 2, 30);
    vec.add(1, 0, 40);
    vec.add(1, 1, 50);
    vec.add(1, 2, 60);
    vec.add(1, 3, 70);

    double RSquared = vec.RSquared();

    EXPECT_FLOAT_EQ(RSquared, 0.02404325);
}

TEST(TCountDistributionVector, writeCombined){
    TCountDistributionVector vec(5);
    fillComplicatedDistr(vec);

    // open output file and write
    TOutputFile out("file.txt");
    vec.writeCombined(out);
    out.close();

    // read and check if expected
    TInputFile in("file.txt", 2);
    std::vector<uint64_t> inVec;
    std::vector<uint64_t> expected = {411, 900, 0, 0, 0, 200};
    int c = 0;
    while (in.read(inVec)){
        EXPECT_EQ(inVec[0], c); EXPECT_EQ(inVec[1], expected[c]);
        c++;
    }
    in.close();
    remove("file.txt");
}

TEST(TCountDistributionVector, writeCombined_name){
    TCountDistributionVector vec(5);
    fillComplicatedDistr(vec);

    // open output file and write
    TOutputFile out("file.txt");
    vec.writeCombined(out, "ID");
    out.close();

    // read and check if expected
    TInputFile in("file.txt", 3);
    std::vector<std::string> inVec;
    std::vector<uint64_t> expected = {411, 900, 0, 0, 0, 200};
    int c = 0;
    while (in.read(inVec)){
        EXPECT_EQ(inVec[0], "ID");
        EXPECT_EQ(inVec[1], toString(c));
        EXPECT_EQ(inVec[2], toString(expected[c]));
        c++;
    }
    in.close();
    remove("file.txt");
}

TEST(TCountDistributionVector, write){
    TCountDistributionVector vec(5);
    fillComplicatedDistr(vec);

    // open output file and write
    TOutputFile out("file.txt");
    vec.write(out);
    out.close();

    // read and check if expected
    TInputFile in("file.txt", 3);
    std::vector<std::string> inVec;

    in.read(inVec);
    EXPECT_EQ(inVec[0], "0");  EXPECT_EQ(inVec[1], "0");
    EXPECT_EQ(inVec[2], toString(11));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "0"); EXPECT_EQ(inVec[1], "1");
    EXPECT_EQ(inVec[2], toString(100));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "10"); EXPECT_EQ(inVec[1], "0");
    EXPECT_EQ(inVec[2], toString(400));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "10"); EXPECT_EQ(inVec[1], "1");
    EXPECT_EQ(inVec[2], toString(800));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "10"); EXPECT_EQ(inVec[1], "2");
    EXPECT_EQ(inVec[2], toString(0));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "10"); EXPECT_EQ(inVec[1], "3");
    EXPECT_EQ(inVec[2], toString(0));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "10"); EXPECT_EQ(inVec[1], "4");
    EXPECT_EQ(inVec[2], toString(0));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "10"); EXPECT_EQ(inVec[1], "5");
    EXPECT_EQ(inVec[2], toString(200));

    in.close();
    remove("file.txt");
}

TEST(TCountDistributionVector, write_filename_labelID_label){
    TCountDistributionVector vec(5);
    fillComplicatedDistr(vec);

    // open output file and write
    vec.write("file.txt",  "ID", "label");

    // read and check if expected
    TInputFile in("file.txt", header);

    std::vector<std::string> header = in.header();
    EXPECT_EQ(header[0], "ID");
    EXPECT_EQ(header[1], "label");
    EXPECT_EQ(header[2], "counts");

    std::vector<std::string> inVec;

    in.read(inVec);
    EXPECT_EQ(inVec[0], "0");  EXPECT_EQ(inVec[1], "0");
    EXPECT_EQ(inVec[2], toString(11));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "0"); EXPECT_EQ(inVec[1], "1");
    EXPECT_EQ(inVec[2], toString(100));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "10"); EXPECT_EQ(inVec[1], "0");
    EXPECT_EQ(inVec[2], toString(400));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "10"); EXPECT_EQ(inVec[1], "1");
    EXPECT_EQ(inVec[2], toString(800));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "10"); EXPECT_EQ(inVec[1], "2");
    EXPECT_EQ(inVec[2], toString(0));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "10"); EXPECT_EQ(inVec[1], "3");
    EXPECT_EQ(inVec[2], toString(0));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "10"); EXPECT_EQ(inVec[1], "4");
    EXPECT_EQ(inVec[2], toString(0));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "10"); EXPECT_EQ(inVec[1], "5");
    EXPECT_EQ(inVec[2], toString(200));

    in.close();
    remove("file.txt");
}

TEST(TCountDistributionVector, write_names){
    TCountDistributionVector vec(5);
    fillComplicatedDistr(vec);

    // open output file and write
    TOutputFile out("file.txt");
    std::vector<std::string> names = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k"};
    vec.write(out,  names);
    out.close();

    // read and check if expected
    TInputFile in("file.txt", 3);
    std::vector<std::string> inVec;

    in.read(inVec);
    EXPECT_EQ(inVec[0], "a");  EXPECT_EQ(inVec[1], "0");
    EXPECT_EQ(inVec[2], toString(11));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "a"); EXPECT_EQ(inVec[1], "1");
    EXPECT_EQ(inVec[2], toString(100));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "k"); EXPECT_EQ(inVec[1], "0");
    EXPECT_EQ(inVec[2], toString(400));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "k"); EXPECT_EQ(inVec[1], "1");
    EXPECT_EQ(inVec[2], toString(800));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "k"); EXPECT_EQ(inVec[1], "2");
    EXPECT_EQ(inVec[2], toString(0));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "k"); EXPECT_EQ(inVec[1], "3");
    EXPECT_EQ(inVec[2], toString(0));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "k"); EXPECT_EQ(inVec[1], "4");
    EXPECT_EQ(inVec[2], toString(0));

    in.read(inVec);
    EXPECT_EQ(inVec[0], "k"); EXPECT_EQ(inVec[1], "5");
    EXPECT_EQ(inVec[2], toString(200));

    in.close();
    remove("file.txt");
}

TEST(TCountDistributionVector, writeAsMatrix_filename_labelID_label){
    TCountDistributionVector vec(5);
    fillComplicatedDistr(vec);

    // open output file and write
    vec.writeAsMatrix("file.txt", "ID", "label");

    // read and check if expected
    TInputFile in("file.txt", header);

    std::vector<std::string> inVec;
    std::vector<std::string> header = in.header();
    EXPECT_EQ(header[0], "ID/label");
    EXPECT_EQ(header[1], "0"); EXPECT_EQ(header[2], "1"); EXPECT_EQ(header[3], "2"); EXPECT_EQ(header[4], "3"); EXPECT_EQ(header[5], "4");  EXPECT_EQ(header[6], "5");

    in.read(inVec);
    EXPECT_EQ(inVec[0], "0");
    EXPECT_EQ(inVec[1], toString(11));
    EXPECT_EQ(inVec[2], toString(100));
    EXPECT_EQ(inVec[3], toString(0));
    EXPECT_EQ(inVec[4], toString(0));
    EXPECT_EQ(inVec[5], toString(0));
    EXPECT_EQ(inVec[6], toString(0));

    for (int i = 1; i < 10; i++){
        in.read(inVec);
        EXPECT_EQ(inVec[0], toString(i));
        EXPECT_EQ(inVec[1], toString(0));
        EXPECT_EQ(inVec[2], toString(0));
        EXPECT_EQ(inVec[3], toString(0));
        EXPECT_EQ(inVec[4], toString(0));
        EXPECT_EQ(inVec[5], toString(0));
        EXPECT_EQ(inVec[6], toString(0));
    }

    in.read(inVec);
    EXPECT_EQ(inVec[0], "10");
    EXPECT_EQ(inVec[1], toString(400));
    EXPECT_EQ(inVec[2], toString(800));
    EXPECT_EQ(inVec[3], toString(0));
    EXPECT_EQ(inVec[4], toString(0));
    EXPECT_EQ(inVec[5], toString(0));
    EXPECT_EQ(inVec[6], toString(200));

    in.close();
    remove("file.txt");
}

TEST(TCountDistributionVector, writeAsMatrix_filename_labelID_valueNames){
    TCountDistributionVector vec(5);
    fillComplicatedDistr(vec);

    // open output file and write
    std::vector<std::string> valNames =  {"a", "b", "c", "d", "e", "f"};
    vec.writeAsMatrix("file.txt", "ID", valNames);

    // read and check if expected
    TInputFile in("file.txt", header);

    std::vector<std::string> inVec;
    std::vector<std::string> header = in.header();
    EXPECT_EQ(header[0], "ID");
    EXPECT_EQ(header[1], "a"); EXPECT_EQ(header[2], "b"); EXPECT_EQ(header[3], "c"); EXPECT_EQ(header[4], "d"); EXPECT_EQ(header[5], "e"); EXPECT_EQ(header[6], "f");

    in.read(inVec);
    EXPECT_EQ(inVec[0], "0");
    EXPECT_EQ(inVec[1], toString(11));
    EXPECT_EQ(inVec[2], toString(100));
    EXPECT_EQ(inVec[3], toString(0));
    EXPECT_EQ(inVec[4], toString(0));
    EXPECT_EQ(inVec[5], toString(0));
    EXPECT_EQ(inVec[6], toString(0));

    for (int i = 1; i < 10; i++){
        in.read(inVec);
        EXPECT_EQ(inVec[0], toString(i));
        EXPECT_EQ(inVec[1], toString(0));
        EXPECT_EQ(inVec[2], toString(0));
        EXPECT_EQ(inVec[3], toString(0));
        EXPECT_EQ(inVec[4], toString(0));
        EXPECT_EQ(inVec[5], toString(0));
        EXPECT_EQ(inVec[6], toString(0));
    }

    in.read(inVec);
    EXPECT_EQ(inVec[0], "10");
    EXPECT_EQ(inVec[1], toString(400));
    EXPECT_EQ(inVec[2], toString(800));
    EXPECT_EQ(inVec[3], toString(0));
    EXPECT_EQ(inVec[4], toString(0));
    EXPECT_EQ(inVec[5], toString(0));
    EXPECT_EQ(inVec[6], toString(200));

    in.close();
    remove("file.txt");
}

//----------------------------------------------
// TCountDistributionMap
//----------------------------------------------

TEST(TCountDistributionMap, add){
    TCountDistributionMap<uint64_t> map;

    map.add(0, 0);
    map.add(0, 1);
    map.add(0, 1);
    map.add(10, 5);

    // ID = 0
    EXPECT_EQ(map[0][0], 1);
    EXPECT_EQ(map[0][1], 2);
    // ID = 10
    EXPECT_EQ(map[10][0], 0);
    EXPECT_EQ(map[10][1], 0);
    EXPECT_EQ(map[10][2], 0);
    EXPECT_EQ(map[10][3], 0);
    EXPECT_EQ(map[10][4], 0);
    EXPECT_EQ(map[10][5], 1);
}

TEST(TCountDistributionMap, size){
    TCountDistributionMap<uint64_t> map;

    map.add(0, 0);
    map.add(0, 1);
    map.add(0, 1);
    map.add(10, 5);

    EXPECT_EQ(map.size(), 2);
}

TEST(TCountDistributionMap, counts){
    TCountDistributionMap<uint64_t> map;

    map.add(0, 0);
    map.add(0, 1);
    map.add(0, 1);
    map.add(10, 5);

    EXPECT_EQ(map.counts(), 4);
}

TEST(TCountDistributionMap, sum){
    TCountDistributionMap<uint64_t> map;

    map.add(0, 0);
    map.add(0, 1);
    map.add(0, 1);
    map.add(10, 5);

    EXPECT_EQ(map.sum(), 7);
}

TEST(TCountDistributionMap, mean){
    TCountDistributionMap<uint64_t> map;

    map.add(0, 0);
    map.add(0, 1);
    map.add(0, 1);
    map.add(10, 5);

    EXPECT_EQ(map.mean(), 1.75);
}

TEST(TCountDistributionMap, exists){
    TCountDistributionMap<uint64_t> map;

    map.add(0, 0);
    map.add(0, 1);
    map.add(0, 1);
    map.add(10, 5);

    EXPECT_EQ(map.exists(0), true);
    EXPECT_EQ(map.exists(10), true);
    EXPECT_EQ(map.exists(1), false);
}

TEST(TCountDistributionMap, operator_square_brackets){
    TCountDistributionMap<uint64_t> map;

    map.add(0, 0);
    map.add(0, 1);
    map.add(0, 1);
    map.add(10, 5);

    EXPECT_EQ(map[0][0], 1);
    EXPECT_EQ(map[0][1], 2);
    EXPECT_EQ(map[10][5], 1);
    EXPECT_THROW(map[1][0], std::runtime_error);
}


//--------------------------------------------
// TMeanVar
//--------------------------------------------

TEST(TMeanVarTest, all){
    TMeanVar<double> meanVar;

    // add
    meanVar.add(1.);
    meanVar.add(2.);
    meanVar.add(0.83);
    meanVar.add(-102.9);
    meanVar.add(0.);
    meanVar.add(100.293);

    EXPECT_EQ(meanVar.counts(), 6);
    EXPECT_FLOAT_EQ(meanVar.sum(), 1.223);
    EXPECT_FLOAT_EQ(meanVar.mean(), 0.2038333);
    EXPECT_FLOAT_EQ(meanVar.sd(), 58.66932);
    EXPECT_FLOAT_EQ(meanVar.variance(),  3442.089);

    // add from other
    TMeanVar<double> meanVar2;
    meanVar2.add(meanVar);

    EXPECT_EQ(meanVar.counts(), 6);
    EXPECT_FLOAT_EQ(meanVar.sum(), 1.223);
    EXPECT_FLOAT_EQ(meanVar.mean(), 0.2038333);
    EXPECT_FLOAT_EQ(meanVar.sd(), 58.66932);
    EXPECT_FLOAT_EQ(meanVar.variance(),  3442.089);

    // clear
    meanVar.clear();
    EXPECT_EQ(meanVar.counts(), 0);
    EXPECT_EQ(meanVar.sum(), 0.);
    EXPECT_EQ(meanVar.mean(), 0.);
    EXPECT_EQ(meanVar.sd(), 0.);
    EXPECT_EQ(meanVar.variance(), 0.);
}

TEST(TMeanVarTest, double_overflow){
    TMeanVar<double> meanVar;

    // overflow
    double limit = std::numeric_limits<double>::max();
    // add x = sqrt(limit) such that x^2 = sumOfSquares = limit
    meanVar.add(sqrt(limit));
    // add one thing more -> too big in order to be stored -> throw
    EXPECT_THROW(meanVar.add(std::nextafter(limit, 0.)), std::runtime_error);
}

TEST(TMeanVarTest, int_overflow){
    TMeanVar<int> meanVar;

    // overflow
    int limit = std::numeric_limits<int>::max();
    // add x = sqrt(limit) such that x^2 = sumOfSquares = limit (not exactly, as sqrt(limit) is floating-point and cut off to an integer
    // -> distance to limit is then 88047 -> sqrt(88047) = 296.72)
    meanVar.add(sqrt(limit));
    // add one thing more -> too big in order to be stored -> throw
    EXPECT_THROW(meanVar.add(297), std::runtime_error);
}

TEST(TMeanVarTest, uint8_t_overflow){
    TMeanVar<uint8_t> meanVar;

    // overflow
    uint8_t limit = std::numeric_limits<uint8_t>::max();
    // add x = sqrt(limit) such that x^2 = sumOfSquares = limit (not exactly, as sqrt(limit) is floating-point and cut off to an integer
    // -> distance to limit is then 30 -> sqrt(30) = 5.47)
    meanVar.add(sqrt(limit));
    // add one thing more -> too big in order to be stored -> throw
    EXPECT_THROW(meanVar.add(6), std::runtime_error);
}

//--------------------------------------------
// TMeanVarVector
//--------------------------------------------

TEST(TMeanVarVectorTest, all){
    TMeanVarVector<double> meanVarVec;

    // add
    meanVarVec.add(0, 1.);
    meanVarVec.add(0, 2.);
    meanVarVec.add(0, 0.83);
    meanVarVec.add(2, -102.9);
    meanVarVec.add(2, 0.);
    meanVarVec.add(2, 100.293);

    // size()
    EXPECT_EQ(meanVarVec.size(), 3);
    // sum()
    EXPECT_FLOAT_EQ(meanVarVec.sum(), 1.223);
    // exists()
    EXPECT_EQ(meanVarVec.exists(0), true);
    EXPECT_EQ(meanVarVec.exists(1), true);
    EXPECT_EQ(meanVarVec.exists(2), true);
    EXPECT_EQ(meanVarVec.exists(3), false);

    // []
    EXPECT_FLOAT_EQ(meanVarVec[0].mean(), 1.276667);
    EXPECT_FLOAT_EQ(meanVarVec[1].mean(), 0);
    EXPECT_FLOAT_EQ(meanVarVec[2].mean(),  -0.869);
    EXPECT_THROW(meanVarVec[3], std::runtime_error);

    // clear
    meanVarVec.clear();
    EXPECT_EQ(meanVarVec.exists(0), false);
    EXPECT_EQ(meanVarVec.exists(1), false);
    EXPECT_EQ(meanVarVec.exists(2), false);
}

// just test for uint8_t, as for other types, it takes ages to reach min/max if we can only add sqrt(limit) per entry in the vector
TEST(TMeanVarVectorTest, uint8_t_overflow) {
    TMeanVarVector<uint8_t> meanVarVec;

    // add
    uint8_t limit = std::numeric_limits<uint8_t>::max();
    for (uint8_t i = 0; i < 17; i++){
        meanVarVec.add(i, sqrt(limit)); // sqrt(limit) = 15 -> 15*17 = 255 = numeric_max
    }

    // sum is exactly numeric max -> ok
    EXPECT_NO_THROW(meanVarVec.sum());

    // add more -> sum is above numeric max -> throw
    meanVarVec.add(17, sqrt(limit));
    EXPECT_THROW(meanVarVec.sum(), std::runtime_error);
}

//--------------------------------------------
// TMeanVarVector
//--------------------------------------------

TEST(TMeanVarMapTest, all){
    TMeanVarMap<uint64_t, double> meanVarMap;

    // add
    meanVarMap.add(0, 1.);
    meanVarMap.add(0, 2.);
    meanVarMap.add(0, 0.83);
    meanVarMap.add(2, -102.9);
    meanVarMap.add(2, 0.);
    meanVarMap.add(2, 100.293);

    // size()
    EXPECT_EQ(meanVarMap.size(), 2);
    // sum()
    EXPECT_FLOAT_EQ(meanVarMap.sum(), 1.223);
    // exists()
    EXPECT_EQ(meanVarMap.exists(0), true);
    EXPECT_EQ(meanVarMap.exists(1), false);
    EXPECT_EQ(meanVarMap.exists(2), true);
    EXPECT_EQ(meanVarMap.exists(3), false);

    // []
    EXPECT_FLOAT_EQ(meanVarMap[0].mean(), 1.276667);
    EXPECT_FLOAT_EQ(meanVarMap[2].mean(),  -0.869);
    EXPECT_THROW(meanVarMap[1], std::runtime_error);
    EXPECT_THROW(meanVarMap[3], std::runtime_error);

    // clear
    meanVarMap.clear();
    EXPECT_EQ(meanVarMap.exists(0), false);
    EXPECT_EQ(meanVarMap.exists(1), false);
    EXPECT_EQ(meanVarMap.exists(2), false);
}

// just test for uint8_t, as for other types, it takes ages to reach min/max if we can only add sqrt(limit) per entry in the vector
TEST(TMeanVarMapTest, uint8_t_overflow) {
    TMeanVarMap<uint64_t, uint8_t> meanVarVec;

    // add
    uint8_t limit = std::numeric_limits<uint8_t>::max();
    for (uint8_t i = 0; i < 17; i++){
        meanVarVec.add(i, sqrt(limit)); // sqrt(limit) = 15 -> 15*17 = 255 = numeric_max
    }

    // sum is exactly numeric max -> ok
    EXPECT_NO_THROW(meanVarVec.sum());

    // add more -> sum is above numeric max -> throw
    meanVarVec.add(17, sqrt(limit));
    EXPECT_THROW(meanVarVec.sum(), std::runtime_error);
}

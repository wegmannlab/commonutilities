//
// Created by caduffm on 6/15/20.
//

#include "TestCase.h"
#include "TFile.h"

//-------------------------------------------------------
// constructor

template <typename T> class ConstructorNoParams : public ::testing::Test {
public:
    T file;
};

using MyTypes = ::testing::Types<TOutputFile, TInputFile>;
TYPED_TEST_SUITE(ConstructorNoParams, MyTypes);

TYPED_TEST(ConstructorNoParams, initializesCorrectly) {
    // check that all member variables are correctly initialized
    EXPECT_FALSE(this->file.isOpen());
    EXPECT_EQ(this->file.curCol(), 0);
    EXPECT_EQ(this->file.lineNumber(), 0);
    EXPECT_EQ(this->file.name(), "");
    EXPECT_FALSE(this->file.isTable());
    EXPECT_EQ(this->file.numCols(), 0);
    EXPECT_EQ(this->file.delim(), "");
    EXPECT_EQ(this->file.type(), variable);
    EXPECT_THROW({try {this->file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(ConstructorWithName, initializesCorrectly) {
    // check that all member variables are correctly initialized
    TOutputFile file("file1.txt");
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_FALSE(file.isTable());
    EXPECT_EQ(file.numCols(), 0);
    EXPECT_EQ(file.delim(), "\t");
    EXPECT_EQ(file.type(), variable);
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(ConstructorWithNameAndDelim, initializesCorrectly) {
    // check that all member variables are correctly initialized
    TOutputFile file("file1.txt", ' ');
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_FALSE(file.isTable());
    EXPECT_EQ(file.numCols(), 0);
    EXPECT_EQ(file.delim(), " ");
    EXPECT_EQ(file.type(), variable);
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(ConstructorWithName_Variable, initializesCorrectly) {
    // check that all member variables are correctly initialized
    TInputFile file("file1.txt", variable);
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_FALSE(file.isTable());
    EXPECT_EQ(file.numCols(), 0);
    EXPECT_EQ(file.delim(), "\t ");
    EXPECT_EQ(file.type(), variable);
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(ConstructorWithNameAndDelim_Variable, initializesCorrectly) {
    // check that all member variables are correctly initialized
    TInputFile file("file1.txt", variable, " ");
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_FALSE(file.isTable());
    EXPECT_EQ(file.numCols(), 0);
    EXPECT_EQ(file.delim(), " ");
    EXPECT_EQ(file.type(), variable);
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(ConstructorWithNameAndNumCols_input, initializesCorrectly) {
    // check that all member variables are correctly initialized
    TInputFile * file = new TInputFile("file1.txt", 3);
    EXPECT_TRUE(file->isOpen());
    EXPECT_EQ(file->curCol(), 0);
    EXPECT_EQ(file->lineNumber(), 0);
    EXPECT_EQ(file->name(), "file1.txt");
    EXPECT_TRUE(file->isTable());
    EXPECT_EQ(file->numCols(), 3);
    EXPECT_EQ(file->delim(), "\t ");
    EXPECT_EQ(file->type(), fixed);
    EXPECT_THROW({try {file->headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(ConstructorWithNameAndNumCols_output, initializesCorrectly) {
    // check that all member variables are correctly initialized
    TOutputFile * file = new TOutputFile("file1.txt", 3);
    EXPECT_TRUE(file->isOpen());
    EXPECT_EQ(file->curCol(), 0);
    EXPECT_EQ(file->lineNumber(), 0);
    EXPECT_EQ(file->name(), "file1.txt");
    EXPECT_TRUE(file->isTable());
    EXPECT_EQ(file->numCols(), 3);
    EXPECT_EQ(file->delim(), "\t");
    EXPECT_EQ(file->type(), fixed);
    EXPECT_THROW({try {file->headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(ConstructorWithNameAndNumColsAndDelim_input, initializesCorrectly) {
    // check that all member variables are correctly initialized
    TInputFile * file = new TInputFile("file1.txt", 3, " ");
    EXPECT_TRUE(file->isOpen());
    EXPECT_EQ(file->curCol(), 0);
    EXPECT_EQ(file->lineNumber(), 0);
    EXPECT_EQ(file->name(), "file1.txt");
    EXPECT_TRUE(file->isTable());
    EXPECT_EQ(file->numCols(), 3);
    EXPECT_EQ(file->delim(), " ");
    EXPECT_EQ(file->type(), fixed);
    EXPECT_THROW({try {file->headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(ConstructorWithNameAndNumColsAndDelim_output, initializesCorrectly) {
    // check that all member variables are correctly initialized
    TOutputFile * file = new TOutputFile("file1.txt", 3, ' ');
    EXPECT_TRUE(file->isOpen());
    EXPECT_EQ(file->curCol(), 0);
    EXPECT_EQ(file->lineNumber(), 0);
    EXPECT_EQ(file->name(), "file1.txt");
    EXPECT_TRUE(file->isTable());
    EXPECT_EQ(file->numCols(), 3);
    EXPECT_EQ(file->delim(), " ");
    EXPECT_EQ(file->type(), fixed);
    EXPECT_THROW({try {file->headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(ConstructorWithNameHeaderOutput, initializesCorrectly) {
    // check that all member variables are correctly initialized
    std::vector<std::string> header = {"one", "two", "three"};
    TOutputFile file("file1.txt", header);
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 1);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.delim(), "\t");
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.headerAt(1), "two");
    EXPECT_EQ(file.headerAt(2), "three");
    EXPECT_THROW({try {file.headerAt(3);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), TFile_Filetype::header);
}

TEST(ConstructorWithNameHeaderInput, initializesCorrectly) {
    // check that all member variables are correctly initialized
    // reads file created before by TEST(ConstructorWithNameHeaderOutput, initializesCorrectly)
    TInputFile file("file1.txt", header);
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 1);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.delim(), "\t ");
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.headerAt(1), "two");
    EXPECT_EQ(file.headerAt(2), "three");
    EXPECT_THROW({try {file.headerAt(3);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), header);
}

TEST(ConstructorWithNameNoHeaderInput, initializesCorrectly) {
    // check that all member variables are correctly initialized
    // reads file created before by TEST(ConstructorWithNameHeaderOutput, initializesCorrectly)
    TInputFile file("file1.txt", fixed);
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.delim(), "\t ");
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), fixed);
}

TEST(ConstructorWithNameHeaderAndDelimOutput, initializesCorrectly) {
    // check that all member variables are correctly initialized
    std::vector<std::string> header = {"one", "two", "three"};
    TOutputFile file("file1.txt", header, ' ');
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 1);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.delim(), " ");
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.headerAt(1), "two");
    EXPECT_EQ(file.headerAt(2), "three");
    EXPECT_THROW({try {file.headerAt(3);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), TFile_Filetype::header);
}

TEST(ConstructorWithNameHeaderAndDelimInput, initializesCorrectly) {
    // check that all member variables are correctly initialized
    TInputFile file("file1.txt", header, " ");
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 1);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.delim(), " ");
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.headerAt(1), "two");
    EXPECT_EQ(file.headerAt(2), "three");
    EXPECT_THROW({try {file.headerAt(3);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), header);
}

TEST(ConstructorWithNameNoHeaderAndDelimInput, initializesCorrectly) {
    // check that all member variables are correctly initialized
    TInputFile file("file1.txt", fixed, " ");
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.delim(), " ");
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), fixed);
}

//-------------------------------------------------------
// default constructor, then open

TEST(ConstructorNoParams_openWithName_Output, initializesCorrectly) {
    TOutputFile file;
    file.open("file1.txt");
    // check that all member variables are correctly initialized
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_FALSE(file.isTable());
    EXPECT_EQ(file.numCols(), 0);
    EXPECT_EQ(file.delim(), "\t");
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), variable);
}

TEST(ConstructorNoParams_openWithNameAndDelim_Output, initializesCorrectly) {
    TOutputFile file;
    file.open("file1.txt", ' ');
    // check that all member variables are correctly initialized
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_FALSE(file.isTable());
    EXPECT_EQ(file.numCols(), 0);
    EXPECT_EQ(file.delim(), " ");
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), variable);
}

TEST(ConstructorNoParams_openWithNameAndVariable_Input, initializesCorrectly) {
    TInputFile file;
    file.open("file1.txt", variable);
    // check that all member variables are correctly initialized
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_FALSE(file.isTable());
    EXPECT_EQ(file.numCols(), 0);
    EXPECT_EQ(file.delim(), "\t ");
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), variable);
}

TEST(ConstructorNoParams_openWithNameAndVariableAndDelim_Input, initializesCorrectly) {
    TInputFile file;
    file.open("file1.txt", variable, " ");
    // check that all member variables are correctly initialized
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_FALSE(file.isTable());
    EXPECT_EQ(file.numCols(), 0);
    EXPECT_EQ(file.delim(), " ");
    EXPECT_THROW({ try { file.headerAt(0); } catch (...) { throw std::runtime_error("blah"); }}, std::runtime_error);
    EXPECT_EQ(file.type(), variable);
}

TEST(ConstructorNoParams_openWithNameAndNumCols_input, initializesCorrectly) {
    TInputFile file;
    file.open("file1.txt", 3);
    // check that all member variables are correctly initialized
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.delim(), "\t ");
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), fixed);
}

TEST(ConstructorNoParams_openWithNameAndNumCols_output, initializesCorrectly) {
    TOutputFile file;
    file.open("file1.txt", 3);
    // check that all member variables are correctly initialized
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.delim(), "\t");
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), fixed);
}

TEST(ConstructorNoParams_openWithNameAndNumColsAndDelim_input, initializesCorrectly) {
    TInputFile * file = new TInputFile;
    file->open("file1.txt", 3, " ");
    // check that all member variables are correctly initialized
    EXPECT_TRUE(file->isOpen());
    EXPECT_EQ(file->curCol(), 0);
    EXPECT_EQ(file->lineNumber(), 0);
    EXPECT_EQ(file->name(), "file1.txt");
    EXPECT_TRUE(file->isTable());
    EXPECT_EQ(file->numCols(), 3);
    EXPECT_EQ(file->delim(), " ");
    EXPECT_THROW({try {file->headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file->type(), fixed);
}

TEST(ConstructorNoParams_openWithNameAndNumColsAndDelim_output, initializesCorrectly) {
    TOutputFile * file = new TOutputFile;
    file->open("file1.txt", 3, ' ');
    // check that all member variables are correctly initialized
    EXPECT_TRUE(file->isOpen());
    EXPECT_EQ(file->curCol(), 0);
    EXPECT_EQ(file->lineNumber(), 0);
    EXPECT_EQ(file->name(), "file1.txt");
    EXPECT_TRUE(file->isTable());
    EXPECT_EQ(file->numCols(), 3);
    EXPECT_EQ(file->delim(), " ");
    EXPECT_THROW({try {file->headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file->type(), fixed);
}

TEST(ConstructorNoParams_openWithNameAndHeader_Output, initializesCorrectly) {
    // check that all member variables are correctly initialized
    std::vector<std::string> header = {"one", "two", "three"};
    TOutputFile file;
    file.open("file1.txt", header);
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 1);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.delim(), "\t");
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.headerAt(1), "two");
    EXPECT_EQ(file.headerAt(2), "three");
    EXPECT_THROW({try {file.headerAt(3);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), TFile_Filetype::header);
}

TEST(ConstructorNoParams_openWithNameAndHeader_Input, initializesCorrectly) {
    // check that all member variables are correctly initialized
    // reads file created before by TEST(ConstructorWithNameHeaderOutput, initializesCorrectly)
    TInputFile file;
    file.open("file1.txt", header);
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 1);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.delim(), "\t ");
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.headerAt(1), "two");
    EXPECT_EQ(file.headerAt(2), "three");
    EXPECT_THROW({try {file.headerAt(3);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), header);
}

TEST(ConstructorNoParams_openWithNameAndNoHeader_Input, initializesCorrectly) {
    // check that all member variables are correctly initialized
    // reads file created before by TEST(ConstructorWithNameHeaderOutput, initializesCorrectly)
    TInputFile file;
    file.open("file1.txt", fixed);
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.delim(), "\t ");
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), fixed);
}

TEST(ConstructorNoParams_openWithNameAndHeaderAndDelim_Output, initializesCorrectly) {
    // check that all member variables are correctly initialized
    std::vector<std::string> header = {"one", "two", "three"};
    TOutputFile file;
    file.open("file1.txt", header, ' ');
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 1);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.delim(), " ");
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.headerAt(1), "two");
    EXPECT_EQ(file.headerAt(2), "three");
    EXPECT_THROW({try {file.headerAt(3);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), TFile_Filetype::header);
}

TEST(ConstructorNoParams_openWithNameAndHeaderAndDelim_Input, initializesCorrectly) {
    // check that all member variables are correctly initialized
    TInputFile file;
    file.open("file1.txt", header, " ");
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 1);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.delim(), " ");
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.headerAt(1), "two");
    EXPECT_EQ(file.headerAt(2), "three");
    EXPECT_THROW({try {file.headerAt(3);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), header);
}

TEST(ConstructorNoParams_openWithNameAndNoHeaderAndDelim_Input, initializesCorrectly) {
    // check that all member variables are correctly initialized
    TInputFile file;
    file.open("file1.txt", fixed, " ");
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.delim(), " ");
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), fixed);
}

//-------------------------------------------------------
// parameterized constructor, then open -> should throw error

TEST(ConstructorWithParams_open_Output, throwsCorrectly) {
    // go over all possible combinations of constructors (with parameters) and open()
    std::vector<TOutputFile * > outputFiles;
    std::vector<std::string> header = {"one", "two", "three"};

    // all possible constructors (with parameters)
    outputFiles.push_back(new TOutputFile("file1.txt"));
    outputFiles.push_back(new TOutputFile("file1.txt", ' '));
    outputFiles.push_back(new TOutputFile("file1.txt", 3));
    outputFiles.push_back(new TOutputFile("file1.txt", 3, ' '));
    outputFiles.push_back(new TOutputFile("file1.txt", header));
    outputFiles.push_back(new TOutputFile("file1.txt", header, ' '));

    for (auto & it : outputFiles){
        // all possible open()
        EXPECT_THROW(it->open("file1.txt"), std::runtime_error);
        EXPECT_THROW(it->open("file1.txt", ' '), std::runtime_error);
        EXPECT_THROW(it->open("file1.txt", 3), std::runtime_error);
        EXPECT_THROW(it->open("file1.txt", 3, ' '), std::runtime_error);
        EXPECT_THROW(it->open("file1.txt", header), std::runtime_error);
        EXPECT_THROW(it->open("file1.txt", header, ' '), std::runtime_error);
        delete it;
    }
}

TEST(ConstructorWithParams_open_Input, throwsCorrectly) {
    // go over all possible combinations of constructors (with parameters) and open()
    std::vector<TInputFile * > inputFiles;

    // all possible constructors (with parameters)
    inputFiles.push_back(new TInputFile("file1.txt", variable));
    inputFiles.push_back(new TInputFile("file1.txt", variable, " "));
    inputFiles.push_back(new TInputFile("file1.txt", fixed));
    inputFiles.push_back(new TInputFile("file1.txt", fixed, " "));
    inputFiles.push_back(new TInputFile("file1.txt", header));
    inputFiles.push_back(new TInputFile("file1.txt", header, " "));
    inputFiles.push_back(new TInputFile("file1.txt", 3));
    inputFiles.push_back(new TInputFile("file1.txt", 3, " "));

    for (auto & it : inputFiles){
        // all possible open()
        EXPECT_THROW(it->open("file1.txt", variable), std::runtime_error);
        EXPECT_THROW(it->open("file1.txt", variable, " "), std::runtime_error);
        EXPECT_THROW(it->open("file1.txt", fixed), std::runtime_error);
        EXPECT_THROW(it->open("file1.txt", fixed, " "), std::runtime_error);
        EXPECT_THROW(it->open("file1.txt", header), std::runtime_error);
        EXPECT_THROW(it->open("file1.txt", header, " "), std::runtime_error);
        EXPECT_THROW(it->open("file1.txt", 3), std::runtime_error);
        EXPECT_THROW(it->open("file1.txt", 3, " "), std::runtime_error);
        delete it;
    }
}

//-------------------------------------------------------
// open - close - open

TEST(OpenClose, openopen_Output) {
    std::vector<std::string> header = {"one", "two", "three"};
    TOutputFile file("file1.txt", header, ' ');
    EXPECT_THROW(file.open("file1.txt", header, ' '), std::runtime_error);
}

TEST(OpenClose, openopen_Input) {
    TInputFile file("file1.txt", header, " ");
    EXPECT_THROW(file.open("file1.txt", header, " "), std::runtime_error);
}

TEST(OpenClose, openCloseOpen_Output) {
    // will overwrite stuff of first open
    std::vector<std::string> header = {"one", "two", "three"};
    TOutputFile file("file1.txt", header, ' ');
    // all member variables are correctly initialized
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 1);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.delim(), " ");
    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.type(), TFile_Filetype::header);

    // now close
    file.close();

    EXPECT_FALSE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 1);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.delim(), " ");
    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.type(), TFile_Filetype::header);

    // now open again, no header -> will overwrite previous stuff
    file.open("file1.txt");
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_FALSE(file.isTable());
    EXPECT_EQ(file.numCols(), 0);
    EXPECT_EQ(file.delim(), "\t");
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), variable);
}

TEST(OpenClose, openCloseOpen_Input) {
    TInputFile file("file1.txt", 3, " ");
    // all member variables are correctly initialized
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.delim(), " ");
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), fixed);

    // now close
    file.close();

    EXPECT_FALSE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.delim(), " ");
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), fixed);

    // now open again
    file.open("file1.txt", variable);
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_FALSE(file.isTable());
    EXPECT_EQ(file.numCols(), 0);
    EXPECT_EQ(file.delim(), "\t ");
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), variable);
}

TEST(OpenClose, closeOpenDifferentName_Output) {
    std::vector<std::string> header = {"one", "two", "three"};
    TOutputFile file("file1.txt", header, ' ');
    file.close();

    file.open("file2.txt");
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file2.txt");
    EXPECT_FALSE(file.isTable());
    EXPECT_EQ(file.numCols(), 0);
    EXPECT_EQ(file.delim(), "\t");
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), variable);
}

TEST(OpenClose, closeOpenDifferentName_Input) {
    TInputFile file("file1.txt", 3, " ");
    file.close();

    file.open("file2.txt", variable);
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file2.txt");
    EXPECT_FALSE(file.isTable());
    EXPECT_EQ(file.numCols(), 0);
    EXPECT_EQ(file.delim(), "\t ");
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), variable);
}

//-------------------------------------------------------
// open - close - reOpen

TEST(OpenClose, openCloseReOpen_Output) {
    // will overwrite stuff of first open
    std::vector<std::string> header = {"one", "two", "three"};
    TOutputFile file("file1.txt", header, ' ');
    // all member variables are correctly initialized
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 1);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.delim(), " ");
    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.type(), TFile_Filetype::header);

    // now close
    file.close();

    EXPECT_FALSE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 1);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.delim(), " ");
    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.type(), TFile_Filetype::header);

    // now open again -> will jump to the end without overwriting
    file.reOpen();
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 1);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.delim(), " ");
    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.type(), TFile_Filetype::header);
}

TEST(OpenClose, openCloseReOpen_Input) {
    TInputFile file("file1.txt", header, " ");
    // all member variables are correctly initialized
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 1);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.delim(), " ");
    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.type(), header);

    // now close
    file.close();

    EXPECT_FALSE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 1);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.delim(), " ");
    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.type(), header);

    // now open again
    file.reOpen();
    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 1);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.delim(), " ");
    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.type(), header);
}

//-------------------------------------------------------
// write headers (output)

TEST(writeHeader, noName) {
    // file not opened -> cannot write header / set number of columns
    TOutputFile file1;
    std::vector<std::string> header = {"one", "two", "three"};
    EXPECT_THROW(file1.writeHeader(header), std::runtime_error);
    TOutputFile file2;
    EXPECT_THROW(file2.noHeader(3), std::runtime_error);
}

TEST(writeHeader, onlyNameWriteHeader) {
    TOutputFile file("file1.txt");

    std::vector<std::string> header = {"one", "two", "three"};
    file.writeHeader(header);

    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 1);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.delim(), "\t");
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.headerAt(1), "two");
    EXPECT_EQ(file.headerAt(2), "three");
    EXPECT_THROW({try {file.headerAt(3);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), TFile_Filetype::header);
}

TEST(writeHeader, onlyNameNoHeader) {
    TOutputFile file("file1.txt");

    file.noHeader(3);

    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.delim(), "\t");
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), fixed);
}

TEST(writeHeader, nameAndNumColsNoHeader) {
    // ok if number of columns is the same
    TOutputFile file("file1.txt", 3);

    file.noHeader(3);

    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 0);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.delim(), "\t");
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_THROW({try {file.headerAt(0);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), fixed);
}

TEST(writeHeader, nameAndNumColsNoHeaderThrow) {
    // throw if number of columns is not the same
    TOutputFile file("file1.txt", 3);
    EXPECT_THROW(file.noHeader(1), std::runtime_error);
}

TEST(writeHeader, nameAndNumColsWriteHeader) {
    TOutputFile file("file1.txt", 3);

    std::vector<std::string> header = {"one", "two", "three"};
    file.writeHeader(header);

    EXPECT_TRUE(file.isOpen());
    EXPECT_EQ(file.curCol(), 0);
    EXPECT_EQ(file.lineNumber(), 1);
    EXPECT_EQ(file.name(), "file1.txt");
    EXPECT_TRUE(file.isTable());
    EXPECT_EQ(file.delim(), "\t");
    EXPECT_EQ(file.numCols(), 3);
    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.headerAt(1), "two");
    EXPECT_EQ(file.headerAt(2), "three");
    EXPECT_THROW({try {file.headerAt(3);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_EQ(file.type(), TFile_Filetype::header);
}

TEST(writeHeader, nameAndNumColsWriteHeaderThrow) {
    // throw if number of columns is not the same
    TOutputFile file("file1.txt", 3);
    std::vector<std::string> header = {"one", "two"};
    EXPECT_THROW( file.writeHeader(header), std::runtime_error);
}

TEST(writeHeader, headerAlreadyWritten) {
    // header already written -> cannot write header again / set number of columns
    std::vector<std::string> header = {"one", "two", "three"};
    TOutputFile file1("file1.txt", header);
    EXPECT_THROW(file1.writeHeader(header), std::runtime_error);
    TOutputFile file2("file1.txt", header);
    EXPECT_THROW(file2.noHeader(3), std::runtime_error);
}

TEST(writeHeader, indexColWithName) {
    TOutputFile file("file1.txt");

    std::vector<std::string> header = {"one", "two", "three"};
    file.writeHeader(header);

    EXPECT_EQ(file.headerAt(0), "one");
    EXPECT_EQ(file.headerAt(1), "two");
    EXPECT_EQ(file.headerAt(2), "three");
    EXPECT_THROW({try {file.headerAt(3);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);

    EXPECT_EQ(file.getIndexOfColname("one"), 0);
    EXPECT_EQ(file.getIndexOfColname("two"), 1);
    EXPECT_EQ(file.getIndexOfColname("three"), 2);
    EXPECT_THROW({try {file.getIndexOfColname("four");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(writeHeader, indexColWithName_fixed) {
    TOutputFile file("file1.txt", 3);

    EXPECT_THROW({try {file.getIndexOfColname("one");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(writeHeader, indexColWithName_variable) {
    TOutputFile file("file1.txt");

    EXPECT_THROW({try {file.getIndexOfColname("one");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

//-------------------------------------------------------
// write (output) and read (input) --> Integration tests

struct WriteOutputFile_Variable {
public:
    TOutputFile file;
    std::vector< std::vector<std::string> > input;
    explicit WriteOutputFile_Variable(const std::string &name, const char& delim) {
        // generate input
        input = {{"1", "2.1568", "three"},
            {"four", "5.273", "-6.3", "7"},
            {"8.27", "-9.26"}};

        // open
        file.open(name, delim);

        //void write(T data)
        file.write(input[0][0]); // could of course also just give values (int, double, string etc.), but this way is easier to compare with TInputFile
        file.write(input[0][1]);
        file.write(input[0][2]);
        file.endLine();

        //operator<<(const T data)
        file << input[1][0] << input[1][1] << input[1][2] << input[1][3] << std::endl;

        //write(const std::vector<T> & data)
        file.write(input[2]);
        TOutputFile::endl(file);

        //writeLine(std::vector<T> & data)
        std::vector<std::string> comment = {"//this is a comment"}; // should not appear in readed stuff
        file.writeLine(comment);
        file.close();
    }
};

TEST(IntegrationTest_Variable, readDefaultDelim) {
    // generate output file
    char delim = '\t';
    WriteOutputFile_Variable writer("file1.txt", delim);

    // now read
    TInputFile file("file1.txt", variable);

    std::vector<std::string> vec;
    int ind1 = 0, ind2 = 0;
    while(file.read(vec)){
        for (auto &it : vec) {
            EXPECT_EQ(it, writer.input[ind1][ind2]);
            ind2++;
        }
        ind2 = 0;
        ind1++;
    }
}

TEST(IntegrationTest_Variable, readZipped) {
    // generate output file
    char delim = '\t';
    WriteOutputFile_Variable writer("file1.txt.gz", delim);

    // now read
    TInputFile file("file1.txt.gz", variable);

    std::vector<std::string> vec;
    int ind1 = 0, ind2 = 0;
    while(file.read(vec)){
        for (auto &it : vec) {
            EXPECT_EQ(it, writer.input[ind1][ind2]);
            ind2++;
        }
        ind2 = 0;
        ind1++;
    }
}

TEST(IntegrationTest_Variable, indexColWithName) {
    char delim = '\t';
    WriteOutputFile_Variable writer("file1.txt", delim);

    // now read
    TInputFile file("file1.txt", variable);
    EXPECT_THROW({try {file.getIndexOfColname("one");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

struct WriteOutputFile_Header {
public:
    TOutputFile file;
    std::vector<std::string> header;
    std::vector< std::vector<std::string> > input;
    explicit WriteOutputFile_Header(const std::string &name, const char& delim) {
        // generate header
        header = {"var1", "var2", "var3"};
        // generate input
        input = {{"1", "2.1568", "three"},
                 {"four", "5.273", "-6.3"},
                 {"7", "eight", "-9.26"}};

        // open
        file.open(name, header, delim);

        //void write(T data)
        file.write(input[0][0]); // could of course also just give values (int, double, string etc.), but this way is easier to compare with TInputFile
        file.write(input[0][1]);
        file.write(input[0][2]);
        file.endLine();

        //operator<<(const T data)
        file << input[1][0] << input[1][1] << input[1][2] << std::endl;

        //write(const std::vector<T> & data)
        file.write(input[2]);
        TOutputFile::endl(file);

        //writeLine(std::vector<T> & data)
        std::vector<std::string> comment = {"//", "this is a", "comment"}; // should not appear in readed stuff
        file.writeLine(comment);
        file.close();
    }
};

TEST(IntegrationTest_Header, readDefaultDelim) {
    // generate output file
    char delim = '\t';
    WriteOutputFile_Header writer("file1.txt", delim);

    // now read
    TInputFile file("file1.txt", header);

    // compare headers
    std::vector<std::string> header = file.header();
    int i = 0;
    for (auto & it: writer.header) {
        EXPECT_EQ(it, header[i]);
        i++;
    }

    // compare data
    std::vector<std::string> vec;
    int ind1 = 0, ind2 = 0;
    while(file.read(vec)){
        for (auto &it : vec) {
            EXPECT_EQ(it, writer.input[ind1][ind2]);
            ind2++;
        }
        ind2 = 0;
        ind1++;
    }
}

TEST(IntegrationTest_Header, indexColWithName) {
    // generate output file
    char delim = '\t';
    WriteOutputFile_Header writer("file1.txt", delim);

    // now read
    TInputFile file("file1.txt", header, "\t");

    EXPECT_EQ(file.headerAt(0), "var1");
    EXPECT_EQ(file.headerAt(1), "var2");
    EXPECT_EQ(file.headerAt(2), "var3");
    EXPECT_THROW({try {file.headerAt(3);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);

    EXPECT_EQ(file.getIndexOfColname("var1"), 0);
    EXPECT_EQ(file.getIndexOfColname("var2"), 1);
    EXPECT_EQ(file.getIndexOfColname("var3"), 2);
    EXPECT_THROW({try {file.getIndexOfColname("var4");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

struct WriteOutputFile_Fixed {
public:
    TOutputFile file;
    std::vector< std::vector<std::string> > input;
    explicit WriteOutputFile_Fixed(const std::string &name, const char& delim) {
        // generate input
        input = {{"1", "2.1568", "three"},
                 {"four", "5.273", "-6.3"},
                 {"7", "eight", "-9.26"}};

        // open
        file.open(name, 3, delim);

        //void write(T data)
        file.write(input[0][0]); // could of course also just give values (int, double, string etc.), but this way is easier to compare with TInputFile
        file.write(input[0][1]);
        file.write(input[0][2]);
        file.endLine();

        //operator<<(const T data)
        file << input[1][0] << input[1][1] << input[1][2] << std::endl;

        //write(const std::vector<T> & data)
        file.write(input[2]);
        TOutputFile::endl(file);

        //writeLine(std::vector<T> & data)
        std::vector<std::string> comment = {"//", "this is a", "comment"}; // should not appear in readed stuff
        file.writeLine(comment);
        file.close();
    }
};

TEST(IntegrationTest_Fixed, readDefaultDelim) {
    // generate output file
    char delim = '\t';
    WriteOutputFile_Fixed writer("file1.txt", delim);

    // now read
    TInputFile file("file1.txt", fixed);

    // compare data
    std::vector<std::string> vec;
    int ind1 = 0, ind2 = 0;
    while(file.read(vec)){
        for (auto &it : vec) {
            EXPECT_EQ(it, writer.input[ind1][ind2]);
            ind2++;
        }
        ind2 = 0;
        ind1++;
    }
}

TEST(IntegrationTest_Fixed, indexColWithName) {
    char delim = '\t';
    WriteOutputFile_Fixed writer("file1.txt", delim);

    // now read
    TInputFile file("file1.txt", fixed);
    EXPECT_THROW({try {file.getIndexOfColname("one");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

//-------------------------------------------------------
// Now try to break it -> check if table restrictions are working

// writing
TEST(TOutput_Header_CheckRestrictions, writeHeaderLater){
    std::vector<std::string> header = {"one", "two", "three"};
    TOutputFile file("file1.txt");
    file << 1 << 2 << 3 << std::endl;
    EXPECT_THROW(file.writeHeader(header), std::runtime_error);
}

TEST(TOutput_Header_CheckRestrictions, writeHeaderInTheMiddle){
    std::vector<std::string> header = {"one", "two", "three"};
    TOutputFile file("file1.txt");
    file << 1 << 2 << 3;
    EXPECT_THROW(file.writeHeader(header), std::runtime_error);
}

TEST(TOutput_Header_CheckRestrictions, streamOperator){
    std::vector<std::string> header = {"one", "two", "three"};
    TOutputFile file("file1.txt", header);
    file << 1 << 2 << 3;
    EXPECT_THROW(file << 4, std::runtime_error);
}

TEST(TOutput_Header_CheckRestrictions, write){
    std::vector<std::string> header = {"one", "two", "three"};
    TOutputFile file("file1.txt", header);
    file.write(1); file.write(2); file.write(3);
    EXPECT_THROW(file.write(4), std::runtime_error);
}

TEST(TOutput_Header_CheckRestrictions, writeVec){
    std::vector<std::string> header = {"one", "two", "three"};
    TOutputFile file("file1.txt", header);
    std::vector<int> vec = {1, 2, 3};
    file.write(vec);

    vec = {1};
    EXPECT_THROW(file.write(vec), std::runtime_error);
}

TEST(TOutput_Header_CheckRestrictions, writeLine){
    std::vector<std::string> header = {"one", "two", "three"};
    TOutputFile file("file1.txt", header);
    std::vector<int> vec = {1, 2, 3, 4};
    EXPECT_THROW(file.writeLine(vec), std::runtime_error);
}

TEST(TOutput_Header_CheckRestrictions, endLine){
    std::vector<std::string> header = {"one", "two", "three"};
    TOutputFile file("file1.txt", header);
    file << 1 << 2 << 3 << std::endl;
    // try to make empty line (twice endLine)
    EXPECT_THROW(file.endLine(), std::runtime_error);
    EXPECT_THROW(file << std::endl, std::runtime_error);
    EXPECT_THROW(TOutputFile::endl(file), std::runtime_error);
}

TEST(TOutput_Fixed_CheckRestrictions, setNoHeaderLater){
    TOutputFile file("file1.txt");
    file << 1 << 2 << 3 << std::endl;
    EXPECT_THROW(file.noHeader(3), std::runtime_error);
}

TEST(TOutput_Fixed_CheckRestrictions, setNoHeaderInTheMiddle){
    TOutputFile file("file1.txt");
    file << 1 << 2 << 3;
    EXPECT_THROW(file.noHeader(3), std::runtime_error);
}

TEST(TOutput_Fixed_CheckRestrictions, streamOperator){
    TOutputFile file("file1.txt", 3);
    file << 1 << 2 << 3;
    EXPECT_THROW(file << 4, std::runtime_error);
}

TEST(TOutput_Fixed_CheckRestrictions, write){
    TOutputFile file("file1.txt", 3);
    file.write(1); file.write(2); file.write(3);
    EXPECT_THROW(file.write(4), std::runtime_error);
}

TEST(TOutput_Fixed_CheckRestrictions, writeVec){
    TOutputFile file("file1.txt", 3);
    std::vector<int> vec = {1, 2, 3};
    file.write(vec);

    vec = {1};
    EXPECT_THROW(file.write(vec), std::runtime_error);
}

TEST(TOutput_Fixed_CheckRestrictions, writeLine){
    TOutputFile file("file1.txt", 3);
    std::vector<int> vec = {1, 2, 3, 4};
    EXPECT_THROW(file.writeLine(vec), std::runtime_error);
}

TEST(TOutput_Fixed_CheckRestrictions, endLine){
    TOutputFile file("file1.txt", 3);
    file << 1 << 2 << 3 << std::endl;
    // try to make empty line (twice endLine)
    EXPECT_THROW(file.endLine(), std::runtime_error);
    EXPECT_THROW(file << std::endl, std::runtime_error);
    EXPECT_THROW(TOutputFile::endl(file), std::runtime_error);
}

// reading

TEST(TInput_Fixed_CheckRestrictions, readExtraCol) {
    // generate output file
    TOutputFile out("file1.txt");
    out << 1 << 2 << 3 << std::endl;
    out << 4 << 5 << 6 << 7 << std::endl;
    out.close();

    // now read
    TInputFile file("file1.txt", fixed);
    EXPECT_EQ(file.numCols(), 3);

    std::vector<std::string> vec;
    EXPECT_NO_THROW(file.read(vec)); // first line is ok
    EXPECT_THROW({try {file.read(vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // second line has extra entry
}

TEST(TInput_Fixed_CheckRestrictions, readMissingCol) {
    // generate output file
    TOutputFile out("file1.txt");
    out << 1 << 2 << 3 << std::endl;
    out << 4 << 5 << std::endl;
    out.close();

    // now read
    TInputFile file("file1.txt", fixed);
    EXPECT_EQ(file.numCols(), 3);

    std::vector<std::string> vec;
    EXPECT_NO_THROW(file.read(vec)); // first line is ok
    EXPECT_THROW({try {file.read(vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // second line has missing entry
}

TEST(TInput_Header_CheckRestrictions, readExtraCol) {
    // generate output file
    TOutputFile out("file1.txt");
    out << 1 << 2 << 3 << std::endl;
    out << 4 << 5 << 6 << 7 << std::endl;
    out.close();

    // now read
    TInputFile file("file1.txt", header);
    EXPECT_EQ(file.numCols(), 3);

    std::vector<std::string> vec;
    EXPECT_THROW({try {file.read(vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // second line has extra entry
}

TEST(TInput_Header_CheckRestrictions, readMissingCol) {
    // generate output file
    TOutputFile out("file1.txt");
    out << 1 << 2 << 3 << std::endl;
    out << 4 << 5 << std::endl;
    out.close();

    // now read
    TInputFile file("file1.txt", header);
    EXPECT_EQ(file.numCols(), 3);

    std::vector<std::string> vec;
    EXPECT_THROW({try {file.read(vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // second line has missing entry
}

//-------------------------------------------------------
// open -> write -> close -> open

TEST(OpenClose_Writing, overwrites){
    // open
    TOutputFile out("file1.txt", 3);
    out << 1 << 2 << 3 << std::endl;
    out << 4 << 5 << 6 << std::endl;
    // close
    out.close();
    // open - different format, different delimitor
    out.open("file1.txt", ' ');
    out << 100 << 99 << 88 << 77 << std::endl;
    out << 1 << std::endl;
    out << "hopp!" << -28.4 << std::endl;
    out.close();

    // now read
    std::vector<std::string> goal = {"100", "99", "88", "77", "1", "hopp!", "-28.4"};
    TInputFile in("file1.txt", variable, " ");
    std::vector<std::string> vec;
    int i = 0;
    while(in.read(vec)){
        for (auto &it : vec) {
            EXPECT_EQ(it, goal[i]);
            i++;
        }
    }
}

TEST(OpenClose_Writing, reopenContinueWriting){
    // open
    TOutputFile out("file1.txt", 3);
    out << 1 << 2 << 3 << std::endl;
    out << 4 << 5 << 6 << std::endl;
    EXPECT_EQ(out.lineNumber(), 2);
    // close
    out.close();
    // re-open
    out.reOpen();
    EXPECT_EQ(out.lineNumber(), 2);
    out << 100 << 99 << 88 << std::endl;
    out << 77 << "hopp!" << 1 << std::endl;
    EXPECT_EQ(out.lineNumber(), 4);
    EXPECT_THROW(out << 9 << 8 << 7 << 6 << std::endl, std::runtime_error); // keeps format
    out.close();

    // now read
    std::vector<std::string> goal = {"1", "2", "3", "4", "5", "6", "100", "99", "88", "77", "hopp!", "1"};
    TInputFile in("file1.txt", fixed);
    std::vector<std::string> vec;
    int i = 0;
    while(in.read(vec)){
        for (auto &it : vec) {
            EXPECT_EQ(it, goal[i]);
            i++;
        }
    }
}

TEST(OpenClose_Reading, startFromBeginning){
    TOutputFile out("file1.txt", 3);
    out << 1 << 2 << 3 << std::endl;
    out << 4 << 5 << 6 << std::endl;
    out << 7 << 8 << 9 << std::endl;
    out.close();
    std::vector<std::string> goal = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};

    // open
    TInputFile in("file1.txt", fixed);
    std::vector<std::string> vec;
    int i = 0;
    while(in.read(vec)){
        for (auto &it : vec) {
            EXPECT_EQ(it, goal[i]);
            i++;
        }
    }
    EXPECT_EQ(in.lineNumber(), 3);
    // close
    in.close();

    // open again
    in.open("file1.txt", fixed);
    i = 0;
    EXPECT_EQ(in.lineNumber(), 0); // jumped to start
    while(in.read(vec)){
        for (auto &it : vec) {
            EXPECT_EQ(it, goal[i]);
            i++;
        }
    }
}

TEST(OpenClose_Reading, reopenContinueReadingBeginning){
    TOutputFile out("file1.txt", 3);
    out << 1 << 2 << 3 << std::endl;
    out << 4 << 5 << 6 << std::endl;
    out << 7 << 8 << 9 << std::endl;
    out << 10 << 11 << 12 << std::endl;
    out.close();
    std::vector<std::string> goal = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};

    // open
    TInputFile in("file1.txt", fixed);
    EXPECT_EQ(in.lineNumber(), 0);
    // close
    in.close();
    // re-open
    std::vector<std::string> vec;
    in.reOpen();
    EXPECT_EQ(in.lineNumber(), 0); // didn't move
    int i = 0;
    while(in.read(vec)){ // read rest
        for (auto &it : vec) {
            EXPECT_EQ(it, goal[i]);
            i++;
        }
    }
}

TEST(OpenClose_Reading, reopenContinueReadingMiddle){
    TOutputFile out("file1.txt", 3);
    out << 1 << 2 << 3 << std::endl;
    out << 4 << 5 << 6 << std::endl;
    out << 7 << 8 << 9 << std::endl;
    out << 10 << 11 << 12 << std::endl;
    out.close();
    std::vector<std::string> goal = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};

    // open
    TInputFile in("file1.txt", fixed);
    std::vector<std::string> vec;

    // read first line
    int i = 0;
    in.read(vec);
    for (auto &it : vec) {
        EXPECT_EQ(it, goal[i]);
        i++;
    }
    EXPECT_EQ(in.lineNumber(), 1);

    // read second line
    in.read(vec);
    for (auto &it : vec) {
        EXPECT_EQ(it, goal[i]);
        i++;
    }
    EXPECT_EQ(in.lineNumber(), 2);

    // close
    in.close();

    // re-open
    in.reOpen();
    EXPECT_EQ(in.lineNumber(), 2); // didn't move
    while(in.read(vec)){ // read rest
        for (auto &it : vec) {
            EXPECT_EQ(it, goal[i]);
            i++;
        }
    }
}

TEST(OpenClose_Reading, reopenContinueReadingEnd){
    TOutputFile out("file1.txt", 3);
    out << 1 << 2 << 3 << std::endl;
    out << 4 << 5 << 6 << std::endl;
    out << 7 << 8 << 9 << std::endl;
    out << 10 << 11 << 12 << std::endl;
    out.close();
    std::vector<std::string> goal = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};

    // open
    TInputFile in("file1.txt", fixed);
    std::vector<std::string> vec;
    EXPECT_EQ(in.lineNumber(), 0);
    int i = 0;
    while(in.read(vec)){ // read everything
        for (auto &it : vec) {
            EXPECT_EQ(it, goal[i]);
            i++;
        }
    }
    EXPECT_EQ(in.lineNumber(), 4);

    // close
    in.close();

    // re-open
    in.reOpen();
    EXPECT_EQ(in.lineNumber(), 4);
    while(in.read(vec)){ // read rest
        for (auto &it : vec) {
            EXPECT_EQ(it, goal[i]);
            i++;
        }
    }
    EXPECT_EQ(in.lineNumber(), 4);
}

// tidy up

TEST(OpenClose_Reading, clean){
    // hack to remove all test files that were produced
    remove("file1.txt");
    remove("file2.txt");
    remove("file1.txt.gz");
}

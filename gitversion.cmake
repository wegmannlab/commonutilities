execute_process(COMMAND git log --pretty=format:'%h' -n 1
        OUTPUT_VARIABLE GIT_REV
        ERROR_QUIET)

# Check whether we got any revision (which isn't
# always the case, e.g. when someone downloaded a zip
# file from Github instead of a checkout)
if ("${GIT_REV}" STREQUAL "")
    set(GIT_REV "N/A")
    set(GIT_HEAD "N/A")
else()
    execute_process(
            COMMAND git rev-parse HEAD
            OUTPUT_VARIABLE GIT_HEAD)

    string(STRIP "${GIT_REV}" GIT_REV)
    string(SUBSTRING "${GIT_REV}" 1 7 GIT_REV)
    string(STRIP "${GIT_HEAD}" GIT_HEAD)
endif()

set(VERSION "#include \"gitversion.h\"
    std::string getGitVersion(){
    return \"${GIT_HEAD}\";
    }")

if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/gitversion.cpp)
    file(READ ${CMAKE_CURRENT_SOURCE_DIR}/gitversion.cpp VERSION_)
else()
    set(VERSION_ "")
endif()

if (NOT "${VERSION}" STREQUAL "${VERSION_}")
    file(WRITE ${CMAKE_CURRENT_SOURCE_DIR}/gitversion.cpp "${VERSION}")
endif()
/*
 * math_functions.cpp
 *
 *  Created on: Dec 11, 2018
 *      Author: phaentu
 */

#include "mathFunctions.h"

//------------------------------------------------
// Log gamma function
// AS245, 2nd algorithm, http://lib.stat.cmu.edu/apstat/245
//------------------------------------------------
double gammaLog(const double & z){
    // alternative: lgamma from cmath library, however, lgamma is not thread-safe
	double x = 0;
	x += 0.1659470187408462e-06 / (z+7);
	x += 0.9934937113930748e-05 / (z+6);
	x -= 0.1385710331296526 / (z+5);
	x += 12.50734324009056 / (z+4);
	x -= 176.6150291498386 / (z+3);
	x += 771.3234287757674 / (z+2);
	x -= 1259.139216722289 / (z+1);
	x += 676.5203681218835 / z;
	x += 0.9999999999995183;
	return log(x) - 5.58106146679532777 - z + (z-0.5) * log(z+6.5);
}

//------------------------------------------------
// Digamma function
//------------------------------------------------
double TDigamma::_psiSeries(const double & z){
    double z1 = 1./z;
    double z2 = z1*z1;
    double z4 = z2*z2;
    double z6 = z4*z2;
    double z8 = z4*z4;
    return log(z) - 0.5*z1 - 0.0833333333333333287074*z2 + 0.00833333333333333287074*z4 - 0.00390625*z6 + 0.004166666666666666608843*z8 - 0.007575757575757575967845*z4*z6 + 0.02109279609279609418726*z6*z6 - 0.0833333333333333287074*z8*z6;
}

double TDigamma::digamma(const double & x){
    uint8_t numShifts = std::floor(std::max(6.-x, 0.));
    double psiShifted = _psiSeries(x+numShifts);
    if (numShifts > 0){
        for (uint8_t i = 1; i <= numShifts; i++){
            psiShifted -= 1./(x+numShifts-i);
        }
    }
    return psiShifted;
}

//------------------------------------------------
// Base class distribution
//------------------------------------------------
void TDistr::checkArgWithMin(const double & val, const double & min, bool minIncluded, const std::string& nameVal, const std::string & distr){
    std::string tmp = "Argument " + nameVal + " (= " + toString(val) + ") of a " + distr + " distribution must be ";
    if (minIncluded && val < min){
        throw std::runtime_error(tmp + ">= " + toString(min) + "!");
    }
    if (!minIncluded && val <= min)
        throw std::runtime_error(tmp + "> " + toString(min) + "!");
}

void TDistr::checkArgWithMax(const double & val, const double & max, bool maxIncluded, const std::string &nameVal, const std::string &distr){
    std::string tmp = "Argument " + nameVal + " (= " + toString(val) + ") of a " + distr + " distribution must be ";
    if (maxIncluded && val > max){
        throw std::runtime_error(tmp + "<= " + toString(max) + "!");
    }
    if (!maxIncluded && val >= max)
        throw std::runtime_error(tmp + "< " + toString(max) + "!");
}

//------------------------------------------------
// Beta distribution
//------------------------------------------------
void TBetaDistr::checkArgs(const double & x, const double & alpha, const double & beta){
    // 0 <= x <= 1
    checkArgWithMin(x, 0., true, "x", "beta");
    checkArgWithMax(x, 1., true, "x", "beta");
    // alpha > 0
    checkArgWithMin(alpha, 0., false, "alpha", "beta");
    // beta > 0
    checkArgWithMin(beta, 0., false, "beta", "beta");
}

double TBetaDistr::density(const double & x, const double & alpha, const double & beta, bool Log){
    // calculates density of a beta distribution
    checkArgs(x, alpha, beta);

    // calculate
    if (Log)
        return calcDensLogBeta(x, alpha, beta);
    return calcDensBeta(x, alpha, beta);
}

double TBetaDistr::calcDensLogBeta(const double & x, const double & alpha, const double & beta){
    double tmp1 = gammaLog(alpha + beta) - gammaLog(alpha) - gammaLog(beta);
    double tmp2 = (alpha - 1.) * log(x) + (beta - 1.) * log(1.-x);
    return tmp1 + tmp2;
}

double TBetaDistr::calcDensBeta(const double & x, const double & alpha, const double & beta){
    const double & tmp1 = tgamma(alpha + beta) / (tgamma(alpha) * tgamma(beta));
    const double & tmp2 = pow(x, alpha - 1.) * pow (1. - x, beta - 1.);
    return tmp1 * tmp2;
}

//------------------------------------------------
// Exponential distribution
//------------------------------------------------
void TExponentialDistr::checkArgs(const double & x, const double & lambda){
    // x >= 0
    checkArgWithMin(x, 0., true, "x", "exponential");
    // lambda > 0
    checkArgWithMin(lambda, 0., false, "lambda", "exponential");
}

double TExponentialDistr::density(const double & x, const double & lambda, bool Log){
    // calculates density of a exponential distribution
    checkArgs(x, lambda);
    // calculate
    if (Log)
        return calcDensLogExponential(x, lambda);
    return calcDensExponential(x, lambda);

}

double TExponentialDistr::calcDensExponential(const double & x, const double & lambda){
    return lambda * exp(-lambda * x);
}

double TExponentialDistr::calcDensLogExponential(const double & x, const double & lambda){
    return log(lambda) - lambda * x;
}

double TExponentialDistr::cumulativeDistrFunction(const double & x, const double & lambda){
    checkArgs(x, lambda);
    return 1. - exp(-lambda * x);
}

double TExponentialDistr::mean(const double & lambda){
    return 1./lambda;
}

//--------------------------------------------------------
//Chisq Distribution
//--------------------------------------------------------
void TChisqDistr::checkArgs(const double & x, int k){
    // x > 0 (if k = 1) and x >= 0 (if k > 1)
    checkArgWithMin(x, 0., k != 1, "x", "chisq");
    // k > 0
    checkArgWithMin(k, 0., false, "k", "chisq");

}

double TChisqDistr::density(const double & x, int k, bool Log){
    // calculates density of a chisq distribution
    checkArgs(x, k);
    // calculate
    if(Log)
        return calcDensLogChisq(x, k);
    return calcDensChisq(x, k);
}

double TChisqDistr::calcDensLogChisq(const double & x, int k){
    const double & kDivD = k/2.;
    return -kDivD * M_LN2 - gammaLog(kDivD) + (kDivD - 1.) * log(x) - x/2.;
}

double TChisqDistr::calcDensChisq(const double & x, int k){
    const double & tmp1 = 1./(pow(2., k/2.) * tgamma(k/2.));
    const double & tmp2 = pow(x, k/2. - 1.) * exp(-x / 2.);
    return tmp1 * tmp2;
}

//--------------------------------------------------------
//Chi Distribution
//--------------------------------------------------------
void TChiDistr::checkArgs(const double & x, int k){
    // x >= 0
    checkArgWithMin(x, 0., true, "x", "chi");
    // k > 0
    checkArgWithMin(k, 0., false, "k", "chi");
}

double TChiDistr::density(const double & x, int k, bool Log){
    // calculates density of a chi distribution
    checkArgs(x, k);
    // calculate
    if(Log)
        return calcDensLogChi(x, k);
    return calcDensChi(x, k);
}

double TChiDistr::calcDensLogChi(const double & x, int k){
    const double & kDivD = k/2.;
    return -(kDivD-1.) * M_LN2 - gammaLog(kDivD) + (k - 1.) * log(x) - (x*x)/2.;
}

double TChiDistr::calcDensChi(const double & x, int k){
    const double & tmp1 = 1./(pow(2., k/2. - 1.) * tgamma(k/2.));
    const double & tmp2 = pow(x, k - 1.) * exp(-(x*x) / 2.);
    return tmp1 * tmp2;
}


//--------------------------------------------------------
//Normal Distribution
//--------------------------------------------------------
void TNormalDistr::checkArgs(const double & sd){
    // var > 0
    checkArgWithMin(sd, 0., false, "sd", "normal");
}

double TNormalDistr::density(const double & x, const double & mean, const double & sd, bool Log){
    // calculates density of a normal distribution (= dnorm in R)
    checkArgs(sd);

    // calculate
    if (Log)
        return calcDensLogNormal(x, mean, sd);
    return calcDensNormal(x, mean, sd);
}

double TNormalDistr::cumulativeDistrFunction(const double & x, const double & mean, const double & sd){
    // = pnorm in R
    checkArgs(sd);

    if(x==mean) return 0.5;
    return 0.5 * normalComplementaryErrorFunction(- M_SQRT1_2 * (x-mean)/sd);
}

double TNormalDistr::invCumulativeDistrFunction(const double & p, const double & mean, const double & sd){
    // = qnorm in R
    checkArgs(sd);
    checkArgWithMin(p, 0., false, "p", "normal");
    checkArgWithMax(p, 1., false, "p", "normal");

    //see book "numerical recipes" pp 321
    return -M_SQRT2 * sd * invNormalComplementaryErrorFunction(2.*p) + mean;
}

double TNormalDistr::calcDensLogNormal(const double & x, const double & mean, const double & sd){
	const double & variance = sd * sd;
    return - 0.5 * log(M_TWO_PI * variance) - 1./(2.*variance) * (x - mean) * (x - mean);
}

double TNormalDistr::calcDensNormal(const double & x, const double & mean, const double & sd){
    return 1. / (M_SQRT_TWO_PI * sd) * exp(-1. / (2.*sd*sd) * (x - mean) * (x - mean));
}

double TNormalDistr::normalComplementaryErrorFunction(const double & x){
    //see book "numerical recipes"
    if(x>=0.) return normalComplementaryErrorCheb(x);
    else return 2.0 - normalComplementaryErrorCheb(-x);
}

double TNormalDistr::normalComplementaryErrorCheb(const double & x){
    //see book "numerical recipes"
    static double coef[28]={-1.3026537197817094, 6.4196979235649026e-1,
                     1.9476473204185836e-2,-9.561514786808631e-3,-9.46595344482036e-4,
                     3.66839497852761e-4,4.2523324806907e-5,-2.0278578112534e-5,
                     -1.624290004647e-6,1.303655835580e-6,1.5626441722e-8,-8.5238095915e-8,
                     6.529054439e-9,5.059343495e-9,-9.91364156e-10,-2.27365122e-10,
                     9.6467911e-11, 2.394038e-12,-6.886027e-12,8.94487e-13, 3.13092e-13,
                     -1.12708e-13,3.81e-16,7.106e-15,-1.523e-15,-9.4e-17,1.21e-16,-2.8e-17};
    int j;
    double t, ty, tmp, d=0., dd=0.;
    t=2./(2.+x);
    ty=4.*t-2;
    for(j=27;j>0;--j){
        tmp=d;
        d=ty*d-dd+coef[j];
        dd=tmp;
    }
    return t * exp(-x * x + 0.5 * (coef[0] + ty * d) - dd);
}

double TNormalDistr::invNormalComplementaryErrorFunction(const double & p){
    //see book "numerical recipes"
    double x,err,t,pp;
    if (p >= 2.0) return -100.;
    if (p <= 0.0) return 100.;
    pp = (p < 1.0)? p : 2. - p;
    t = sqrt(-2.*log(pp/2.));
    x = -0.70711*((2.30753+t*0.27061)/(1.+t*(0.99229+t*0.04481)) - t);
    for (int j=0;j<2;j++) {
        err = erfc(x) - pp;
        x += err/(M_2_SQRTPI * exp(-(x * x))-x*err);
    }
    return (p < 1.0? x : -x);
}

//--------------------------------------------------------
//Binomial Distribution
//--------------------------------------------------------
void TBinomialDistr::checkArgs(int n, int k, const double & p){
    // n >= 0
    checkArgWithMin(n, 0., true, "n", "binomial");
    // k >= 0
    checkArgWithMin(k, 0., true, "k", "binomial");
    // k <= n
    checkArgWithMax(k, n, true, "k", "binomial");
    // p >= 0
    checkArgWithMin(p, 0., true, "p", "binomial");
    // p <= 1
    checkArgWithMax(p, 1., true, "p", "binomial");
}

double TBinomialDistr::density(int n, int k, const double & p, bool Log){
    // calculates density of a binomial distribution
    checkArgs(n, k, p);

    // calculate
    if (Log)
        return calcDensLogBinom(n, k, p);
    return calcDensBinom(n, k, p);
}

double TBinomialDistr::calcDensLogBinom(int n, int k, const double & p){
    return chooseLog(n,k) + k*log(p) + (n-k)*log(1.-p);
}

double TBinomialDistr::calcDensBinom(int n, int k, const double & p){
    return choose(n,k) * pow(p, k) * pow(1.-p, n-k);
}

//------------------------------------------------
// Bernouilli distribution
//------------------------------------------------
void TBernouilliDistr::checkArgs(const bool & x, const double & pi){
    // pi => 0 and pi <= 1
    checkArgWithMin(pi, 0., true, "pi", "bernouilli");
    checkArgWithMax(pi, 1., true, "pi", "bernouilli");
}

double TBernouilliDistr::density(const bool & x, const double & pi, const bool & Log){
    // calculates density of a bernouilli distribution
    checkArgs(x, pi);
    // calculate
    if (Log)
        return calcDensLogBernouilli(x, pi);
    return calcDensBernouilli(x, pi);

}

double TBernouilliDistr::calcDensBernouilli(const bool & x, const double & pi){
    if (x){
        return pi;
    } else {
        return 1.-pi;
    }
}

double TBernouilliDistr::calcDensLogBernouilli(const bool & x, const double & pi){
    if (x){
        if (pi == 0.)
            throw std::runtime_error("In function double TBernouilliDistr::calcDensLogBernouilli(const bool & x, double pi): pi is exactly 0., and log(pi) results in -Inf!");
        return log(pi);
    } else {
        if (pi == 1.)
            throw std::runtime_error("In function double TBernouilliDistr::calcDensLogBernouilli(const bool & x, double pi): pi is exactly 1., and log(1-pi) results in -Inf!");
        return log(1.-pi);
    }
}

double TBernouilliDistr::mean(const double & pi){
    return pi;
}

//------------------------------------------------
// TDirichletDistr distribution
//------------------------------------------------
void TDirichletDistr::_checkSumX(const std::vector<double> & x){
    double sum = 0.;
    for (auto & val : x){
        sum += val;
    }
    if (std::abs(1.-sum) > 0.0000001){ // allow for numeric imprecision
        throw std::runtime_error("Argument x in a Dirichlet distribution must sum to 1 (actually: it sums to " + toString(sum) + ").");
    }
}

void TDirichletDistr::_checkX(const std::vector<double> & x, const std::vector<double> & alpha){
    // x => 0 and x <= 1
    for (auto & val : x){
        checkArgWithMin(val, 0., true, "x", "Dirichlet");
        checkArgWithMax(val, 1., true, "x", "Dirichlet");
    }
    // x sum to 1
    _checkSumX(x);

    // length of x and alpha must match (=K)
    if (x.size() != alpha.size()) {
        throw std::runtime_error("The size of x (=" + toString(x.size()) + ") and of alpha (=" + toString(alpha.size()) + ") in a Dirichlet distribution must be the same!");
    }
}

void TDirichletDistr::_checkAlpha(const std::vector<double> &alpha){
    // alpha > 0
    for (auto & val : alpha){
        checkArgWithMin(val, 0., false, "alpha", "Dirichlet");
    }
}

void TDirichletDistr::_checkArgs(const std::vector<double> & x, const std::vector<double> & alpha){
    // x => 0 and x <= 1
    _checkX(x, alpha);
    // alpha > 0
    _checkAlpha(alpha);
}

double TDirichletDistr::density(const std::vector<double> & x, const std::vector<double> & alpha, bool Log){
    // calculates density of a bernouilli distribution
    _checkArgs(x, alpha);
    // calculate
    if (Log)
        return _calcDensLogDirichlet(x, alpha);
    return _calcDensDirichlet(x, alpha);
}

double TDirichletDistr::_calcBetaFunction(const std::vector<double> & alpha){
    size_t K = alpha.size();
    double prodGamma_alphaK = 1.;
    double sum_alphaK = 0.;
    for (size_t k = 0; k < K; k++){
        prodGamma_alphaK *= tgamma(alpha[k]);
        sum_alphaK += alpha[k];
    }
    return prodGamma_alphaK / tgamma(sum_alphaK);
}

double TDirichletDistr::_calcProductDirichlet(const std::vector<double> & x, const std::vector<double> & alpha){
    // calculates prod_k x_k^(alpha_k - 1)
    size_t K = alpha.size();
    double prod = 1.;
    for (size_t k = 0; k < K; k++){
        prod *= pow(x[k], alpha[k]-1.);
    }
    return prod;
}

double TDirichletDistr::_calcLogProductDirichlet(const std::vector<double> & x, const std::vector<double> & alpha){
    // calculates log(prod_k x_k^(alpha_k - 1)) = sum_k (alpha_k - 1) * log(x_k)
    size_t K = alpha.size();
    double sum = 0.;
    for (size_t k = 0; k < K; k++){
        sum += (alpha[k]-1.) * log(x[k]);
    }
    return sum;
}

double TDirichletDistr::_calcLogBetaFunction(const std::vector<double> & alpha){
    size_t K = alpha.size();
    double sumLnGamma_alphaK = 0.;
    double sum_alphaK = 0.;
    for (size_t k = 0; k < K; k++){
        sumLnGamma_alphaK += gammaLog(alpha[k]);
        sum_alphaK += alpha[k];
    }
    return sumLnGamma_alphaK - gammaLog(sum_alphaK);
}

double TDirichletDistr::_calcDensDirichlet(const std::vector<double> & x, const std::vector<double> & alpha){
    double prod = _calcProductDirichlet(x, alpha);
    double betaConst = _calcBetaFunction(alpha);
    return prod/betaConst;
}

double TDirichletDistr::_calcDensLogDirichlet(const std::vector<double> & x, const std::vector<double> & alpha){
    double logProd = _calcLogProductDirichlet(x, alpha);
    double logBetaConst = _calcLogBetaFunction(alpha);
    return -logBetaConst + logProd;
}

//------------------------------------------------
// TDirichletDistrConst distribution
//------------------------------------------------

TDirichletDistrReUsable::TDirichletDistrReUsable() {
    _betaConstant = 0.;
    _logBetaConstant = 0.;
}

TDirichletDistrReUsable::TDirichletDistrReUsable(const std::vector<double> &alpha) {
    _betaConstant = 0.;
    _logBetaConstant = 0.;
    resetParameters(alpha);
}

void TDirichletDistrReUsable::resetParameters(const std::vector<double> &alpha) {
    _alpha = alpha;

    // check if all alpha are valid (alpha > 0)
    _checkAlpha(alpha);

    // calculate temporary values
    _betaConstant = _calcBetaFunction(alpha);
    _logBetaConstant = _calcLogBetaFunction(alpha);
}

double TDirichletDistrReUsable::density(const std::vector<double> & x, bool Log) const {
    // calculates density of a Dirichlet distribution
    _checkX(x, _alpha);
    // calculate
    if (Log){
        double logProd = _calcLogProductDirichlet(x, _alpha);
        return -_logBetaConstant + logProd;
    } else {
        double prod = _calcProductDirichlet(x, _alpha);
        return prod/_betaConstant;
    }
}

double TDirichletDistrReUsable::getBetaConstant(bool Log) const {
    if (Log){
        return _logBetaConstant;
    } else {
        return _betaConstant;
    }
}

const std::vector<double>& TDirichletDistrReUsable::alpha() const{
    return _alpha;
}

//--------------------------------------------------------
//Gamma Distribution
//--------------------------------------------------------
void TGammaDistr::checkArgs(const double & x, const double & alpha, const double & beta){
    // x > 0
    checkArgWithMin(x, 0., false, "x", "gamma");
    // alpha > 0
    checkArgWithMin(alpha, 0., false, "alpha", "gamma");
    // beta > 0
    checkArgWithMin(beta, 0., false, "beta", "gamma");
}

double TGammaDistr::density(const double & x, const double & alpha, const double & beta, bool Log){
    // calculates density of a gamma distribution
    checkArgs(x, alpha, beta);

    // calculate
    if (Log)
        return calcDensLogGamma(x, alpha, beta);
    return calcDensGamma(x, alpha, beta);
}

double TGammaDistr::calcDensLogGamma(const double & x, const double & alpha, const double & beta){
    return alpha * log(beta) - gammaLog(alpha) + (alpha-1.)*log(x) - beta * x;
}

double TGammaDistr::calcDensGamma(const double & x, const double & alpha, const double & beta){
    return pow(beta, alpha) / tgamma(alpha) * pow(x, alpha-1.) * exp(-beta*x);
}


double TGammaDistr::cumulativeDistrFunction(const double & x, const double & alpha, const double & beta){
    checkArgs(x, alpha, beta);
    //Adapted from kfunc.c of samtools
    return TIncompleteGamma::lower(alpha, beta * x); // do not need to divide by exp(gammaln(alpha)), it is already regularized;
}

//--------------------------------------------------------
// Generalized pareto distribution
//--------------------------------------------------------
void TParetoDistr::checkArgs(const double & x, const double & locationMu, const double & scaleSigma, const double & shapeXi){
    checkArgWithMin(scaleSigma, 0., false, "scaleSigma", "generalized pareto");
    if(x<locationMu) throw std::runtime_error("Problem calculating cumulative function for generalized pareto: x < mu (location)!");
    if(shapeXi < 0.0 && x > locationMu - scaleSigma / shapeXi) throw std::runtime_error("Problem calculating cumulative function for generalized pareto: x > mu - sigma/xi!");
}

double TParetoDistr::density(const double & x, const double & locationMu, const double & scaleSigma, const double & shapeXi, bool Log){
    checkArgs(x, locationMu, scaleSigma, shapeXi);

    // calculate
    if (Log)
        return calcDensLogPareto(x, locationMu, scaleSigma, shapeXi);
    return calcDensPareto(x, locationMu, scaleSigma, shapeXi);
}

double TParetoDistr::calcDensLogPareto(const double & x, const double & locationMu, const double & scaleSigma, const double & shapeXi){
    if(locationMu == 0.0){
        return -log(scaleSigma) - (x - locationMu) / scaleSigma;
    } else {
        return -log(scaleSigma) + (-1.0-1.0/shapeXi) * log(1.0 + shapeXi * (x - locationMu )/scaleSigma);
    }
}

double TParetoDistr::calcDensPareto(const double & x, const double & locationMu, const double & scaleSigma, const double & shapeXi){
    // add formulas here...
    throw std::runtime_error("Density of pareto is currently not implemented!");
}

double TParetoDistr::cumulativeDistrFunction(const double & x, const double & locationMu, const double & scaleSigma, const double & shapeXi){
    checkArgs(x, locationMu, scaleSigma, shapeXi);
    if(shapeXi == 0.0){
        return 1.0 - exp((locationMu - x)/scaleSigma);
    } else {
        return 1.0 - pow(1.0 + shapeXi*(x - locationMu)/scaleSigma, -1.0/shapeXi);
    }
}

//--------------------------------------------------------
//Kolmogorov-Smirnov Distribution
//--------------------------------------------------------
void TKolmogorovSmirnovDistr::_checkArgs(const double & z){
    checkArgWithMin(z, 0., true, "z", "Kolmogorov-Smirnov");
}

double TKolmogorovSmirnovDistr::_invxlogx(const double & y){
    // For negative y, 0 > y > -e^(-1), return x such that y = x log(x).
    // The value returned is always the smaller of the two roots and is in the range 0 < x < e^(-1) .
    // From Book Numerical Recipes, pp. 309 (chapter 6.11, Inverse of the Function x log(x))
    static const double ooe = 0.367879441171442322;
    double t,u,to=0.;
    if (y >= 0. || y <= -ooe) throw std::runtime_error("In function 'double TKolmogorovSmirnovDistr::invxlogx(double y)': no such inverse value!");
    if (y < -0.2) u = log(ooe-sqrt(2*ooe*(y+ooe))); // First approximation by inverse of Taylor series.
    else u = -10.;
    do {
        u += (t=(log(y/u)-u)*(u/(1.+u)));
        if (t < 1.e-8 && std::fabs(t+to)<0.01*std::fabs(t)) break;
        to = t;
    } while (std::fabs(t/u) > 1.e-15);
    return exp(u);
}

double TKolmogorovSmirnovDistr::cumulativeDistrFunction(const double & z){
    // Return cumulative distribution function
    // From Book Numerical Recipes, pp. 334 (chapter 6.14.12, Kolmogorov-Smirnov Distribution)
    _checkArgs(z);
    if (z == 0.) return 0.;
    if (z < 1.18) {
        double y = exp(-1.23370055013616983/sqrt(z));
        return 2.25675833419102515*sqrt(-log(y))
               *(y + pow(y,9) + pow(y,25) + pow(y,49));
    } else {
        double x = exp(-2.*sqrt(z));
        return 1. - 2.*(x - pow(x,4) + pow(x,9));
    }
}
double TKolmogorovSmirnovDistr::complementaryCumulativeDistrFunction(const double & z){
    // Return complementary cumulative distribution function.
    // From Book Numerical Recipes, pp. 334 (chapter 6.14.12, Kolmogorov-Smirnov Distribution)
    _checkArgs(z);
    if (z == 0.) return 1.;
    if (z < 1.18) return 1.-cumulativeDistrFunction(z);
    double x = exp(-2.*sqrt(z));
    return 2.*(x - pow(x,4) + pow(x,9));
}

double TKolmogorovSmirnovDistr::invComplementaryCumulativeDistrFunction(const double & q){
    // Return inverse of the complementary cumulative distribution function.
    // From Book Numerical Recipes, pp. 334 (chapter 6.14.12, Kolmogorov-Smirnov Distribution)
    double y,logy,yp,x,xp,f,ff,u,t;

    checkArgWithMin(q, 0., false, "q", "Kolmogorov-Smirnov");
    checkArgWithMax(q, 1., true, "q", "Kolmogorov-Smirnov");

    if (q == 1.) return 0.;
    if (q > 0.3) {
        f = -0.392699081698724155*sqrt(1.-q);
        y = _invxlogx(f);
        // Initial guess.
        do {
            yp = y;
            logy = log(y);
            ff = f/sqrt(1.+ pow(y,4)+ pow(y,12));
            u = (y*logy-ff)/(1.+logy);
            // Newton’s method correction.
            y = y - (t=u/std::max(0.5,1.-0.5*u/(y*(1.+logy)))); // Halley.
        } while (std::fabs(t/y) > 1.e-15);
        return 1.57079632679489662/sqrt(-log(y));
    } else {
        x = 0.03;
        do {
            // Iteration (6.14.59).
            xp = x;
            x = 0.5*q+pow(x,4)-pow(x,9);
            if (x > 0.06) x += pow(x,16)-pow(x,25);
        } while (std::fabs((xp-x)/x)>1.e-15);
        return sqrt(-0.5*log(x));
    }
}

double TKolmogorovSmirnovDistr::invCumulativeDistrFunction(const double & p){
    // Return inverse of the cumulative distribution function.
    // From Book Numerical Recipes, pp. 334 (chapter 6.14.12, Kolmogorov-Smirnov Distribution)
    return invComplementaryCumulativeDistrFunction(1.-p);
}

void runOneSampleKolmogorovSmirnovTest(std::vector<double> & Data, double CumulFunc(const double&), double & d, double & prob){
    // tests if a sample of a probability distribution comes from the same distribution as a reference probability distribution
    // Given an array Data [0..n-1],
    // and given a function of a single variable CumulFunc that is a cumulative distribution function ranging from 0
    // (for smallest values of its argument) to 1 (for largest values of its argument),
    // this routine returns the Kolmogorov–Smirnov statistic d and the p-value prob.
    // Small values of prob show that the cumulative distribution function of data is significantly
    // different from func. The array data is modified by being sorted into ascending order.
    // Algorithm from Book Numerical Receipes, pp. 737 (4.3.3 Kolmogorov-Smirnov Test)

    // sort the data
    std::sort(Data.begin(), Data.end());

    // loop over sorted data points
    double cdfAtMaxDistance = 0.;
    d = 0.;
    for (size_t j = 0; j < Data.size(); j++){
        double cdf = static_cast<double>(j+1) / static_cast<double>(Data.size()); // cdf of data
        double cdf_func = CumulFunc(Data[j]); // cdf of function
        double tmp_dist = std::max(std::fabs(cdfAtMaxDistance - cdf_func), std::fabs(cdf - cdf_func));
        if (tmp_dist > d) { // Maximum distance
            d = tmp_dist;
            cdfAtMaxDistance = cdf;
        }
    }

    // compute p-value
    double sqrtN = sqrt(Data.size());
    prob = TKolmogorovSmirnovDistr::complementaryCumulativeDistrFunction((sqrtN+0.12+0.11/sqrtN)*d);
}

//--------------------------------------------------------
//Incomplete Gamma function
//--------------------------------------------------------

/* The following computes regularized incomplete gamma functions.
 * Taken from kfunc.c from samtools. Original comment:
* Formulas are taken from Wiki, with additional input from Numerical
* Recipes in C (for modified Lentz's algorithm) and AS245
* (http://lib.stat.cmu.edu/apstat/245).
*/
#define KF_GAMMA_EPS 1e-14
#define KF_TINY 1e-290

double TIncompleteGamma::lower(const double & alpha, const double & z){
    if(z <= 1.0 || z < alpha) return _lower(alpha, z);
    else return 1.0 - _upper(alpha, z);
}

double TIncompleteGamma::upper(const double & alpha, const double & z){
    if(z <= 1.0 || z < alpha) return 1.0 - _lower(alpha, z);
    else return _upper(alpha, z);
}

// regularized lower incomplete gamma function, by series expansion
double TIncompleteGamma::_lower(const double & alpha, const double & z){
    double sum, x;
    int k;
    for (k = 1, sum = x = 1.; k < 100; ++k) {
        sum += (x *= z / (alpha + k));
        if (x / sum < KF_GAMMA_EPS) break;
    }
    return exp(alpha * log(z) - z - gammaLog(alpha + 1.0) + log(sum));
}

// regularized upper incomplete gamma function, by continued fraction
double TIncompleteGamma::_upper(const double & alpha, const double & z){
    int j;
    double C, D, f;
    f = 1. + z - alpha; C = f; D = 0.;
    // Modified Lentz's algorithm for computing continued fraction
    // See Numerical Recipes in C, 2nd edition, section 5.2
    for (j = 1; j < 100; ++j) {
        double a = j * (alpha - j), b = (j<<1) + 1 + z - alpha, d;
        D = b + a * D;
        if (D < KF_TINY) D = KF_TINY;
        C = b + a / C;
        if (C < KF_TINY) C = KF_TINY;
        D = 1. / D;
        d = C * D;
        f *= d;
        if (fabs(d - 1.) < KF_GAMMA_EPS) break;
    }
    return exp(alpha * log(z) - z - gammaLog(alpha) - log(f));
}

//--------------------------------------------------------
// Incomplete Beta function
// Reference: Press et al., Numerical Recipes, 3rd edition, pp. 270-273
//--------------------------------------------------------
double TIncompleteBeta::incompleteBeta(const double & a, const double & b, const double & x){
    // returns incomplete beta function I_x(a, b) for positive a and b, and x between 0 and 1
    double bt;
    static const int SWITCH = 3000; // when to switch to quadrature method
    if (a <= 0.0 || b <= 0.0) throw std::runtime_error("Arguments a (= " + toString(a) + ") and b (= "+ toString(b)+ ") for incomplete beta function must both be >0!");
    if (x < 0.0 || x > 1.0) throw std::runtime_error("Argument x (= " + toString(x) + ") for incomplete beta function must be 0 <= x <= 1!");
    if (x == 0.0 || x == 1.0) return x;
    if (a > SWITCH && b > SWITCH) return _betaiapprox(a,b,x);
    bt=exp(gammaLog(a+b)-gammaLog(a)-gammaLog(b)+a*log(x)+b*log(1.0-x));
    if (x < (a+1.0)/(a+b+2.0)) return bt*_betacf(a,b,x)/a;
    else return 1.0-bt*_betacf(b,a,1.0-x)/b;
}

double TIncompleteBeta::_betacf(const double & a, const double & b, const double & x){
    // evaluates continued fraction for incomplete beta function by modified Lentz's method
    int m,m2;
    double aa,c,d,del,h,qab,qam,qap;
    const double EPS = std::numeric_limits<double>::epsilon();
    const double FPMIN = std::numeric_limits<double>::min()/EPS;
    qab=a+b;
    qap=a+1.0;
    qam=a-1.0;
    c=1.0;
    d=1.0-qab*x/qap;
    if (fabs(d) < FPMIN) d=FPMIN;
    d=1.0/d;
    h=d;
    for (m=1;m<10000;m++) {
        m2=2*m;
        aa=m*(b-m)*x/((qam+m2)*(a+m2));
        d=1.0+aa*d;
        if (fabs(d) < FPMIN) d=FPMIN;
        c=1.0+aa/c;
        if (fabs(c) < FPMIN) c=FPMIN;
        d=1.0/d;
        h *= d*c;
        aa = -(a+m)*(qab+m)*x/((a+m2)*(qap+m2));
        d=1.0+aa*d;
        if (fabs(d) < FPMIN) d=FPMIN;
        c=1.0+aa/c;
        if (fabs(c) < FPMIN) c=FPMIN;
        d=1.0/d;
        del=d*c;
        h *= del;
        if (fabs(del-1.0) <= EPS) break;
    }
    return h;
}

double TIncompleteBeta::_betaiapprox(const double & a, const double & b, const double & x){
    // incomplete beta by quadrature; returns I_x(a, b)
    int j;
    double xu,t,sum,ans;
    double a1 = a-1.0, b1 = b-1.0, mu = a/(a+b);
    double lnmu=log(mu),lnmuc=log(1.-mu);
    // y and w are coefficients for Gauss-Legendre quadrature
    static const double y[18] = {0.0021695375159141994,
                                 0.011413521097787704,0.027972308950302116,0.051727015600492421,
                                 0.082502225484340941, 0.12007019910960293,0.16415283300752470,
                                 0.21442376986779355, 0.27051082840644336, 0.33199876341447887,
                                 0.39843234186401943, 0.46931971407375483, 0.54413605556657973,
                                 0.62232745288031077, 0.70331500465597174, 0.78649910768313447,
                                 0.87126389619061517, 0.95698180152629142};
    static const double w[18] = {0.0055657196642445571,
                                 0.012915947284065419,0.020181515297735382,0.027298621498568734,
                                 0.034213810770299537,0.040875750923643261,0.047235083490265582,
                                 0.053244713977759692,0.058860144245324798,0.064039797355015485,
                                 0.068745323835736408,0.072941885005653087,0.076598410645870640,
                                 0.079687828912071670,0.082187266704339706,0.084078218979661945,
                                 0.085346685739338721,0.085983275670394821};

    t = sqrt(a*b/(pow(a+b, 2)*(a+b+1.0)));
    if (x > a/(a+b)) {
        if (x >= 1.0) return 1.0;
        xu = std::min(1.,std::max(mu + 10.*t, x + 5.0*t));
    } else {
        if (x <= 0.0) return 0.0;
        xu = std::max(0.,std::min(mu - 10.*t, x - 5.0*t));
    }
    sum = 0;
    for (j=0;j<18;j++) {
        t = x + (xu-x)*y[j];
        sum += w[j]*exp(a1*(log(t)-lnmu)+b1*(log(1-t)-lnmuc));
    }
    ans = sum*(xu-x)*exp(a1*lnmu-gammaLog(a)+b1*lnmuc-gammaLog(b)+gammaLog(a+b));
    return ans>0.0? 1.0-ans : -ans;
}

double TIncompleteBeta::inverseIncompleteBeta(const double & p, const double & a, const double & b){
    // inverse of incomplete beta function
    // Returns x such that I_x(a,b) = p for argument p between 0 and 1. Uses Halley's method
    const double EPS = 1.e-8;
    double pp,t,u,err,x,al,h,w,afac,a1=a-1.,b1=b-1.;
    int j;
    if (p <= 0.) return 0.;
    else if (p >= 1.) return 1.;
    else if (a >= 1. && b >= 1.) {
        pp = (p < 0.5)? p : 1. - p;
        t = sqrt(-2.*log(pp));
        x = (2.30753+t*0.27061)/(1.+t*(0.99229+t*0.04481)) - t;
        if (p < 0.5) x = -x;
        al = (pow(x, 2)-3.)/6.;
        h = 2./(1./(2.*a-1.)+1./(2.*b-1.));
        w = (x*sqrt(al+h)/h)-(1./(2.*b-1)-1./(2.*a-1.))*(al+5./6.-2./(3.*h));
        x = a/(a+b*exp(2.*w));
    } else {
        double lna = log(a/(a+b)), lnb = log(b/(a+b));
        t = exp(a*lna)/a;
        u = exp(b*lnb)/b;
        w = t + u;
        if (p < t/w) x = pow(a*w*p,1./a);
        else x = 1. - pow(b*w*(1.-p),1./b);
    }
    afac = -gammaLog(a)-gammaLog(b)+gammaLog(a+b);
    for (j=0;j<10;j++) {
        if (x == 0. || x == 1.) return x;
        err = incompleteBeta(a,b,x) - p;
        t = exp(a1*log(x)+b1*log(1.-x) + afac);
        u = err/t;
        x -= (t = u/(1.-0.5*std::min(1.,u*(a1/x - b1/(1.-x)))));
        if (x <= 0.) x = 0.5*(x + t);
        if (x >= 1.) x = 0.5*(x + t + 1.);
        if (fabs(t) < EPS*x && j > 0) break;
    }
    return x;
}

//------------------------------------------------
// binomial p-values
//------------------------------------------------
double TBinomPValue::_binomPValue(const int & k, const int & l) const {
    // p = 0.5
    double cumul = 0.0;
    //int n = k + l;
    for(int i = 0; i <= std::min(k,l); i++){
        cumul += exp(chooseLog(l, i) + -0.6931472*l); // -0.6931472 is log(0.5)
    }
    return cumul;
}

TBinomPValue::TBinomPValue(){
    tableIsInitalized = false;
    binomPValueTable = nullptr;
    initializeTable();
}

TBinomPValue::~TBinomPValue() {
    if (tableIsInitalized){
        for(int i=0; i<binomPValueTableSize; ++i)
            delete[] binomPValueTable[i];
        delete[] binomPValueTable;
    }
}

void TBinomPValue::initializeTable(){
    binomPValueTable = new double*[binomPValueTableSize];

    //loop over all entries
    for(int i=0; i<binomPValueTableSize; i++) {
        //get row size
        int rowLength = std::floor(i / 2) + 1;

        //allocate memory
        binomPValueTable[i] = new double[rowLength];

        //fill row
        for (int j = 0; j < rowLength; j++)
            binomPValueTable[i][j] = _binomPValue(j, i - j);
    }
    tableIsInitalized = true;
}

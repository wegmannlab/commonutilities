/*
 * math_functions.h
 *
 *  Created on: Dec 10, 2018
 *      Author: phaentu
 */

#ifndef MATHFUNCTIONS_H_
#define MATHFUNCTIONS_H_

#include <cmath>
#include <limits>
#include <array>
#include <cassert>
#include <numeric>
#include "stringFunctions.h"

//some usful constants not defined
#define M_TWO_PI		6.283185307179586231996	/* 2*pi */
#define M_SQRT_TWO_PI	2.506628274631000241612	/* sqrt(2*pi) */

//------------------------------------------------
// Gamma function
//------------------------------------------------
double gammaLog(const double & z);

//------------------------------------------------
// Digamma function
//------------------------------------------------
class TDigamma {
protected:
    static double _psiSeries(const double & z);
public:
    static double digamma(const double & x);
};

//------------------------------------------------
// Incomplete gamma function
//------------------------------------------------
class TIncompleteGamma {
protected:
    static double _lower(const double & alpha, const double & z);
    static double _upper(const double & alpha, const double & z);
public:
    static double lower(const double & alpha, const double & z);
    static double upper(const double & alpha, const double & z);
};

//------------------------------------------------
// Incomplete beta function
//------------------------------------------------

class TIncompleteBeta {
protected:
    static double _betacf(const double & a, const double & b, const double & x);
    static double _betaiapprox(const double & a, const double & b, const double & x);
public:
    static double incompleteBeta(const double & a, const double & b, const double & x);
    static double inverseIncompleteBeta(const double & p, const double & a, const double & b);
};

//------------------------------------------------
// Base class distribution
//------------------------------------------------
class TDistr {
protected:
    static void checkArgWithMin(const double & val, const double & min, bool minIncluded, const std::string& nameVal, const std::string & distr);
    static void checkArgWithMax(const double & val, const double & max, bool maxIncluded, const std::string& nameVal, const std::string & distr);

    // every distribution class should have:
    static void checkArgs(){}; // check if input arguments are valid
    // functions to calculate the density
    static double density(){return 0.;}; // public, calls the two functions below
    static double calcDens(){return 0.;}; // private
    static double calcDensLog(){return 0.;}; // private
    // functions to calculate the cumulative distribution function
    static double cumulativeDistrFunction(){return 0.;};
    // functions to calculate the inverse cumulative distribution function
    static double invCumulativeDistrFunction(){return 0.;};
};

//------------------------------------------------
// Beta distribution
//------------------------------------------------
class TBetaDistr : public TDistr {
protected:
    static double calcDensLogBeta(const double & x, const double & alpha, const double & beta);
    static double calcDensBeta(const double & x, const double & alpha, const double & beta);
    static void checkArgs(const double & x, const double & alpha, const double & beta);
public:
    TBetaDistr()= default;
    static double density(const double & x, const double & alpha, const double & beta, bool Log = false);
};

//------------------------------------------------
// Exponential distribution
//------------------------------------------------
class TExponentialDistr : public TDistr {
protected:
    static double calcDensLogExponential(const double & x, const double & lambda);
    static double calcDensExponential(const double & x, const double & lambda);
    static void checkArgs(const double & x, const double & lambda);

public:
    TExponentialDistr()= default;
    static double density(const double & x, const double & lambda, bool Log = false);
    static double cumulativeDistrFunction(const double & x, const double & lambda);
    static double mean(const double & lambda);
};

//--------------------------------------------------------
//Chisq Distribution
//--------------------------------------------------------
class TChisqDistr : public TDistr {
protected:
    static double calcDensLogChisq(const double & x, int k);
    static double calcDensChisq(const double & x, int k);
    static void checkArgs(const double & x, int k);

public:
    TChisqDistr()= default;
    static double density(const double & x, int k, bool Log = false);
};

//--------------------------------------------------------
//Chi Distribution
//--------------------------------------------------------
class TChiDistr : public TDistr {
protected:
    static double calcDensLogChi(const double & x, int k);
    static double calcDensChi(const double & x, int k);
    static void checkArgs(const double & x, int k);

public:
    TChiDistr()= default;
    static double density(const double & x, int k, bool Log = false);
};

//--------------------------------------------------------
//Normal Distribution
//--------------------------------------------------------
class TNormalDistr : public TDistr {
protected:
    static double calcDensLogNormal(const double & x, const double & mean, const double & sd);
    static double calcDensNormal(const double & x, const double & mean, const double & sd);
    static double normalComplementaryErrorFunction(const double & x);
    static double normalComplementaryErrorCheb(const double & x);
    static double invNormalComplementaryErrorFunction(const double & p);
    static void checkArgs(const double & sd);

public:
    TNormalDistr()= default;
    static double density(const double & x, const double & mean = 0.0, const double & sd = 1.0, bool Log = false);
    static double cumulativeDistrFunction(const double & x, const double & mean = 0.0, const double & sd = 1.0);
    static double invCumulativeDistrFunction(const double & p, const double & mean = 0.0, const double & sd = 1.0);
};

//--------------------------------------------------------
//Binomial Distribution
//--------------------------------------------------------
class TBinomialDistr : public TDistr {
protected:
    static double calcDensLogBinom(int n, int k, const double & p);
    static double calcDensBinom(int n, int k, const double & p);
    static void checkArgs(int n, int k, const double & p);

public:
    TBinomialDistr()= default;
    static double density(int n, int k, const double & p, bool Log = false);
};

//------------------------------------------------
// Bernouilli distribution
//------------------------------------------------
class TBernouilliDistr : public TDistr {
protected:
    static double calcDensLogBernouilli(const bool & x, const double & pi);
    static double calcDensBernouilli(const bool & x, const double & pi);
    static void checkArgs(const bool & x, const double & pi);

public:
    TBernouilliDistr()= default;
    static double density(const bool & x, const double & pi, const bool & Log = false);
    static double mean(const double & pi);
};

//------------------------------------------------
// Dirichlet distribution
//------------------------------------------------
class TDirichletDistr : public TDistr {
protected:
    // check input arguments
    static void _checkArgs(const std::vector<double> & x, const std::vector<double> & alpha);
    static void _checkX(const std::vector<double> & x, const std::vector<double> &alpha);
    static void _checkSumX(const std::vector<double> &x);
    static void _checkAlpha(const std::vector<double> & alpha);

    // calculate densities
    static double _calcDensLogDirichlet(const std::vector<double> & x, const std::vector<double> & alpha);
    static double _calcDensDirichlet(const std::vector<double> & x, const std::vector<double> & alpha);
    static double _calcBetaFunction(const std::vector<double> &alpha);
    static double _calcLogBetaFunction(const std::vector<double> &alpha);
    static double _calcProductDirichlet(const std::vector<double> &x, const std::vector<double> &alpha);
    static double _calcLogProductDirichlet(const std::vector<double> &x, const std::vector<double> &alpha);

public:
    TDirichletDistr()= default;
    static double density(const std::vector<double> & x, const std::vector<double> & alpha, bool Log = false);
};

class TDirichletDistrReUsable : public TDirichletDistr {
protected:
    // parameters
    std::vector<double> _alpha;

    // temporary values, stored for speed
    double _betaConstant;
    double _logBetaConstant;

public:
    TDirichletDistrReUsable();
    TDirichletDistrReUsable(const std::vector<double> & alpha);
    void resetParameters(const std::vector<double> & alpha);
    double density(const std::vector<double> & x, bool Log = false) const;
    double getBetaConstant(bool Log = false) const;
    const std::vector<double>& alpha() const;
};


//--------------------------------------------------------
//Gamma Distribution
//--------------------------------------------------------
class TGammaDistr : public TDistr {
protected:
    static double calcDensLogGamma(const double & x, const double & alpha, const double & beta);
    static double calcDensGamma(const double & x, const double & alpha, const double & beta);
    static void checkArgs(const double & x, const double & alpha, const double & beta);

public:
    TGammaDistr() = default;
    static double density(const double & x, const double & alpha, const double & beta, bool Log = false);
    static double cumulativeDistrFunction(const double & x, const double & alpha, const double & beta);
};

//--------------------------------------------------------
//Generalized Pareto Distribution
//--------------------------------------------------------

class TParetoDistr : public TDistr {
protected:
    static double calcDensLogPareto(const double & x, const double & locationMu, const double & scaleSigma, const double & shapeXi);
    static double calcDensPareto(const double & x, const double & locationMu, const double & scaleSigma, const double & shapeXi);
    static void checkArgs(const double & x, const double & locationMu, const double & scaleSigma, const double & shapeXi);
public:
    TParetoDistr()=default;
    static double density(const double & x, const double & locationMu, const double & scaleSigma, const double & shapeXi, bool Log = false);
    static double cumulativeDistrFunction(const double & x, const double & locationMu, const double & scaleSigma, const double & shapeXi);
};

//--------------------------------------------------------
//Kolmogorov-Smirnov Distribution
//--------------------------------------------------------

class TKolmogorovSmirnovDistr : public TDistr {
protected:
    static void _checkArgs(const double & z);
    static double _invxlogx(const double & y);

public:
    TKolmogorovSmirnovDistr() = default;
    static double cumulativeDistrFunction(const double & z);
    static double complementaryCumulativeDistrFunction(const double & z);
    static double invComplementaryCumulativeDistrFunction(const double & q);
    static double invCumulativeDistrFunction(const double & z);
};

//--------------------------------------------------------
//Kolmogorov-Smirnov Test
//--------------------------------------------------------

void runOneSampleKolmogorovSmirnovTest(std::vector<double> & Data, double CumulFunc(const double&), double & d, double & prob);

//----------------------------------------------------------------
// Factorials, see Numerical recipes (third edition), chapter 6.1
//----------------------------------------------------------------
class TFactorial {
public:
	template <typename T> static double factorial(const T & n){
		static_assert(std::is_integral<T>::value, "Integral required.");
	    //Returns the value n! as a floating-point number.
	    static std::array<double, 171> factorialTable{};
	    static bool init = true;
	    if (init) {
	        init = false;
	        factorialTable[0] = 1.;
	        for (int i=1;i<171;i++)
	            factorialTable[i] = i*factorialTable[i-1];
	    }
	    if (n < 0 || n > 170)
	        throw std::runtime_error("Argument n (= " + toString(n) + " in factorial must be 0 <= n <= 170!");
	    return factorialTable[n];
	}

	template <typename T> static double factorialLog(const T & n){
		static_assert(std::is_integral<T>::value, "Integral required.");
	    // Returns ln(n!)
	    static const int NTOP = 2000;
	    static std::array<double, NTOP> factorialTable{};
	    static bool init=true;
	    if (init) {
	        init = false;
	        for (int i=0;i<NTOP;i++) factorialTable[i] = gammaLog(i+1.);
	    }
	    if (n < 0)
	        throw std::runtime_error("Argument n (= " + toString(n) + " in factorialLog must be >= 0!");
	    if (n < NTOP)
	        return factorialTable[n];
	    return gammaLog(n + 1.);
	}
};

//------------------------------------------------
// choose function (binomial coefficient)
//------------------------------------------------
template <typename T> double chooseLog(const T & n, const T & k){
	return TFactorial::factorialLog(n) - TFactorial::factorialLog(k) - TFactorial::factorialLog(n-k);
}

template <typename T> int choose(const T & n, const T & k){
    // note: results are not accurate if n is large (if factorial(n) reaches numerical limits of double)
    return TFactorial::factorial(n)  / TFactorial::factorial(k) / TFactorial::factorial(n-k);
}

//------------------------------------------------
// binomial p-value
//------------------------------------------------
class TBinomPValue {
private:
    double _binomPValue(const int & k, const int & l) const;
    const int binomPValueTableSize = 100;
    double ** binomPValueTable;
    bool tableIsInitalized;
    void initializeTable();
public:
    TBinomPValue();
    ~TBinomPValue();
    template <typename T> double binomPValue(const T & k, const T & l) const {
    	static_assert(std::is_integral<T>::value, "Integral required.");
    	int n = l + k;
        if (n < binomPValueTableSize){
           return binomPValueTable[n][std::min(k,l)];
        }
        return _binomPValue(k, l);
    }
};

//------------------------------------------------
// Logit and logistic function
//------------------------------------------------
template <typename T> double logit(const T & x) {
    assert(x >= 0. && x <= 1.);
    return log((double) x / (1. - (double) x));
}

template <typename T> double logistic(const T & x) {
    return 1. / (1. + exp(- (double) x));
}

//------------------------------------------------
// types and numeric limits
//------------------------------------------------

//------------------------------------------------
// addition

template<typename T>
bool checkForNumericOverflow_addition(T x, T y) {
    // overflow
    if (y > 0 && x > std::numeric_limits<T>::max() - y) { //x + y would overflow
        return false;
    }
    // underflow
    if (y < 0 && x < std::numeric_limits<T>::lowest() - y) { //x + y would underflow
        return false;
    }
    return true;
}

// for unsigned integers: y argument is passed as integer, otherwise we could never subtract!
template<typename T, std::enable_if_t<std::is_integral<T>::value, int> = 0, std::enable_if_t<std::is_unsigned<T>::value, int> = 0>
bool checkForNumericOverflow_addition(T x, int y) {
    // overflow
    if (y > 0 && x > std::numeric_limits<T>::max() - y) { //x + y would overflow
        return false;
    }
    // underflow
    if (y < 0 && x < std::numeric_limits<T>::lowest() - y) { //x + y would underflow
        return false;
    }
    return true;
}

//------------------------------------------------
// subtraction

template<typename T>
bool checkForNumericOverflow_subtraction(T x, T y) {
    // overflow
    if (y < 0 && x > std::numeric_limits<T>::max() + y) { //x - y would overflow
        // x - (-y) = x + y -> check for max
        return false;
    }
    // underflow
    if (y > 0 && x < std::numeric_limits<T>::lowest() + y) { //x - y would underflow
        return false;
    }
    return true;
}

// for unsigned integers: y argument is passed as integer, otherwise we could never subtract!
template<typename T, std::enable_if_t<std::is_integral<T>::value, int> = 0, std::enable_if_t<std::is_unsigned<T>::value, int> = 0>
bool checkForNumericOverflow_subtraction(T x, int y) {
    // overflow
    if (y < 0 && x > std::numeric_limits<T>::max() + y) { //x - y would overflow
        // x - (-y) = x + y -> check for max
        return false;
    }
    // underflow
    if (y > 0 && x < std::numeric_limits<T>::lowest() + y) { //x - y would underflow
        return false;
    }
    return true;
}

//------------------------------------------------
// multiplication
//------------------------------------------------
// things to realize:

//        I-------------------------|-------------------------I
//       min                        0                        max

//      -> can cross numeric max by...
//          1) x>0 * y>0
//          2) x<0 * y<0
//      -> can cross numeric min by...
//          1) x>0 * y<0
//          2) x>0 * y>0

//      -> numeric_min is (usually) negative and numeric_max is always positive

//      -> when multiplying/dividing two negative values, a positive value results -> this complicates things, as
//          comparison with < and > then changes -> we need to take these different cases into account!

template<typename T>
bool checkForNumericOverflow_multiplication(T x, T y) {
    if (y == 0){ // avoid division by zero errors
        return true;
    }
    // overflow
    if ((x > 0 && y > 0) && x > std::numeric_limits<T>::max()/y) { // for the case both are positive -> max()/y > 0 -> x must be larger than this
        return false;
    } // overflow
    else if ((x < 0 && y < 0) && -x > -(std::numeric_limits<T>::max()/y)) { // for the case both are negative -> max()/-y < 0 -> x must be smaller than this
        return false;
    }
    // underflow
    if ((x > 0 && y < 0) && x > std::numeric_limits<T>::lowest()/y) { // for the case that x positive and y negative -> lowest() is also negative -> -lowest()/-y > 0 -> x must be larger than this
        return false;
    } // underflow
    else if((x < 0 && y > 0) && x < std::numeric_limits<T>::lowest()/y){ // for the case that x negative and y positive -> lowest()/-y < 0 -> x must be smaller
        return false;
    }
    return true;
}

// for signed integers: also need to check if one number is -1 and another is min(), because multiplying them we get abs(min()) which is 1 higher than max()
template<typename T, std::enable_if_t<std::is_integral<T>::value, int> = 0, std::enable_if_t<std::is_signed<T>::value, int> = 0>
bool checkForNumericOverflow_multiplication(int x, int y) {
    if (y == 0){ // avoid division by zero errors
        return true;
    }
    else if ((x == -1 && y == std::numeric_limits<T>::lowest()) || (y == -1 && x == std::numeric_limits<T>::lowest())){
        return false;
    }
    // overflow
    else if ((x > 0 && y > 0) && x > std::numeric_limits<T>::max()/y) { // for the case both are positive -> max()/y > 0 -> x must be larger than this
        return false;
    } // overflow
    else if ((x < 0 && y < 0) && std::abs(x) > std::abs(std::numeric_limits<T>::max()/y)) { // for the case both are negative -> max()/-y < 0 -> x must be smaller than this
        return false;
    }
    // underflow
    // check first if y!=-1, because otherwise we divide lowest()/-1 -> abs(min()) -> higher than max()
    if (y != -1 && (x > 0 && y < 0) && x > std::numeric_limits<T>::lowest()/y) { // for the case that x positive and y negative -> lowest() is also negative -> -lowest()/-y > 0 -> x must be larger than this
        return false;
    } // underflow
    else if(y != -1 && (x < 0 && y > 0) && x < std::numeric_limits<T>::lowest()/y){ // for the case that x negative and y positive -> lowest()/-y < 0 -> x must be smaller
        return false;
    }
    return true;
}
#endif /* MATHFUNCTIONS_H_ */

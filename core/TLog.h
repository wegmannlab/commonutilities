/*
 * TLog.h
 *
 *  Created on: Oct 17, 2010
 *      Author: wegmannd
 */

#ifndef TLOG_H_
#define TLOG_H_

#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <sys/time.h>
#include "progressTools.h"
#include "stringFunctions.h"

class TLog{
private:
	std::ofstream _file;
	bool _isFile;
	std::string _filename;
	bool _isVerbose;
	bool _printWarnings;
	long _lastLineStartInFile;

	//string constants
	std::string _indentSymbol = "   ";
	std::string _listSymbol = "-";
	std::string _concludeSymbol = "->";
	std::string _numberSymbol = ") ";

	//Indent
	int _numIndent;
	std::string _indentOnlyTabs;

	void _fillIndentString(){
		_indentOnlyTabs="";
		for(int i=0; i<_numIndent; ++i) _indentOnlyTabs += _indentSymbol;
	};

	template<typename... Ts>
	std::string _listString(const Ts&... out){
		return _indentOnlyTabs + _listSymbol + toString(out...);
	};

	template<typename... Ts>
	std::string _concludeString(const Ts&... out){
		return _indentOnlyTabs + _indentSymbol + _concludeSymbol + toString(out...);
	};

	//numbering
	int _numberingLevel;
	std::vector<int> _numberingIndex;
	TTimer _timer;

	template<typename... Ts>
	std::string _numberString(const Ts&... out){
		std::string s = _indentOnlyTabs + toString(_numberingIndex[_numberingLevel]) + _numberSymbol;
		//make sure numbers up to 99 align
		if(_numberingIndex[_numberingLevel] < 10) s += ' ';

		return s + toString(out...);
	};

	//general function to write
	void _write(const std::string & Out, const bool & ToFile=true, const bool & ToOut=true){
		if(ToFile && _isFile){
			_file << Out << std::endl;
			_lastLineStartInFile=_file.tellp();
		}
		if(ToOut && _isVerbose) std::cout << Out << std::endl;
	};

	void _overWrite(const std::string  & out, const bool & ToFile=true, const bool & ToOut=true){
		if(ToFile && _isFile){
			_file.seekp(_lastLineStartInFile);
			_file << out << std::endl;
			_lastLineStartInFile=_file.tellp();
		}
		if(ToOut && _isVerbose) std::cout << '\xd' << out << std::endl;
	};

	void _flush(const std::string & out, const bool & ToFile=true, const bool & ToOut=true){
		if(ToFile && _isFile) _file << out << std::flush;
		if(ToOut && _isVerbose) std::cout << out << std::flush;
	};

	void _overFlush(const std::string & out, const bool & ToFile=true, const bool & ToOut=true){
		if(ToFile && _isFile){
			 _file.seekp(_lastLineStartInFile);
			 _file << out << std::flush;
		}
		if(ToOut && _isVerbose) std::cout << '\xd' << out << std::flush;
	};

	void _writeError(const std::string & out){
		if(_isFile){
			_file << out << std::endl << std::endl;
			_lastLineStartInFile=_file.tellp();
		}
		std::cerr << out << std::endl << std::endl;
	};

	//Init
	void _init(){
		_isFile = false;
		_isVerbose = true;
		_printWarnings = true;
		_numIndent = 0;
		_lastLineStartInFile = 0;
		_fillIndentString();
		_numberingLevel = -1;
	};

public:

	TLog(){
		_init();
		setDefaultSymbols();
	};

	TLog(const std::string & IndentSymbol, const std::string & ListSymbol, const std::string & ConcludeSymbol, const std::string & NumberSymbol){
		_init();
		setSymbols(IndentSymbol, ListSymbol, ConcludeSymbol, NumberSymbol);
	};

	TLog(std::string & Filename){
		_init();
		setDefaultSymbols();
		openFile(Filename);
	};

	void close(){
		newLine();
		if(_isFile) _file.close();
		_isFile=false;
	};

	void setDefaultSymbols(){
		_indentSymbol = "   ";
		_listSymbol = "- ";
		_concludeSymbol = "-> ";
		_numberSymbol = ") ";
	};

	void setSymbols(const std::string & IndentSymbol = "   ", const std::string & ListSymbol = "- ", const std::string & ConcludeSymbol = "-> ", const std::string & NumberSymbol = ") "){
		setIndentSymbol(IndentSymbol);
		setListSymbol(ListSymbol);
		setConcludeSymbol(ConcludeSymbol);
		setNumberSymbol(NumberSymbol);
	};

	void setIndentSymbol(const std::string & IndentSymbol){
		_indentSymbol = IndentSymbol;
	};
	void setListSymbol(const std::string & ListSymbol){
		_listSymbol = ListSymbol;
	};
	void setConcludeSymbol(const std::string & ConcludeSymbol){
		_concludeSymbol = ConcludeSymbol;
	};
	void setNumberSymbol(const std::string & NumberSymbol){
		_numberSymbol = NumberSymbol;
	};


	~TLog(){ close();};

	void openFile(std::string Filename){
		list("Writing log to '" + Filename + "'");
		_filename = Filename;
		_file.open(_filename.c_str());
		if(!_file) throw "Unable to open logfile '"+ _filename +"'!";
		_isFile = true;
		_lastLineStartInFile = _file.tellp();
	};

	void setVerbose(bool Verbose){ _isVerbose=Verbose;};
	bool verbose(){return _isVerbose;};
	void suppressWarings(){_printWarnings=false;};
	void showWarings(){_printWarnings=true;};

	void newLine(){
		if(_isFile){
			_file << std::endl;
			_lastLineStartInFile=_file.tellp();
		}
		std::cout << std::endl;
	};

	std::string getFilename(){
		if(_isFile) return _filename;
		else return "";
	};

	template<typename T>
	void add(const T & out){
		if(_isFile) _file << out;
		if(_isVerbose) std::cout << out;
	};

	//---------------------------------------------------------
	// write, list, conclude
	//---------------------------------------------------------
	template<typename... Ts>
	void write(const Ts&... out){
		_write(toString(out...));
	};

	void done(){
		_write(" done!");
	};

	void doneTime(){
		_timer.stop();
		_write(" done (in " + _timer.formattedTime() + ")!");
	};

	template<typename... Ts>
	void list(const Ts&... out){
		_write(_listString(out...));
	};

	template<typename... Ts>
	void conclude(const Ts&... out){
		_write(_concludeString(out...));
	};

	//---------------------------------------------------------
	// flush, listFlush, ...
	//---------------------------------------------------------
	template<typename... Ts>
	void flush(const Ts&... out){
		_flush(toString(out...));
	};

	template<typename... Ts>
	void listFlush(const Ts&... out){
		_flush(_listString(out...));
	};

	template<typename... Ts>
	void listFlushDots(const Ts&... out){
		_flush(_listString(out...) +  " ...");
	};

	template<typename... Ts>
	void listFlushTime(const Ts&... out){
		_timer.start();
		_flush(_listString(out...));
	};

	//---------------------------------------------------------
	//Indent
	//---------------------------------------------------------
	void addIndent(const int & n=1){
		_numIndent += n;
		_fillIndentString();
	};

	void removeIndent(const int & n=1){
		_numIndent -= n;
		if(_numIndent<0) _numIndent=0;
		_fillIndentString();
	};

	void clearIndent(){
		_numIndent = 0;
		_fillIndentString();
	};

	template<typename... Ts>
	void startIndent(const Ts&... out){
		list(out...);
		addIndent();
	};

	template<typename... Ts>
	void endIndent(const Ts&... out){
		list(out...);
		removeIndent();
	};

	void endIndent(){
		removeIndent();
	};

	//---------------------------------------------------------
	//Numbering
	//---------------------------------------------------------
	void addNumberingLevel(){
		++_numberingLevel;
		_numberingIndex.push_back(1);
		addIndent();
	};

	void removeNumberingLevel(){
		if(_numberingLevel>=0){
			_numberingIndex.erase(_numberingIndex.end()-1);
			--_numberingLevel;
			removeIndent();
		}
	};

	template<typename... Ts>
	void startNumbering(const Ts&... out){
		list(out...);
		addNumberingLevel();
	};

	void endNumbering(){
		removeNumberingLevel();
	};

	template<typename... Ts>
	void endNumbering(const Ts&... out){
		number(out...);
		removeNumberingLevel();
	};

	template<typename... Ts>
	void number(const Ts&... out){
		_write(_numberString(out...));
		++_numberingIndex[_numberingLevel];
	};

	template<typename... Ts>
	void numberWithIndent(const Ts&... out){
		number(out...);
		addIndent();
	};

	template<typename... Ts>
	void numberFlush(const Ts&... out){
		_flush(_numberString(out...));
		++_numberingIndex[_numberingLevel];
	};

	//---------------------------------------------------------
	// overWrite, overList
	//---------------------------------------------------------
	template<typename... Ts>
	void overWrite(const Ts&... out){
		_overWrite(toString(out...));
	};

	template<typename... Ts>
	void overList(const Ts&... out){
		_overWrite(_listString(out...));
	};

	template<typename... Ts>
	void overFlush(const Ts&... out){
		_overFlush(toString(out...));
	};

	template<typename... Ts>
	[[deprecated("use overListFlush")]] void listOverFlush(const Ts&... out){
		_overFlush(_listString(out...));
	};

	//identical as above but with more meanignful name
	template<typename... Ts>
	void overListFlush(const Ts&... out){
		_overFlush(_listString(out...));
	};

	template<typename... Ts>
	void overNumber(const Ts&... out){
		_overWrite(_numberString(out...));
	};

	template<typename... Ts>
	void overNumberFlush(const Ts&... out){
		_overFlush(_numberString(out...));
	};

	//---------------------------------------------------------
	// file only, std::out only
	//---------------------------------------------------------
	template<typename... Ts>
	void writeFileOnly(const Ts&... out){
		_write(toString(out...), true, false);
	};

	template<typename... Ts>
	void listFileOnly(const Ts&... out){
		_write(_listString(out...), true, false);
	};

	template<typename... Ts>
	void flushFileOnly(const Ts&... out){
		_flush(toString(out...), true, false);
	};

	template<typename... Ts>
	void listFlushFileOnly(const Ts&... out){
		_flush(_listString(out...), true, false);
	};

	template<typename... Ts>
	void writeNoFile(const Ts&... out){
		_write(toString(out...), false, true);
	};

	template<typename... Ts>
	void listNoFile(const Ts&... out){
		_write(_listString(out...), false, true);
	};

	template<typename... Ts>
	void flushNoFile(const Ts&... out){
		_flush(toString(out...), false, true);
	};

	template<typename... Ts>
	void listFlushNoFile(const Ts&... out){
		_flush(_listString(out...), false, true);
	};

	//---------------------------------------------------------
	//warning / error
	//---------------------------------------------------------
	template<typename... Ts>
	void warning(const Ts&... out){
		if(_printWarnings){
			_writeError("WARNING: " + toString(out...));
		}
	};

	template<typename... Ts>
	void error(const Ts&... out){
		_writeError("ERROR: " + toString(out...));
	};

	//---------------------------------------------------------
	//fixed width
	//---------------------------------------------------------

	template<typename T>
	void flushFixedWidth(const T & out, const int & width){
		if(_isFile) _file << std::setw(width) << out << std::flush;
		if(_isVerbose) std::cout << std::setw(width) << out << std::flush;
	};

	template<typename T>
	void flushNumberFixedWidth(const T & out, const int & precision, const int & width){
		if(_isFile){
			std::ios::fmtflags old_settings = _file.flags();
			_file << std::setw(width) << std::fixed << std::setprecision(precision) << out << std::flush;
			_file.flags(old_settings);
		}
		if(_isVerbose){
			std::ios::fmtflags old_settings = std::cout.flags();
			std::cout << std::setw(width) << std::fixed << std::setprecision(precision) << out << std::flush;
			std::cout.flags(old_settings);
		}
	};

	template<typename T>
	void writeFixedWidth(const T & out, const int & width){
		if(_isFile){
			_file << std::setw(width) << out << std::endl;
			_lastLineStartInFile=_file.tellp();
		}
		if(_isVerbose) std::cout << std::setw(width) << out << std::endl;
	};

	template<typename T>
	void flushScientific(const T & out, const int & precision, const int & width){
		if(_isFile){
			std::ios::fmtflags old_settings = _file.flags();
			_file << std::setw(width) << std::scientific << std::setprecision(precision)  << out << std::flush;
			_file.flags(old_settings);
		}
		if(_isVerbose){
			std::ios::fmtflags old_settings = std::cout.flags();
			std::cout << std::setw(width) << std::scientific << std::setprecision(precision)  << out << std::flush;
			std::cout.flags(old_settings);
		}
	};
	template<typename T>
	void writeScientific(const T & out, const int & precision){
		if(_isFile){
			_file << std::scientific << std::setprecision(precision) << out << std::endl;
			_lastLineStartInFile=_file.tellp();
		}
		if(_isVerbose){
			std::cout.precision(precision);
			std::cout << std::scientific << std::setprecision(precision) << out << std::endl;
		}
	};

};





#endif /* TLOG_H_ */

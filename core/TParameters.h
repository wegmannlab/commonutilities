//---------------------------------------------------------------------------

#ifndef TParametersH
#define TParametersH

#include <map>
#include <vector>
#include <set>
#include <fstream>
#include "TLog.h"
#include "stringFunctions.h"
#include "TFile.h"

//---------------------------------------------------------------------------
// TParameter
//  a single parameter and its value
//---------------------------------------------------------------------------
class TParameter{
public:
	std::string name;
	mutable std::string value;
	mutable bool used;
	mutable bool isFileName;

	TParameter(){
		used = false;
		isFileName = false;
	};
	TParameter(const std::string& Name){
		name = Name;
		used = false;
		isFileName = false;
	};
	TParameter(const std::string& Name, const std::string& Value){
		name = Name;
		value = Value;
		used = false;
		isFileName = false;
	};
    friend bool operator<(const TParameter& left, const TParameter& right){
    	return left.name < right.name;
    };
    bool operator<(const std::string & right) const{
		return this->name < right;
	};
    friend bool operator<(const std::string & left, const TParameter & right){
    	return left < right.name;
    };
    friend bool operator<(const TParameter & left, const std::string & right){
    	return left.name < right;
    };
};

//---------------------------------------------------------------------------
// TParameters
// a set of TParameter and functions to interact with it.
//---------------------------------------------------------------------------
class TParameters{
private:
    std::set<TParameter> _parameters;
    bool _inputFileRead;
    std::string _inputFileName;
    std::string _nameExecutable;
    bool _argsAreSpaced; // format with which parameters were specified, used for outputting command in matching format if developer error occurs

    void _initialize(std::vector<std::string> & commandLineParams, TLog* logfile);
    std::string _getParameter(const std::string & Name, bool mandatory, const std::string & Explanation, bool isFilename = false);

    void _parseArgsWithSpace(std::vector<std::string>::iterator it, std::vector<std::string> & commandLineParams);
    void _parseArgsWithEqualSign(std::vector<std::string>::iterator it, std::vector<std::string> & commandLineParams);

    template <typename T> T _convertAndCheck(const std::string & Name, const std::string & Param){
		try{
			return convertStringCheck<T>(Param);
		}
		catch (std::string & error){
			throw "Failed to parse parameter '" + Name + "': " + error;
		}
		catch (const char* error){
			throw "Failed to parse parameter '" + Name + "': " + error;
		}
		catch(std::exception & error){
			throw "Failed to parse parameter '" + Name + "': " + error.what();
		}
		catch (...){
			throw "Failed to parse parameter '" + Name + "'!";
		}
	};

public:
    TParameters();
    TParameters(std::vector<std::string> & commandLineParams, TLog* Logfile);
    TParameters(int & argc, char** argv, TLog* Logfile);
    void init(int & argc, char** argv, TLog* Logfile);
    void clear();

    //read parameters
    void addParameter(const std::string& name);
    void addParameter(const std::string& name, const std::string &value);

    bool readInputfile(const std::string& fileName, TLog* logfile);
    std::string getInputFile() const { return _inputFileName;};
    bool hasInputFileBeenRead() const {return _inputFileRead;};

    //parameter exists
    bool parameterExists(const std::string& Name);

    //fill parameter
    template <typename T, typename... Ts>
    void fillParameter(const std::string & Name, T & Dest, Ts ...input){
    	Dest = getParameter(Name, input...);
    };

    //get parameter
    template <typename T = std::string> T getParameter(const std::string & Name, const bool mandatory=true){
    	std::string str = _getParameter(Name, mandatory, "");
    	return _convertAndCheck<T>(Name, str);
    };

    template <typename T = std::string> T getParameter(const std::string & Name, const std::string & Explanation){
    	std::string str = _getParameter(Name, true, Explanation);
    	return _convertAndCheck<T>(Name, str);
    };

    template <typename T = std::string> T getParameterWithDefault(const std::string & Name, const T & Default){
        if(parameterExists(Name)){
        	std::string str = _getParameter(Name, true, "");
        	return _convertAndCheck<T>(Name, str);
        } else {
        	return Default;
        }
    };

    //get parameter filename
    std::string getParameterFilename(const std::string & Name, const bool& mandatory=true);
    std::string getParameterFilename(const std::string & Name, const std::string & Explanation);
    void setParameterIsFilename(const std::string & Name);

    //fill parameter Container
    template<typename CONTAINER> void fillParameterIntoContainer(const std::string& Name, CONTAINER & Container, const char Delim, const bool mandatory=true){
		std::string str = _getParameter(Name, mandatory, "");
		fillContainerFromString(str, Container, Delim, true);
	};

    template<typename CONTAINER> void fillParameterIntoContainer(const std::string& Name, CONTAINER & Container, const char Delim, const std::string & Explanation){
		std::string str = _getParameter(Name, true, Explanation);
		fillContainerFromString(str, Container, Delim, true);
	};

    template<typename CONTAINER> void fillParameterIntoContainerWithDefault(const std::string & Name, CONTAINER & Container, const char Delim, const CONTAINER & Default){
    	if(parameterExists(Name)){
    		fillParameterIntoContainer(Name, Container, Delim, true);
    	} else {
    		Container = Default;
    	}
	};

    //access parameter lists
    std::string getListOfUnusedParameters();
    std::string getListOfUsedParametersAndVals_commandLineFormat();
    void writeUsedParametersAndValsToFile(TOutputFile & file);
    std::vector<std::string> getVecOfUsedFilenames();
    std::string getNameExecutable(){return _nameExecutable;};
};
#endif

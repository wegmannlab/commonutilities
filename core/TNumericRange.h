/*
 * TRange.h
 *
 *  Created on: May 14, 2021
 *      Author: phaentu
 */

#ifndef COMMONUTILITIES_TNUMERICRANGE_H_
#define COMMONUTILITIES_TNUMERICRANGE_H_


#include <cstdint>
#include <limits>
#include "stringFunctions.h"

//-------------------------------------
// TNumericRange
//-------------------------------------
template <typename T>
class TNumericRange{
private:
	T _min, _max;
	bool _minIncluded, _maxIncluded;

	constexpr void _check(){
		if(_min > _max){
			throw "Min (" + toString(_min) + ") > Max (" + toString(_max) + ")!";
		}
	};

public:
	template <typename... Ts>
	constexpr TNumericRange(const Ts& ...input){
		set(input...);
	};

	~TNumericRange() = default;

	//setters
	constexpr void set(){
		_min = std::numeric_limits<T>::lowest();
		_max = std::numeric_limits<T>::max();
		_minIncluded = true;
		_maxIncluded = true;
	};

	constexpr void set(const T& Min, const bool & MinIncluded, const T& Max, const bool & MaxIncluded){
		_min = Min;
		_max = Max;
		_minIncluded = MinIncluded;
		_maxIncluded = MaxIncluded;
		_check();
	};

	constexpr void set(std::string Range){
		trimString(Range);

		//allows for ranges of the form (min, max]
		//check if min is included
		if(Range[0] == '('){
			_minIncluded = false;
			Range.erase(0,1);
		} else if (Range[0] == '['){
			_minIncluded = true;
			Range.erase(0,1);
		} else {
			_minIncluded = true;
		}

		//extract min
		size_t pos = Range.find_first_of(',');
		if(pos == std::string::npos){
			throw "Range does not contain a ','!";
		}

		std::string minS = getTrimmedString(Range.substr(0,pos));
		if(minS.length() == 0){
			_min = std::numeric_limits<T>::lowest();
		} else {
			_min = convertStringCheck<T>(minS);
			//fillFromStringCheck(minS, _min);
		}

		//check if max is included
		size_t last = Range.length() - 1;
		if(Range[last] == ')'){
			_maxIncluded = false;
			Range.erase(last,1);
		} else if (Range[last] == ']'){
			_maxIncluded = true;
			Range.erase(last,1);
		} else {
			_maxIncluded = true;
		}

		//extract max
		std::string maxS = getTrimmedString(Range.substr(pos+1, Range.length() - pos - 1));
		if(maxS.length() == 0){
			_max = _max = std::numeric_limits<T>::max();
		} else {
			_max = convertStringCheck<T>(maxS);
			//fillFromStringCheck(maxS, _max);
		}

		//check
		_check();
	};

	//getters
	const T& min() const { return _min; };
	const T& max() const { return _max; };
	const bool minIncluded() const { return _minIncluded; };
	const bool maxIncluded() const { return _maxIncluded; };

	std::string rangeString() const{
		std::string s;
		if(_minIncluded){
			s += '[';
		} else {
			s += '(';
		}

		s += toString(_min) + ',' + toString(_max);

		if(_maxIncluded){
			s += ']';
		} else {
			s += ')';
		}

		return s;
	};

	//test if a value is within range
	constexpr bool within(const T& Value) const{
		if ((_minIncluded && Value < _min) || (!_minIncluded && Value <= _min) || (_maxIncluded && Value > _max) || (!_maxIncluded && Value >= _max)) {
			return false;
		} else {
			return true;
		}
	};
};

#endif /* COMMONUTILITIES_TNUMERICRANGE_H_ */


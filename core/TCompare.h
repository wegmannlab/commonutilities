//
// Created by madleina on 25.06.20.
//

#ifndef TCOMPARE_H
#define TCOMPARE_H

#include "TFile.h"
#include "stringFunctions.h"

//-------------------------------------------------------
// TColumn
//-------------------------------------------------------

enum DataTypes {_float, _double, _int, _string };

class TColumn {
protected:
    double _range;
    DataTypes _dataType;

    // comparing
    bool _compareString(const std::string & val1, const std::string& val2) const;
    bool _compareInt(const std::string & val1, const std::string& val2) const;
    bool _compareFloat(const std::string & val1, const std::string& val2) const;
    bool _compareDouble(const std::string & val1, const std::string& val2) const;

public:
    TColumn();

    // comparing
    bool entriesAreEqual(const std::string & val1, const std::string& val2);
    bool headerEntriesAreEqual(const std::string & val1, const std::string& val2);

    // setters
    void setDataType(const std::string& type);
    void setRange(double Range);

    // getters
    DataTypes dataType() {return _dataType;};
    double range() const {return _range;};
};

//-------------------------------------------------------
// TFileComparer
//-------------------------------------------------------

class TFileComparer{
protected:
    // files to compare
    TInputFile _file1;
    TInputFile _file2;
    // output file where differences are written to
    std::string _nameDiffFile;
    TOutputFile _diffFile;

    // store data types for fair comparison of columns (e.g. compare doubles within range)
    std::vector<TColumn> _cols;

    // store number of differences between the two files
    int _numDiff;
    bool _limitedNumDiff;
    int _maxNumDiff;
    bool _reachedMaxNumDiff;

    void _init(const std::string& filenameOut);
    void _close();

    // comparing
    void _compareHeader();
    void _compareOneLine(const std::vector<std::string>& vecFile1, const std::vector<std::string>& vecFile2);
    void _addDifference(int curCol, const std::string& val1, const std::string& val2);

public:
    TFileComparer();
    TFileComparer(const std::string &filenameIn1, const std::string &filenameIn2, const std::string& filenameOut, const TFile_Filetype& type, const std::string& delim = DEFAULTDELIM_IN);
    TFileComparer(const std::string &filenameIn1, const std::string &filenameIn2, const std::string& filenameOut, int numCols, const std::string& delim = DEFAULTDELIM_IN);
    ~TFileComparer();

    void open(const std::string &filenameIn1, const std::string &filenameIn2, const std::string& filenameOut, const TFile_Filetype& type, const std::string& delim = DEFAULTDELIM_IN);
    void open(const std::string &filenameIn1, const std::string &filenameIn2, const std::string& filenameOut, int numCols, const std::string & delim = DEFAULTDELIM_IN);

    // set data type for columns
    void setDataTypes(const std::vector<std::string>& dataTypes);
    void setDataType(int col, const std::string& dataType, double Range = 0);
    void setDataTypes(const std::vector<std::string>& dataTypes, const std::vector<double>& ranges);

    // getters
    std::vector<TColumn> getCols(){return _cols;};
    int numDiff() const{return _numDiff;};
    std::string outName(){ return _nameDiffFile;};

    // stop comparing after x differences?
    void limitNumDiffTo(int num);

    // compare
    bool filesAreEqual();
};

#endif //TCOMPARE_H

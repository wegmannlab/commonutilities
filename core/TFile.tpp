// contains template implementations for TFile

//-------------------------------------------------------
// TOutputFile
//-------------------------------------------------------
template<class T>
void TOutputFile::_write(const std::vector<T> & data){
    if(!_isOpen)
        throw std::runtime_error("Can not write to file '" + _name + "': file not open!");

    //write first of line
    typename std::vector<T>::const_iterator it=data.begin();
    if(_curCol == 0){
        *_filePointer << *it;
        it++;
    }

    //write others
    for(; it!=data.end(); it++){
        *_filePointer << _delim << *it;
    }

    _curCol += data.size();
};

template<typename T>
void TOutputFile::writeLine(std::vector<T> & data){
    ++_lineNum;
    if(_table && data.size() != static_cast<unsigned int>(_table->numColumns()))
        throw std::runtime_error("Can not write line in file '" + _name + "': wrong number of columns (" + toString(data.size()) + " instead of " + toString(_table->numColumns()) + ")!");
    if(_curCol != 0)
        throw std::runtime_error("Can not write line in file '" + _name + "': Last line was not finished in file '" + _name + "'!");
    _write(data);
    endLine();
};

template<typename T>
void TOutputFile::write(T data){
    if (_curCol == 0) ++_lineNum;
    if(_table && _curCol == _table->numColumns())
        throw std::runtime_error("Extra value on line " + toString(_lineNum) + " in file '" + _name + "'!");
    if(_curCol > 0) *_filePointer << _delim;
    *_filePointer << data;
    ++_curCol;
};

template<typename T>
void TOutputFile::write(const std::vector<T> & data){
    if (_curCol == 0) ++_lineNum;
    if(_table && _curCol + data.size() > static_cast<unsigned int>(_table->numColumns()))
        throw std::runtime_error("Extra values on line " + toString(_lineNum) + " in file '" + _name + "'!");
    _write(data);
};

template<typename T>
TOutputFile& TOutputFile::operator<<(const T data){
    write(data);
    return *this;
};

//-------------------------------------
// TColumnSelector
//-------------------------------------
template<typename T> void TColumnSelector::fill(std::vector<T> & full, std::vector<T> & dest){
	dest.resize(_usedColumns.size());
	for(size_t i = 0; i < _usedColumns.size(); ++i){
		dest[i] = full[_usedColumns[i]];
	}
};

//-------------------------------------------------------
// TInputFile
//-------------------------------------------------------
template<typename T> bool TInputFile::_readNextLineFromFile(std::vector<T> & vec, bool check){
    _line.clear();
    while(_line.empty() && _filePointer->good() && !_filePointer->eof()){
        std::getline(*_filePointer, _line);

        //skip comments
        _line = extractBefore(_line, _delim_comment);

        //remove endline chararcters
        trimEndlineString(_line);
    }

    if(!_filePointer->good() || _filePointer->eof()){
        close();
        return false;
    }

    ++_lineNum; // now "accept" -> displays correct line in error message & if file is finished
    //split into vector
    fillContainerFromStringAny(_line, vec, _delim, check, check); // if check=true, skipEmtpy must also be true

    return true;
};

template<typename T> bool TInputFile::_read(std::vector<T> & vec, bool check){
    if(!_isOpen || !_readNextLineFromFile(vec, check)){
        return false;
    }
    //check if we get the correct number of columns
    if(_table && vec.size() != static_cast<unsigned int>(_table->numColumns())){
        throw "Wrong number of columns in file '" + _name + "' on line " + toString(_lineNum) + ": found " + toString(vec.size()) + ", expected " + toString(_table->numColumns()) + "!";
    }

    return true;
};

template<typename T> bool TInputFile::read(std::vector<T> & vec, bool check){
	if(_limitColumns){
		static std::vector<T> tmp;
		if(!_read(tmp, check)){
			return false;
		}
		_limitColumns->fill(tmp, vec);
		return true;
	} else {
		return _read(vec, check);
	}
    return true;
};


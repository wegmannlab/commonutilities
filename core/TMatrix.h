//
// Created by madleina on 05.08.20.
//

#ifndef TMATRIX_H_
#define TMATRIX_H_

#include "stringFunctions.h"
#include "TRandomGenerator.h"

// assertions
// #define NDEBUG
#include <cassert>
// comment to use assert()
#define assertm(exp, msg) assert(((void)msg, exp))

//-------------------------------------------
// TMatrix
//-------------------------------------------

// TODO: template!

// forward declaration
class TBandMatrix;

class TMatrix {
    // rectangular matrix
    // certain functions (i.e. diagonals) only make sense for square matrices
    //     -> those use assert to check if rows==cols
protected:
    std::vector<std::vector<double> > _mat; // TODO: linearize -> use DataVector class of mcmcframework? If so, remove double storage of dimensionality
    uint32_t _rows;
    uint32_t _cols;
    bool _initialized;

    void _initialize(const double& initial);

public:
    TMatrix();
    TMatrix(int size);
    TMatrix(int size, const double& initial);
    TMatrix(int rows, int cols);
    TMatrix(int rows, int cols, const double& initial);
    TMatrix(const TMatrix& other);
    TMatrix(const TMatrix& other, const double& scale);
    virtual ~TMatrix();

    // resizing
    void clear();
    void resize(int size);
    void resize(int size, const double& initial);
    void resize(int rows, int cols);
    void resize(int rows, int cols, const double& initial);

    // reset
    virtual void set(const double& value);

    // Matrix-Matrix operations
    TMatrix& operator=(const TMatrix& other);
    TMatrix operator+(const TMatrix& other);
    TMatrix& operator+=(const TMatrix& other);
    TMatrix operator-(const TMatrix& other);
    TMatrix& operator-=(const TMatrix& other);
    TMatrix operator*(const TMatrix& other) const;
    TMatrix& operator*=(const TMatrix& other);

    // transpose
    TMatrix transpose();

    // fill randomly
    virtual void fillUniformRandom(TRandomGenerator* randomGenerator);

    // fill from other TMatrix
    void fillFromMatrix(const TMatrix & other);
    void fillFromMatrix(const TMatrix & other, const double & Scale);
    void fillFromProduct(const TMatrix & first, const TMatrix & second);
    void fillFromSquare(const TMatrix &other);

    // fill from other TBandMatrix
    virtual void fillFromMatrix(const TBandMatrix & other);
    virtual void fillFromMatrix(const TBandMatrix & other, const double & Scale);
    virtual void fillFromProduct(const TBandMatrix & first, const TBandMatrix & second);
    virtual void fillFromSquare(const TBandMatrix &other);
    virtual void fillAsExponential(const TBandMatrix & Q);

    // Matrix-scalar operations
    TMatrix operator+(const double& val);
    TMatrix operator+=(const double& val);
    TMatrix operator-(const double& val);
    TMatrix operator-=(const double& val);
    TMatrix operator*(const double& val);
    TMatrix operator*=(const double& val);
    TMatrix operator/(const double& val);
    TMatrix operator/=(const double& val);
    void addToDiag(const double& val);

    // Matrix-vector operations
    virtual std::vector<double> operator*(const std::vector<double>& other);

    // compare
    int numDiffEntries(const TMatrix & other);

    // Access data
    virtual double& operator()(const uint32_t& row, const uint32_t& col);
    virtual const double& operator()(const uint32_t& row, const uint32_t& col) const;
    std::vector<double> diag_vec();
    virtual std::vector<double> row(const uint32_t& row);
    std::vector<double> col(const uint32_t& col);

    // operations
    virtual double rowSum(const uint32_t& row) const;
    virtual double colSum(const uint32_t& col) const;

    // getters
    uint32_t rows() const;
    uint32_t cols() const;
    virtual uint32_t size() const;

    void print() const;
};

//-------------------------------------------
// TBandMatrix
//-------------------------------------------

class TBandMatrix : public TMatrix {
    // a band matrix is a sparse matrix where only the main diagonal (+ possibly more diagonals on either side)
    // have non-zero entries
    // The diagonals are numbered 0:(2*bandwidth) with the bandwidth^th diagonal being the longest one

    // need to re-define all functions that return TMatrix in base class
    //      (this is ugly, but other options are worse:
    //       -> use dynamic_cast to cast to derived class is slow
    //       -> use CRTP is complicated, and I'm not sure if this will be 1) fast and 2) possible for all functions (https://en.m.wikipedia.org/wiki/Curiously_recurring_template_pattern)
    //       -> use free functions is super hard to read and to debug (https://stackoverflow.com/questions/41652069/inherited-functions-to-return-derived-class-not-base-class)

protected:
    uint32_t _bandwidth;
    std::vector<uint32_t> _lengthOfDiags;
    uint32_t _numDiag;
    double _zero; // needed for const return

    void _initialize(uint32_t size, uint32_t bandwidth, const double& initial);
    bool _onBand(uint32_t row, uint32_t col) const;

public:
    TBandMatrix();
    TBandMatrix(uint32_t size, uint32_t bandwidth);
    TBandMatrix(uint32_t size, uint32_t bandwidth, const double& initial);
    TBandMatrix(const TBandMatrix& other);
    TBandMatrix(const TBandMatrix& other, const double& scale);
    virtual ~TBandMatrix() = default;

    // resizing
    void resize(uint32_t size, uint32_t bandwidth);
    void resize(uint32_t size, uint32_t bandwidth, const double& initial);

    // reset
    void set(const double& value) override;

    // Matrix-Matrix operations
    TBandMatrix& operator=(const TBandMatrix& other);
    TBandMatrix operator+(const TBandMatrix& other);
    TBandMatrix& operator+=(const TBandMatrix& other);
    TBandMatrix operator-(const TBandMatrix& other);
    TBandMatrix& operator-=(const TBandMatrix& other);
    TBandMatrix operator*(const TBandMatrix& other) const;
    TBandMatrix& operator*=(const TBandMatrix& other);

    // transpose
    TBandMatrix transpose();

    // Matrix-scalar operations
    TBandMatrix operator+(const double& val);
    TBandMatrix operator+=(const double& val);
    TBandMatrix operator-(const double& val);
    TBandMatrix operator-=(const double& val);
    TBandMatrix operator*(const double& val);
    TBandMatrix operator*=(const double& val);
    TBandMatrix operator/(const double& val);
    TBandMatrix operator/=(const double& val);

    // Matrix-vector operations
    std::vector<double> operator*(const std::vector<double>& other) override;

    // fill randomly
    void fillUniformRandom(TRandomGenerator* randomGenerator) override;

    // fill from other TBandMatrix
    void fillFromMatrix(const TBandMatrix & other) override;
    void fillFromMatrix(const TBandMatrix & other, const double & Scale) override;
    void fillFromProduct(const TBandMatrix & first, const TBandMatrix & second) override;
    void fillFromSquare(const TBandMatrix &other) override {fillFromProduct(other, other);};
    virtual void fillAsExponential(const TBandMatrix & Q) override;

    // Access the individual elements
    double& operator()(const uint32_t& row, const uint32_t& col) override;
    const double& operator()(const uint32_t& row, const uint32_t& col) const override;
    double atDiag(uint32_t diag, uint32_t index) const;
    std::vector<double> row(const uint32_t& row) override;

    // operations
    double rowSum(const uint32_t& row) const override;
    double colSum(const uint32_t& row) const override;

    // getters
    uint32_t bandwidth() const;
    uint32_t numDiag() const;
    std::vector<uint32_t> lengthOfDiags() const;
    uint32_t size() const override;
};

#endif //TMATRIX_H_

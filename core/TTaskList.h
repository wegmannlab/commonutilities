/*
 * TTaskList.h
 *
 *  Created on: Mar 31, 2019
 *      Author: phaentu
 */

#ifndef TTASKLIST_H_
#define TTASKLIST_H_

#include <algorithm>
#include "TParameters.h"
#include "TLog.h"
#include "TTask.h"

//---------------------------------------------------------------------------
// TTaskList
//---------------------------------------------------------------------------
class TTaskList{
private:
	std::map< std::string, TTask* > _taskMap_all;
	std::map< std::string, TTask* > _taskMap_regular;
	std::map< std::string, TTask* > _taskMap_debug;
	std::vector<std::string> _generalCitations;

	void _addTaskToAll(const std::string name, TTask* task){
		_taskMap_all.insert(std::pair<std::string, TTask*>(name, task));
	};

	TTask* _getTask(const std::string task, TLog* logfile){
		std::map< std::string, TTask* >::iterator it = _taskMap_all.find(task);
		if(it == _taskMap_all.end()){
			logfile->setVerbose(true);
			printAvailableTasks(logfile);
			_throwErrorUnknownTask(task);
		}
		return it->second;
	};

	void _throwErrorUnknownTask(std::string origTask){
		//calculate Levenshtein Distance and return closes match
		//initialize crazy values
		std::string bestMatch;
		unsigned int minDistance = 99999999;

		//do comparison irrespective of case: do all in lower case
		std::string task = origTask;
		std::transform(task.begin(), task.end(), task.begin(), ::tolower);

		//now loop over all others
		for(std::map< std::string, TTask* >::iterator it = _taskMap_regular.begin(); it != _taskMap_regular.end(); ++it){
			//get string in lower case
			std::string s = it->first;
			std::transform(s.begin(), s.end(), s.begin(), ::tolower);

			//now compare
			unsigned int dist = levenshteinDistance(task, s, 0, 1); //insertion, deletion costs = 1,2
			if(dist < minDistance){
				bestMatch = it->first;
				minDistance = dist;
			}
		}

		//report best unless it has less than two characters overlapping
		if(minDistance < 2*(task.length()-1))
			throw "Unknown task '" + origTask + "'! Did you mean '" + bestMatch  + "'?";
		else
			throw "Unknown task '" + origTask + "'!";
	};

public:
	TTaskList(){};

	~TTaskList(){
		for(std::map< std::string, TTask* >::iterator it = _taskMap_all.begin(); it != _taskMap_all.end(); ++it){
			delete it->second;
		}
	};

	void addGeneralCitation(const std::string Citation){
		_generalCitations.push_back(Citation);
	};

	void addRegularTask(const std::string name, TTask* task){
		_taskMap_regular.insert(std::pair<std::string, TTask*>(name, task));
		_addTaskToAll(name, task);
	};

	void addDebugTask(const std::string name, TTask* task){
		_taskMap_debug.insert(std::pair<std::string, TTask*>(name, task));
		_addTaskToAll(name, task);
	};

	bool taskExists(const std::string task){
		if(_taskMap_all.find(task) != _taskMap_all.end())
			return true;
		else
			return false;
	};

	void run(TParameters & parameters, TLog* logfile){
		std::string task = parameters.getParameter<std::string>("task");
		run(task, parameters, logfile);
	};

	void run(const std::string taskName, TParameters & parameters, TLog* logfile){
        //check if task exists
		TTask* task = _getTask(taskName, logfile);

		//make sure it contains general citations to be listed
		for(auto& c : _generalCitations){
			task->addCitation(c);
		}

		//now run task
		task->run(taskName, parameters, logfile);
	};

	void printAvailableTasks(TLog* logfile){
		//get longest name
		unsigned int maxLen = 0;
		for(std::map< std::string, TTask* >::iterator it = _taskMap_regular.begin(); it != _taskMap_regular.end(); ++it){
			if(maxLen < it->first.length())
				maxLen = it->first.length();
		}

		//now print all tasks
		logfile->startNumbering("Available tasks:");
		for(std::map< std::string, TTask* >::iterator it = _taskMap_regular.begin(); it != _taskMap_regular.end(); ++it){
			std::string s = it->first;
			s.append(maxLen - it->first.length() + 3, ' '); //add white spaces to align explanations
			logfile->number(s + it->second->explanation());
		}

		logfile->endIndent();
	};
};



#endif /* TTASKLIST_H_ */

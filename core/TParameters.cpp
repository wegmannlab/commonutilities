//---------------------------------------------------------------------------
#include "TParameters.h"
#include <sstream>
//---------------------------------------------------------------------------
TParameters::TParameters(){
	_inputFileRead = false;
    _argsAreSpaced = false;
};

TParameters::TParameters(std::vector<std::string> & commandLineParams, TLog* logfile){
	_inputFileRead = false;
	_argsAreSpaced = false;
    _initialize(commandLineParams, logfile);
};

TParameters::TParameters(int & argc, char** argv, TLog* logfile){
    init(argc, argv, logfile);
};

void TParameters::init(int &argc, char **argv, TLog *logfile) {
    _inputFileRead = false;
    _argsAreSpaced = false;
    std::vector<std::string> commandLineParams;

    //first is name of executable
    _nameExecutable = argv[0];
    //others are parameters
    for(int i=1;i<argc;++i)
        commandLineParams.push_back(argv[i]);
    _initialize(commandLineParams, logfile);
}

void TParameters::clear(){
	_inputFileRead = false;
	_inputFileName = "";
	_parameters.clear();
};

void TParameters::addParameter(const std::string& name){
	//check if parameter already exists
	auto p = _parameters.emplace(name);
	if(!p.second){
		p.first->value = "";
		p.first->used = false;
		p.first->isFileName = false;
	}
};

void TParameters::addParameter(const std::string& name, const std::string& value){
	//check if parameter already exists
	auto p = _parameters.emplace(name, value);
	if(!p.second){
		p.first->value = value;
		p.first->used = false;
        p.first->isFileName = false;
    }
};

void TParameters::_initialize(std::vector<std::string> & commandLineParams, TLog* logfile){
	//check if first is name of an input file which means no '=' or no '-'!
	if (commandLineParams.empty())
	    return;
    auto it=commandLineParams.begin();
    if(!stringContains(*it, '=') && !stringContains(*it, '-')){
		if (!readInputfile(*it, logfile)){
		    // first argument is not an input file -> interpret as name of task
		    addParameter("task", *it);
		    logfile->list("Interpreting '" + *it + "' as name of task.");
        }
		it++;
	}

	// check which mode is used: arg=val or --arg val
	if (it != commandLineParams.end()) {
        if (stringStartsWith(*it, "--")) // check if first argument starts with --
            _parseArgsWithSpace(it, commandLineParams);
        else
            _parseArgsWithEqualSign(it, commandLineParams);
    }
    /*for (auto iti = _parameters.begin(); iti != _parameters.end(); iti++)
        std::cout << iti->name << "   " << iti->value << std::endl;*/
};

void TParameters::_parseArgsWithSpace(std::vector<std::string>::iterator it, std::vector<std::string> & commandLineParams){
    // --arg val
    // example call: ./myprogram --vcf vcf.gz --gtl gtl.txt --storeProb --doThis --goAhead -3 --check --recal prob=7
    // only allow for "=" if 1) is a value (previous parameter had --, e.g. --param prob=7) or
    //                       2) if it starts with -- anyways, e.g. --param=7
    //                       Not ok: --param value param2=value2
    _argsAreSpaced = true;

    bool previousParamWasFlagged = false;
    std::string Name;

    for(; it!=commandLineParams.end(); ++it){
        if (stringContains(*it, '=')) { // string contains a = -> check if we should throw
            if (previousParamWasFlagged || stringStartsWith(*it, "--")) {
                // all ok
            } else throw "Specify all arguments either as '--arg val' OR as 'arg=val', but don't mix these two options! (Argument '" +
                      *it + "' contains a =).";
        }
        if(stringStartsWith(*it, "--")){ // flag
            // check if previous argument was a flag as well -> if yes, store previous as flag without value
            if (previousParamWasFlagged)
                addParameter(Name);
            // don't store the current flag yet, as you don't know if there will be a value or not
            Name = extractAfterLast(*it, "-");
            previousParamWasFlagged = true;
            if (it == commandLineParams.end() - 1)
                // exception: if current is the last parameter -> store it
                addParameter(Name);
        } else { // value
            if (!previousParamWasFlagged)
                throw "Parameter '" + *it + "' requires flag!";
            addParameter(Name, *it);
            previousParamWasFlagged = false;
        }
    }
}

void TParameters::_parseArgsWithEqualSign(std::vector<std::string>::iterator it, std::vector<std::string> & commandLineParams){
    // arg=val
    // example call: ./myprogram task=simulate vcf=vcf.gz whatever
    _argsAreSpaced = false;

    for(; it!=commandLineParams.end(); ++it){
        if(stringStartsWith(*it, "--")){
            // check if correct format is used
            throw "Specify all arguments either as '--arg val' OR as 'arg=val', but don't mix these two options! (Argument '" + *it + "' starts with --).";
        }
        std::string Name = extractBefore(*it, '=');
        if(stringContains(*it, '=')){
            addParameter(Name, extractAfter(*it, '='));
        } else {
            addParameter(Name);
        }
    }
}

//---------------------------------------------------------------------------
bool TParameters::readInputfile(const std::string& fileName, TLog* logfile){
    // try opening the file
	std::ifstream is (fileName.c_str());
	if(!is){
	    // file could not be opened -> interpret as task
        return false;
	} else {
        _inputFileName = fileName;
        logfile->listFlush("Reading inputfile '" + (std::string) _inputFileName + "' ...");

        std::string buf, Name, my_value;
		std::string line;

		while(is.good() && !is.eof()){
			getline(is, line);
			line=extractBeforeDoubleSlash(line);
			trimString(line);
			if(!line.empty()){
				Name=extractBeforeWhiteSpace(line);
				trimString(line);
				my_value=extractBeforeDoubleSlash(line);
				if(!Name.empty()){
					if(my_value.empty()){
						addParameter(Name);
					} else {
						addParameter(Name, my_value);
					}
				}
			}
		}
		_inputFileRead=true;
		logfile->done();
        return true;
	}
};

//---------------------------------------------------------------------------
bool TParameters::parameterExists(const std::string& Name){
	auto p = _parameters.find(Name);
	if(p != _parameters.end()){
		p->used = true;
		return true;
	}
    return false;
};

//---------------------------------------------------------------------------
std::string TParameters::_getParameter(const std::string & Name, const bool mandatory, const std::string & explanation, const bool isFilename){
	auto p = _parameters.find(Name);
	if(p != _parameters.end()){
		p->used = true;
		if (isFilename) p->isFileName = true;
		return p->value;
	}
	if(mandatory){
		std::string error;
		if(_inputFileRead) error = "The parameter '" + Name + "' is not defined on the command line nor in the input file '" + _inputFileName + "'! ";
		else error = "The parameter '" + Name + "' is not defined! ";

		if(!explanation.empty())
			error += "\n" + explanation;

		throw error;
	}
	return "";
};

std::string TParameters::getParameterFilename(const std::string & Name, const bool& mandatory){
    return _getParameter(Name, mandatory, "", true);
};

std::string TParameters::getParameterFilename(const std::string & Name, const std::string & Explanation){
    return _getParameter(Name, true, Explanation, true);
};

void TParameters::setParameterIsFilename(const std::string & Name) { // allows to modify isFileName
    auto p = _parameters.find(Name);
    if(p != _parameters.end()){
        p->used = true;
        p->isFileName = true;
    }
}

//---------------------------------------------------------------------------
std::string TParameters::getListOfUsedParametersAndVals_commandLineFormat(){
    std::string command;
    for(auto& p : _parameters){
        if(p.used){
            // fill in correct format
            if (_argsAreSpaced){ // --arg val or --arg
                command += "--" + p.name + " ";
                if (!p.value.empty()) command += p.value + " ";
            } else { // arg=val or arg
                command += p.name;
                if (!p.value.empty()) command += "=" + p.value;
                command += " ";
            }
        }
    }
    return command;
}

std::vector<std::string> TParameters::getVecOfUsedFilenames(){
    std::vector<std::string> vec;
    for(auto& p : _parameters){
        if(p.used && p.isFileName)
            vec.push_back(p.value);
    }
    return vec;
}

void TParameters::writeUsedParametersAndValsToFile(TOutputFile & file){
    for(auto& p : _parameters){
        if(p.used)
            file << p.name << p.value << std::endl;
    }
}

std::string TParameters::getListOfUnusedParameters(){
	std::string parameterList;
	for(auto& p : _parameters){
		if(!p.used){
			if(!parameterList.empty()){
				parameterList += ", ";
			}
			parameterList += p.name;
		}
	}
	return parameterList;
};

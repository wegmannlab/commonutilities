/*
 * TMain.h
 *
 *  Created on: Apr 1, 2019
 *      Author: phaentu
 */

#ifndef TMAIN_H_
#define TMAIN_H_

#include <utility>
#include "TTaskList.h"
#include "gitversion.h"
#include "IntegrationTests/TTesting.h"
#include "IntegrationTests/TTest.h"
#include "globalConstants.h"

//---------------------------------------------------------------------------
// TMain
//---------------------------------------------------------------------------
class TMain{
private:
    TTaskList _taskList;
    TTestList _testList;
	std::string _applicationName;
	std::string _version;
	std::string _organization;
	std::string _web;
	std::string _title;
	std::string _email;
	TLog _logfile;
	TTimer _timer;
	TParameters _parameters;
	bool _hadErrors;

    void _fillTitleString(){
		_title.clear();
		std::vector<std::string> tmp;

		//application name and version (plus underline and extra space)
		tmp.push_back(_applicationName + " " + _version);
		tmp.emplace_back(tmp[0].length() + 2, '-');
		tmp.emplace_back("");

		//add lab
		if(!_organization.empty())	tmp.emplace_back(_organization);

		//add website
		if(!_web.empty()) tmp.push_back(_web);

		//commit (first extra space)
		tmp.emplace_back("");
		tmp.push_back(std::string("Commit ") + getGitVersion());

		//get max length
		size_t len = tmp[0].length();
		for(size_t i=1; i<tmp.size(); ++i){
			if(len < tmp[i].length())
				len = tmp[i].length();
		}
		len += 2; //add one space on either side

		//now compile title. Add spaces in front to align center
		for(auto & i : tmp){
			if(i.empty()){
				_title += '\n';
			} else {
				//add extra spaces
				std::string add((len - i.length()) / 2, ' ');
				_title += add + i + '\n';
			}
		}
	};

    void _initialize(){
		//open logfile
		_logfile.newLine();
		_hadErrors = false;

		//compile and write title
		_fillTitleString();
		_logfile.write(_title);

		//set global variables
		__GLOBAL_APPLICATION_NAME__ = _applicationName;
		__GLOBAL_APPLICATION_VERSION__ = _version;
		__GLOBAL_APPLICATION_COMMIT__ = getGitVersion();
    };

    std::string _constructDeveloperErrorFile(const std::string& errorWhat, const std::string& usedFileNames, const std::string& errorParamsFile){
        // open file
        std::string filename = _applicationName + "_errorInfo.txt";
        TOutputFile file(filename, ' ');

        // write general info (name, commit etc.)
        file << _applicationName << _version << std::endl;
        file << "Commit:" << getGitVersion() << std::endl;
        file << "Email developer:" << _email << std::endl;
        file.endLine(); // empty line

        file << _parameters.getNameExecutable() << _parameters.getListOfUsedParametersAndVals_commandLineFormat() << std::endl;
        file << "ERROR:" << errorWhat << std::endl;
        file.endLine(); // empty line
        file << "Files attached to email:" << errorParamsFile +  "," << filename + "," << usedFileNames << std::endl;

        return filename;
    }

    std::string _constructUsedParamsFile(){
        // open file
        std::string filename = _applicationName + "_usedParams.txt";
        TOutputFile file(filename, 2);
        // write used parameters in a format that can be re-used by program
        _parameters.writeUsedParametersAndValsToFile(file);
        return filename;
    }

    void _handleDeveloperError(const std::string& errorWhat){
        std::vector<std::string> usedFileNames = _parameters.getVecOfUsedFilenames();
        std::string errorParamsFile = _constructUsedParamsFile();
        std::string errorInfoFile = _constructDeveloperErrorFile(errorWhat, concatenateString(usedFileNames, ", "), errorParamsFile);

        // error message
        _logfile.newLine();
        _logfile.clearIndent();
        _logfile.write("Yep, this error was caused by a bug. We apologize.");
        _logfile.write("You may help to get this bug fixed by writing an email to " + _email + ". Please attach the following " + toString(usedFileNames.size() + 2) + " files:");
        _logfile.addNumberingLevel();
        _logfile.number(errorInfoFile);
        _logfile.number(errorParamsFile);
        for (auto &it : usedFileNames)
            _logfile.number(it);
        _logfile.removeNumberingLevel();
        _logfile.newLine();
    }

public:
    TMain(const std::string ApplicationName, const std::string Version, const std::string Organization, const std::string Web, const std::string Email){
        //store name
		_applicationName = ApplicationName;
		_version = Version;
		_organization = Organization;
		_web = Web;
		_email = Email;

		//open logfile
		_initialize();
    };

    TMain(const std::string ApplicationName, const std::string Version, const std::string Organization, const std::string Email){
		//store name
		_applicationName = ApplicationName;
		_version = Version;
		_organization = Organization;
		_email = Email;

		//open logfile
		_initialize();
	};

    TMain(const std::string ApplicationName, const std::string Version, const std::string Email){
		//store name
		_applicationName = ApplicationName;
		_version = Version;
		_email = Email;

		//open logfile
		_initialize();
	};

    ~TMain(){};

    void addGeneralCitations(const std::string Citation){
    	_taskList.addGeneralCitation(Citation);
    };

	void addRegularTask(const std::string& name, TTask* task){
		_taskList.addRegularTask(name, task);
	};

	void addDebugTask(const std::string& name, TTask* task){
		_taskList.addDebugTask(name, task);
	};

    void addTest(const std::string& name, TTest* test){
        _testList.addTest(name, test);
    }

    void addTestSuite(const std::string& name, const std::vector<std::string> & testSuiteNames){
        _testList.addTestSuite(name, testSuiteNames);
    }

	int run(int argc, char* argv[]){
		try{
			//read parameters from the command line
            _parameters.init(argc, argv, &_logfile);

            // print used executable
            _logfile.list("Used executable: ", _parameters.getNameExecutable());

            //verbose?
			bool silent = _parameters.parameterExists("silent");
			if(silent) _logfile.listNoFile("Running in silent mode (omit argument 'silent' to get a status report on screen)");
			_logfile.setVerbose(!silent);

			//warnings?
			bool suppressWarnings=_parameters.parameterExists("suppressWarnings");
			if(suppressWarnings){
				_logfile.list("Suppressing Warnings.");
				_logfile.suppressWarings();
			}

			//open log file, if requested
			std::string logFilename = _parameters.getParameterFilename("logFile", false);
			if(logFilename.length() > 0){
				_logfile.list("Will write log to file '" + logFilename + "'.");
				_logfile.openFile(logFilename);
				_logfile.writeFileOnly(_title);
			}

			//is a task provided?
			if(!_parameters.parameterExists("task")){
				_logfile.setVerbose(true);
				_taskList.printAvailableTasks(&_logfile);
				throw "The parameter 'task' is not defined!";
			}

            //run requested task
			std::string task = _parameters.getParameter<std::string>("task");
            if(task == "test"){
                //run testing utilities
                _logfile.startIndent("Testing of " + _applicationName + " functionalities:");
                TTesting test(&_testList, _parameters, &_logfile);
                test.runTests(_parameters, &_taskList);
            } else {
                //run requested task
                _taskList.run(task, _parameters, &_logfile);
            }
            _logfile.clearIndent();

			//report not used arguments
			std::string unusedArguments = _parameters.getListOfUnusedParameters();
			if(!unusedArguments.empty()){
				_logfile.newLine();
				_logfile.warning("The following arguments were not used: " + unusedArguments + "!");
			}
	    }
		catch (std::string & error){ // user error
			_logfile.error(error);
			_hadErrors = true;
		}
		catch (const char* error){ // user error
			_logfile.error(error);
			_hadErrors = true;
		}
		catch(std::exception & error){ // developer error
            _logfile.error(error.what());
            _handleDeveloperError(error.what());
			_hadErrors = true;
		}
		catch (...){
			_logfile.error("unhandled error!");
			_hadErrors = true;
		}
		_logfile.clearIndent();

		//report end of program and return exit status
		if(_hadErrors){
			_logfile.list(_applicationName + " terminated with errors in ", _timer.formattedTime(), "!");
			_logfile.close();
			return 1;
		} else {
			_logfile.list(_applicationName + " terminated successfully in ", _timer.formattedTime(), "!");
			_logfile.close();
			return 0;
		}
	};
};


#endif /* TMAIN_H_ */

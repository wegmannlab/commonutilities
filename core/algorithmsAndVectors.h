//
// Created by madleina on 19.04.21.
//

#ifndef BANGOLIN_ALGORITHMSANDVECTORS_H
#define BANGOLIN_ALGORITHMSANDVECTORS_H

#include <cmath>
#include <array>
#include <cassert>
#include <numeric>
#include "stringFunctions.h"

//------------------------------------------------
// Algorithms
//------------------------------------------------
template <typename T, typename U> typename std::enable_if<std::is_integral<U>::value>::type rankSort(const T & input, std::vector<U> & ranks, bool decreasing = false) {
    // creates a vector of ranks (corresponding to the index of the new sorted vector inside the original vector)
    // e.g. input = {5, 2, 1, 4, 3}
    // will return ranks = {2, 1, 4, 3, 0}
    // corresponds to sort(input, index.return=T)$ix in R
    // stolen from: https://stackoverflow.com/questions/1577475/c-sorting-and-keeping-track-of-indexes

    // initialize original index locations
    ranks.resize(input.size());
    std::iota(ranks.begin(), ranks.end(), 0);

    // sort indexes based on comparing values in v
    // using std::stable_sort instead of std::sort to avoid unnecessary index re-orderings when v contains elements of equal values
    if (decreasing) {
        std::stable_sort(ranks.begin(), ranks.end(),
                         [&input](size_t i1, size_t i2) { return input[i1] > input[i2]; });
    } else {
        std::stable_sort(ranks.begin(), ranks.end(),
                         [&input](size_t i1, size_t i2) { return input[i1] < input[i2]; });
    }
};

template <typename T> std::vector<size_t> rankSort(const T & input, bool decreasing = false) {
    std::vector<size_t> ranks;
    rankSort(input, ranks, decreasing);
    return ranks;
};

template <typename T, typename C> double euclideanDistance(const std::vector<T> & x, const std::vector<C> & y) {
    // calculate Euclidean distance: sqrt(sum_{d=1}^D (x_d-y_d)^2)
    assert(x.size() == y.size());

    double sum = 0.;
    for (size_t d = 0; d < x.size(); d++){
        double tmp = x[d] - y[d];
        sum += tmp*tmp;
    }
    return sqrt(sum);
};

template<class Type1, class Type2>
void findIntersection(const Type1 & Vec1,
                      const Type2 & Vec2,
                      std::vector<std::pair<bool, size_t>> & Res1,
                      std::vector<std::pair<bool, size_t>> & Res2){
    // find intersection of Vec1 and Vec2
    // Vec1 and Vec2 should NOT contain duplicates! Need to check this in advance!
    // and return (for each element) 1) if it belongs to intersection and 2) new index in intersection vector

    // init storage
    Res1.resize(Vec1.size(), std::pair<bool, size_t>(false, 0));
    Res2.resize(Vec2.size(), std::pair<bool, size_t>(false, 0));

    // go over all elements of Vec1
    size_t curIndex = 0;
    for (size_t i = 0; i < Vec1.size(); i++){
        auto value1 = Vec1[i];
        // find value1 inside Vec2
        for (size_t j = 0; j < Vec2.size(); j++){
            auto value2 = Vec2[j];
            if (value1 == value2){
                Res1[i].first = true;
                Res1[i].second = curIndex;
                Res2[j].first = true;
                Res2[j].second = curIndex;
                curIndex++;
            }
        }
    }
};

template<class T>
std::pair<bool, size_t> findDuplicate(const T & Vec){
    for (size_t i = 0; i < Vec.size(); i++){
        for (size_t j = 0; j < Vec.size(); j++){
            if (i != j && Vec[i] == Vec[j]){
                return std::make_pair(true, j);
            }
        }
    }
    return std::make_pair(false, 0);
};

template<template<class> typename Container, class Type>
void fillCumulative(const Container<Type> & probabilities, Container<Type> & cumulative){
	//only works for floating point types
	static_assert(std::is_floating_point<Type>::value,
                  "void fillCumulative(const Container<Type> & probabilities, const Container<Type> & cumulative): can only be instantiated with floating point types");

	if(probabilities.size() == 0){
		cumulative.clear();
	} else {
		//make sure probabilities sum to one
		double sum = std::accumulate(probabilities.begin(), probabilities.end(), 0.0);

		//fill cumulative
		cumulative.resize(probabilities.size());
		cumulative[0] = probabilities[0] / sum;

		for(size_t i = 1; i < (probabilities.size()-1); ++i){
			cumulative[i] = cumulative[i-1] + probabilities[i] / sum;
		}

		cumulative[cumulative.size() - 1] = 1.0;
	}
};

/* *
 * Do binary search within a SORTED container and return index of match
 *
 * @tparam ForwardIterator is the type of iterator
 * @tparam Type is the type of the values that are searched for, e.g. std::string/double/int etc.
 *
 * @param[in] First is an iterator pointing to the first element in a vector/array -> NOTE: must be sorted!!
 * @param[in] Last is an iterator pointing to the last element in a vector/array -> NOTE: must be sorted!!
 * @param[in] Value is the value to be searched inside [First, Last)
 *
 * @return index of match
 *         if there are duplicate elements: index of the first match is returned
 *         if there is no match: throw runtime error
 */

template<class ForwardIterator, class Type>
size_t binarySearch_getIndex(ForwardIterator First, ForwardIterator Last, const Type & Value) {
    // do binary search and return index of matching element
    // throws runtime error if there is no match
    // note: if there are duplicates, the index of the first matching element is returned
    auto it = std::lower_bound(First, Last, Value);
    if (it == Last || *it != Value) {
        throw std::runtime_error("In function 'binarySearch_getIndex': Failed to find Value '" + toString(Value) + "'!");
    } else {
        size_t index = std::distance(First, it);
        return index;
    }
}

#endif //BANGOLIN_ALGORITHMSANDVECTORS_H

/*
 * TFile.cpp
 *
 *  Created on: May 26, 2020
 *      Author: phaentu
 */

#include "TFile.h"

//-------------------------------------------------------
// TFileTable
//-------------------------------------------------------
TFileTable::TFileTable(int numCols, const std::string& name){
    _init();
    noHeader(numCols, name);
};

TFileTable::TFileTable(const std::vector<std::string> &Header, const std::string& name){
    _init();
    setHeader(Header, name);
};

void TFileTable::_init(){
    _numCols = 0;
    _type = variable;
};

std::string TFileTable::headerAt(int col, const std::string& name) const{
    if(_type == fixed)
        throw "Cannot read header: file '" + name + "' has no header!";
    if(col < _numCols)
        return _header[col];
    throw "Cannot read header: file '" + name + "' does not have column " + toString(col) + "!";
};

int TFileTable::getIndexOfColname(const std::string& colname, const std::string& name) const{
    if(_type == fixed)
        throw "Cannot read header: file '" + name + "' has no header!";
    int i = 0;
    for (auto & col : _header){
        if (col == colname)
            return i;
        i++;
    }
    throw "Column with name " + colname + " does not exist in file '" + name + "'";
}

int TFileTable::getIndexOfColnameStartsWith_First(const std::string& colname, const std::string& name) const{
    if(_type == fixed)
        throw "Cannot read header: file '" + name + "' has no header!";
    int i = 0;
    for (auto & col : _header){
        if (stringStartsWith(col, colname) || colname == col) // return after first occurrence
            return i;
        i++;
    }
    throw "Column that contains " + colname + " does not exist in file '" + name + "'";
}

int TFileTable::getIndexOfColnameStartsWith_Last(const std::string& colname, const std::string& name) const{
    if(_type == fixed)
        throw "Cannot read header: file '" + name + "' has no header!";
    int i = 0;
    int last = -1;
    for (auto & col : _header){
        if (stringStartsWith(col, colname) || colname == col)
            last = i;
        i++;
    }
    if (last >= 0){
        return last;
    } else throw "Column that contains " + colname + " does not exist in file '" + name + "'";
}

void TFileTable::setHeader(const std::vector<std::string> &Header, const std::string& name) {
    // check
    if (_type == fixed && static_cast<unsigned int>(_numCols) != Header.size()) // check if numCols (if previously set) matches header size
        throw std::runtime_error("Number of columns (" + toString(_numCols) + ") in file '" + name + "' does not match header size (" + toString(Header.size()) + ").");
    if (_type == header)
        throw std::runtime_error("Header already processed in file '" + name + "'!");
    // checks passed -> set header
    _header = Header;
    _numCols = _header.size();
    _type = header;
};

void TFileTable::noHeader(int numCols, const std::string& name) {
    // check
    if (numCols <= 0)
        throw std::runtime_error("Number of columns (=" + toString(numCols) + ") in file " + name + " must be positive!");
    if (_type == header)
        throw std::runtime_error("Header already processed in file '" + name + "'!");
    if (_type == fixed && _numCols != numCols) // check if numCols (if previously set) matches new numCols
        throw std::runtime_error("Number of columns previously set (" + toString(_numCols) + ") in file '" + name + "' does not match given number of columns (" + toString(numCols) + ").");
    // checks passed -> set numCols
    _numCols = numCols;
    _type = fixed;
};

//-------------------------------------------------------
// TFile
//-------------------------------------------------------
TFile::TFile() {
    _isOpen = false;

    // counters
    _lineNum = 0;
    _curCol = 0;
};

TFile::TFile(const TFile & other){
	_name = other._name;
	_isOpen = other._isOpen;
	_delim = other._delim;
	_curCol = other._curCol;
	_lineNum = other._lineNum;
	_table.reset(new TFileTable(*other._table));
};

TFile::TFile(TFile && other){
	_name = std::move(other._name);
	_isOpen = other._isOpen;
	other._isOpen = false;
	_delim = std::move(other._delim);
	_curCol = other._curCol;
	_lineNum = other._lineNum;
	_table.reset(new TFileTable(*other._table));
	other._table.release();
};

void TFile::_setName(const std::string & Name){
    if (Name.empty())
        throw "Please provide a valid file name!";
    _name = Name;
};

void TFile::_openFile(){
    throw std::runtime_error("void _openFile() not implemented for base class TFile!");
};

void TFile::_closeFile(){
    throw std::runtime_error("void _closeFile() not implemented for base class TFile!");
};

void TFile::_reOpenFile(){
    throw std::runtime_error("void _reOpenFile() not implemented for base class TFile!");
};

void TFile::open(const std::string& Name){
    if(_isOpen)
        throw std::runtime_error("Can not open file '" + Name + "': file already open!");
    if (_table) // if open -> closed -> open: reset all previous stuff
        _table.release();
    _setName(Name);
    _openFile();
    _lineNum = 0;
    _curCol = 0;
};

void TFile::open(const std::string &Name, int numCols) {
    open(Name);
    noHeader(numCols);
};

void TFile::reOpen(){
    // open -> close -> reOpen:
    //     will append to end of TOutputFile
    //     will jump to line that was read last of TInputFile
    // Alternatively: open -> close -> open (with same name):
    //     will overwrite everything that was previously written to TOutputFile
    //     will re-start at the beginning of TInputFile
    if(_isOpen)
        throw std::runtime_error("Can not re-open file '" + _name + "': file already open!");
    _reOpenFile();
    _isOpen = true;
};

void TFile::noHeader(int numCols){
    // check
    if (!_isOpen)
        throw std::runtime_error("Can not set number of columns: file '" + _name + "' not open!");
    if (_lineNum > 0 || _curCol > 0)
        throw std::runtime_error("Can not set number of columns of file '" + _name + "': first line has already been processed!");
    if (!_table)
        _table = std::make_unique<TFileTable>(numCols, _name); // create, if necessary
    else _table->noHeader(numCols, _name);
};

void TFile::close(){
    _closeFile();
};

//-------------------------------------------------------
// TOutputFile
//-------------------------------------------------------

// constructors

TOutputFile::TOutputFile() : TFile() {
    _filePointer = nullptr;
};

TOutputFile::TOutputFile(TOutputFile && other) : TFile(std::move(other)){
	_filePointer = other._filePointer;
	other._filePointer = nullptr;
};

TOutputFile::TOutputFile(const std::string &Name, const char& delim) : TFile() {
    _filePointer = nullptr;
    TOutputFile::open(Name, delim);
};

TOutputFile::TOutputFile(const std::string &Name, int NumCols, const char& delim) : TFile() {
    _filePointer = nullptr;
    TOutputFile::open(Name, NumCols, delim);
};

TOutputFile::TOutputFile(const std::string &Name, const std::vector<std::string> &Header, const char & delim) : TFile() {
    _filePointer = nullptr;
    TOutputFile::open(Name, Header, delim);
};

// open

void TOutputFile::open(const std::string &Name, const char& delim) {
    TFile::open(Name);
    _delim = delim;
};

void TOutputFile::open(const std::string &Name, int numCols, const char& delim) {
    TFile::open(Name, numCols);
    _delim = delim;
};

void TOutputFile::open(const std::string &Name, const std::vector<std::string> &Header, const char& delim) {
    TFile::open(Name);
    _delim = delim;
    writeHeader(Header);
};

TOutputFile::~TOutputFile() {
    close();
};

void TOutputFile::_openFile(){
    std::string ending = readAfterLast(_name, '.');
    if(ending == "gz"){
        _filePointer = new gz::ogzstream(_name.c_str());
    } else {
        _filePointer = new std::ofstream(_name.c_str());
    }
    _isOpen = true;

    if(!(*_filePointer) || _filePointer->fail() || !_filePointer->good()){
        throw "Failed to open file '" + _name + "' for writing!";
    }
};

void TOutputFile::_reOpenFile(){
    // append to file
    std::string ending = readAfterLast(_name, '.');
    if(ending == "gz"){
        _filePointer = new gz::ogzstream(_name.c_str(), std::ios::app);
    } else {
        _filePointer = new std::ofstream(_name.c_str(), std::ios::app);
    }
    if(!(*_filePointer) || _filePointer->fail() || !_filePointer->good()){
        throw "Failed to re-open file '" + _name + "' for writing!";
    }
};

// close

void TOutputFile::_closeFile() {
    if (_isOpen) {
        delete _filePointer;
        _isOpen = false;
    }
};

// writing data
void TOutputFile::setPrecision(int Precision){
    *_filePointer << std::setprecision(Precision);
};

// new line
void TOutputFile::endLine(){
    if(_table && _curCol != _table->numColumns())
        throw std::runtime_error("Can not end line in file '" + _name + "': missing/extra values on line " + toString(_lineNum) + "!");
    *_filePointer << "\n";
    _curCol = 0;
};

// writing header
void TOutputFile::writeHeader(const std::vector<std::string>& Header){
    if (_lineNum > 0 || _curCol > 0) // make sure we don't write stuff first and then the header
        throw std::runtime_error("Can not write header in file '" + _name + "': first lines have already been written!");
    if (!_table)
        _table = std::make_unique<TFileTable>(Header, _name);
    else _table->setHeader(Header, _name);

    ++_lineNum;
    _write(Header);
    endLine();
};

//------------------------------------------------
// TColumnSelector
//------------------------------------------------
TColumnSelector::TColumnSelector(const std::vector<std::string> & RequiredColumns, const std::vector<std::string> & Header, const std::string & Filename){
	//match headers
	_usedColumns.resize(RequiredColumns.size());
	for(size_t i=0; i < RequiredColumns.size(); ++i){
		//check if this header exists
		for(size_t j=0; j <= Header.size(); ++j){
            if(j == Header.size()){
                //header does not exist
                throw "File '" + Filename + "' does not contain column '" + RequiredColumns[i] + "'!";
            }
			if(Header[j] == RequiredColumns[i]){
				//header found!
				_usedColumns[i] = j;
				break;
			}
		}
	}
};

//-------------------------------------------------------
// TInputFile
//-------------------------------------------------------

// constructors

TInputFile::TInputFile() : TFile() {
    _filePointer = nullptr;
};

TInputFile::TInputFile(const std::string &Name, const TFile_Filetype & type, const std::string& delim, const std::string & DelimComment) : TFile() {
    _filePointer = nullptr;
    TInputFile::open(Name, type, delim, DelimComment);
};

TInputFile::TInputFile(const std::string &Name, int NumCols, const std::string& delim, const std::string & DelimComment) : TFile() {
    _filePointer = nullptr;
    TInputFile::open(Name, NumCols, delim, DelimComment);
};

TInputFile::TInputFile(const std::string& Name, const std::vector<std::string> & LimitToTheseColumns, const std::string& Delim, const std::string & DelimComment){
	_filePointer = nullptr;
	TInputFile::open(Name, LimitToTheseColumns, Delim, DelimComment);
};

TInputFile::~TInputFile() {
    close();
};

TInputFile::TInputFile(TInputFile && other) : TFile(std::move(other)){
	std::swap(_line, other._line);
	_filePointer = other._filePointer;
	other._filePointer = nullptr;
};

// open

void TInputFile::open(const std::string &Name, int numCols, const std::string& delim, const std::string & DelimComment) {
    TFile::open(Name, numCols);
    _delim = delim;
    _delim_comment = DelimComment;
};

void TInputFile::open(const std::string &Name, const TFile_Filetype & type, const std::string& delim, const std::string & DelimComment) {
    TFile::open(Name);
    _delim = delim;
    _delim_comment = DelimComment;
    if (type == TFile_Filetype::header){
        _readHeader();
    } else if (type == fixed){
        _guessNumColumns();
    }
};

void TInputFile::open(const std::string& Name,
									 const std::vector<std::string> & LimitToTheseColumns,
									 const std::string& Delim,
									 const std::string & DelimComment){
	//open file
	open(Name, TFile_Filetype::header, Delim, DelimComment);

	//limit columns
	_limitColumns = std::make_unique<TColumnSelector>(LimitToTheseColumns, header(), _name);
};

void TInputFile::_openFile(){
    //check if file is gzipped: ending must be .gz
    std::string ending = readAfterLast(_name, '.');
    if(ending == "gz"){
        _filePointer = new gz::igzstream(_name.c_str());
    } else {
        _filePointer = new std::ifstream(_name.c_str());
    }
    _isOpen = true;

    if(!(*_filePointer) || _filePointer->fail() || !_filePointer->good()){
        _closeFile();
        throw "Failed to open file '" + _name + "' for reading! Does the file exist?";
    }
};

void TInputFile::_reOpenFile(){
    _openFile();

    // now jump to previous line position
    // (actually: read from beginning but ignore previously parsed lines
    //            -> other solution: use seekg; but this is only possible if table has fixed with and is tricky with delims etc.)
    std::vector<std::string> trash;
    uint64_t stop = _lineNum;
    _lineNum = 0; // reset
    for (uint64_t line = 0; line < stop; line++)
        read(trash);
};

// close

void TInputFile::_closeFile(){
    if(_isOpen){
        delete _filePointer;
        _isOpen = false;
    }
};

// delimitor for comments

void TInputFile::setDelimComment(const std::string &Delim) {
    // set delimitor for comment in constructor/open()/with this function
    // instead of passing it as a default argument to read()
    // because when opening the file with _guessNumColumns(), we already need to know the comment delimitor
    _delim_comment = Delim;
}

// reading headers

void TInputFile::_readHeader(){
    std::vector<std::string> vec;
    if(!_readNextLineFromFile(vec)){
        throw "File '" +  _name + "' is empty!";
    }
    _table = std::make_unique<TFileTable>(vec, _name);
};

void TInputFile::_guessNumColumns(){
    //get number of columns from first line with data
    std::vector<std::string> vec;
    if(!_readNextLineFromFile(vec)){
        throw "File '" +  _name + "' is empty!";
    }
    _table = std::make_unique<TFileTable>(vec.size(), _name);
    _filePointer->seekg(0); //rewind
    --_lineNum;
};


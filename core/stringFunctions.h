/*
 * stringFunctions.h
 *
 *  Created on: May 4, 2012
 *      Author: wegmannd
 */

#ifndef STRINGFUNCTIONS_H_
#define STRINGFUNCTIONS_H_

#include <string>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>
#include <set>
#include <sstream>
#include <cstdio>
#include <iomanip>
#include <stdexcept>
#include <algorithm>
#include <cmath>
#include <ctime>
#include <memory>
#include <cstring>

//-------------------------------------------------
// Conversion to string
//-------------------------------------------------
template <typename T, std::enable_if_t<std::is_arithmetic<T>::value, bool> = true>
std::string toString(const T & input){
	return std::to_string(input);
};

template<typename T, std::enable_if_t<!std::is_arithmetic<T>::value, bool> = true>
std::string toString(const T& input){
	return (std::string) input;
};

template <typename... Ts >
std::string toString(const Ts&... Values){
    constexpr auto numberOfValues = double{sizeof...(Values)};
    if constexpr (numberOfValues > 0){
    	return (toString(Values) + ...);
    } else {
    	return "";
    }
};

//For containers: concatenate the string of each element
template <typename CONTAINER> std::string containerToString(const CONTAINER & vec){
	std::string s;
	for(auto it = vec.cbegin(); it != vec.cend(); ++it){
		s += toString(*it);
	}
	return s;
};

//with specific precision
template <typename T> std::string to_string_with_precision(const T a_value, const int digits=6){
    std::ostringstream out;
    out.precision(digits);
    out << std::fixed << a_value;
    return out.str();
};

//as percent string (e.g. used to reprt progress)
template <typename T> std::string toPercentString(const T & input, const int digits = 0){
    return to_string_with_precision(input * 100.0, digits);
};

template<typename T> std::string toPercentString(const T & num, const T & total, const int digits = 0){
	double input = (double) num / (double) total;
    return to_string_with_precision(input * 100.0, digits);
};

//-------------------------------------------------
// String contains
//-------------------------------------------------
bool stringContains(const std::string & haystack, const std::string& needle);
bool stringContainsAny(const std::string & haystack, const std::string& needle);
bool stringContains(const std::string & haystack, const char& needle);
bool stringContainsOnly(const std::string & haystack, const std::string& needle);
//numbers
bool stringContainsNumbers(const std::string & haystack);
bool stringContainsOnlyNumbers(const std::string & haystack);
bool stringIsProbablyANumber(const std::string & haystack);
bool stringIsProbablyABool(const std::string & haystack);
//letters
bool stringContainsLetters(const std::string & haystack);
bool stringContainsOnlyLetters(const std::string & haystack);

// starts with
bool stringStartsWith(const std::string & haystack, const std::string& needle);
bool stringStartsWith(const std::string & haystack, const char& needle);

//test for uniqueness
template <typename T> bool allEntriesAreUnique(std::vector<T> & vec){
	typename std::vector<T>::iterator it_second;
	for(auto it=vec.begin(); it != --vec.end(); ++it){
		for(it_second = it+1; it_second!=vec.end(); ++it_second){
			if(*it == *it_second) return false;
		}
	}
	return true;
};

template <typename T> typename std::vector<T>::iterator getIteratorToFirstNonUniqueEntry(std::vector<T> & vec){
    auto it_second = vec.end();
	for(auto it=vec.begin(); it!= --vec.end(); ++it){
		for(it_second = it+1; it_second!=vec.end(); ++it_second){
            if(*it == *it_second) return it;
		}
	}
	return it_second;
};

/* *
 * Finding the nth Instance of a Substring
 * Stolen from: https://www.oreilly.com/library/view/c-cookbook/0596007612/ch04s11.html
 *
 * @tparam DelimType is the type of delimiter, e.g. std::string or char
 *
 * @param[in] N is the number of occurrences
 * @param[in] HayStack is the string to search in
 * @param[in] Needle is the pattern to search for
 *
 * @return position of Nth match. If it didn't match N times, std::string::npos is returned
 *
 * Attention: does not deal with overlapping matches (e.g. beginning of the string matches part of the end of the same string,
 * as in the word “abracadabra,” where the last four characters are the same as the first four)
 */

template <typename DelimType>
std::string::size_type findNthInstanceInString(const size_t & N, const std::string& HayStack, const DelimType & Needle) {
    std::string::size_type i = HayStack.find(Needle); // find the first occurrence

    size_t j;
    for (j = 1; j < N && i != std::string::npos; ++j) {
        i = HayStack.find(Needle, i + 1); // find the next occurrence
    }

    if (j == N) {
        return i;
    } else {
        return std::string::npos;
    }
}

//-------------------------------------------------
// Conversion from string
//-------------------------------------------------
//convert: fills into variable, allows for templating other functions
void fillFromString(const std::string& s, std::string & dest);
void fillFromString(const std::string& s, int & dest);
void fillFromString(const std::string& s, uint8_t & dest);
void fillFromString(const std::string& s, uint16_t & dest);
void fillFromString(const std::string& s, uint32_t & dest);
void fillFromString(const std::string& s, uint64_t & dest);
void fillFromString(const std::string& s, long & dest);
void fillFromString(const std::string& s, double & dest);
void fillFromString(const std::string& s, float & dest);
void fillFromString(const std::string& s, bool & dest);

template <typename T>
struct has_constructor_from_string {
    /* If T provides a constructor from string, this is the declaration of SFINAE that will succeed,
     * of size 4 bytes
     */
    template<typename U>
    static int32_t SFINAE(decltype(U(std::string()))*);

    /* Otherwise the ellipsis will accept just about anything (and has minimum priority) so in the fallback case
     * we'll use this definition and SFINAE will be 1 byte big
     */
    template<typename U>
    static int8_t SFINAE(...);

    // Check what size SFINAE ended up being, this tells us if the constructor matched the right signature or not
    static const bool value = sizeof(SFINAE<T>(nullptr)) == sizeof(int32_t);
};

template <typename T>
typename std::enable_if<has_constructor_from_string<T>::value, T>::type
convertString(const std::string& s){
	return T(s);
};

template <typename T>
typename std::enable_if<!has_constructor_from_string<T>::value, T>::type
convertString(const std::string& s){
	T tmp = T{};
	fillFromString(s, tmp);
	return tmp;
};

//next checks whether entries are numbers
void fillFromStringCheck(const std::string& s, std::string & dest);
void fillFromStringCheck(const std::string& s, int & dest);
void fillFromStringCheck(const std::string& s, uint8_t & dest);
void fillFromStringCheck(const std::string& s, uint16_t & dest);
void fillFromStringCheck(const std::string& s, uint32_t & dest);
void fillFromStringCheck(const std::string& s, uint64_t & dest);
void fillFromStringCheck(const std::string& s, long & dest);
void fillFromStringCheck(const std::string& s, double & dest);
void fillFromStringCheck(const std::string& s, float & dest);
void fillFromStringCheck(const std::string& s, bool & dest);

template <typename T>
typename std::enable_if<has_constructor_from_string<T>::value, T>::type
convertStringCheck(const std::string& s){
	return T(s);
};

template <typename T>
typename std::enable_if<!has_constructor_from_string<T>::value, T>::type
convertStringCheck(const std::string& s){
	T tmp = T{};
	fillFromStringCheck(s, tmp);
	return tmp;
};

// TODO: switch to std::from_chars as soon as GCC compiler is ready (not yet supported)
// this is a new (C++17), templated, fast and elegant string conversion tool. Usage:
/*template <typename T> T convertStringCheck(const std::string& s){
    auto value = T{};
    auto [ptr, error] = std::from_chars(s.data(), s.data() + s.size(), value);
    if (error == std::errc()){ // ok, no error occurred
        return value;
    } else if (error == std::errc::invalid_argument){
        throw "String '" + s +"' is not a number!";
    } else if (error == std::errc::result_out_of_range){
        throw "String '" + s +"' is out of range! Distance: " + ptr - s.data() + ".";
    }
};*/

// time
std::string timeToString(const time_t & time, const bool & local);
time_t stringToTime(const std::string & input, const bool & local);

//-------------------------------------------------
// Compare and replace
//-------------------------------------------------
uint32_t levenshteinDistance(const std::string& s, const std::string& t, const int & insertionCost, const int & deletionCost);

std::string stringReplace(const char & Needle, const std::string& Replace, const std::string & Haystack);
std::string stringReplace(const std::string& Needle, const std::string& Replace, const std::string & Haystack);

template <typename T> bool equalAs(const std::string & s1, const std::string & s2, const T & maxDiff){
    auto val1 = convertStringCheck<T>(s1);
    auto val2 = convertStringCheck<T>(s2);
    return !(std::fabs(val1 - val2) > maxDiff);
}


//-------------------------------------------------
// Erase, extract and trim
//-------------------------------------------------
void eraseAllOccurences(std::string & s, const std::string& needle, const bool & any = false);
[[deprecated("use eraseAllOccurences with any=false")]] void eraseAllOccurencesExactly(std::string & s, const std::string& needle);
[[deprecated("use eraseAllOccurences with any=true")]] void eraseAllOccurencesAny(std::string & s, const std::string& needle);
void eraseAllOccurences(std::string & s, const char & needle);

void eraseAllWhiteSpaces(std::string & s);
void eraseAllNonAlphabeticCharacters(std::string &s);

//read
std::string readBefore(const std::string & s, const std::string& needle, const bool & any = false);
std::string readBefore(const std::string & s, const char & needle);

std::string readUntil(const std::string & s, const std::string& needle, const bool & any = false);
std::string readUntil(const std::string & s, const char & needle);

std::string readBeforeLast(const std::string & s, const std::string& needle, const bool & any = false);
std::string readBeforeLast(const std::string & s, const char & needle);

std::string readUntilLast(const std::string & s, const std::string& needle, const bool & any = false);
std::string readUntilLast(const std::string & s, const char & needle);

std::string readAfter(const std::string & s, const std::string& needle, const bool & any = false);
std::string readAfter(const std::string & s, const char & needle);

std::string readAfterLast(const std::string & s, const std::string& needle, const bool & any = false);
std::string readAfterLast(const std::string & s, const char & needle);

//manipulations
std::string extractBefore(std::string & s, const std::string& needle, const bool & any = false);
[[deprecated("use extractBefore with any=false")]] std::string extractBeforeExactly(std::string & s, const std::string& needle);
[[deprecated("use extractBefore with any=true")]]  std::string extractBeforeAny(std::string & s, const std::string& needle);
std::string extractBefore(std::string & s, const char & needle);

std::string extractUntil(std::string & s, const std::string& needle, const bool & any = false);
[[deprecated("use extractUntil with any=false")]] std::string extractUntilExactly(std::string & s, const std::string& needle);
[[deprecated("use extractUntil with any=true")]] std::string extractUntilAny(std::string & s, const std::string& needle);
std::string extractUntil(std::string & s, const char & needle);

std::string extractBeforeLast(std::string & s, const std::string& needle, const bool & any = false);
[[deprecated("use extractBeforeLast with any=false")]] std::string extractBeforeLastExactly(std::string & s, const std::string& needle);
[[deprecated("use extractBeforeLast with any=true")]] std::string extractBeforeLastAny(std::string & s, const std::string& needle);
std::string extractBeforeLast(std::string & s, const char & needle);

std::string extractUntilLast(std::string & s, const std::string& needle, const bool & any = false);
[[deprecated("use extractUntilLast with any=false")]] std::string extractUntilLastExactly(std::string & s, const std::string& needle);
[[deprecated("use extractUntilLast with any=true")]] std::string extractUntilLastAny(std::string & s, const std::string& needle);
std::string extractUntilLast(std::string & s, const char & needle);

std::string extractAfter(std::string & s, const std::string& needle, const bool & any = false);
[[deprecated("use extractAfter with any=false")]] std::string extractAfterExactly(std::string & s, const std::string& needle);
[[deprecated("use extractAfter with any=true")]] std::string extractAfterAny(std::string & s, const std::string& needle);
std::string extractAfter(std::string & s, const char & needle);

std::string extractAfterLast(std::string & s, const std::string& needle, const bool & any = false);
[[deprecated("use extractAfterLast with any=false")]]  std::string extractAfterLastExactly(std::string & s, const std::string& needle);
[[deprecated("use extractAfterLast with any=true")]]  std::string extractAfterLastAny(std::string & s, const std::string& needle);
std::string extractAfterLast(std::string & s, const char & needle);

std::string extractPath(std::string & s);
std::string extractBeforeDoubleSlash(std::string & s);
std::string extractBeforeWhiteSpace(std::string & s);

//trim
void trimString(std::string & s);
void trimString(std::string & s, const std::string& what);
void trimEndlineString(std::string & s);
std::string getTrimmedString(const std::string & s);
std::string getTrimmedString(std::string & s, const std::string& what);
std::string	getTrimmedEndlineString(std::string & s);

// split
std::string split(std::string& s, const std::string& delim, const bool & any = true);
std::string split(std::string& s, const char & delim);

std::string splitAtLast(std::string& s, const std::string& delim, const bool & any = true);
std::string splitAtLast(std::string& s, const char & delim);

std::string splitAtPos(std::string& s, const std::string::size_type & l, const size_t & Length = 1);

//-------------------------------------------------
// Concatenation
//-------------------------------------------------
 template <typename T> std::string concatenateString(const T & vec, const std::string& delim){
	std::string s;
	if(vec.size()>0){
		auto it = vec.cbegin();
		s = toString(*it);
		++it;
		for(; it!=vec.cend(); ++it){
			s += delim + toString(*it);
		}
	}
	return s;
};

template <typename T> std::string concatenateString(const T & vec){
	return concatenateString(vec, "");
};

template <typename T> void fillConcatenatedString(const T & vec, std::string & s){
    s = concatenateString(vec, "");
};

template <typename T> void fillConcatenatedString(const T & vec, std::string & s, const std::string delim){
	s = concatenateString(vec, delim);
};

template <typename T> std::string concatenateString(const T & vec, const std::string& delim, const size_t from){
	std::string s;
	if(vec.size()>0 && from < vec.size()){
		auto it = vec.begin() + from;
		s = toString(*it);
		++it;
		for(; it!=vec.end(); ++it){
			s += delim + toString(*it);
		}
	}
	return s;
};

template <typename T> std::string concatenateString(const T & vec, const size_t & from){
	return concatenateString(vec, "", from);
};

template <typename T> void fillConcatenatedString(const T & vec, std::string & s, const size_t & from){
    s = concatenateString(vec, "", from);
};

template <typename T> void fillConcatenatedString(const T & vec, std::string & s, const std::string & delim, const size_t & from){
	s = concatenateString(vec, delim, from);
};

std::string concatenateString(const double* array, const int & length, const std::string& delim);

//function replicating the past behavior of R
template <typename T, typename U> std::vector<std::string> paste(const T & first, const U & second, const std::string & delim){
	if(first.size() == second.size()){
		//equal length: simple pasting
		std::vector<std::string> ret(first.size());

		for(size_t i = 0; i < first.size(); ++i){
			ret[i] = toString(first[i]) + delim + toString(second[i]);
		}

		return ret;
	} else if(first.size() % second.size() == 0){
		//recycle second
		std::vector<std::string> ret(first.size());

		size_t j = 0;
		for(size_t i = 0; i < first.size(); ++i, ++j){
			if(j == second.size()){
				j = 0;
			}
			ret[i] = toString(first[i]) + delim + toString(second[j]);
		}

		return ret;
	} else if(second.size() % first.size() == 0){
		//recycle first
		std::vector<std::string> ret(second.size());

		size_t j = 0;
		for(size_t i = 0; i < second.size(); ++i, ++j){
			if(j == first.size()){
				j = 0;
			}
			ret[i] = toString(first[j]) + delim + toString(second[i]);
		}

		return ret;
	} else {
		throw std::runtime_error("template <typename T, typename U> std::vector<std::string> paste(const T & first, const U & second, const std::string & delim): sizes are not multiples oif each other!");
	}
};

//-------------------------------------------------
// Split into vector/set/list
//-------------------------------------------------

/* *
 * Split a string into a container
 *
 * @tparam ContainerType is the type of container where the elements should be stored in, e.g. std::vector/std::set/std::list etc.
 * @tparam StorageType is the type that should be stored inside the container, e.g. std::string/double/int etc.
 *
 * @param[in] String is the string to split
 * @param[out] Container is the container to fill. Container can be std::vector, std::set, std::unordered_set, std::list etc.
 *              or any other instance that has clear(), insert() and end() functions defined
 * @param[in] Delim is the delimiter used to split the string. It is given as a string (and not templated for char), in order to
 *              be able to use the .size() function
 * @param[in] SkipEmpty is a bool indicating whether empty strings (e.g. if there are two consecutive delimiters) should be skipped
 * @param[in] Check is a bool indicating whether string to StorageType conversion should be checked on validity
 * @param[in] Any is a bool indicating whether it is sufficient to match any character of Delim in order to split (Any = true),
 *              or whether the entire sequence of Delim must match (Any=false)
 */

template<typename StorageType, template <typename...> class ContainerType>
void fillContainerFromString(std::string String, ContainerType<StorageType> & Container, const std::string & Delim, const bool & SkipEmpty = false, const bool & Check = false, const bool & Any = false){
    if (!SkipEmpty && Check){
        throw std::runtime_error("In function 'fillContainerFromString': Can not skipEmpty and check at the same time!"); // not skipping empty but checking doesn't make sense, as stringConversion will fail
    }
    Container.clear();
    if (!String.empty()){
        std::string::size_type l;
        std::string left;
        do{
            if (Any) {
                l = String.find_first_of(Delim);
                left = splitAtPos(String, l, 1); // just remove single char
            } else {
                l = String.find(Delim);
                left = splitAtPos(String, l, Delim.size()); // remove entire Delim
            }
            trimString(left);
            // skipEmpty?
            if (SkipEmpty && left.empty()){
                continue;
            }
            // check or don't
            StorageType tmp;
            if (Check) {
                tmp = convertStringCheck<StorageType>(left);
            } else {
                tmp = convertString<StorageType>(left);
            }
            Container.insert(Container.end(), tmp);

        } while(l != std::string::npos);
    }
};

template<typename StorageType, template <typename...> class ContainerType>
void fillContainerFromString(std::string String, ContainerType<StorageType> & Container, const char & Delim, const bool & SkipEmpty = false, const bool & Check = false, const bool & Any = false){
    // Same as above, but takes Delim as 'char'

    std::string delimString = {Delim};
    fillContainerFromString(String, Container, delimString, SkipEmpty, Check, Any);
}

template<typename StorageType, template <typename...> class ContainerType, class DelimType>
void fillContainerFromStringAny(std::string String, ContainerType<StorageType> & Container, const DelimType & Delim, const bool & SkipEmpty = false, const bool & Check = false){
    // little helper function - if any=true, but otherwise default for SkipEmpty and Check, this function provides a nicer interface

    fillContainerFromString(String, Container, Delim, SkipEmpty, Check, true);
}

template<typename StorageType, template <typename...> class ContainerType, class DelimType>
void fillProbabilityContainerFromString(std::string String, ContainerType<StorageType> & Container, const DelimType & Delim, const bool & SkipEmpty = false, const bool & Any = false){
    std::string delimString = {Delim}; // convert to string, because + operator is not defined for string and char
    if(!stringContainsOnly(String, "-0123456789." + delimString)){
		throw "Wrong format of probability string '" + String + "': it may only contain floating point numbers delimited by '" + delimString + "' (e.g. 0.1" + delimString + "0.2" + delimString + "0.5).";
	}

	fillContainerFromString(String, Container, delimString, SkipEmpty, true, Any); // check = true

	//check if all numbers are within [0,1]
	for(auto& it: Container){
		if(it < 0.0 || it > 1.0){
			throw "Wrong format of probability string '" + String + "': all probabilities must be within [0,1], which is not the case for " + toString(it) + "!";
		}
	}
};

template<typename StorageType, template <typename...> class ContainerType, class DelimType>
void fillProbabilityContainerFromStringAny(std::string String, ContainerType<StorageType> & Container, const DelimType & Delim, const bool & SkipEmpty = false){
    // little helper function - if any=true, but otherwise default for SkipEmpty, this function provides a nicer interface
    fillProbabilityContainerFromString(String, Container, Delim, SkipEmpty, true);
};

template<typename StorageType, template <typename...> class ContainerType>
void fillContainerFromStringWhiteSpace(std::string String, ContainerType<StorageType> & Container, const bool & SkipEmpty = false, const bool & Check = false){
    fillContainerFromString(String, Container, " \t", SkipEmpty, Check, true);
}

//-------------------------------------------------
// Reading from file
//-------------------------------------------------
//read from file -> deprecated. Use TInputfile instead

inline std::string readFromFile(std::ifstream & is){
	std::string line;
	getline(is, line);
	return line;
};

template<typename StorageType, template <typename...> class ContainerType, class DelimType>
void fillContainerFromLine(std::ifstream & is, ContainerType<StorageType> & Container, const DelimType & Delim, const bool & SkipEmpty = false, const bool & Check = false, const bool & Any = false){
    fillContainerFromString(readFromFile(is), Container, Delim, SkipEmpty, Check, Any);
};

template<typename StorageType, template <typename...> class ContainerType, class DelimType>
void fillContainerFromLineAny(std::ifstream & is, ContainerType<StorageType> & Container, const DelimType & Delim, const bool & SkipEmpty = false, const bool & Check = false){
    // little helper function - if any=true, but otherwise default for SkipEmpty and Check, this function provides a nicer interface
    fillContainerFromLine(is, Container, Delim, SkipEmpty, Check, true);
};

template<typename StorageType, template <typename...> class ContainerType>
void fillContainerFromLineWhiteSpace(std::ifstream & is, ContainerType<StorageType> & Container, const bool & SkipEmpty = false, const bool & Check = false){
    fillContainerFromStringWhiteSpace(readFromFile(is), Container, SkipEmpty, Check);
};

template<typename StreamType>
bool readUntilDelimiter(StreamType * FilePointer, std::string & String, const char & Delimiter, const std::string & DelimiterComment = "//"){
    String.clear();
    if (FilePointer->good() && !FilePointer->eof()){
        std::getline(*FilePointer, String, Delimiter);

        //skip comments
        String = extractBefore(String, DelimiterComment, false);
    }

    if(!FilePointer->good() || FilePointer->eof()){
        return false;
    }
    return true;
}

//-------------------------------------------------
// Sequences and indexes
//-------------------------------------------------
// number to letter (Excel column numbering)
std::string numericToLowerCaseAlphabetIndex(const int & Index);
std::string numericToUpperCaseAlphabetIndex(const int & Index);
int lowerCaseAlphabetIndexToNumeric(const std::string & Input);
int upperCaseAlphabetIndexToNumeric(const std::string & Input);

//expand / repeat index
bool addRepeatedIndexIfRepeated(const std::string & orig, std::vector<std::string> & vec);
bool addExpandedIndexIfToExpand(const std::string & orig, std::vector<std::string> & vec);
void addRepeatedIndex(const std::string & orig, std::vector<std::string> & vec);
void addExpandIndex(const std::string & orig, std::vector<std::string> & vec);
void addRepeatedAndExpandIndexes(const std::string & orig, std::vector<std::string> & vec);
void repeatAndExpandIndexes(const std::vector<std::string> & orig, std::vector<std::string> & vec);

template <typename T> void repeatIndexes(std::vector<std::string> & orig, std::vector<T> & vec){
    vec.clear();
	std::vector<std::string> tmp;
	for(auto it=orig.begin(); it!=orig.end(); ++it){
		addRepeatedIndex(*it, tmp);
	}
	for(auto it=tmp.begin(); it!=tmp.end(); ++it){
		vec.push_back(convertStringCheck<T>(*it));
	}
};

void addRepeatedAndExpandedIndexesOfSub(const std::string & orig, std::vector<std::vector<std::string>> & vec, const std::string& delim);
void repeatAndExpandIndexesOfSubs(const std::vector<std::string> & orig, std::vector<std::vector<std::string>> & vec, const std::string& delim);

template<typename T> std::vector<std::string> expandOverMultiDimensions(std::vector<T> vec){
    // example: you have vector {3, 2, 2} (of some size, in this case 3) and would like get all combinations of values up to the value in the vector
    // in this case:
    // 0_0_0
    // 0_0_1
    // 0_1_0
    // 0_1_1
    // 1_0_0
    // 1_0_1
    // 1_1_0
    // 1_1_1
    // 2_0_0
    // 2_0_1
    // 2_1_0
    // 2_1_1
    // this function solves this with a recursion; and return a vector of strings containing these combinations
    std::vector<std::string> result;
    std::vector<std::string> remaining;
    if (vec.size() > 1){
        std::vector<T> vecWithoutFirstElement(vec.begin() + 1, vec.end());
        remaining = expandOverMultiDimensions<T>(vecWithoutFirstElement);
    }
    for (int i = 0; i < vec.at(0); i++){
        if (vec.size() == 1)
            result.push_back(toString(i));
        else {
            for (auto & val : remaining){
                result.push_back(toString(i) + "_" + val);
            }
        }
    }
    return result;
}

#endif /* STRINGFUNCTIONS_H_ */

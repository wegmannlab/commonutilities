
#include "stringFunctions.h"

std::string toString(const char* input) {
    std::string s(input);
    return s;
}
std::string toString(){
    return "";
}

std::string toString(const std::string & input) {
    return input;
}

//-------------------------------------------------
// String contains
//-------------------------------------------------
bool stringContains(const std::string & haystack, const std::string& needle){
    // returns true if all of the characters in needle can be found in haystack
    if (haystack.empty()) return false;
    return haystack.find(needle) != std::string::npos;
};

bool stringContainsAny(const std::string & haystack, const std::string& needle){
    // returns true if at least one of the characters in needle can be found in haystack
    if (haystack.empty()) return false;
    return haystack.find_first_of(needle) != std::string::npos;
};

bool stringContains(const std::string & haystack, const char & needle){
    if (haystack.empty()) return false;
    return haystack.find_first_of(needle) != std::string::npos;
};

bool stringContainsOnly(const std::string & haystack, const std::string& needle){
    if (haystack.empty()) return false;
    return haystack.find_first_not_of(needle) == std::string::npos;
};

//numbers
bool stringContainsNumbers(const std::string & haystack){
    return stringContainsAny(haystack, "1234567890");
};
bool stringContainsOnlyNumbers(const std::string & haystack){
    return stringContainsOnly(haystack, "1234567890");
};
bool stringIsProbablyANumber(const std::string & haystack){
    return stringContainsOnly(haystack, "1234567890.Ee-+");
};
bool stringIsProbablyABool(const std::string & haystack){
    return haystack == "true" || haystack == "false" || haystack == "1" || haystack == "0";
};

//letters
bool stringContainsLetters(const std::string & haystack){
    return stringContainsAny(haystack, "abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUVWXYZäöüÄÖÜàéèÀÉÈ");
};

bool stringContainsOnlyLetters(const std::string & haystack){
    return stringContainsOnly(haystack, "abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUVWXYZäöüÄÖÜàéèÀÉÈ");
};

bool stringStartsWith(const std::string & haystack, const std::string& needle){
    if (haystack.empty()) return false;
    return haystack.rfind(needle, 0) == 0;
};

bool stringStartsWith(const std::string & haystack, const char& needle){
    if (haystack.empty()) return false;
    return haystack.rfind(needle, 0) == 0;
};

//-------------------------------------------------
// Conversion from string
//-------------------------------------------------
int stringToInt(const std::string & s){
    return atoi(s.c_str());
};

uint32_t stringToUnsignedInt(const std::string & s){
    return atoi(s.c_str());
};

uint64_t stringToLong(const std::string & s){
    return atol(s.c_str());
};

double stringToDouble(const std::string & s){
    return atof(s.c_str());
};

float stringToFloat(const std::string & s){
    return atof(s.c_str());
};

bool stringToBool(const std::string & s){
    return s == "true" || s == "1";
}

void fillFromString(const std::string& s, std::string & dest){ dest = s; };
void fillFromString(const std::string& s, int & dest){ dest = stringToInt(s); };
void fillFromString(const std::string& s, uint8_t & dest){ dest = stringToUnsignedInt(s); };
void fillFromString(const std::string& s, uint16_t & dest){ dest = stringToUnsignedInt(s); };
void fillFromString(const std::string& s, uint32_t & dest){ dest = stringToUnsignedInt(s); };
void fillFromString(const std::string& s, uint64_t & dest){ dest = stringToLong(s); };
void fillFromString(const std::string& s, long & dest){ dest = stringToLong(s); };
void fillFromString(const std::string& s, double & dest){ dest = stringToDouble(s); };
void fillFromString(const std::string& s, float & dest){ dest = stringToFloat(s); };
void fillFromString(const std::string& s, bool & dest){ dest = stringToBool(s); };

long stringToLongCheck(const std::string & s){
    // does not support scientific notation (other option would be to use stof, but then you would have a different range)
    if (!stringIsProbablyANumber(s))
        throw "String '" + s + "' is not a number!";
    try {
        return std::stol(s);
    } catch (std::invalid_argument & error) {
        throw "String '" +s+"' is not a number!";
    } catch (std::out_of_range & error) {
        throw "String '" +s+"' is out of range!";
    }
};

int stringToIntCheck(const std::string & s){
    // does not support scientific notation (other option would be to use stof, but then you would have a different range)
    if (!stringIsProbablyANumber(s))
        throw "String '" + s + "' is not a number!";
    try {
        return std::stoi(s);
    } catch (std::invalid_argument & error) {
        throw "String '" + s +"' is not a number!";
    } catch (std::out_of_range & error) {
        throw "String '" + s +"' is out of range!";
    }
};

uint64_t stringToUint64tCheck(const std::string & s){
    // does not support scientific notation (other option would be to use stof, but then you would have a different range)
    if (!stringIsProbablyANumber(s)) {
        throw "String '" + s + "' is not a number!";
    } if (stringStartsWith(s, '-')){ // little hack, because stoull() does not recognize negative values as invalid and hence turns "-1" into the numerical max.
        throw "String '" + s + "' is negative!";
    }
    try {
        return std::stoull(s);
    } catch (std::invalid_argument & error) {
        throw "String '" + s +"' is not a number!";
    } catch (std::out_of_range & error) {
        throw "String '" + s +"' is out of range!";
    }
};

double stringToDoubleCheck(const std::string & s){
    if (!stringIsProbablyANumber(s))
        throw "String '" + s + "' is not a number!";
    try {
        return std::stod(s);
    } catch (std::invalid_argument & error) {
        throw "String '" + s +"' is not a number!";
    } catch (std::out_of_range & error) {
        throw "String '" + s +"' is out of range!";
    }
};

float stringToFloatCheck(const std::string & s){
    if (!stringIsProbablyANumber(s))
        throw "String '" + s + "' is not a number!";
    try {
        return std::stof(s);
    } catch (std::invalid_argument & error) {
        throw "String '" + s +"' is not a number!";
    } catch (std::out_of_range & error) {
        throw "String '" + s +"' is out of range!";
    }
};

bool stringToBoolCheck(const std::string & s){
    if (s == "true" || s == "1")
        return true;
    else if (s == "false" || s == "0")
        return false;
    else throw "Can not convert string '" + s + "' to a boolean!";
}

void fillFromStringCheck(const std::string& s, std::string & dest){ dest = s; };
void fillFromStringCheck(const std::string& s, int & dest){ dest = stringToIntCheck(s); };
void fillFromStringCheck(const std::string& s, uint8_t & dest){
    // first convert string to uin64_t (largest container)
    uint64_t tmp = stringToUint64tCheck(s);
    // now cast to uint8_t, but check for numeric overflow
    if (tmp <= std::numeric_limits<uint8_t>::max()){ // ok, safe to convert
        dest = static_cast<uint8_t>(tmp);
    } else {
        throw "String '" + s +"' is out of range!";
    }
};
void fillFromStringCheck(const std::string& s, uint16_t & dest){
    // first convert string to uin64_t (largest container)
    uint64_t tmp = stringToUint64tCheck(s);
    // now cast to uint16_t, but check for numeric overflow
    if (tmp <= std::numeric_limits<uint16_t>::max()){ // ok, safe to convert
        dest = static_cast<uint16_t>(tmp);
    } else {
        throw "String '" + s +"' is out of range!";
    }
};
void fillFromStringCheck(const std::string& s, uint32_t & dest){
    // first convert string to uin64_t (largest container)
    uint64_t tmp = stringToUint64tCheck(s);
    // now cast to uint16_t, but check for numeric overflow
    if (tmp <= std::numeric_limits<uint32_t>::max()){ // ok, safe to convert
        dest = static_cast<uint32_t>(tmp);
    } else {
        throw "String '" + s +"' is out of range!";
    }
};
void fillFromStringCheck(const std::string& s, uint64_t & dest){ dest = stringToUint64tCheck(s); };
void fillFromStringCheck(const std::string& s, long & dest){ dest = stringToLongCheck(s); };
void fillFromStringCheck(const std::string& s, double & dest){ dest = stringToDoubleCheck(s); };
void fillFromStringCheck(const std::string& s, float & dest){ dest = stringToFloatCheck(s); };
void fillFromStringCheck(const std::string& s, bool & dest){ dest = stringToBoolCheck(s); };

std::string timeToString(const time_t & time, const bool & local){
    struct tm * timeinfo;
    char buffer [20];

    if (local)
        timeinfo = localtime(&time);
    else
        timeinfo = gmtime (&time);

    strftime (buffer, 20, "%Y-%m-%d %H:%M:%S", timeinfo);
    return std::string(buffer);
};

time_t stringToTime(const std::string & input, const bool & local){
    tm timeptr{};
    timeptr.tm_isdst = -1;

    if(strptime(input.c_str(), "%Y-%m-%d %H:%M:%S", &timeptr) == nullptr){
        if(strptime(input.c_str(), "%d/%m/%Y %H:%M:%S", &timeptr) == nullptr)
            throw "Failed to convert date / time '" + input + "'!";
    }

    if (local)
        return mktime(&timeptr);
    return timegm(&timeptr);
};

//-----------------------------------------------------------------------
// Compare and replace
//-----------------------------------------------------------------------
unsigned int levenshteinDistance(const std::string& s, const std::string& t, const int & insertionCost, const int & deletionCost){
	//calculate Levenshtein Distance. Code adapted from Wikipedia
	unsigned int m = s.length();
	unsigned int n = t.length();

	// create two work vectors of integer distances
	unsigned int* v0 = new unsigned int[n + 1];
	unsigned int* v1 = new unsigned int[n + 1];

	 // initialize v0 (the previous row of distances)
	// this row is A[0][i]: edit distance for an empty s
	// the distance is just the number of characters to delete from t
	for(unsigned int i=0; i<=n; ++i){
		v0[i] = i;
	}

	for(unsigned int i=0; i<m; ++i){
		// calculate v1 (current row distances) from the previous row v0
		// first element of v1 is A[i+1][0]
		//   edit distance is delete (i+1) chars from s to match empty t
		v1[0] = i + 1;

		// use formula to fill in the rest of the row
		for(unsigned int j=0; j<n; ++j){
			// calculating costs for A[i+1][j+1]
			unsigned int minDist = v0[j + 1] + deletionCost; //deletion cost
			if(v1[j] + 1 < minDist){
				minDist = v1[j] + insertionCost; //insertion cost
			}
			if(s[i] == t[j]){ //substitution cost
				if(v0[j] < minDist){
					minDist = v0[j];
				}
			} else {
				if(v0[j] + 1 < minDist){
					minDist = v0[j] + insertionCost + deletionCost;
				}
			}

			v1[j + 1] = minDist;
		}

		// copy v1 (current row) to v0 (previous row) for next iteration
	   unsigned int* tmp = v0;
	   v0 = v1;
	   v1 = tmp;
	}

	// after the last swap, the results of v1 are now in v0
	int ret = v0[n];
	delete[] v0;
	delete[] v1;

	return ret;
};

std::string stringReplace(const char & Needle, const std::string& Replace, const std::string & Haystack){
    std::string haystack = Haystack;
    std::string s;
    std::string::size_type l=haystack.find_first_of(Needle);
    while(l!=std::string::npos){
        s+=haystack.substr(0,l)+Replace;
        haystack.erase(0, l+1);
        l=haystack.find_first_of(Needle);
    }

    s+=haystack;
    return s;
};

std::string stringReplace(const std::string& Needle, const std::string& Replace, const std::string & Haystack){
    std::string haystack = Haystack;
    std::string s;
    std::string::size_type l=haystack.find(Needle);
    while(l!=std::string::npos){
        s+=haystack.substr(0,l)+Replace;
        haystack.erase(0, l+Needle.size());
        l=haystack.find(Needle);
    }
    s+=haystack;
    return s;
};

//-----------------------------------------------------------------------
// Erase, extract and trim
//-----------------------------------------------------------------------
void eraseAllOccurences(std::string & s, const std::string & needle, const bool & any){
    if (any){ // match any
        std::string::size_type l=s.find_first_of(needle);
        while(l!=std::string::npos){
            s.erase(l, 1);
            l=s.find_first_of(needle);
        }
    } else { // match exactly
        std::string::size_type l=s.find(needle);
        while(l!=std::string::npos){
            s.erase(l, needle.size());
            l=s.find(needle);
        }
    }
}

void eraseAllOccurencesExactly(std::string & s, const std::string& needle){
    eraseAllOccurences(s, needle, false);
};

void eraseAllOccurencesAny(std::string & s, const std::string& needle){
    eraseAllOccurences(s, needle, true);
};

void eraseAllOccurences(std::string & s, const char & needle){
    std::string needleString;
    needleString = needle;
    eraseAllOccurences(s, needleString);
};

void eraseAllWhiteSpaces(std::string & s){
	eraseAllOccurences(s, " \t\f\v\n\r", true);
};

void eraseAllNonAlphabeticCharacters(std::string & s){
    s.erase(remove_if(s.begin(), s.end(), [](char c) { return !isalpha(c); } ), s.end());
}

// read

/////////////////// read before ///////////////////

std::string readBefore(const std::string & s, const std::string & needle, const bool & any){
    std::string sModified = s;
    return extractBefore(sModified, needle, any);
}

std::string readBefore(const std::string & s, const char & needle){
    std::string sModified = s;
    return extractBefore(sModified, needle);
}

/////////////////// read until ///////////////////

std::string readUntil(const std::string & s, const std::string & needle, const bool & any){
    std::string sModified = s;
    return extractUntil(sModified, needle, any);
}

std::string readUntil(const std::string & s, const char & needle){
    std::string sModified = s;
    return extractUntil(sModified, needle);
}

/////////////////// read before last ///////////////////

std::string readBeforeLast(const std::string & s, const std::string& needle, const bool & any){
    std::string sModified = s;
    return extractBeforeLast(sModified, needle, any);
};

std::string readBeforeLast(const std::string & s, const char & needle){
    std::string sModified = s;
    return extractBeforeLast(sModified, needle);
};

/////////////////// read until last ///////////////////

std::string readUntilLast(const std::string & s, const std::string& needle, const bool & any){
    std::string sModified = s;
    return extractUntilLast(sModified, needle, any);
};

std::string readUntilLast(const std::string & s, const char & needle){
    std::string sModified = s;
    return extractUntilLast(sModified, needle);
};

/////////////////// read after ///////////////////

std::string readAfter(const std::string & s, const std::string& needle, const bool & any){
    std::string sModified = s;
    return extractAfter(sModified, needle, any);
};

std::string readAfter(const std::string & s, const char & needle){
    std::string sModified = s;
    return extractAfter(sModified, needle);
};

/////////////////// read after last ///////////////////

std::string readAfterLast(const std::string & s, const std::string& needle, const bool & any){
    std::string sModified = s;
    return extractAfterLast(sModified, needle, any);
};

std::string readAfterLast(const std::string & s, const char & needle){
    std::string sModified = s;
    return extractAfterLast(sModified, needle);
};

/////////////////// extract before ///////////////////
std::string extractBefore(std::string & s, const std::string& needle, const bool & any){
    // will return everything before (and not including) the first match of needle
    // will modify s to contain everything after (including) needle
    // if any: will match any character in needle; else: will match needle exactly
    std::string ret;
    std::string::size_type l;
    if (any) l = s.find_first_of(needle);
    else l=s.find(needle);
    if(l!=std::string::npos){
        ret=s.substr(0,l);
        s.erase(0, l);
    } else {
        ret=s;
        s.clear();
    }
    return ret;
}

std::string extractBeforeExactly(std::string & s, const std::string& needle){
    return extractBefore(s, needle, false);
};

std::string extractBeforeAny(std::string & s, const std::string& needle){
    return extractBefore(s, needle, true);
};

std::string extractBefore(std::string & s, const char & needle){
    std::string needleString;
    needleString = needle;
    return extractBefore(s, needleString);
};

std::string extractBeforeDoubleSlash(std::string & s){
    // will return everything before (and not including) //
    // will modify s to contain everything after (and including) //
	return extractBefore(s, "//", false);
};

std::string extractBeforeWhiteSpace(std::string & s){
    // will return everything before (and not including) the first whitespace
    // will modify s to contain everything after (including) the first whitespace
	return extractBefore(s, " \t\f\v\n\r", true);
};

/////////////////// extract until ///////////////////

std::string extractUntil(std::string & s, const std::string& needle, const bool & any){
    // will return everything before (and including) the first match of needle
    // will modify s to contain everything after (and not including) the first match of needle
    // if any: will match any character in needle; else: will match needle exactly
    int size = 1;
    if (!any) size = needle.size();
    std::string ret;
    std::string::size_type l;

    if (any) l=s.find_first_of(needle);
    else l=s.find(needle);
    if(l!=std::string::npos){
        ret=s.substr(0,l+size);
        s.erase(0, l+size);
    } else {
        ret=s;
        s.clear();
    }
    return ret;
}

std::string extractUntilExactly(std::string & s, const std::string& needle){
    return extractUntil(s, needle, false);
};

std::string extractUntilAny(std::string & s, const std::string& needle){
    return extractUntil(s, needle, true);
};

std::string extractUntil(std::string & s, const char & needle){
    std::string needleString;
    needleString = needle;
    return extractUntil(s, needleString);
};

/////////////////// extract before last ///////////////////

std::string extractBeforeLast(std::string & s, const std::string& needle, const bool & any){
    // will return everything before (and not including) the last match of needle
    // will modify s to contain everything after (and including) the last match of needle
    // if any: will match any character in needle; else: will match needle exactly
    std::string ret;
    std::string::size_type l;
    if (any) l=s.find_last_of(needle);
    else l=s.rfind(needle);
    if(l!=std::string::npos){
        ret=s.substr(0,l);
        s.erase(0, l);
    } else {
        ret=s;
        s.clear();
    }
    return ret;
}

std::string extractBeforeLastExactly(std::string & s, const std::string& needle){
    return extractBeforeLast(s, needle, false);
};

std::string extractBeforeLastAny(std::string & s, const std::string& needle){
    return extractBeforeLast(s, needle, true);
};

std::string extractBeforeLast(std::string & s, const char & needle){
    std::string needleString;
    needleString = needle;
    return extractBeforeLast(s, needleString);
};

/////////////////// extract until last ///////////////////

std::string extractUntilLast(std::string & s, const std::string& needle, const bool & any){
    // will return everything before (and including) the last match of needle
    // will modify s to contain everything after (and not including) the last match of needle
    // if any: will match any character in needle; else: will match needle exactly
    int size = 1;
    if (!any) size = needle.size();
    std::string ret;
    std::string::size_type l;

    if (any) l=s.find_last_of(needle);
    else l=s.rfind(needle);
    if(l!=std::string::npos){
        ret=s.substr(0,l+size);
        s.erase(0, l+size);
    } else {
        ret=s;
        s.clear();
    }
    return ret;
}

std::string extractUntilLastExactly(std::string & s, const std::string& needle){
    return extractUntilLast(s, needle, false);
};

std::string extractUntilLastAny(std::string & s, const std::string& needle){
    return extractUntilLast(s, needle, true);
};

std::string extractUntilLast(std::string & s, const char & needle){
    std::string needleString;
    needleString = needle;
    return extractUntilLast(s, needleString);
};

std::string extractPath(std::string & s){
    // will return everything before (and including) the last /
    // will modify s to contain everything after (and not including) the last /
    return extractUntilLast(s, "/");
};

/////////////////// extract after ///////////////////

std::string extractAfter(std::string & s, const std::string& needle, const bool & any){
    // will return everything after (while not including) the first match of needle
    // will modify s to contain everything before (and including) the first match of needle
    // if any: will match any character in needle; else: will match needle exactly
    int size = 1;
    if (!any) size = needle.size();
    std::string ret;
    std::string::size_type l;

    if (any) l=s.find_first_of(needle);
    else l=s.find(needle);
    if(l!=std::string::npos){
        ret=s.substr(l+size);
        s.erase(l+size);
    } else {
        ret=s;
        s.clear();
    }
    return ret;
}

std::string extractAfterExactly(std::string & s, const std::string& needle){
    return extractAfter(s, needle, false);
};

std::string extractAfterAny(std::string & s, const std::string& needle){
    return extractAfter(s, needle, true);
};

std::string extractAfter(std::string & s, const char & needle){
    std::string needleString;
    needleString = needle;
    return extractAfter(s, needleString);
};

/////////////////// extract after last ///////////////////

std::string extractAfterLast(std::string & s, const std::string& needle, const bool & any) {
    // will return everything after (while not including) the last match of needle
    // will modify s to contain everything before (and including) the last match of needle
    // if any: will match any character in needle; else: will match needle exactly
    int size = 1;
    if (!any) size = needle.size();
    std::string ret;
    std::string::size_type l;

    if (any) l=s.find_last_of(needle);
    else l=s.rfind(needle);
    if(l!=std::string::npos){
        ret=s.substr(l+size);
        s.erase(l+size);
    } else {
        ret=s;
        s.clear();
    }
    return ret;
}

std::string extractAfterLastExactly(std::string & s, const std::string& needle) {
    return extractAfterLast(s, needle, false);
};

std::string extractAfterLastAny(std::string & s, const std::string& needle) {
    return extractAfterLast(s, needle, true);
};

std::string extractAfterLast(std::string & s, const char & needle){
    std::string needleString;
    needleString = needle;
    return extractAfterLast(s, needleString);
};

/////////////////// trim ///////////////////
void trimString(std::string & s, const std::string& what){
    // will trim all occurrences of what in the beginning and in the end of s
    // will match any character of what (what does not have to match entirely)
	//from beginning
	std::string::size_type l=s.find_first_not_of(what);
	if(l==std::string::npos){
		s.clear();
	} else {
		s.erase(0, l);
		//from end
		l=s.find_last_not_of(what);
		if(l!=std::string::npos)
			s.erase(l+1);
	}
};

void trimString(std::string & s){
    // will trim all trailing and ending whitespaces of s.
    // Whitespaces in the middle of s will be kept (i.e. as soon as a non-whitespace character occurs)
    trimString(s, " \t\f\v\n\r");
};

void trimEndlineString(std::string & s){
    // will trim all trailing and ending endline characters of s.
    // endline characters in the middle of s will be kept (i.e. as soon as a non-endline character occurs)
    trimString(s, "\f\v\n\r");
};

std::string getTrimmedString(const std::string & s){
	std::string tmp = s;
	trimString(tmp);
	return tmp;
};

std::string getTrimmedString(std::string & s, const std::string& what){
	std::string tmp = s;
	trimString(tmp, what);
	return tmp;
};

std::string getTrimmedEndlineString(std::string & s){
	std::string tmp = s;
	trimEndlineString(tmp);
	return tmp;
};

/////////////////// split ///////////////////

std::string splitAny(std::string& s, const std::string& delim){
    std::string before;
    std::string::size_type l = s.find_first_of(delim);
    if (l != std::string::npos){
        before = s.substr(0,l);
        s.erase(0, l+1);
    } else {
        before=s;
        s.clear();
    }
    return before;
}

std::string splitExactly(std::string& s, const std::string& delim){
    std::string before;
    std::string::size_type l = s.find(delim);
    if (l != std::string::npos){
        before = s.substr(0,l);
        s.erase(0, l+delim.size());
    } else {
        before=s;
        s.clear();
    }
    return before;
}

std::string split(std::string& s, const std::string& delim, const bool & any){
    if (any) return splitAny(s, delim);
    return splitExactly(s, delim);
}

std::string split(std::string& s, const char & delim){
    std::string delimString;
    delimString = delim;
    return split(s, delimString);
}

/////////////////// split at last ///////////////////

std::string splitLastAny(std::string& s, const std::string& delim){
    std::string before;
    std::string::size_type l = s.find_last_of(delim);
    if (l != std::string::npos){
        before = s.substr(0,l);
        s.erase(0, l+1);
    } else {
        before=s;
        s.clear();
    }
    return before;
}

std::string splitLastExactly(std::string& s, const std::string& delim){
    std::string before;
    std::string::size_type l = s.rfind(delim);
    if (l != std::string::npos){
        before = s.substr(0,l);
        s.erase(0, l+delim.size());
    } else {
        before=s;
        s.clear();
    }
    return before;
}

std::string splitAtLast(std::string& s, const std::string& delim, const bool & any){
    if (any) return splitLastAny(s, delim);
    return splitLastExactly(s, delim);
}

std::string splitAtLast(std::string& s, const char & delim){
    std::string delimString;
    delimString = delim;
    return splitAtLast(s, delimString);
}

/////////////////// split at position ///////////////////

std::string splitAtPos(std::string& s, const std::string::size_type & l, const size_t & Length){
    std::string before;
    if (l != std::string::npos){
        before = s.substr(0,l);
        s.erase(0, l+Length);
    } else {
        before=s;
        s.clear();
    }
    return before;
}

//-------------------------------------------------
// Concatenation
//-------------------------------------------------

std::string concatenateString(const double* array, const int & length, const std::string& delim){
	std::string s;
	if(length>0){
		s = toString(array[0]);
		for(int i=1; i<length; ++i){
			s += delim + toString(array[i]);
		}
	}
	return s;
};

//-------------------------------------------------
// Sequences and indexes
//-------------------------------------------------

std::string _numericToAlphabet(const int & Index, const char & Start){
    // function to convert a numeric index to an alphabet string
    // corresponds to column names in Excel
    // example for Start = 'a':
    // 0 -> "a"
    // 1 -> "b"
    // ...
    // 25 -> "z"
    // 26 -> "aa"
    // 27 -> "ab"
    // ...
    int dividend = Index+1;
    std::string result;
    int modulo;

    while (dividend > 0){
        modulo = (dividend - 1) % 26;
        result = static_cast<char>(Start + modulo) + result;
        dividend = static_cast<int>((dividend - modulo) / 26);
    }

    return result;
}

std::string numericToLowerCaseAlphabetIndex(const int & Index){
    return _numericToAlphabet(Index, 'a');
}

std::string numericToUpperCaseAlphabetIndex(const int & Index){
    return _numericToAlphabet(Index, 'A');
}

int _alphabetIndexToNumeric(const std::string & Input, const char & Start){
    // reverse function of _numericToAlphabet
    int result = 0;
    for (const auto& character : Input){
        result *= 26;
        result += character - Start + 1;
    }
    return result - 1;
}

int lowerCaseAlphabetIndexToNumeric(const std::string & Input){
    return _alphabetIndexToNumeric(Input, 'a');
}

int upperCaseAlphabetIndexToNumeric(const std::string & Input){
    return _alphabetIndexToNumeric(Input, 'A');
}

//replace entry blah{x} with x times a blah entry
//replace entry blah[x] with blah_1, ..., blah_x
bool addRepeatedIndexIfRepeated(const std::string & orig, std::vector<std::string> & vec){
	std::string::size_type pos = orig.find_last_of('{');
    if(pos != std::string::npos && pos != 0){
		if(orig.find_first_of('{') != pos) throw "Multiple '{' characters in string to repeat '" + orig + "'!";
		if(orig.find_last_of('}') != orig.size()-1)  throw "String to repeat '" + orig + "' does not end with '}'!";
		if(orig.find_first_of('[') != std::string::npos)  throw "String to repeat '" + orig + "' contains a conflicting '[' character!";
		if(orig.find_first_of(']') != std::string::npos)  throw "String to repeat '" + orig + "' contains a conflicting ']' character!";
		std::string tmp = orig.substr(0, pos);
		int len = convertStringCheck<int>(orig.substr(pos + 1, orig.size() - pos - 2));
		if(len <= 0) throw "Request to repeat string '" + orig + "' zero times!";
		for(int i=1; i<=len; ++i) vec.push_back(tmp);
		return true;
	} else return false;
}

bool addExpandedIndexIfToExpand(const std::string & orig, std::vector<std::string> & vec){
	std::string::size_type pos = orig.find_last_of('[');
	if(pos != std::string::npos){
		if(orig.find_first_of('[') != pos) throw "Multiple '[' characters in string to expand '" + orig + "'!";
		std::string::size_type pos2 = orig.find_last_of(']');
		if(pos2 == std::string::npos) throw "Missing closing ']' in string to expand '" + orig + "'!";
		if(orig.find_first_of(']') != pos2) throw "Multiple ']' characters in string to expand '" + orig + "'!";
		if(pos2 < pos) throw "Unable to understand string to expand '" + orig + "': wrong order of '[' and ']'!";
		int len = convertString<int>(orig.substr(pos + 1, pos2 - pos));
		if(len <= 0) throw "Request to expand string '" + orig + "' zero times!";
		std::string tmp = orig.substr(0, pos);
		std::string tmp2 = orig.substr(pos2+1);
		for(int i=1; i<=len; ++i) vec.push_back(tmp + toString(i) + tmp2);
		return true;
	} else return false;
}

void addRepeatedIndex(const std::string & orig, std::vector<std::string> & vec){
	if(!addRepeatedIndexIfRepeated(orig, vec)) vec.push_back(orig);
}

void addExpandIndex(const std::string & orig, std::vector<std::string> & vec){
	if(!addExpandedIndexIfToExpand(orig, vec)) vec.push_back(orig);
};

void addRepeatedAndExpandIndexes(const std::string & orig, std::vector<std::string> & vec){
	if(!addRepeatedIndexIfRepeated(orig, vec)){
		if(!addExpandedIndexIfToExpand(orig, vec)) vec.push_back(orig);
	}
};

void repeatAndExpandIndexes(const std::vector<std::string> & orig, std::vector<std::string> & vec){
	vec.clear();
	for(auto & it : orig){
		addRepeatedAndExpandIndexes(it, vec);
	}
};

void addRepeatedAndExpandedIndexesOfSub(const std::string & orig, std::vector< std::vector<std::string> > & vec, const std::string& delim){
	std::vector<std::string> origVec;
	fillContainerFromStringAny(orig, origVec, delim, true);
	auto* tmpVec = new std::vector<std::string>[orig.size()];
	unsigned int times = 1;

	//expand individually
	unsigned int i=0;
	for(auto it=origVec.begin(); it!=origVec.end(); ++it, ++i){
		addRepeatedAndExpandIndexes(*it, tmpVec[i]);
		if(tmpVec[i].size() > 1){
			if(times > 1){
				if(tmpVec[i].size()!=times) throw "Unequal number of expansions / repeats in '" + orig + "'!";
			} else times =  tmpVec[i].size();
		}
	}

	//construct new vectors
	for(i=0; i<times; ++i) vec.emplace_back();
	for(i=0; i<origVec.size(); ++i){
		if(tmpVec[i].size()==1){
			auto it=vec.rbegin();
			for(unsigned int j=0; j<times; ++j, ++it)
				it->push_back(*(tmpVec[i].begin()));
		} else {
			auto it=vec.rbegin();
			for(auto sIt=tmpVec[i].rbegin(); sIt!=tmpVec[i].rend(); ++sIt, ++it){
				it->push_back(*sIt);
			}
		}
	}
	delete[] tmpVec;
}

void repeatAndExpandIndexesOfSubs(const std::vector<std::string> & orig, std::vector< std::vector<std::string> > & vec, const std::string& delim){
	vec.clear();
	for(auto & it : orig){
		addRepeatedAndExpandedIndexesOfSub(it, vec, delim);
	}
}


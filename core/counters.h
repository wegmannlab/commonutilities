/*
 * TCounters.h
 *
 *  Created on: May 29, 2020
 *      Author: wegmannd
 */

#ifndef COMMONUTILITIES_COUNTERS_H_
#define COMMONUTILITIES_COUNTERS_H_

#include "TFile.h"
#include "mathFunctions.h"
#include <map>
#include <vector>

//----------------------------------------------
// TCountDistribution
// assembles the distribution on integers
//----------------------------------------------
class TCountDistributionVector; // forward declaration
class TCountDistribution{
    // example: you've got the values:
    //      1 7 4 1 2 7 7 2
    // this class stores them as a histogram ( = count distribution) like this:
    //      0  0
    //      1  2
    //      2  2
    //      3  0
    //      4  1
    //      5  0
    //      6  0
    //      7  3

    // size of _dist vector is the maximal value + 1 (start counting at 0)
    friend class TCountDistributionVector;
private:
	std::vector<uint64_t> _dist;

public:
	TCountDistribution()=default;
    TCountDistribution(const uint32_t Value){
        _dist.resize(Value, 0);
    };

    void resize(const uint32_t Value){
        _dist.resize(Value, 0);
    };

	uint64_t& operator[](const uint32_t Value){
	    // resize if element is outside range
        if(_dist.size() <= Value){
            _dist.resize(Value+1, 0);
        }
		return _dist[Value];
	};

    const uint64_t& operator[](const uint32_t Value) const{
        // throw if element is outside range
        if(Value >= _dist.size()){
            throw std::runtime_error("In function 'uint64_t& operator[](const uint32_t Value)': No entry for value " + toString(Value) + " in TCountDistribution!");
        }
        return _dist[Value];
    };

	uint64_t at(const uint32_t Value) const{
	    // return 0 if element is outside range
		if(Value >= _dist.size()){
			return 0;
		} else {
			return _dist[Value];
		}
	};

	void clear(){
		_dist.clear();
	};

	void add(const uint32_t Value){
		if(_dist.size() <= Value){
			_dist.resize(Value+1, 0);
		}
		++_dist[Value];
	};

	void add(const uint32_t Value, const uint64_t Frequency){
		if(_dist.size() <= Value){
			_dist.resize(Value+1, 0);
		}
		_dist[Value] += Frequency;
	};

	void add(const TCountDistribution & other){
		for(size_t i=0; i<other._dist.size(); ++i){
			add(i, other._dist[i]);
		}
	};

	bool empty() const{
		for(auto& i : _dist){
			if(i > 0) return false;
		}
		return true;
	};

	uint64_t counts() const{
		uint64_t c = 0;
		for(auto& i : _dist){
			c += i;
		}
		return c;
	};

	uint64_t countsLarger(const uint32_t Value) const{
		uint64_t n = 0;
		for(size_t i=Value+1; i<_dist.size(); ++i){
			n += _dist[i];
		}
		return n;
	};

	uint64_t countsLargerZero() const{
		return countsLarger(0);
	};

	uint64_t sum() const{
		uint64_t s = 0;
		for(size_t i=1; i<_dist.size(); ++i){
			s += i * _dist[i]; // histogram -> multiply value with how many times it occurs
		}
		return s;
	};

	double mean() const{
        return (double) sum() / (double) counts();
	};

	uint32_t min() const{
		//get smallest entry that is non-zero
		for(size_t i=0; i<_dist.size(); ++i){
			if(_dist[i] > 0){
				return i;
			}
		}
		return 0;
	};

	uint32_t max() const{
        if(_dist.empty()){
            return 0;
        }

		//get largest entry that is non-zero
		for(size_t i=_dist.size()-1; i>=0; --i){
			if(_dist[i] > 0){
				return i;
			}
		}
		return 0;
	};

	double fracLarger(const uint32_t Value) const{
		return (double) countsLarger(Value) / (double) counts();
	};

	double fracLargerZero() const{
		return (double) countsLargerZero() / (double) counts();
	};

	void write(TOutputFile & out) const{
		for(size_t i=0; i<_dist.size(); ++i){
			out << i << _dist[i] << std::endl;
		}
	};

	void write(TOutputFile & out, const std::string name) const{
		for(size_t i=0; i<_dist.size(); ++i){
			out << name << i << _dist[i] << std::endl;
		}
	};

	void write(const std::string filename, const std::string label) const{
		TOutputFile out(filename, {label, "counts"});
		write(out);
	};
};

//----------------------------------------------
// TCountDistributionVector
//----------------------------------------------
class TCountDistributionVector{
    // 2D-histogram
private:
	std::vector<TCountDistribution> _distVec;

	void _writeAsMatrix(const std::string filename, const std::vector<std::string> & header) const{
		//open file
		TOutputFile out(filename, header);

		//write matrix
		uint32_t dim = header.size() - 1; //first is label
        for(uint32_t i=0; i<_distVec.size(); ++i){ // rows = IDs
			//write label (rowname)
			out << i;
			// write values
            for(uint32_t j=0; j<dim; ++j) // cols = values
                out << _distVec[i].at(j);
			out << std::endl;
		}
	};

public:
	TCountDistributionVector() = default;
	TCountDistributionVector(const size_t Size){
		_distVec.resize(Size);
	};

	void clear(){
		_distVec.clear();
	};

	void resize(const size_t Size){
		_distVec.resize(Size);
	};

	void add(const uint32_t ID, const uint32_t Value){
		if(_distVec.size() <= ID){
			_distVec.resize(ID+1);
		}
		_distVec[ID].add(Value);
	};

	void add(const uint32_t ID, const uint32_t Value, const uint64_t Frequency){
		if(_distVec.size() <= ID){
			_distVec.resize(ID+1);
		}
		_distVec[ID].add(Value, Frequency);
	};

	void add(const uint32_t ID, const TCountDistribution & Other){
		if(_distVec.size() <= ID){
			_distVec.resize(ID+1);
		}
		_distVec[ID].add(Other);
	};

	void add(TCountDistributionVector & vec){
		for(size_t i=0; i<vec._distVec.size(); ++i){
			add(i, vec._distVec[i]);
		}
	};

	size_t size() const{
		return _distVec.size();
	};

	uint64_t counts() const{
		uint64_t c = 0.0;
		for(auto& i : _distVec){
			c += i.counts();
		}
		return c;
	};

	uint64_t countsLarger(const uint32_t Value) const{
		uint64_t c = 0;
		for(auto& i : _distVec){
			c += i.countsLarger(Value);
		}
		return c;
	};

	uint64_t countsLargerZero() const{
		uint64_t c = 0;
		for(auto& i : _distVec){
			c += i.countsLargerZero();
		}
		return c;
	};

	uint64_t sum() const{
		uint64_t s = 0;
		for(auto& i : _distVec){
			s += i.sum();
		}
		return s;
	};

	double mean() const{
		return (double) sum() / (double) counts();
	};

	uint64_t min() const{
		if(_distVec.empty()){
			return 0;
		}
		uint64_t m = _distVec[0].min();
		for(auto& i : _distVec){
			if(i.min() < m){
				m = i.min();
			}
		}
		return m;
	};

	uint64_t max() const{
		if(_distVec.empty()){
			return 0;
		}
		uint64_t m = _distVec[0].max();
		for(auto& i : _distVec){
			if(i.max() > m){
				m = i.max();
			}
		}
		return m;
	};

	double fracLarger(const uint32_t Value) const{
		return (double) countsLarger(Value) / (double) counts();
	};

	double fracLargerZero() const{
		return (double) countsLargerZero() / (double) counts();
	};

	bool exists(const uint32_t ID) const{
		if(ID < _distVec.size())
			return true;
		else
			return false;
	};

	const TCountDistribution& operator[](const uint32_t ID) const{
	    // can not automatically resize, as this would require a value
		if(!exists(ID))
			throw std::runtime_error("In function 'const TCountDistribution& operator[](const uint32_t ID) const': No entry with key " + toString(ID) + " in TCountDistributionVector!");
		return _distVec[ID];
	};

	void fillCombinedDistribution(TCountDistribution & combined) const{
		combined.clear();
		for(auto& i : _distVec){
			combined.add(i);
		}
	};

	double RSquared(){
		//calculates correlation (R^2) between ID and values
		double n = counts();
		double sum_x{}, sum_y{}, sum_x2{}, sum_y2{}, sum_xy{};

		//calculate sums
		for(size_t i=0; i<_distVec.size(); ++i){
			for(size_t j = 0; j < _distVec[i]._dist.size(); ++j){
				sum_x += _distVec[i][j] * i;
				sum_y += _distVec[i][j] * j;
				sum_x2 += _distVec[i][j] * i * i;
				sum_y2 += _distVec[i][j] * j * j;
				sum_xy += _distVec[i][j] * i * j;
			}
		}

		//calculate correlation
		double cor = (n*sum_xy - sum_x*sum_y) / sqrt(n*sum_x2 - sum_x*sum_x) / sqrt(n*sum_y2 - sum_y*sum_y);

		//return R^2
		return cor * cor;
	};

	void writeCombined(TOutputFile & out) const{
		TCountDistribution combined;
		fillCombinedDistribution(combined);
		combined.write(out);
	};

	void writeCombined(TOutputFile & out, const std::string name) const{
		TCountDistribution combined;
		fillCombinedDistribution(combined);
		combined.write(out, name);
	};

	void write(TOutputFile & out) const{
		for(size_t i=0; i<_distVec.size(); ++i){
			if(!_distVec[i].empty()){
				_distVec[i].write(out, toString(i));
			}
		}
	};

	void write(const std::string filename, const std::string label_ID, const std::string label) const{
		TOutputFile out(filename, {label_ID, label, "counts"});
		write(out);
	};

	void write(TOutputFile & out, const std::vector<std::string> & names) const{
		if(_distVec.size() != names.size())
			throw std::runtime_error("In function 'void write(TOutputFile & out, const std::vector<std::string> & names) const': Can not write TCountDistributionVector: names have wrong size!");
		for(size_t i=0; i<_distVec.size(); ++i){
			if(!_distVec[i].empty()){
				_distVec[i].write(out, names[i]);
			}
		}
	};

	void writeAsMatrix(const std::string filename, const std::string label_ID, const std::string label) const{
		//compile header
		std::vector<std::string> header = {label_ID + "/" + label};
		for(uint32_t i=0; i<=max(); ++i){
			header.push_back(toString(i));
		}

		//write
		_writeAsMatrix(filename, header);
	};

	void writeAsMatrix(const std::string filename, const std::string label_ID, std::vector<std::string> & valueNames) const{
		//check if dim matches valueNames
		if(max() > valueNames.size()){
			throw "Can not write TCountDistributionVector as matrix: value names have wrong size!";
		}

		//compile header
		std::vector<std::string> header = {label_ID};
		header.insert(header.end(), valueNames.begin(), valueNames.end());

		//write
		_writeAsMatrix(filename, header);
	};
};

//----------------------------------------------
// TCountDistributionMap
//----------------------------------------------
template<class T> class TCountDistributionMap{
private:
	std::map<T, TCountDistribution> _dist;

public:
	TCountDistributionMap()= default;

	void clear(){
		_dist.clear();
	};

	void add(const T ID, const uint32_t Value){
		_dist[ID].add(Value);
	};

	size_t size() const{
		return _dist.size();
	};

	uint64_t counts() const{
		uint64_t c = 0.0;
		for(auto& i : _dist){
			c += i.second.counts();
		}
		return c;
	};

	uint64_t sum() const{
		uint64_t s = 0;
		for(auto& i : _dist){
			s += i.second.sum();
		}
		return s;
	};

	double mean() const{
		return (double) sum() / (double) counts();
	};

	bool exists(const T ID) const{
		return _dist.find(ID) != _dist.end();
	};

	const TCountDistribution& operator[](const T ID){
		if(!exists(ID))
			throw std::runtime_error("No entry with key " + toString(ID) + " in TCountDistributionMap!");
		return _dist[ID];
	};
};

//----------------------------------------------
// TMeanVar
// calculates mean and variance
//----------------------------------------------
template<class T>
class TMeanVar{
private:
	uint64_t _counter;
	T _sum;
	T _sumOfSquares;

public:
	TMeanVar(){
		clear();
	};

	void clear(){
		_counter = 0;
		_sum = 0;
		_sumOfSquares = 0;
	};

	void add(const T Value){
        // check for numeric overflow when adding value to sumOfSquares
        // no need to check for overflow of sum, as sumOfSquares > sum
        T square = Value*Value;
        if (!checkForNumericOverflow_addition(_sumOfSquares, square)){
            throw std::runtime_error("In function 'void TMeanVar::add(const T Value)': Numeric under- or overflow occured!");
        }

        _sum += Value;
		_sumOfSquares += square;
		_counter++;
	};

	void add(const TMeanVar & other){
        // check for numeric overflow when adding value to sumOfSquares
        // no need to check for overflow of sum, as sumOfSquares > sum
        if (!checkForNumericOverflow_addition(_sumOfSquares, other._sumOfSquares)){
            throw std::runtime_error("In function 'void TMeanVar::add(const TMeanVar & other)': Numeric under- or overflow occured!");
        }

        _counter += other._counter;
		_sum += other.sum();
		_sumOfSquares = other._sumOfSquares;
	};

	uint64_t counts() const{
		return _counter;
	};

	T sum() const{
		return _sum;
	};

	double mean() const{
        if (_counter == 0)
            return 0.;
		return static_cast<double>(_sum) / static_cast<double>(_counter);
	};

	double variance() const{
        // variance here is population variance (not MLE sample variance, as such not the same as in R)
        if (_counter == 0)
            return 0.;
        double Mean = mean();
		return static_cast<double>(_sumOfSquares) / static_cast<double>(_counter) - Mean * Mean;
	};

    double sd() const{
        return sqrt(variance());
    };
};

//----------------------------------------------
// TMeanVarVector
//----------------------------------------------
template<class T>
class TMeanVarVector{
private:
	std::vector<TMeanVar<T>> _meanVar;

public:
	TMeanVarVector(){};
	TMeanVarVector(const uint32_t Size){
		_meanVar.resize(Size);
	};

	void clear(){
		_meanVar.clear();
	};

	void add(const uint32_t ID, const T Value){
		if(_meanVar.size() <= ID){
			_meanVar.resize(ID+1);
		}
		_meanVar[ID].add(Value);
	};

	size_t size() const{
		return _meanVar.size();
	};

	T sum() const{
		T sum = 0;
		for(auto& it : _meanVar){
		    // check for numeric under- and overflow
            if (!checkForNumericOverflow_addition(sum, it.sum())){
                throw std::runtime_error("In function 'T TMeanVarVector::sum() const': Numeric under- or overflow occured!");
            }
            sum += it.sum();
		}
		return sum;
	};

	bool exists(const uint32_t ID) const{
		if(ID < _meanVar.size())
			return true;
		else
			return false;
	};

	const TMeanVar<T>& operator[](const uint32_t ID){
		if(!exists(ID))
			throw std::runtime_error("No entry with key " + toString(ID) + " in TMeanVarVector!");
		return _meanVar[ID];
	};
};

//----------------------------------------------
// TMeanVarMAP
//----------------------------------------------
template<class U, class T> // U is for type of ID (e.g. uint64_t), T is for type of value (e.g. double)
class TMeanVarMap{
private:
	std::map<U, TMeanVar<T>> _meanVar;

public:
	TMeanVarMap(){};

	void clear(){
		_meanVar.clear();
	};

	void add(const U ID, const T Value){
		_meanVar[ID].add(Value);
	};

	size_t size() const{
		return _meanVar.size();
	};

	T sum() const{
		T sum = 0;
		for(auto& it : _meanVar){
            // check for numeric under- and overflow
            if (!checkForNumericOverflow_addition(sum, it.second.sum())){
                throw std::runtime_error("In function 'T TMeanVarMap::sum() const': Numeric under- or overflow occured!");
            }
            sum += it.second.sum();
		}
		return sum;
	};

	bool exists(const U ID) const{
		return _meanVar.find(ID) != _meanVar.end();
	};

	const TMeanVar<T>& operator[](const U ID){
		if(!exists(ID))
			throw std::runtime_error("No entry with key " + toString(ID) + " in TMeanVarMap!");
		return _meanVar[ID];
	};
};


#endif /* COMMONUTILITIES_COUNTERS_H_ */

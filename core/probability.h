/*
 * TProbability.h
 *
 *  Created on: May 14, 2021
 *      Author: Daniel Wegmann
 */

#ifndef CORE_PROBABILITY_H_
#define CORE_PROBABILITY_H_


#include "strongTypes.h"
#include <math.h>
#include "stringFunctions.h"

//TODO: add Density and LogDensity!

//-------------------------------------
// Probability
//-------------------------------------
class LogProbability;
class Log10Probability;

class Probability : public StrongTypes::StrongType<double, Probability, StrongTypes::Orderable,
																		StrongTypes::Printable>{
private:
	constexpr void _ensureRange(){
		if(get() < 0.0 || get() > 1.0){
			throw std::runtime_error("constexpr void Probability::_ensureRange(): Probability is not within [0,1]!");
		}
	};

	[[nodiscard]] constexpr double _logProbabilityToProbabilty(const double& LogProb){
		return exp(LogProb);
	};

	[[nodiscard]] constexpr double _log10ProbabilityToProbabilty(const double& LogProb){
		return pow(10.0, LogProb);
	};

public:
	Probability() : StrongType(0.0) {};
	constexpr Probability(const double & Prob) : StrongType(Prob) { _ensureRange(); };
	explicit Probability(const LogProbability & Prob);
	explicit Probability(const std::string & ProbString){
        _value = convertStringCheck<double>(ProbString);
        _ensureRange();
	};

	~Probability() = default;

	//assignments
	constexpr void operator=(const double & Prob){
		_value = Prob;
		_ensureRange();
	};

	constexpr void operator=(const float & Prob){
		_value = Prob;
		_ensureRange();
	};

	void operator=(const LogProbability& LogProb);
	void operator=(const Log10Probability& LogProb);

	//convert
	[[nodiscard]] constexpr operator LogProbability() const;
	[[nodiscard]] constexpr operator Log10Probability() const;

	[[nodiscard]] explicit operator std::string() const{
		return toString(_value);
	};

	//manipulate
	//ToDo: unclear why StrongTypes::multiplicable does not work
	//multiplication always works
	[[nodiscard]] constexpr Probability operator*(const Probability & other) const {
		return Probability(_value * other.get());
	};

	constexpr Probability& operator*=(const Probability & other){
		_value *= other.get();
		return *this;
	};

	[[nodiscard]] constexpr Probability operator*(const double & other) const {
		//constructor will check range
		return Probability(_value * other);
	};

	constexpr Probability& operator*=(const double & other){
		_value *= other;
		_ensureRange();
		return *this;
	};

	//division only works if other > this
	[[nodiscard]] constexpr Probability operator/(const Probability & other) const {
		//constructor will check range
        return Probability(_value / other.get());
    };

    constexpr Probability& operator/=(const Probability & other){
        if(other.get() < _value){
			throw std::runtime_error("constexpr Probability& operator/=(const Probability & other): division results in value > 1.0!");
		}
        _value /= other.get();
        return *this;
    };

    [[nodiscard]] constexpr Probability operator/(const double & other) const {
		//constructor will check range
		return Probability(_value / other);
	};

	constexpr Probability& operator/=(const double & other){
		_value /= other;
		_ensureRange();
		return *this;
	};


    //additions may or may not be permissible: check range!
    [[nodiscard]] constexpr Probability operator+(const Probability & other) const {
    	//constructor will check range
		return Probability(_value + other.get());
	};

    constexpr Probability& operator+=(const Probability & other) {
    	_value += other.get();
    	if(_value > 1.0){
    		throw std::runtime_error("constexpr Probability& operator+=(const Probability & other): addition results in value > 1.0!");
    	}
    	return *this;
    };

    [[nodiscard]] constexpr Probability operator+(const double & other) const {
		//constructor will check range
		return Probability(_value + other);
	};

	constexpr Probability& operator+=(const double & other){
		_value += other;
		_ensureRange();
		return *this;
	};

    constexpr void makeComplement(){
    	_value= 1.0 - _value;
    };

    constexpr Probability complement() const {
		return Probability(1.0 - _value);
	};
};

//-------------------------------------
// LogProbability
// The natural log (base e) of a probability
//-------------------------------------
//TODO: think about minimal value (what is the complement of LogProbability p(Probability(1)))?
class LogProbability : public StrongTypes::StrongType<double, Probability, StrongTypes::Orderable,
																		   StrongTypes::Addable,
																		   StrongTypes::Printable>{
private:
	constexpr void _ensureRange(){
		if(get() > 0.0){
			throw std::runtime_error("constexpr void LogProbability::_ensureRange(): LogProbability is > 0!");
		}
	};

	[[nodiscard]] constexpr double _probabilityToLogProbabilty(const double & Prob){
		return log(Prob);
	};

	[[nodiscard]] constexpr double _log10ProbabilityToLogProbabilty(const double & Log10Prob){
		return 2.30258509299404590109 * Log10Prob;
	};

	[[nodiscard]] constexpr double _complement(const double & LogProb){
		//see https://cran.r-project.org/web/packages/Rmpfr/vignettes/log1mexp-note.pdf
		if(LogProb > -0.6931472){
			return log(-expm1(LogProb));
		} else {
			return log1p(-exp(LogProb));
		}
	};

public:
	LogProbability() : StrongType(0.0) {};
	explicit constexpr LogProbability(const double & LogProb) : StrongType(LogProb) { _ensureRange(); };
	explicit constexpr LogProbability(const float & LogProb) : StrongType(LogProb) { _ensureRange(); };

	explicit constexpr LogProbability(const Probability & Prob){
		_value = _probabilityToLogProbabilty( Prob.get());
	};

	explicit LogProbability(const Log10Probability & Log10Prob);

	explicit LogProbability(const std::string & ProbString){
        _value = convertStringCheck<double>(ProbString);
        _ensureRange();
	};

	~LogProbability() = default;

	//assignments
	constexpr void operator=(const double & LogProb){
		_value = LogProb;
		_ensureRange();
	};

	constexpr void operator=(const float & LogProb){
		_value = LogProb;
		_ensureRange();
	};

	constexpr void operator=(const Probability & Prob){
		_value = _probabilityToLogProbabilty(Prob.get());
	};

	constexpr void operator=(const Log10Probability & Prob);

	//convert
	[[nodiscard]] explicit operator Probability() const{
		return Probability(*this);
	};

	[[nodiscard]] explicit operator Log10Probability() const;

	[[nodiscard]] explicit operator std::string() const{
		return toString(_value);
	};

	//manipulate
    //additions may or may not be permissible: check range!
    [[nodiscard]] constexpr LogProbability operator+(const LogProbability & other) const {
    	//constructor will check range
		return LogProbability(_value + other.get());
	};

    constexpr LogProbability& operator+=(const LogProbability & other) {
    	_value += other.get();
    	if(_value > 0.0){
    		throw std::runtime_error("constexpr LogProbability& operator+=(const LogProbability & other): addition results in value > 0.0!");
    	}
    	return *this;
    };

    constexpr void makeComplement(){
		_value = log1p(-exp(_value));
	};

	constexpr LogProbability complement() const {
		return LogProbability(log1p(-exp(_value)));
	};
};

//-------------------------------------
// Log10Probability
// The log10 of a probability
//-------------------------------------
class Log10Probability : public StrongTypes::StrongType<double, Probability, StrongTypes::Orderable,
																		     StrongTypes::Addable,
																		     StrongTypes::Printable>{
private:
	constexpr void _ensureRange(){
		if(get() > 0.0){
			throw std::runtime_error("constexpr void LogProbability::_ensureRange(): LogProbability is > 0!");
		}
	};

	constexpr double _probabilityToLog10Probabilty(const double & Prob){
		return log10(Prob);
	};

	constexpr double _logProbabilityToLog10Probabilty(const double & LogProb){
		return 0.43429448190325181667 * LogProb;
	};

public:
	Log10Probability() : StrongType(0.0) {};
	explicit constexpr Log10Probability(const double & Log10Prob) : StrongType(Log10Prob) { _ensureRange(); };
	explicit constexpr Log10Probability(const float & Log10Prob) : StrongType(Log10Prob) { _ensureRange(); };

	explicit constexpr Log10Probability(const Probability & Prob){
		_value = _probabilityToLog10Probabilty( Prob.get());
	};

	explicit constexpr Log10Probability(const LogProbability & LogProb){
		_value = _logProbabilityToLog10Probabilty(LogProb.get());
	};

	explicit Log10Probability(const std::string & ProbString){
        _value = convertStringCheck<double>(ProbString);
        _ensureRange();
	};

	~Log10Probability() = default;

	//assignments
	constexpr void operator=(const double & LogProb){
		_value = LogProb;
		_ensureRange();
	};

	constexpr void operator=(const float & LogProb){
		_value = LogProb;
		_ensureRange();
	};

	constexpr void operator=(const Probability & Prob){
		_value = _probabilityToLog10Probabilty(Prob.get());
	};

	constexpr void operator=(const LogProbability & LogProb){
		_value = _logProbabilityToLog10Probabilty(LogProb.get());
	};

	//convert
	[[nodiscard]] explicit operator Probability() const{
		return Probability(*this);
	};

	[[nodiscard]] explicit operator LogProbability() const{
		return LogProbability(*this);
	};

	[[nodiscard]] explicit operator std::string() const{
		return toString(_value);
	};
};


#endif /* CORE_PROBABILITY_H_ */

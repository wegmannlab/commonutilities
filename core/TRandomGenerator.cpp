/*
 * TRandomGenerator.cpp
 *
 *  Created on: Sep 24, 2009
 *      Author: wegmannd
 */

#include "TRandomGenerator.h"

void TRandomGenerator::setSeed(long addToSeed, bool seedIsFixed){
	if(seedIsFixed){
		if(addToSeed<0) addToSeed=-addToSeed;
        usedSeed = addToSeed;
        _Idum = -addToSeed;
	} else {
		_Idum = get_randomSeedFromCurrentTime(addToSeed);
		if(_Idum==0) _Idum=get_randomSeedFromCurrentTime(_Idum);
		if(_Idum < 0) _Idum=-_Idum;
		if(_Idum > 161803398) _Idum = _Idum % 161803397;
		usedSeed = _Idum;
		_Idum = -_Idum;
	}
};

long TRandomGenerator::get_randomSeedFromCurrentTime(long & addToSeed){
	struct timeval time;
	gettimeofday(&time, 0);
	long microseconds = time.tv_sec * 1000 + time.tv_usec / 1000;

	//microseconds = microseconds - 38*365*24*3600; //substract 38 years...
	microseconds = (double) (addToSeed + microseconds);
   return microseconds ;
}

#define MBIG 1000000000L
#define MSEED 161803398L
#define MZ 0
#define FAC (1.0/MBIG)

double TRandomGenerator::ran3(){
        static int inext,inextp;
        static long ma[56];
        static int iff=0;
        long mj,mk;
        int i,ii,k;

        if ((_Idum < 0) || (iff == 0) ) {
                iff=1;
                mj=MSEED-(_Idum < 0 ? -_Idum : _Idum);
                mj %= MBIG;
                ma[55]=mj;
                mk=1;
                for (i=1;i<=54;++i) {
                        ii=(21*i) % 55;
                        ma[ii]=mk;
                        mk=mj-mk;
                        if (mk < MZ) mk += MBIG;
                        mj=ma[ii];
                }
                for (k=1;k<=4;++k)
                        for (i=1;i<=55;++i) {
                                ma[i] -= ma[1+(i+30) % 55];
                                if (ma[i] < MZ) ma[i] += MBIG;
                        }
                inext=0;
                inextp=31;
                _Idum=1;
        }
        if (++inext == 56) inext=1;
        if (++inextp == 56) inextp=1;
        mj=ma[inext]-ma[inextp];
        if (mj < MZ) mj += MBIG;
        ma[inext]=mj;
        return mj*FAC;
}
#undef MBIG
#undef MSEED
#undef MZ
#undef FAC

//--------------------------------------------------------
//Binomial Distribution
//--------------------------------------------------------
double TRandomGenerator::getBinomialRand(double pp, long n){
	int j;
	static int nold=(-1);
	double am,em,g,angle,p,bnl,sq,t,y;
	static double pold=(-1.0),pc,plog,pclog,en,oldg;

	p=(pp <= 0.5 ? pp : 1.0-pp);
	am=n*p;

	if (n < 25){
		bnl=0;
		for (j=1;j<=n;j++)
			if (ran3() < p) ++bnl;
	} else if (am < 1.0) {
		g=exp(-am);
		t=1.0;
		for (j=0;j<=n;j++) {
			t *= ran3();
			if (t < g) break;
		}
		bnl=(j <= n ? j : n);
	} else {
		if (n != nold) {
			en=n;
			oldg=gammaLog(en+1.0);
			nold=n;
		}
		if (p != pold) {
			pc=1.0-p;
			plog=log(p);
			pclog=log(pc);
			pold=p;
		}
		sq=sqrt(2.0*am*pc);

		do {
			do {
				angle=3.141592654*ran3();
				y=tan(angle);
				em=sq*y+am;
			} while (em < 0.0 || em >= (en+1.0));

			em=floor(em);
			t=1.2*sq*(1.0+y*y)*exp(oldg-gammaLog(em+1.0)
					-gammaLog(en-em+1.0)+em*plog+(en-em)*pclog);
		} while (ran3() > t);
		bnl=em;
	}
	if (p != pp) bnl=n-bnl;
	return bnl;
}

//--------------------------------------------------------
//Uniform Distribution
//--------------------------------------------------------
double TRandomGenerator::getRand(double min, double max){
	//return a random number between min and max
	 return (getRand() * (max - min) + min);
};

int TRandomGenerator::getRand(int min, int maxPlusOne){
	//return an random integer between min and maxPlusOne-1
	float r=1.0;
	while(r==1.0) r=getRand(); //we have a number in [0,1[
	return min+floor(r*(maxPlusOne-min));
};

bool TRandomGenerator::pickOneOfTwo(const double prob){
    return getRand() < prob;
};

long TRandomGenerator::getRand(long min, long maxPlusOne){
	//return an random integer between min and maxPlusOne-1
	double r=1.0;
	while(r==1.0) r=getRand(); //we have a number in [0,1[
	return min+floor(r*(maxPlusOne-min));
};

char TRandomGenerator::getRandomAlphaNumerichCharacter(){
	//choose a random letter or number
	//numbers are 48 - 57 (10)
	//uppercase letters are 65 - 90 (26)
	//lowercase letters are 97 - 122 (26)
	//in total: 62
	int index = getRand(48, 110);
	if(index > 57) index += 8;
	if(index > 90) index += 7;

	return (char) index;
};

std::string TRandomGenerator::getRandomAlphaNumericString(const int length){
	std::string s;
	for(int i=0; i<length; ++i){
		s+= getRandomAlphaNumerichCharacter();
	}
	return s;
};

void TRandomGenerator::shuffle(std::vector<int> & vec){
	int j;

	for(int i=vec.size()-1; i>0; --i){
		j = sample(i+1);
		std::swap(vec[i], vec[j]);
	}
};

//--------------------------------------------------------
//Normal Random and associated functions
//--------------------------------------------------------
/* Returns a Normal random variate based on a unit variate,
   using a random generator as a source of uniform deviates.
   Adapted from the algorithm described in the book Numerical Recipes by
   Press et al.
*/
double TRandomGenerator::getNormalRandom (double dMean, double dStdDev){
  double w, x1, x2;
   do {
      x1 = 2. * ran3() - 1.;
      x2 = 2. * ran3() - 1.;
      w = x1*x1 + x2*x2;
   } while (w >= 1. || w < 1E-30);

   w = sqrt((-2.*log(w))/w);
   x1 *= w;
   return (x1 * dStdDev + dMean);
}

double TRandomGenerator::getLogNormalRandom (double dMean, double dStdDev){
    double r = getNormalRandom (dMean, dStdDev);
    return exp(r);
}

//--------------------------------------------------------
//Gamma Distribution
//--------------------------------------------------------
/*Random deviates from standard gamma distribution with density
         a-1
        x    exp[ -x ]
f(x) = ----------------
         Gamma[a]

where a is the shape parameter.  The algorithm for integer a comes
from numerical recipes, 2nd edition, pp 292-293.  The algorithm for
a<1 uses code from p 213 of Statistical Computing, by Kennedy and
Gentle, 1980 edition.  This algorithm was originally published in:

Ahrens, J.H. and U. Dieter (1974), "Computer methods for sampling from
Gamma, Beta, Poisson, and Binomial Distributions".  COMPUTING
12:223-246.

The mean and variance of these values are both supposed to equal a.
My tests indicate that they do.

This algorithm has problems when a is small.  In single precision, the
problem  arises when a<0.1, roughly.  That is why I have declared
everything as double below.  Trouble is, I still don't know how small
a can be without causing trouble.  Mean and variance are ok at least
down to a=0.01.  f(x) doesn't seem to have a series expansion around
x=0.
****************************************************************/
double TRandomGenerator::getGammaRand(double a, double b) {
   if(b <= 0) {
	   throw "Negative value received in getGammaRand()!";
   }
   return getGammaRand(a)/b;
}


double TRandomGenerator::getGammaRand(double a) {

  int ia;
  double u, b, p, x, y=0.0, recip_a;

  if(a <= 0) {
	  throw "Negative value received in getGammaRand()!";
  }

  ia = (int) floor(a);  /* integer part */
  a -= ia;        /* fractional part */
  if(ia > 0) {
    y = getGammaRand(ia);  /* gamma deviate w/ integer argument ia */
    if(a==0.0) return(y);
  }

  /* get gamma deviate with fractional argument "a" */
  b = (M_E + a)/M_E;
  recip_a = 1.0/a;
  for(;;) {
    u = ran3();
    p = b*u;
    if(p > 1) {
      x = -log((b-p)/a);
      if( ran3() > pow(x, (double) a-1.0)) continue;
      break;
    }
    else {
      x = pow(p, recip_a);
      if( ran3() > exp(-x)) continue;
      break;
    }
  }
  return(x+y);
}

double TRandomGenerator::getGammaRand(int ia){
	/****************************************************************
	gamma deviate for integer shape argument.  Code modified from pp
	292-293 of Numerical Recipes in C, 2nd edition.
	****************************************************************/
  int j;
  double am,e,s,v1,v2,x,y;

  if (ia < 1) throw "Argument below 1 in getGammaRand()!";
  if (ia < 6){
    x=1.0;
    for (j=0; j<ia; j++)
      x *= ran3();
    x = -log(x);
  } else {
    do {
      do {
	    do{                         /* next 4 lines are equivalent */
	    	v1=2.0*ran3()-1.0;       /* to y = tan(Pi * uni()).     */
	    	v2=2.0*ran3()-1.0;
	    } while (v1*v1+v2*v2 > 1.0);
	    y=v2/v1;
	    am=ia-1;
	    s=sqrt(2.0*am+1.0);
	    x=s*y+am;
      } while (x <= 0.0);
      e=(1.0+y*y)*exp(am*log(x/am)-s*y);
    } while (ran3() > e);
  }
  return(x);
}

//--------------------------------------------------------
//Chi-Square Distribution
//--------------------------------------------------------

/* Numerical Recipes 3rd edition, pp. 371: Chisquare(v) = Gamma(v/2, 1/2) = 2Gamma(v/2, 1)
*/

double TRandomGenerator::getChisqRand(double a){
	   if(a <= 0) {
		   throw "Negative value received in getChisqRand()!";
	   }
	   return getGammaRand(a/2)/0.5;
}

//--------------------------------------------------------
//Beta Distribution
//--------------------------------------------------------
/*
   returns a variate that is Beta distributed on the interval [a,b]
   with shape parameters alpha and beta.

   The Beta function has two shaping parameters, alpha and beta.
   Setting these parameters to 1.5 and 1.5 yields a normal-like
   distribution, but without tails. If alpha and beta are equal to
   1 it is a uniform distribution.

   If alpha and beta are less than 1, use a rejection algorithm;
   Otherwise use the fact that if x is distributed Gamma(alpha) and y
   Gamma(beta) then x/(x+y) is Beta(alpha, beta).

   The rejection algorithm first a Beta variate is found over the
   interval [0, 1] with not the most efficient algorithm.  This is then
   scaled at the end to desired range.

   It may be tempting to re-use the second number drawn as the first
   random number of the next iteration, and simply draw one more.
   *** Don't do it.  You will produce an incorrect distribution.  You
   must draw two new numbers for the rejection sampling to be correct.

   References:
   - Ripley, Stochastic Simulations, John Wiley and Sons, 1987, p 90.
   - J.H.Maindonald, Statistical Computation, John Wiley and Sons,
     1984, p 370.
*/
double TRandomGenerator::getBetaRandom (const double & alpha, const double & beta, const double & a, const double & b){
  if (b <= a) throw "Bad shape or range for a beta variate!";
  return (a + getBetaRandom(alpha, beta) * (b-a));   /* Scale to interval [a, b] */
}


double TRandomGenerator::getBetaRandom(const double & alpha, const double & beta) {
  double u1, u2, w;

  if (alpha <= 0 || beta <= 0) throw "Bad shape or range for a beta variate!";

  if ((alpha < 1) && (beta < 1))
    /* use rejection */
    do {
      u1 = ran3(); /* Draw two numbers */
      u2 = ran3();

      u1 = pow(u1, (double) 1.0/alpha); /* alpha and beta are > 0 */
      u2 = pow(u2, (double) 1.0/beta);

      w = u1 + u2;

    } while (w > 1.0);

  else {
    /* use relation to Gamma */
    u1 = getGammaRand(alpha);
    u2 = getGammaRand(beta);
    w  = u1 + u2;
  }

  return u1/w;
};

//--------------------------------------------------------
//Dirichlet Distribution
//--------------------------------------------------------
void TRandomGenerator::fillDirichletRandom(const int K, double* alpha, double* res){
	double sum = 0.0;
	for(int k=0; k<K; ++k){
		res[k] = getGammaRand(alpha[k], 1.0);
		sum += res[k];
	}

	for(int k=0; k<K; ++k){
		res[k] /= sum;
	}
};

//--------------------------------------------------------
//Poisson Distribution
//--------------------------------------------------------
//Poisson Random from Wikipedia. Works only for small Lambda and may be a bit inefficient.
double TRandomGenerator::getPoissonRandom(const double & lambda){
	double L=exp(-lambda);
	int k=0;
	double p=1;
	do{
		++k;
		p*=getRand();
	} while (p>L);
	return k-1;
}

//--------------------------------------------------------
//Exponential Distribution
//--------------------------------------------------------
//Exponential Random from Wikipedia
double TRandomGenerator::getExponentialRandom(const double & lambda){
	return -log(getRand()) / lambda;
}

double TRandomGenerator::getExponentialRandomTruncated(const double & lambda, const double & lowerBound, const double & upperBound){
	//copied from stack overflow
	return -log(exp(-lambda * lowerBound) - (exp(-lambda * lowerBound) - exp(-lambda * upperBound)) * getRand()) / lambda;
}


double TRandomGenerator::getGeneralizedParetoRand(const double & locationMu, const double & scaleSigma, const double & shapeXi){
	if(shapeXi == 0.0){
		return locationMu - scaleSigma * log(getRand());
	} else {
		return locationMu + scaleSigma * (pow(getRand(), -shapeXi) - 1.0) / shapeXi;
	}
}

//--------------------------------------------------------
//Geometric Distribution
//--------------------------------------------------------
int TRandomGenerator::getGeometricRandom(double & p){
	return log(getRand())/log(1.0-p);
}

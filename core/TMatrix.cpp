//
// Created by madleina on 05.08.20.
//

#include "TMatrix.h"

//-------------------------------------------
// TMatrix
//-------------------------------------------

// Parameter Constructors
TMatrix::TMatrix() {
    clear();
}

TMatrix::TMatrix(int size) {
    _rows = static_cast<uint32_t>(size);
    _cols = static_cast<uint32_t>(size);
    _initialized = false;
    _initialize(0.);
}

TMatrix::TMatrix(int size, const double& initial) {
    _rows = static_cast<uint32_t>(size);
    _cols = static_cast<uint32_t>(size);
    _initialized = false;
    _initialize(initial);
}

TMatrix::TMatrix(int rows, int cols) {
    _rows = static_cast<uint32_t>(rows);
    _cols = static_cast<uint32_t>(cols);
    _initialized = false;
    _initialize(0);
}

TMatrix::TMatrix(int rows, int cols, const double& initial) {
    _rows = static_cast<uint32_t>(rows);
    _cols = static_cast<uint32_t>(cols);
    _initialized = false;
    _initialize(initial);
}

// Copy Constructor
TMatrix::TMatrix(const TMatrix& other) {
    _rows = other.rows();
    _cols = other.cols();
    _mat = other._mat;

    _initialized = true;
}

TMatrix::TMatrix(const TMatrix& other, const double& scale) {
    _rows = other.rows();
    _cols = other.cols();
    _mat = other._mat;
    (*this) *= scale;

    _initialized = true;
}

// Destructor
TMatrix::~TMatrix() = default;

// initialize
void TMatrix::_initialize(const double& initial) {
    _mat.resize(_rows);
    for (uint32_t i=0; i < _rows; i++)
        _mat[i].resize(_cols);
    set(initial);
    _initialized = true;
}

// resizing
void TMatrix::clear(){
	_mat.clear();
	_rows = 0;
	_cols = 0;
	_initialized = false;
}

void TMatrix::resize(int size) {
    resize(size, size, 0);
}

void TMatrix::resize(int size, const double& initial) {
    resize(size, size, initial);
}

void TMatrix::resize(int rows, int cols) {
    resize(rows, cols, 0);
}

void TMatrix::resize(int rows, int cols, const double& initial) {
    if (!_initialized || _rows != static_cast<uint32_t>(rows) || _cols != static_cast<uint32_t>(cols)){
        _rows = static_cast<uint32_t>(rows);
        _cols = static_cast<uint32_t>(cols);
        _initialize(initial);
    }
}

// reset all elements to a single value
void TMatrix::set(const double& value) {
    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++)
            (*this)(i, j) = value;
    }
}

// Assignment Operator
TMatrix& TMatrix::operator=(const TMatrix& other) {
    if (&other == this)
        return *this;

    fillFromMatrix(other);
    return *this;
}

// Addition of two matrices
TMatrix TMatrix::operator+(const TMatrix& other) {
    TMatrix result(*this);
    result += other;

    return result;
}

// Cumulative addition of this matrix and another
TMatrix& TMatrix::operator+=(const TMatrix& other) {
    assertm(_rows == other.rows(), ("number of rows of other differs from number of rows of this!"));
    assertm(_cols == other.cols(), ("number of cols of other differs from number of cols of this!"));

    for (uint32_t i=0; i<_rows; i++) {
        for (uint32_t j=0; j<_cols; j++)
            (*this)(i, j) += other(i, j);
    }

    return *this;
}

// Subtraction of this matrix and another
TMatrix TMatrix::operator-(const TMatrix& other) {
    TMatrix result(*this);
    result -= other;

    return result;
}

// Cumulative subtraction of this matrix and another
TMatrix& TMatrix::operator-=(const TMatrix& other) {
    assertm(other.rows() == _rows, ("number of rows of other differs from number of rows of this!"));
    assertm(other.cols() == _cols, ("number of cols of other differs from number of cols of this!"));

    for (uint32_t i=0; i<_rows; i++) {
        for (uint32_t j=0; j<_cols; j++) {
            (*this)(i, j) -= other(i, j);
        }
    }

    return *this;
}

// Left multiplication of this matrix and another (matrix multiplication, equivalent to this %*% other in R)
TMatrix TMatrix::operator*(const TMatrix& other) const {
    TMatrix result;
    result.fillFromProduct(*this, other);
    return result;
}

// Cumulative left multiplication of this matrix and another (matrix multiplication, equivalent to this %*% other in R)
TMatrix& TMatrix::operator*=(const TMatrix& other) {
    TMatrix result = (*this) * other;
    (*this) = result;
    return *this;
}

void TMatrix::fillUniformRandom(TRandomGenerator* randomGenerator){
    for (uint32_t i=0; i<_rows; i++) {
        for (uint32_t j=0; j<_cols; j++) {
            (*this)(i, j) = randomGenerator->getRand();
        }
    }
}

void TMatrix::fillFromMatrix(const TMatrix & other){
    fillFromMatrix(other, 1.);
}

void TMatrix::fillFromMatrix(const TMatrix & other, const double & Scale){
    resize(static_cast<int>(other.rows()), static_cast<int>(other.cols()));
    for(uint32_t i=0; i<_rows; ++i){
        for(uint32_t j=0; j<_cols; ++j){
            (*this)(i, j) = other(i, j) * Scale;
        }
    }
}

void TMatrix::fillFromMatrix(const TBandMatrix & other){
    fillFromMatrix(other, 1.);
}

void TMatrix::fillFromMatrix(const TBandMatrix & other, const double & Scale){
    resize(static_cast<int>(other.rows()), static_cast<int>(other.cols()));
    set(0);

    //go through diagonals
    for (uint32_t i = 0; i < _rows; i++){
        for (uint32_t j = 0; j < _cols; j++)
            _mat[i][j] = other(i, j) * Scale;
    }
}

void TMatrix::fillFromProduct(const TMatrix & first, const TMatrix & second){
    // left.numCols = right.numRows
    assertm(first.cols() == second.rows(), ("number of cols of first differs from number of rows of second!"));

    resize(first.rows(), second.cols(), 0);
    set(0.);
    //now calculate product
    for (uint32_t i=0; i<first.rows(); i++) {
        for (uint32_t j=0; j<second.cols(); j++) {
            for (uint32_t k=0; k<first.cols(); k++) {
                (*this)(i, j) += first(i, k) * second(k, j);
            }
        }
    }
}

void TMatrix::fillFromProduct(const TBandMatrix & first, const TBandMatrix & second){
    TBandMatrix tmp;
    tmp.fillFromProduct(first, second);
    fillFromMatrix(tmp);
}

void TMatrix::fillFromSquare(const TMatrix &other) {
    fillFromProduct(other, other);
}

void TMatrix::fillFromSquare(const TBandMatrix &other) {
    fillFromProduct(other, other);
}

// Calculate a transpose of this matrix
TMatrix TMatrix::transpose() {
    TMatrix result(_cols, _rows, 0.);

    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            result(j,i) = (*this)(i, j);
        }
    }

    return result;
}

// Matrix/scalar addition
TMatrix TMatrix::operator+(const double& other) {
    TMatrix result(_rows, _cols, 0.0);

    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            result(i,j) = (*this)(i, j) + other;
        }
    }

    return result;
}

TMatrix TMatrix::operator+=(const double& other) {
    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            (*this)(i, j) += other;
        }
    }

    return *this;
}

// Matrix/scalar subtraction
TMatrix TMatrix::operator-(const double& other) {
    TMatrix result(_rows, _cols, 0.0);

    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            result(i,j) = (*this)(i, j) - other;
        }
    }

    return result;
}

TMatrix TMatrix::operator-=(const double& other) {
    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            (*this)(i, j) -= other;
        }
    }

    return *this;
}

// Matrix/scalar multiplication
TMatrix TMatrix::operator*(const double& other) {
    TMatrix result(_rows, _cols, 0.0);

    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            result(i,j) = (*this)(i, j) * other;
        }
    }

    return result;
}

TMatrix TMatrix::operator*=(const double& other) {
    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            (*this)(i, j) *= other;
        }
    }

    return *this;
}

// Matrix/scalar division
TMatrix TMatrix::operator/(const double& other) {
    TMatrix result(_rows, _cols, 0.0);

    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            result(i,j) = (*this)(i, j) / other;
        }
    }

    return result;
}


TMatrix TMatrix::operator/=(const double& other) {
    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            (*this)(i, j) /= other;
        }
    }

    return *this;
}

// Multiply a matrix with a vector
std::vector<double> TMatrix::operator*(const std::vector<double>& other) {
    assertm(_cols == other.size(), ("size of vector differs from number of cols of matrix!"));

    std::vector<double> result(_rows, 0.);

    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            result[i] += (*this)(i, j) * other[j];
        }
    }

    return result;
}

// Compare matrices
int TMatrix::numDiffEntries(const TMatrix & other){
    assertm(other.rows() == _rows, ("number of rows of other differs from number of rows of this!"));
    assertm(other.cols() == _cols, ("number of cols of other differs from number of cols of this!"));

    int res = 0;
    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            if ((*this)(i, j) != other(i,j))
                res++;
        }
    }
    return res;
}

// Obtain a vector of the diagonal elements
std::vector<double> TMatrix::diag_vec() {
    assertm(_rows == _cols, ("number of rows differs from number of cols - can only get diagonal from square matrices!"));
    std::vector<double> result(this->_rows, 0.0);

    for (uint32_t i=0; i < this->_rows; i++) {
        result[i] = (*this)(i, i);
    }

    return result;
}

// Add to diagonal
void TMatrix::addToDiag(const double& val) {
    assertm(_rows == _cols, ("number of rows differs from number of cols - can only take add to diagonal from square matrices!"));
    for (uint32_t i=0; i < this->_rows; i++) {
        (*this)(i, i) += val;
    }
}

std::vector<double> TMatrix::row(const uint32_t &row) {
    assertm(row < _rows, ("row is >= than _rows!"));

    return _mat[row];
}

std::vector<double> TMatrix::col(const uint32_t &col) {
    assertm(col < _cols, ("col is >= than _cols!"));
    return this->transpose().row(col);
}

// Access the individual elements
double& TMatrix::operator()(const uint32_t & row, const uint32_t & col) {
    assertm(row < _rows, ("row is >= than _rows!"));
    assertm(col < _cols, ("col is >= than _cols!"));
    return _mat[row][col];
}

// Access the individual elements (const)
const double& TMatrix::operator()(const uint32_t& row, const uint32_t& col) const {
    assertm(row < _rows, ("row is >= than _rows!"));
    assertm(col < _cols, ("col is >= than _cols!"));
    return _mat[row][col];
}

// calculate row sum
double TMatrix::rowSum(const uint32_t &row) const {
    assertm(row < _rows, ("row is >= than _rows!"));

    double sum = 0.;
    for (uint32_t col=0; col < this->_cols; col++) {
        sum += _mat[row][col];
    }
    return sum;
}

// calculate column sum
double TMatrix::colSum(const uint32_t &col) const {
    assertm(col < _cols, ("col is >= than _cols!"));

    double sum = 0.;
    for (uint32_t row=0; row < this->_rows; row++) {
        sum += _mat[row][col];
    }
    return sum;
}

// Get the number of rows of the matrix
uint32_t TMatrix::rows() const {
    return _rows;
}

// Get the number of columns of the matrix
uint32_t TMatrix::cols() const {
    return _cols;
}

// Get the number of columns of the matrix
uint32_t TMatrix::size() const {
    assertm(_rows == _cols, ("number of rows differs from number of cols - size is only defined for square matrices!"));
    return _rows;
}

void TMatrix::print() const {
    for (uint32_t i = 0; i < _rows; i++){
        for (uint32_t j = 0; j < _cols; j++){
            std::cout << (*this)(i, j) << ", ";
        }
        std::cout << std::endl;
    }
}

void TMatrix::fillAsExponential(const TBandMatrix & Q){
    //We get exp(Q) using a poor man's algorithm:
    // 1) Choose a k
    // 2) Devide all entries by 1/(2^k)
    // 3) Square the matrix Q k times

    // 1) Choose k such that 3 * 2^k > numStates AND 1/2^k < 0.1 (i.e. k>3)
    int k = Q.rows() / 3.0;
    k = log(k)/log(2) + 10;
    if(k<4) k=4;

    // 2) Divide all entries by 1 / (2^k)
    double scale = 1.0 / (double) pow(2, k);
    auto ** QScaledBand = new TBandMatrix*[2];
    QScaledBand[0] = new TBandMatrix(Q, scale);
    QScaledBand[0]->addToDiag(1.0);
    QScaledBand[1] = new TBandMatrix();
    int QScaledBandIndex = 0;

    // 3) Square matrix k times.
    //Change from bandMarix to Matrix once the diagonals are filled
    for(int i=0; i<k; ++i){
        QScaledBandIndex = 1 - QScaledBandIndex;
        QScaledBand[QScaledBandIndex]->fillFromSquare(*QScaledBand[1 - QScaledBandIndex]);
    }
    fillFromMatrix(*QScaledBand[QScaledBandIndex]);

    //clean up memory
    delete QScaledBand[0];
    delete QScaledBand[1];
    delete[] QScaledBand;
}

//-------------------------------------------
// TBandMatrix
//-------------------------------------------

// Constructor
TBandMatrix::TBandMatrix() : TMatrix() {
    _bandwidth = 0;
    _zero = 0;
    _numDiag = 0;
}

TBandMatrix::TBandMatrix(uint32_t size, uint32_t bandwidth) : TMatrix() {
    _initialize(size, bandwidth, 0);
}

TBandMatrix::TBandMatrix(uint32_t size, uint32_t bandwidth, const double& initial) : TMatrix() {
    _initialize(size, bandwidth, initial);
}

// Copy Constructor
TBandMatrix::TBandMatrix(const TBandMatrix& other) : TMatrix() {
    TBandMatrix::fillFromMatrix(other);
}

TBandMatrix::TBandMatrix(const TBandMatrix& other, const double& scale) : TMatrix() {
    TBandMatrix::fillFromMatrix(other, scale);
}

void TBandMatrix::_initialize(uint32_t size, uint32_t bandwidth, const double& initial){
    // set size and bandwidth
    if (bandwidth > size)
        throw std::runtime_error("In function 'TBandMatrix::TBandMatrix(uint32_t size, uint32_t bandwidth, const T & initial)': bandwidth is > than size!");
    this->_rows = size;
    this->_cols = size;
    _bandwidth = bandwidth;
    _zero = 0;

    // now compute dimensions of matrix that is stored
    _numDiag = 2 * _bandwidth + 1; // number of rows = number of non-zero diagonals
    this->_mat.resize(_numDiag);
    _lengthOfDiags.resize(_numDiag);
    for (uint32_t i=0; i < _numDiag; i++) {
        _lengthOfDiags[i] = this->_rows - abs(static_cast<int>(i - _bandwidth)); // number of cols = length of each diagonal (differs between diags)
        this->_mat[i].resize(_lengthOfDiags[i], initial);
    }

    // re-set values (resize only stretches/squeezes, but does not re-set already set values)
    set(initial);

    this->_initialized = true;
}

bool TBandMatrix::_onBand(uint32_t row, uint32_t col) const{
    if(static_cast<int>(row) < static_cast<int>(col - _bandwidth) || row > (col + _bandwidth))
        return false;
    return true;
}


// resizing
void TBandMatrix::resize(uint32_t size, uint32_t bandwidth) {
    if (!_initialized || bandwidth != _bandwidth || size != this->_rows)
        _initialize(size, bandwidth, 0);
}

void TBandMatrix::resize(uint32_t size, uint32_t bandwidth, const double& initial) {
    if (!_initialized || bandwidth != _bandwidth || size != this->_rows)
        _initialize(size, bandwidth, initial);
}

// reset all elements to a single value
void TBandMatrix::set(const double& value) {
    // only set values on the diagonals
    for (uint32_t d=0; d<_numDiag; ++d){
        for(uint32_t i=0; i<_lengthOfDiags[d]; ++i)
            this->_mat[d][i] = value;
    }
}

// Access the individual elements
double& TBandMatrix::operator()(const uint32_t& row, const uint32_t& col) {
    assertm(row < this->_rows, ("row is >= than _rows!"));
    assertm(col < this->_cols, ("col is >= than _cols!"));

    // re-compute index
    if(!_onBand(row, col))
        return _zero;
    if(row < col)
        return this->_mat[col - row + _bandwidth][row];
    return this->_mat[col - row + _bandwidth][col];
}

// Access the individual elements (const)
const double& TBandMatrix::operator()(const uint32_t& row, const uint32_t& col) const {
    assertm(row < this->_rows, ("row is >= than _rows!"));
    assertm(col < this->_cols, ("col is >= than _cols!"));

    // re-compute index
    if(!_onBand(row, col))
        return _zero;
    if(row < col)
        return this->_mat[col - row + _bandwidth][row];
    return this->_mat[col - row + _bandwidth][col];
}

std::vector<double> TBandMatrix::row(const uint32_t &row) {
    assertm(row < this->_rows, ("row is >= than _rows!"));

    std::vector<double> result;
    result.resize(this->_cols);
    for (uint32_t col = 0; col < this->_cols; col++)
        result[col] = (*this)(row, col);
    return result;
}

double TBandMatrix::rowSum(const uint32_t &row) const{
    assertm(row < this->_rows, ("row is >= than _rows!"));

    double sum = 0.;
    for (uint32_t col = 0; col < this->_cols; col++)
        sum += (*this)(row, col);
    return sum;
}

double TBandMatrix::colSum(const uint32_t &col) const{
    assertm(col < this->_cols, ("col is >= than _cols!"));

    double sum = 0.;
    for (uint32_t row = 0; row < this->_rows; row++)
        sum += (*this)(row, col);
    return sum;
}

double TBandMatrix::atDiag(uint32_t diag, uint32_t index) const{
    assertm(diag < this->_rows, ("diag is >= than this->_rows!"));
    assertm(index < _lengthOfDiags[diag], ("index is >= than _lengthOfDiags[diag]!"));

    return this->_mat[diag][index];
}

// fill
void TBandMatrix::fillUniformRandom(TRandomGenerator* randomGenerator){
    for(uint32_t d=0; d<_numDiag; ++d){
        for(uint32_t i=0; i<_lengthOfDiags[d]; ++i)
            this->_mat[d][i] = randomGenerator->getRand();
    }
}

void TBandMatrix::fillFromMatrix(const TBandMatrix & other){
    fillFromMatrix(other, 1.);
}

void TBandMatrix::fillFromMatrix(const TBandMatrix & other, const double & Scale){
    //check dimensions
    resize(other.size(), other.bandwidth());
    //copy data and scale
    for(uint32_t d=0; d< _numDiag; ++d){
        for(uint32_t i=0; i< _lengthOfDiags[d]; ++i)
            this->_mat[d][i] = other._mat[d][i] * Scale;
    }
}

void TBandMatrix::fillFromProduct(const TBandMatrix & first, const TBandMatrix & second){
    //set dimensions and fill with zeros
    if(first.size()!=second.size())
        throw std::runtime_error("void TBandMatrix::fillFromProduct(const TBandMatrix & first, const TBandMatrix & second): provided matrices are of different size!");
    resize(first.size(), std::min(first.size(), first.bandwidth() + second.bandwidth()), 0);
    set(0.); // need this, in case we don't resize!

    //Now use an approach to multiply band matrices based on diagonals.
    //Specifically, we profit from the fact that A=a1 + ... + an) and B=b1 + ... + bn), where
    //ai and bi are matrices with all zeros except the ith diagonal, which corresponds to the
    //ith diagonal of matrices A and B, respectively. In this case, A*B = (a1 + ... + an) * (b1 + ... + bn)
    //So we have to multiply each pair of diagonals, which will only add to values on a diagonal again.
    int fillDiag, r, s;
    int rAndS; //r + s
    int i1, i2, ir; //indexes for first, second and resulting diagonal
    int len; //length of filling diagonal: last index to fill + 1
    for(uint32_t d1 = 0; d1 < first.numDiag(); ++d1){
        for(uint32_t d2 = 0; d2 < second.numDiag(); ++d2){
            //set r, s: index of diagonal, counting from longest diagonal. Lower is negative
            r = static_cast<int>(d1) - static_cast<int>(first.bandwidth());
            s = static_cast<int>(d2) - static_cast<int>(second.bandwidth());
            rAndS = r + s;
            fillDiag = rAndS + static_cast<int>(_bandwidth); //in internal numbering
            len = static_cast<int>(this->_rows) - abs(rAndS);
            if(fillDiag>=0 && fillDiag < static_cast<int>(_numDiag)){
                //discriminate cases
                if(r >= 0 && s >= 0){ //r,s >= 0
                    i1 = 0;
                    i2 = r;
                    ir = 0;
                } else if(r <= 0 && s <= 0){ //r,s, <= 0
                    i1 = -s;
                    i2 = 0;
                    ir=0;
                } else if(r < 0){
                    i1 = 0;
                    i2 = 0;
                    if(r+s < 0){ //r < 0, s > 0, r+s < 0
                        ir = len - (static_cast<int>(this->_rows) + r);
                    } else { //r < 0, s > 0, r+s >= 0
                        ir = len - (static_cast<int>(this->_rows) - s);
                    }
                } else {
                    ir = 0;
                    if(r+s < 0){ //r > 0, s < 0, r+s < 0
                        i1 = abs(rAndS);
                        i2 = 0;
                        len = static_cast<int>(this->_rows) + s;
                    } else { //r > 0, s < 0, r+s >= 0
                        i1 = 0;
                        i2 = abs(rAndS);
                        len = static_cast<int>(this->_rows) - r;
                    }
                }
                //now fill diagonal
                for(; ir<len; ++ir, ++i1, ++i2){
                    this->_mat[fillDiag][ir] += first._mat[d1][i1] * second._mat[d2][i2];
                }
            }
        }
    }
}

void TBandMatrix::fillAsExponential(const TBandMatrix & Q){
    //We get exp(Q) using a poor man's algorithm:
    // 1) Choose a k
    // 2) multiply all entries by 1/(2^k)
    // 3) Square the matrix (I+Q*scale) k times

    // 1) Choose k such that 3 * 2^k > numStates AND 1/2^k < 0.1 (i.e. k>3)
    int k = Q.size() / 3.0;
    k = log2(k) + 10;
    if(k<4) k=4;

    // 2) Divide all entries by 1 / (2^k)
    double scale = 1.0 / (double) pow(2, k);
    auto ** QScaledBand = new TBandMatrix*[2];
    QScaledBand[0] = new TBandMatrix(Q, scale);
    QScaledBand[0]->addToDiag(1.0);
    QScaledBand[1] = new TBandMatrix();
    int QScaledBandIndex = 0;

    // 3) Square matrix k-1 times.
    for(int i=0; i<(k-1); ++i){
        QScaledBandIndex = 1 - QScaledBandIndex;
        QScaledBand[QScaledBandIndex]->fillFromSquare(*QScaledBand[1 - QScaledBandIndex]);
    }

    //last into this storage
    fillFromSquare(*QScaledBand[QScaledBandIndex]);

    //clean up memory
    delete QScaledBand[0];
    delete QScaledBand[1];
    delete[] QScaledBand;
}

// Calculate a transpose of this matrix
TBandMatrix TBandMatrix::transpose() {
    TBandMatrix result(_cols, _bandwidth);

    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            result(j,i) = (*this)(i, j);
        }
    }

    return result;
}

TBandMatrix& TBandMatrix::operator=(const TBandMatrix& other) {
    if (&other == this)
        return *this;

    fillFromMatrix(other);
    return *this;
}

// Addition of two matrices
TBandMatrix TBandMatrix::operator+(const TBandMatrix& other) {
    TBandMatrix result(*this);
    result += other;
    //result.print();

    return result;
}

// Cumulative addition of this matrix and another
TBandMatrix& TBandMatrix::operator+=(const TBandMatrix& other) {
    assertm(_rows == other.rows(), ("number of rows of other differs from number of rows of this!"));
    assertm(_cols == other.cols(), ("number of cols of other differs from number of cols of this!"));

    // if bandwidth of other is larger -> we would need to change size of this-matrix
    // easier to add this to other, as this does not require resizing
    // TODO: find out if this is more inefficient than resizing and change if needed
    if (other.bandwidth() > _bandwidth){
        TBandMatrix result(other);
        result += *this;
        (*this) = result;
        return *this;
    }

    for (uint32_t i=0; i<_rows; i++) {
        for (uint32_t j=0; j<_cols; j++) {
            (*this)(i, j) += other(i, j);
        }
    }

    return *this;
}

// Subtraction of this matrix and another
TBandMatrix TBandMatrix::operator-(const TBandMatrix& other) {
    TBandMatrix result(*this);
    result -= other;

    return result;
}

// Cumulative subtraction of this matrix and another
TBandMatrix& TBandMatrix::operator-=(const TBandMatrix& other) {
    assertm(other.rows() == _rows, ("number of rows of other differs from number of rows of this!"));
    assertm(other.cols() == _cols, ("number of cols of other differs from number of cols of this!"));

    // if bandwidth of other is larger -> we would need to change size of this-matrix
    // easier to subtract this to other, as this does not require resizing
    // TODO: find out if this is more inefficient than resizing and change if needed
    if (other.bandwidth() > _bandwidth){
        TBandMatrix result(other);
        result -= *this;
        (*this) = result;
        return *this;
    }

    for (uint32_t i=0; i<_rows; i++) {
        for (uint32_t j=0; j<_cols; j++) {
            (*this)(i, j) -= other(i, j);
        }
    }

    return *this;
}

// Left multiplication of this matrix and another (matrix multiplication, equivalent to this %*% other in R)
TBandMatrix TBandMatrix::operator*(const TBandMatrix& other) const {
    TBandMatrix result;
    result.fillFromProduct(*this, other);
    return result;
}

// Cumulative left multiplication of this matrix and another (matrix multiplication, equivalent to this %*% other in R)
TBandMatrix& TBandMatrix::operator*=(const TBandMatrix& other) {
    TBandMatrix result = (*this) * other;
    (*this) = result;
    return *this;
}

// Matrix/scalar addition
TBandMatrix TBandMatrix::operator+(const double& other) {
    TBandMatrix result(_rows, _bandwidth);

    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            if(_onBand(i, j))
                result(i,j) = (*this)(i, j) + other;
        }
    }

    return result;
}

TBandMatrix TBandMatrix::operator+=(const double& other) {
    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            if(_onBand(i, j))
                (*this)(i, j) += other;
        }
    }

    return *this;
}

// Matrix/scalar subtraction
TBandMatrix TBandMatrix::operator-(const double& other) {
    TBandMatrix result(_rows, _bandwidth);

    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            if(_onBand(i, j))
                result(i,j) = (*this)(i, j) - other;
        }
    }

    return result;
}


TBandMatrix TBandMatrix::operator-=(const double& other) {
    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            if(_onBand(i, j))
                (*this)(i, j) -= other;
        }
    }

    return *this;
}

// Matrix/scalar multiplication
TBandMatrix TBandMatrix::operator*(const double& other) {
    TBandMatrix result(_rows, _bandwidth);
    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            if(_onBand(i, j))
                result(i,j) = (*this)(i, j) * other;
        }
    }

    return result;
}

TBandMatrix TBandMatrix::operator*=(const double& other) {
    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            if(_onBand(i, j))
                (*this)(i, j) *= other;
        }
    }

    return *this;
}

// Matrix/scalar division
TBandMatrix TBandMatrix::operator/(const double& other) {
    TBandMatrix result(_rows, _bandwidth);

    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            if(_onBand(i, j))
                result(i,j) = (*this)(i, j) / other;
        }
    }

    return result;
}

TBandMatrix TBandMatrix::operator/=(const double& other) {
    for (uint32_t i=0; i < _rows; i++) {
        for (uint32_t j=0; j < _cols; j++) {
            if(_onBand(i, j))
                (*this)(i, j) /= other;
        }
    }

    return *this;
}

// Multiply a matrix with a vector
std::vector<double> TBandMatrix::operator*(const std::vector<double>& other) {
    return TMatrix::operator*(other);
}

// getters
uint32_t TBandMatrix::bandwidth() const {
    return _bandwidth;
}

uint32_t TBandMatrix::numDiag() const {
    return _numDiag;
}

std::vector<uint32_t> TBandMatrix::lengthOfDiags() const {
    return _lengthOfDiags;
}

uint32_t TBandMatrix::size() const {
    return this->_rows;
}

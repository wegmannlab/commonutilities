/*
 * TTask.h
 *
 *  Created on: Mar 31, 2019
 *      Author: phaentu
 */

#ifndef TTASK_H_
#define TTASK_H_

#include "TLog.h"
#include "TParameters.h"
#include "TRandomGenerator.h"

//---------------------------------------------------------------------------
// How to use it:
// Create a class for each task, as shown in the example below for task NAME
//---------------------------------------------------------------------------
//	class TTask_NAME:public TTask{
//		public:
//		TTask_NAME(){ _explanation = "SAY WHAT THIS TASK IS DOING"; };

//		void run(TParameters & Parameters, TLog* Logfile){
//			SPECIFY HOW TO EXECUTE THIS TASK
//		};
//	};
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// TTask
//---------------------------------------------------------------------------
class TTask{
protected:
	std::string _explanation;
	std::set<std::string> _citations;
	TRandomGenerator* _randomGenerator;
	bool _randomGeneratorInitialized;

	void initializeRandomGenerator(TParameters & Parameters, TLog* Logfile){
		if(!_randomGeneratorInitialized){
			Logfile->listFlush("Initializing random generator ...");

			if(Parameters.parameterExists("fixedSeed")){
				_randomGenerator = new TRandomGenerator(Parameters.getParameter<long>("fixedSeed"), true);
			} else if(Parameters.parameterExists("addToSeed")){
				_randomGenerator = new TRandomGenerator(Parameters.getParameter<long>("addToSeed"), false);
			} else {
				_randomGenerator = new TRandomGenerator();
			}
			Logfile->write(" done with seed " + toString(_randomGenerator->usedSeed) + "!");
			_randomGeneratorInitialized = true;
		}
	};

public:
	TTask(){
		_randomGenerator = nullptr;
		_randomGeneratorInitialized = false;
	};

	virtual ~TTask(){
		if(_randomGeneratorInitialized){
			delete _randomGenerator;
		}
	};

	void addCitation(const std::string Citation){
		_citations.insert(Citation);
	};

	void run(std::string taskName, TParameters & Parameters, TLog* Logfile){
		Logfile->startIndent(_explanation + " (task = " + taskName + "):");

		//print citations
		printCitations(Logfile);

		initializeRandomGenerator(Parameters, Logfile);

		//now run task
		run(Parameters, Logfile);
		Logfile->endIndent();
	};

	virtual void run(TParameters & Parameters, TLog* Logfile){
		throw "Base task can not be run!";
	};

	std::string explanation(){ return _explanation; };

	void printCitations(TLog* Logfile){
		//write citations, if there are any
		if(!_citations.empty()){
			Logfile->startIndent("Relevant citations:");
			for(auto it = _citations.begin(); it != _citations.end(); ++it){
				Logfile->list(*it);
			}
			Logfile->endIndent();
		}
	};
};



#endif /* TTASK_H_ */

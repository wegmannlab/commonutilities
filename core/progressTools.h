/*
 * progressTools.h
 *
 *  Created on: May 16, 2020
 *      Author: phaentu
 */

#ifndef COMMONUTILITIES_PROGRESSTOOLS_H_
#define COMMONUTILITIES_PROGRESSTOOLS_H_

#include "TLog.h"
#include "stringFunctions.h"
#include <sys/time.h>
#include <cmath>

//a suite of tools to measure and report progress during runtime

enum timeFormat : uint8_t {full=0, iso=1};

//----------------------------------------------------
// TTimer
// a class to measure and report execution time
//----------------------------------------------------
class TTimer{
private:
	struct timeval _start, _end;
	bool running;

	inline void _recordEnd(){
		if(running){
			gettimeofday(&_end, NULL);
		}
	};

public:
	TTimer(){
		start();
	};

	void start(){
		gettimeofday(&_start, NULL);
		running = true;
	};

	inline void stop(){
		if(running){
			_recordEnd();
			running = false;
		}
	};

	int seconds(){
		_recordEnd();
		return _end.tv_sec  - _start.tv_sec;
	};

	double minutes(){
		_recordEnd();
		return round(100.0 * (double) seconds() / 60.0) / 100.0;
	};

	double hours(){
		_recordEnd();
		return round(100.0 * seconds() / 3600.0) / 100.0;
	};

	std::string formattedTime(timeFormat format = full){
		_recordEnd();

		//get num days, hours, minutes and seconds
		int sec = seconds();

		int hours = sec / 3600;
		sec = sec % 3600;

		int minutes = sec / 60;
		sec = sec % 60;

		//assemble string
		std::string s;

		//hours
		if(hours > 0){
			if(format == full){
				if(hours == 1){
					s = "1 hour";
				} else {
					s = toString(hours) + " hours";
				}
			} else if(format == iso){
				if(hours < 10){
					s = "0" + toString(hours) + ":";
				} else {
					s = toString(hours) + ":";
				}
			}
		}

		//minutes
		if(minutes > 0 || s.size() > 0){
			if(!s.empty()){
				s += " ";
			}
			if(format == full){
				if(minutes == 1){
					s += "1  minute";
				} else {
					s += toString(minutes) + " minutes";
				}
			} else if(format == iso){
				if(minutes < 10){
					s += "0" + toString(minutes) + ":";
				} else {
					s += toString(minutes) + ":";
				}
			}
		}

		//seconds
		if(format == full){
			if(!s.empty()){
				s += " ";
			}
			if(sec == 1){
				s += "1 second";
			} else {
				s += toString(sec) + " seconds";
			}
		} else if(format == iso){
			if(sec < 10){
				s += "0" + toString(sec);
			} else {
				s += toString(sec);
			}
		}

		//return string
		return s;
	};
};



#endif /* COMMONUTILITIES_PROGRESSTOOLS_H_ */

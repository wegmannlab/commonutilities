#ifndef TRandomGenerator_H_
#define TRandomGenerator_H_

#include <cmath>
#include <sys/time.h>
#include <iostream>
#include <sstream>
#include <limits>
#include <numeric>
#include "mathFunctions.h"

class TRandomGenerator{
private:
	long _Idum;

	double ran3();
	long get_randomSeedFromCurrentTime(long & addToSeed);

public:
	long usedSeed;

	TRandomGenerator(long addToSeed){
		setSeed(addToSeed);
	};
	TRandomGenerator(long addToSeed, bool seedIsFixed){
		setSeed(addToSeed, seedIsFixed);
	};
	TRandomGenerator(){
		setSeed(0);
	};
	virtual ~TRandomGenerator() = default;
	void setSeed(long addToSeed, bool seedIsFixed=false);

	//uniform
	virtual double getRand() { return ran3(); };

	double getRandNotOne(){
		double r = getRand();
		while(r == 1.0){
			r = getRand(); //we have a number in [0,1[
		}
		return r;
	};

	template <typename T> void fillRand(T & vec){
		for(auto& v : vec){
			v = getRand();
		}
	};

	//uniform integer
	double getRand(double min, double max);
	int getRand(int min, int maxPlusOne);

	//pick one templates
	template <typename T> T sample(const T & numElements){
		if(numElements == 1) return 0;
		return floor(getRandNotOne()*(numElements));
	};

	virtual bool pickOneOfTwo(const double prob=0.5);

	template <typename T, typename N> N pickOne(const N & numElements, T* probsCumulative){
		if(numElements == 1){
			return 0;
		}
		double r = getRandNotOne();
		size_t i = 0;
		while(r > probsCumulative[i]){
			++i;
		}
		return i;
	};

	template <class T> uint32_t pickOne(const T & probsCumulative){
		if(probsCumulative.size() == 1){
			return 0;
		}
		double r = getRandNotOne();
		size_t i = 0;
		while(r > probsCumulative[i]){
			++i;
		}
		return i;
	};

	template <typename T> uint32_t pickOneRawProbabilities(const T & probs){
		if(probs.size() == 1){
			return 0;
		}
		double r = getRandNotOne();
		size_t i = 0;
		double prob = probs[0];
		while(r > prob){
			++i;
			prob += probs[i];
		}
		return i;
	};

	long getRand(long min, long maxPlusOne);
	char getRandomAlphaNumerichCharacter();
	std::string getRandomAlphaNumericString(const int length);
	void shuffle(std::vector<int> & vec);

	//normal
	virtual double getNormalRandom (double dMean, double dStdDev);
	double getLogNormalRandom (double dMean, double dStdDev);

	//binomial
	double getBinomialRand(double pp, long n);

	//multinomial
	template <typename T> void fillMultinomialRandom(const uint32_t & size, const std::vector<double> & probsCumulative, std::vector<T> counts){
		//set counts to zero
		counts.resize(probsCumulative.size());
		std::fill(counts.begin(), counts.end(), 0.0);

		//sample
		for(uint32_t i=0; i<size; ++i){
			++counts[pickOne(probsCumulative)];
		}
	};

	template <typename T, typename U> void fillMultinomialRandomRawProbabilities(const uint32_t & size, const T & probs, U & counts){
		//check for dimension
		if(probs.size() != counts.size()){

			std::cout << probs.size() << " vs " << counts.size() << std::endl;

			throw std::runtime_error("template <typename T> void fillMultinomialRandomRawProbabilities(const uint32_t & size, const std::vector<double> & probs, T counts): Incompatible dimensions!");
		}

		//set counts to zero
		std::fill(counts.begin(), counts.end(), 0.0);

		//get cumulative probabilities
		std::vector<double> probsCumulative;
		probsCumulative.resize(probs.size());
		std::partial_sum(probs.begin(), probs.end(), probsCumulative.begin());

		//sample
		for(uint32_t i=0; i<size; ++i){
			++counts[pickOne(probsCumulative)];
		}
	};

	//gamma
	double getGammaRand(double a, double b);
	double getGammaRand(double a);
	double getGammaRand(int ia);

	//chisquared
	double getChisqRand(double a);

	//beta
	double getBetaRandom (const double & alpha, const double & beta, const double & a, const double & b);
	double getBetaRandom (const double & alpha, const double & beta);

	//Betabinomial
	double getBetaBinomialRandom(const uint32_t & size, const double & alpha, const double & beta){
		//sample p from beta
		double p = getBetaRandom(alpha, beta);

		//sample from binomial
		return getBinomialRand(p, size);
	};

	//Dirichlet
	void fillDirichletRandom(const int K, double* alpha, double* res);
	template <typename T, typename U> void fillDirichletRandom(const T & alpha, U & res){
		res.resize(alpha.size());
		double sum = 0.0;
		for(unsigned long k=0; k<alpha.size(); ++k){
			res[k] = getGammaRand(alpha[k], 1.0);
			sum += res[k];
		}

		for(auto& r : res){
			r /= sum;
		}
	};

	//Polya (Dirichlet-Multinomial)
	template <typename T, typename U> void fillPolyaRandom(const uint32_t & size, const T & alpha, U & counts){
		//sample p from dirichlet
		static std::vector<double> dir;
		fillDirichletRandom(alpha, dir);

		//sample from multinomial
		fillMultinomialRandomRawProbabilities(size, dir, counts);
	};

	//Poisson
	double getPoissonRandom(const double & lambda);

	//exponential
	double getExponentialRandom(const double & lambda);
	double getExponentialRandomTruncated(const double & lambda, const double & lowerBound, const double & upperBound);

	//generalized Pareto
	double getGeneralizedParetoRand(const double & locationMu, const double & scaleSigma, const double & shapeXi);

	//geometric
	int getGeometricRandom(double & p);
};

#endif /* TRandomGenerator_H_ */

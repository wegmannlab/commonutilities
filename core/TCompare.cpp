//
// Created by madleina on 25.06.20.
//

#include "TCompare.h"

//-------------------------------------------------------
// TColumn
//-------------------------------------------------------

TColumn::TColumn() {
    _dataType = _string;
    _range = 0.;
}

void TColumn::setDataType(const std::string& type){
    if (type == "double")
        _dataType = _double;
    else if (type == "float")
        _dataType = _float;
    else if (type == "int")
        _dataType = _int;
    else if (type == "string")
        _dataType = _string;
    else
        throw std::runtime_error("Invalid data type '" + type + "'!");
}

void TColumn::setRange(double Range) {
    // check if range is valid
    if (Range < 0)
        throw std::runtime_error("Range (= " + toString(Range) + ") should be >= 0!");
    if (_dataType == _string  && Range != 0) // if string, we do not allow differences
        throw std::runtime_error("Range (= " + toString(Range) + ") must be 0 for data types string!");
    if (_dataType == _int && Range != static_cast<int>(Range)) // check if range is int if we compare ints
        throw std::runtime_error("Range (= " + toString(Range) + ") must be an integer for data types int!");
    _range = Range;
}

bool TColumn::entriesAreEqual(const std::string & val1, const std::string& val2){
    if (_dataType == _string || _range == 0.) // if exact match required -> no need to convert to float/double/int
        return _compareString(val1, val2);
    else if (_dataType == _int)
        return _compareInt(val1, val2);
    else if (_dataType == _double)
        return _compareDouble(val1, val2);
    else // float
        return _compareFloat(val1, val2);
}

bool TColumn::headerEntriesAreEqual(const std::string & val1, const std::string& val2){
        return _compareString(val1, val2);
}

bool TColumn::_compareString(const std::string & val1, const std::string& val2) const{
    return val1 == val2;
};

bool TColumn::_compareInt(const std::string & val1, const std::string& val2) const{
    return equalAs<int>(val1, val2, static_cast<int>(_range));
};

bool TColumn::_compareFloat(const std::string & val1, const std::string& val2) const{
    return equalAs<float>(val1, val2, static_cast<float>(_range));
};

bool TColumn::_compareDouble(const std::string & val1, const std::string& val2) const{
    return equalAs<double>(val1, val2, _range);
};

//-------------------------------------------------------
// TFileComparer
//-------------------------------------------------------

TFileComparer::TFileComparer()= default;

TFileComparer::TFileComparer(const std::string &filenameIn1, const std::string &filenameIn2, const std::string& filenameOut, const TFile_Filetype& type, const std::string& delim) {
    open(filenameIn1, filenameIn2, filenameOut, type, delim);
};

TFileComparer::TFileComparer(const std::string &filenameIn1, const std::string &filenameIn2, const std::string& filenameOut, int numCols, const std::string &delim) {
    open(filenameIn1, filenameIn2, filenameOut, numCols, delim);
};

void TFileComparer::_init(const std::string& filenameOut){
    _numDiff = 0;
    _reachedMaxNumDiff = false;
    _maxNumDiff = -1;
    _limitedNumDiff = false;
    _nameDiffFile = filenameOut; // store filename, as we only open output file if there are differences

    // columns: data type and allowed range. Default: all strings, allowed range = 0
    _cols.assign(_file1.numCols(), TColumn());
};

TFileComparer::~TFileComparer() = default;

void TFileComparer::open(const std::string &filenameIn1, const std::string &filenameIn2, const std::string& filenameOut, int numCols, const std::string &delim){
    _file1.open(filenameIn1, numCols, delim);
    _file2.open(filenameIn2, numCols, delim);
    _init(filenameOut);
};

void TFileComparer::open(const std::string &filenameIn1, const std::string &filenameIn2, const std::string& filenameOut, const TFile_Filetype & type, const std::string &delim) {
    if (type == variable)
        throw std::runtime_error("Comparing files of variable number of columns is currently not implemented!");
    _file1.open(filenameIn1, type, delim);
    _file2.open(filenameIn2, type, delim);
    if (_file1.numCols() != _file2.numCols())
        throw "File '" + _file1.name() + "' has a different number of columns than file '" + _file2.name() + "' (" + toString(_file1.numCols()) + " vs " + toString(_file2.numCols())+ ").";
    _init(filenameOut);
};

void TFileComparer::_close(){
    _file1.close();
    _file2.close();
    _diffFile.close();
};

void TFileComparer::limitNumDiffTo(int num){
    if (num < 0) throw std::runtime_error("Maximal number of differences between files must be >= 0!");
    _maxNumDiff = num;
    _limitedNumDiff = true;
};

void TFileComparer::setDataTypes(const std::vector<std::string>& dataTypes) {
    // construct default vec (specifying a default vector in function declaration is cumbersome)
    std::vector<double> ranges(_cols.size());
    std::fill(ranges.begin(), ranges.end(), 0);
    setDataTypes(dataTypes, ranges);
};

void TFileComparer::setDataTypes(const std::vector<std::string>& dataTypes, const std::vector<double>& ranges) {
    // check if size of vectors match number of columns
    if (dataTypes.size() != _cols.size())
        throw std::runtime_error("Can not set data types of size " + toString(dataTypes.size()) + ": files '" + _file1.name() + ", " + _file2.name() + "' only have " + toString(_file1.numCols()) + " columns!");
    if (ranges.size() != _cols.size())
        throw std::runtime_error("Can not set ranges of size " + toString(ranges.size()) + ": files '" + _file1.name() + ", " + _file2.name() + "' only have " + toString(_file1.numCols()) + " columns!");

    // now store
    for (int col = 0; col < static_cast<int>(dataTypes.size()); col++)
        setDataType(col, dataTypes[col], ranges[col]);
};

void TFileComparer::setDataType(int col, const std::string& dataType, double Range){
    // check if col is valid
    if (col >= _file1.numCols())
        throw std::runtime_error("Can not set data type for column " + toString(col) + ": files '" + _file1.name() + ", " + _file2.name() + "' only have " + toString(_file1.numCols()) + " columns!");

    // store
    _cols[col].setDataType(dataType);
    _cols[col].setRange(Range);
};

void TFileComparer::_addDifference(int curCol, const std::string& val1, const std::string& val2){
    if (_numDiff == 0){ // open file, if it is the first difference
        std::vector<std::string> header = {"row", "col", _file1.name(), _file2.name()};
        _diffFile.open(_nameDiffFile, header);
    }
    ++_numDiff;

    // do we reach the maximal number of differences to be printed to file?
    if (_limitedNumDiff && _numDiff > _maxNumDiff) _reachedMaxNumDiff = true;
    else _diffFile << _file1.lineNumber() << curCol + 1 << val1 << val2 << std::endl;  // curCol + 1 because we start counting at 0
};

void TFileComparer::_compareHeader(){
    for (int col = 0; col < _file1.numCols(); col++){
        bool equal = _cols[col].headerEntriesAreEqual(_file1.headerAt(col), _file2.headerAt(col));
        if (!equal) {
            _addDifference(col, _file1.headerAt(col), _file2.headerAt(col));
            if (_reachedMaxNumDiff) break;
        }
    }
};

void TFileComparer::_compareOneLine(const std::vector<std::string>& vecFile1, const std::vector<std::string>& vecFile2){
    for (int col = 0; col < _file1.numCols(); col++){ // go over all cols
        bool equal = _cols[col].entriesAreEqual(vecFile1[col], vecFile2[col]); // is entry equal?
        if (!equal) {
            _addDifference(col, vecFile1[col], vecFile2[col]); // entry is not equal -> write to file
            if (_reachedMaxNumDiff) break;
        }
    }
};

bool TFileComparer::filesAreEqual(){
    if (_file1.type() == header) // first compare headers (these are strings, so the data_type comparison does not apply to header)
        _compareHeader();

    // now compare rest
    std::vector<std::string> vecFile1, vecFile2;
    while(_file1.read(vecFile1)){
        if (!_file2.read(vecFile2)) // check if end of file2 has been reached
            throw "File '" + _file1.name() + "' has more lines than file '" + _file2.name() + "'!";
        _compareOneLine(vecFile1, vecFile2);
        if (_reachedMaxNumDiff) break;
    }

    // reached end of file1 -> does file2 have more lines?
    if (!_reachedMaxNumDiff && _file2.read(vecFile2))
        throw "File '" + _file2.name() + "' has more lines than file '" + _file1.name() + "'!";

    // close files (necessary if you want to read diffFile before destructor is called)
    _close();

    // return true if files are identical and false otherwise
    return _numDiff == 0;
};

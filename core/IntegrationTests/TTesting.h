/*
 * TTesting.h
 *
 *  Created on: Dec 11, 2017
 *      Author: phaentu
 */

#ifndef TTESTING_H_
#define TTESTING_H_

#include <stdexcept>
#include "stringFunctions.h"
#include "TParameters.h"
#include "TLog.h"
#include "TTestList.h"

//-----------------------------------
//TTesting
//-----------------------------------
class TTesting{
private:
    TLog * logfile;
    std::string outputName;
    TTestList * testList;

    void printTests();

public:
    TTesting(TTestList * TestList, TParameters & params, TLog* Logfile);
    ~TTesting();
    void runTests(TParameters & params, TTaskList * taskList);
};


#endif /* TTESTING_H_ */

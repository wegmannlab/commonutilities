//
// Created by caduffm on 3/3/20.
//

#include "TTest.h"

TTest::TTest(){
    _name = "empty";
    taskList = nullptr;
    logfile = nullptr;
};

bool TTest::runMain(const std::string& task){
    logfile->startIndent("Running task '" + task + "':");
    _testParams.addParameter("task", task);
    _testParams.addParameter("verbose", "");

    if(!logfile)
    	throw "logfile was not initialized!";

    if(!taskList)
    	throw "taskList was not initialized!";

    //open task switcher and run task
    bool returnVal = true;
    try{
        taskList->run(task, _testParams, logfile);
    }
    catch (std::string & error){
        logfile->conclude(error);
        returnVal = false;
    }
    catch (const char* error){
        logfile->conclude(error);
        returnVal = false;
    }
    catch(std::exception & error){
        logfile->conclude(error.what());
        returnVal = false;
    }
    catch (...){
        logfile->conclude("unhandled error!");
        returnVal = false;
    }
    logfile->endIndent();
    return returnVal;
}

bool TTest::run(TParameters & parameters, TLog* Logfile, TTaskList * TaskList){
    taskList = TaskList;
    logfile = Logfile;
    return false;
}

//
// Created by caduffm on 3/3/20.
//

#ifndef APPROXWF2_TTEST_H
#define APPROXWF2_TTEST_H

#include "TLog.h"
#include "TParameters.h"
#include "TTaskList.h"

class TTest{
protected:
    TParameters _testParams;
    std::string _name;
    TTaskList * taskList;
    TLog * logfile;

    bool runMain(const std::string &task);

public:
    TTest();
    virtual ~TTest()= default;

    std::string name(){return _name;};
    virtual bool run(TParameters & parameters, TLog* logfile, TTaskList * TaskList);
};

#endif //APPROXWF2_TTEST_H

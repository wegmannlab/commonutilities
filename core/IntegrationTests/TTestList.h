//
// Created by caduffm on 3/3/20.
//

#ifndef TTESTLIST_H
#define TTESTLIST_H

#include "TParameters.h"
#include "TTest.h"

//------------------------------------------
//TTestList
//------------------------------------------
class TTestList{
private:
    // map of name and test pointer of all possible tests (it does not matter whether they will be performed or not)
    std::map< std::string, TTest*> testMap;

    //maps test suit to test names
    std::map<std::string, std::vector<std::string> > testSuites;

    //vector of pointers to tests that should be performed
    std::vector<TTest*> testsToPerform;

public:
    TTestList()= default;

   ~TTestList(){
        for(auto & it : testMap)
            delete it.second;
    }

    ///////////////////////////////////////////////////////////////////////////////////
    // functions to add tests and test suites; no matter if they will be used or not //
    ///////////////////////////////////////////////////////////////////////////////////
    void addTest(const std::string& name, TTest* test){
        testMap.insert(std::pair<std::string, TTest*>(name, test));
    }

    void addTestSuite(const std::string& name, const std::vector<std::string>& suite){
        testSuites.insert(std::pair<std::string, std::vector<std::string>>(name, suite));
    }

    ///////////////////////////////////////////////////////////////////////////////////
    // now: check if test should actually be performed                               //
    ///////////////////////////////////////////////////////////////////////////////////
    void parseTests(TParameters & params){
        // go over every test in test map and check if it should be performed
        for (auto & it : testMap){
            if (params.parameterExists(it.first)) {
                initializeTest(it.first);
            }
        }
    }

    void parseSuites(TParameters & params){
        // go over every test suite in test suite map and check if it should be performed
        for (auto & testSuite : testSuites){
            if (params.parameterExists(testSuite.first)){
                for (const auto & i : testSuite.second) {
                    initializeTest(i);
                }
            }
        }
    };

    void initializeTest(const std::string & name){
        // only initialize test if it is not yet in testsToPerform
        auto it1 = testMap.find(name); // find task corresponding to name
        if (it1 != testMap.end()){ // check if tests exists (e.g. testSuite may contain test that does not exist)
            auto it2 = std::find(testsToPerform.begin(), testsToPerform.end(), it1->second);
            if (it2 == testsToPerform.end()){
                // if test is not yet in testsToPerform -> add it
                testsToPerform.push_back(it1->second);
            }
        }
    }

    size_t size(){
        return testsToPerform.size();
    };

    void printTestToLogfile(TLog* logfile){
        for(auto & it : testsToPerform){
            logfile->list(it->name());
        }
    };

    TTest* operator[](size_t num){
        if(num < testsToPerform.size())
            return testsToPerform[num];
        else throw std::runtime_error("Test number out of range!");
    };
};

#endif //APPROXWF2_TTESTLIST_H

/*
 * probability.cpp
 *
 *  Created on: May 14, 2021
 *      Author: Daniel Wegmann
 */


#include "probability.h"

//------------------------------------------------
// Probability
//------------------------------------------------
void Probability::operator=(const LogProbability& LogProb){
	_value = _logProbabilityToProbabilty(LogProb.get());
};

void Probability::operator=(const Log10Probability& Log10Prob){
	_value = _log10ProbabilityToProbabilty(Log10Prob.get());
};

constexpr Probability::operator LogProbability() const{
	return LogProbability(*this);
};

constexpr Probability::operator Log10Probability() const{
	return Log10Probability(*this);
};

//------------------------------------------------
// LogProbability
//------------------------------------------------
LogProbability::LogProbability(const Log10Probability & Log10Prob){
	_value = _log10ProbabilityToLogProbabilty( Log10Prob.get() );
};

constexpr void LogProbability::operator=(const Log10Probability & Prob){
	_value = _log10ProbabilityToLogProbabilty(Prob.get());
};

 LogProbability::operator Log10Probability() const{
	return Log10Probability(*this);
};




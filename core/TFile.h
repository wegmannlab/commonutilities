/*
 * TFile.h
 *
 *  Created on: Jan 8, 2019
 *      Author: phaentu
 */

#ifndef TFILE_H_
#define TFILE_H_

#include "stringFunctions.h"
#include "gzstream.h"
#include <ios>

#define DEFAULTDELIM_OUT '\t'
#define DEFAULTDELIM_IN "\t "
#define DEFAULTDELIM_COMMENT "//"

enum TFile_Filetype : uint8_t {fixed, header, variable};


//------------------------------------------------
// TFileTable
//------------------------------------------------
class TFileTable {
    // table = file where all lines have the same number of columns
protected:
    // type: fixed (no header); or header (header, of same length as number of columns)
	TFile_Filetype _type;
    std::vector<std::string> _header;

    int _numCols;

    void _init();

public:
    TFileTable(int numCols, const std::string& name);
    TFileTable(const std::vector<std::string> &header, const std::string& name);

    void setHeader(const std::vector<std::string>& header, const std::string& name);
    void noHeader(int numCols, const std::string& name);
    std::string headerAt(int col, const std::string& name) const;
    int getIndexOfColname(const std::string& colname, const std::string& name) const;
    int getIndexOfColnameStartsWith_First(const std::string& colname, const std::string& name) const;
    int getIndexOfColnameStartsWith_Last(const std::string& colname, const std::string& name) const;
    const std::vector<std::string>& getHeader() const {return _header;};
    int numColumns() const{ return _numCols; };
    TFile_Filetype type() const{
        return _type;
    };
};

//------------------------------------------------
// TFile
//------------------------------------------------
class TFile {
    // TFile can be a table (= fixed number of columns, possibly with header) or variable (= number of columns can vary, no header)
    // if table: when writing/reading, we check if numCols is correct
protected:
    std::string _name;
    bool _isOpen;
    std::string _delim;

    // counters
    int _curCol;
    uint64_t _lineNum;

    // table or not
    std::unique_ptr<TFileTable> _table;

    virtual void _openFile();
    virtual void _closeFile();
    virtual void _reOpenFile();
    void _setName(const std::string & Name);

    TFile(); // protected, as base class should not be instantiated
    TFile(const TFile & other);
    TFile(TFile && other);

public:
    virtual ~TFile() = default;

    // open
    virtual void open(const std::string& Name);
    virtual void open(const std::string& Name, int numCols);
    void reOpen();

    // close
    void close();

    // header
    void noHeader(int numCols);

    // getters
    bool isOpen() const{ return _isOpen; };
    std::string name() const{ return _name; };
    uint64_t lineNumber() const{ return _lineNum; };
    int curCol() const {return _curCol;};
    std::string delim() const {return _delim;};
    int numCols() const {
        if (_table) return _table->numColumns();
        return curCol();
    };
    bool isTable() const{ return _table != nullptr;};
    std::string headerAt(int col) const {
        if (_table) return _table->headerAt(col, _name);
        else throw std::runtime_error("No header for file '" + _name + "' that is not a table!");
    }
    int getIndexOfColname(const std::string& colname) const{
        if (_table) return _table->getIndexOfColname(colname, _name);
        else throw std::runtime_error("No header for file '" + _name + "' that is not a table!");
    }
    int getIndexOfColnameStartsWith_First(const std::string& colname) const{
        if (_table) return _table->getIndexOfColnameStartsWith_First(colname, _name);
        else throw std::runtime_error("No header for file '" + _name + "' that is not a table!");
    }
    int getIndexOfColnameStartsWith_Last(const std::string& colname) const{
        if (_table) return _table->getIndexOfColnameStartsWith_Last(colname, _name);
        else throw std::runtime_error("No header for file '" + _name + "' that is not a table!");
    }
    const std::vector<std::string>& header() const{
        if (_table)
            return _table->getHeader();
        else throw std::runtime_error("No header for file '" + _name + "' that is not a table!");
    }
    TFile_Filetype type() const{
        if (_table)
            return _table->type();
        return variable;
    }
};

//-------------------------------------------------------
// TOutputFile
//-------------------------------------------------------

class TOutputFile : public TFile {
protected:
    std::ostream* _filePointer;

    void _openFile() override;
    void _closeFile() override;
    void _reOpenFile() override;
    template<class T> void _write(const std::vector<T> & data);

public:
    // constructors
    TOutputFile();
    TOutputFile(const TOutputFile & other) = delete;
    TOutputFile(TOutputFile && other);
    explicit TOutputFile(const std::string& Name, const char & delim = DEFAULTDELIM_OUT);
    TOutputFile(const std::string& Name, int NumCols, const char & delim = DEFAULTDELIM_OUT);
    TOutputFile(const std::string& Name, const std::vector<std::string>& Header, const char & delim = DEFAULTDELIM_OUT);

    ~TOutputFile() override;

    // open
    void open(const std::string& Name, const char & delim = DEFAULTDELIM_OUT);
    void open(const std::string& Name, int numCols, const char & delim = DEFAULTDELIM_OUT);
    void open(const std::string& Name, const std::vector<std::string>& Header, const char & delim = DEFAULTDELIM_OUT);

    //writing header
    void writeHeader(const std::vector<std::string>& Header);

    //writing data
    void setPrecision(int Precision);

    template<typename T> void writeLine(std::vector<T> & data);
    template<typename T> void write(T data);
    template<typename T> void write(const std::vector<T> & data);
    template<typename T> TOutputFile& operator<<(T data);

    //new line
    virtual void endLine();

    //overloading std::endl
    typedef TOutputFile& (*TOutputFileManipulator)(TOutputFile&);

    // take in a function with the custom signature
    TOutputFile& operator<<(TOutputFile::TOutputFileManipulator manip){
        return manip(*this);
    };

    static TOutputFile& endl(TOutputFile& of){
        of.endLine();
        return of;
    };

    // this is the type of std::cout
    typedef std::basic_ostream<char, std::char_traits<char> > CoutType;

    // this is the function signature of std::endl
    typedef CoutType& (*StandardEndLine)(CoutType&);

    // define an operator<< to take in std::endl
    TOutputFile& operator<<(StandardEndLine manip){
        this->endLine();
        return *this;
    };
};

//-------------------------------------
// TColumnSelector
//-------------------------------------
class TColumnSelector{
private:
	std::vector<size_t> _usedColumns;

public:
	TColumnSelector(const std::vector<std::string> & RequiredColumns, const std::vector<std::string> & Header, const std::string & Filename);
	~TColumnSelector() = default;

	int numColumns() const{ return _usedColumns.size(); };

	template<typename T> void fill(std::vector<T> & full, std::vector<T> & dest);
};

//-------------------------------------------------------
// TInputFile
//-------------------------------------------------------

class TInputFile : public TFile{
protected:
    std::istream* _filePointer;
    std::string _line;
    std::string _delim_comment; // which delimiter is used to comment lines
    std::unique_ptr<TColumnSelector> _limitColumns; //allow to limit columns

    void _openFile() override;
    void _closeFile() override ;
    void _reOpenFile() override;

    template<typename T> bool _readNextLineFromFile(std::vector<T> & vec, bool check = false);
    void _readHeader();
    void _guessNumColumns();

    template<typename T> bool _read(std::vector<T> & vec, bool check);

public:
    // constructors
    TInputFile();
    explicit TInputFile(const std::string& Name, const TFile_Filetype & type, const std::string& delim = DEFAULTDELIM_IN, const std::string & DelimComment = DEFAULTDELIM_COMMENT);
    TInputFile(const std::string& Name, int NumCols, const std::string& delim = DEFAULTDELIM_IN, const std::string & DelimComment = DEFAULTDELIM_COMMENT);
    TInputFile(const std::string& Name, const std::vector<std::string> & LimitToTheseColumns, const std::string& Delim = DEFAULTDELIM_IN, const std::string & DelimComment = DEFAULTDELIM_COMMENT);

    TInputFile(const TInputFile & other) = delete;
    TInputFile(TInputFile && other);

    // open
    void open(const std::string& Name, const TFile_Filetype & type, const std::string& delim = DEFAULTDELIM_IN, const std::string & DelimComment = DEFAULTDELIM_COMMENT);
    void open(const std::string& Name, int numCols, const std::string & delim = DEFAULTDELIM_IN, const std::string & DelimComment = DEFAULTDELIM_COMMENT);
    void open(const std::string& Name, const std::vector<std::string> & LimitToTheseColumns, const std::string& Delim = DEFAULTDELIM_IN, const std::string & DelimComment = DEFAULTDELIM_COMMENT);

    // set delimitor for comments
    void setDelimComment(const std::string & Delim);

    ~TInputFile() override;

    //read data into vector
    template<typename T> bool read(std::vector<T> & vec, bool check = false);
};


#include "TFile.tpp"

#endif /* TFILE_H_ */
